#usage: 
#(1) chdir to snapshot.00000XXXXXX exists
#(2) combine_xyz.sh <number of atoms>

for n in `ls snap*/*.xyz`
 do 
	echo $1 >> combined.xyz
	echo " " >> combined.xyz 
	tail -n $1 $n | sort -n --key=5 >> combined.xyz 
done
