# SC-MD
SC-MD: A Shift/Collapse (SC) Hybrid MPI/OpenMP/GPU/MIC Molecular Dynamics Code for Many-Body Potentials

**SC-MD** is a fast and highly scalable molecular dynamics (MD) simulator specialized for many-body potential on large-scale computing platforms. 

## 1. Features
* Shift/Collapse algorithm minimizing internode data transfer and eliminate computation redundancy thereby achieving high scalability for fine-grain parallelism many-body MD simulations on extreme-scale clusters
* Highly optimized code using MPI and MPI+GPU parallelization, while also workable on MPI+OpenMP scheme
* Currently support Vashishta potential for SiO2 system

## 2. Publications
* M. Kunaseth, R. K. Kalia, A. Nakano, K. Nomura, and P. Vashishta. A scalable parallel algorithm for dynamic range-limited n-tuple computation in many-body molecular dynamics simulation,
[Proceedings of the International Conference on High Performance Computing, Networking, Storage and Analysis (SC'13) (2013)](https://dl.acm.org/citation.cfm?id=2503235)
* M. Kunaseth, S. Hannongbua, A. Nakano. Shift/collapse on neighbor list (SC-NBL): Fast evaluation of dynamic many-body potentials in molecular dynamics simulations. 
[Computer Physics Communications. 235(2019) 88-94. DOI: https://doi.org/10.1016/j.cpc.2018.09.021](https://doi.org/10.1016/j.cpc.2018.09.021)
