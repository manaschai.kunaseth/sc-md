#ifndef __POTENTIAL_H__
#define __POTENTIAL_H__

#include "constants.h"
#include "input.h"

typedef struct potential_st {
	//! Force lookup table
	double***		forceTable;

	//! Potential lookup table
	double***		potentialTable;

	//! Potential cutoff use for linked cell cutoff.
	//! For each n-body, rcut[nBody] = MAX(rcut-of-each-atomType);
	double			rcut[MAX_NBODY];

	double			rij2max;

	//! Table size
	int			tableNBin;

	//! Table spacing
	double			dh2;
	/*
	//! Number of atom types in the potential
	int			nTypes;
	*/

	//! Potential name
	char			name[POTENTIAL_NAME_MAX];

	//! Extension of potential data
	void*			extension;

} POTENTIAL;


void initPotential(POTENTIAL*);


#endif
