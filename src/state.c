#include "state.h"
#include "utils.h"
#include <stdlib.h>
#include <assert.h>
#include "mpi.h"
#include "memory.h"

#define ATOM_CAPACITY_FACTOR 2.0  
#define BASE_NATOM_PER_NODE 1024

void initState(STATE* s,int nAtom)
{
	//Over allocation for atom data
	int nAtomReal = BASE_NATOM_PER_NODE+nAtom*ATOM_CAPACITY_FACTOR;
	
	s->r     = (double(*)[3])(scMalloc(sizeof(double[3])*nAtomReal));
	s->rv    = (double(*)[3])(scMalloc(sizeof(double[3])*nAtomReal));
	s->ra    = (double(*)[3])(scMalloc(sizeof(double[3])*nAtomReal));
	s->rType = (int*)(scMalloc(sizeof(int)*nAtomReal));
	s->rgid  = (unsigned long*)(scMalloc(sizeof(unsigned long)*nAtomReal));
	s->scCacheMask = (unsigned int*)scMalloc(sizeof(unsigned int)*nAtomReal);
	s->rUpdateFlag = (double*)scMalloc(sizeof(double)*nAtomReal);
	
	s->nMax  = nAtomReal;
	s->nAtomCache = 0;
	s->nAtomOwn = 0;

	s->kineticEnergy   = 0.0;
	for (int i = 0;i < MAX_NBODY;i++)
		s->potentialEnergy[i] = 0.0;

	/*
	This code is not needed. Memory tracking is report comprehensively by scMalloc/scFree

	double memNeed = nAtomReal*(3*sizeof(double[3])+2*sizeof(int)+sizeof(unsigned long)+sizeof(double));
	double memNeedSum;
	int nNodes;

  	MPI_Allreduce (&memNeed, &memNeedSum, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	MPI_Comm_size(MPI_COMM_WORLD, &nNodes);

	#if (VERBOSE > 1)
	reportVerboseFixedDouble("Average memory allocated for atom data            [MB]: ",memNeedSum/(nNodes*1024.0*1024));
	reportVerboseFixedDouble("Total memory allocated for atom data in all tasks [MB]: ",memNeedSum/(1024.0*1024));
	#endif
	*/
}

void resetState(STATE* s)
{
	//Reset parameter of resident atom in state to be ready for next compute step.
	//i.e. Reset acceleration, reset energy.

	int nAtomAll = s->nAtomOwn + s->nAtomCache;
	double (*ra)[3] = s->ra;
	unsigned int *scMask = s->scCacheMask;
	//double *rUpdateFlag = s->rUpdateFlag;
	for (int i = 0;i < nAtomAll;i++) {
		scMask[i] = SC_CACHE_MASK_NONE;
		//rUpdateFlag[i] = 1.0;
		for (int a = 0;a < 3;a++)
			ra[i][a] = 0.0;
	}

	s->kineticEnergy = 0.0;
	for (int i = 0;i < MAX_NBODY;i++)
		s->potentialEnergy[i] = 0.0;
}

void freeState(STATE *s)
{
	scFree(s->r);
	scFree(s->rv);
	scFree(s->ra);
	scFree(s->rType);
	scFree(s->rgid);
	scFree(s->scCacheMask);
	scFree(s->rUpdateFlag);
}
