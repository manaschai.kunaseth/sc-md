#ifndef __COMPUTE_ACCEL_LJ_H__
#define __COMPUTE_ACCEL_LJ_H__

#include "state.h"
#include "potential.h"
#include "params.h"
#include "cell.h"

void computeAccel_lj(STATE* s, POTENTIAL* pot, PARAMS* p, CELL_INFO *cellInfo);
//void computeAccel_silica_scnbl(STATE* s, POTENTIAL* pot, PARAMS* p, CELL_INFO *cellInfo);

//void computeAccel_silica_thread(STATE* s, POTENTIAL* pot, PARAMS* p, CELL_INFO *cellInfo);
void computeAccel_lj_thread_gpu(STATE* s, POTENTIAL* pot, PARAMS* p, CELL_INFO *cellInfo);
//void computeAccel_silica_thread_mic(STATE* s, POTENTIAL* pot, PARAMS* p, CELL_INFO *cellInfo);

#endif

