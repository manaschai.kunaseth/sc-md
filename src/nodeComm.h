#ifndef __NODE_COMM_H__
#define __NODE_COMM_H__

#include "state.h"

typedef struct comm_info_st {

	//! Node ID of the current node 
	int		myId;
	int		nNodes;	
	//! Node Vector ID of the current node 
	int		myVid[3];

	//! Node IDs of the nearest-neighbor nodes 
	int		myNN[6];

	//! Node IDs of the nearest-neighbor nodes in reverse 
	int		myNNr[6];

	//! total number of MPI tasks in x,y,z directions 
	int		vProc[3];

	//! coordinate shifting vector to the ku-th neighbors 
   	double		shiftVector[6][3];
	double  	rCacheRadius;
	int		myParity[3];

	//! size of buffer data per atom during communication
	int		nCopySizePerAtom;
	int		nMoveSizePerAtom;

	//! type of extra buffer
	int		extraCopyDataType;
	int		extraMoveDataType;
	/*For future implementation
	//CommGroup for local group reduction (i.e. file write, etc.)	
	//nNode in local group.
	*/

} COMM_INFO;

typedef struct atom_cache_data_st {

	//! List of indices of cached atoms
	int** 	cacheList;

	//! Number of cached atoms
	//  Separated in each direction.
	//int	cacheListN;

	//! Maximum number of cacheListN (the same in all directions)
	int	cacheListNMax;

	//! Number of cached atoms to be sent in each direction
	int	directionalCacheSendN[6];

	//! Number of cached atoms to be received in each direction
	int	directionalCacheReceiveN[6];
} ATOM_CACHE_DATA;

void initAtomCache(ATOM_CACHE_DATA* cache,STATE* s);
void initCommunication(COMM_INFO* commInfo);

void atomCopy(STATE* s,ATOM_CACHE_DATA* ac,COMM_INFO* commInfo, double* boxSize);
void atomMove(STATE* s,COMM_INFO* commInfo, double* boxSize);
void forceReturn(STATE*,ATOM_CACHE_DATA*,COMM_INFO*);

#endif
