#include "cell.h"
#include "state.h"
#include "params.h"
#include "computePattern.h"
#include "simulate.h"
#include "utils.h"
#include "constants.h"
#include "memory.h"
#include <stdlib.h>
#include <stdio.h>

#define BASE_NATOM_PER_CELL 8

static CELL_LIST* initCellList(CELL_LIST *cl,STATE* s,double* boxSize,double rcut, double rCache);
static void initCellInfo_nBody(CELL_INFO *cellInfo,double* boxSize,double rcut, double rCache, int nBody);
static void buildCellPattern(CELL_INFO *cellInfo, COMPUTE_PATTERN* pat);


void initCellInfoSet(CELL_INFO *cellInfoSet)
{
	PARAMS* p = getParamsObj();
	INPUT_PARAMS* inp = getInputObj();
	//Set up cell info depend on potential.
	if (paramsGetPotentialType() == POT_SIO2) {
		//2 body for silica
		initCellInfo_nBody(&cellInfoSet[INDEX_2B],p->boxSize,paramsGetRCut(2)/p->cell_nlayer[2],p->commInfo.rCacheRadius,2); 
		COMPUTE_PATTERN *pat2 = simulateGetComputePattern(2);
		buildCellPattern(&cellInfoSet[INDEX_2B],pat2);
		//qsort(cellInfoSet[INDEX_2B].computePatternCell.cellPatternOffset,cellInfoSet[INDEX_2B].computePatternCell.nPattern,sizeof(int)*2,compare_2B);

		//3 body for silica
		if (paramsGetComputationMethod() == COMP_SC) {
			initCellInfo_nBody(&cellInfoSet[INDEX_3B],p->boxSize,paramsGetRCut(3),p->commInfo.rCacheRadius,3);
			COMPUTE_PATTERN *pat3 = simulateGetComputePattern(3);
			buildCellPattern(&cellInfoSet[INDEX_3B],pat3);
		} 
		else if (paramsGetComputationMethod() == COMP_SCNBL) {
			initCellInfo_nBody(&cellInfoSet[INDEX_3B],p->boxSize,paramsGetRCut(2),p->commInfo.rCacheRadius,2);
			COMPUTE_PATTERN *antiPat2 = simulateGetComputePattern(3);
			buildCellPattern(&cellInfoSet[INDEX_3B],antiPat2);
		} 
		
		else {
			terminateSingle("Unknown computation method type.",1);
		}
	}
	else if (paramsGetPotentialType() == POT_LJ) {
		//2 body for LJ
		initCellInfo_nBody(&cellInfoSet[INDEX_2B],p->boxSize,paramsGetRCut(2)/p->cell_nlayer[2],p->commInfo.rCacheRadius,2); 
		COMPUTE_PATTERN *pat2 = simulateGetComputePattern(2);
		buildCellPattern(&cellInfoSet[INDEX_2B],pat2);
	}
	else {
		terminateSingle("Unknown potential type.",1);
	}
}


void initCellInfo_nBody(CELL_INFO *cellInfo,double* boxSize,double rcut, double rCache,int nBody)
{

	// Number of cell list
	for (int a = 0;a < 3;a++) {
		cellInfo->vCell[a] = boxSize[a]/rcut;
		cellInfo->cellDimension[a] = boxSize[a]/cellInfo->vCell[a];
	}

	//How many cells are cached cell. i.e. cell that contain only atom imported from neighbor nodes.
	//For future improvement, nCellCache should double (*2) for 6-side caching.
	for (int a = 0;a < 3;a++) 
		cellInfo->nCellCache[a] = (int)(rCache/cellInfo->cellDimension[a]+1);	

	//Total number of cells
	cellInfo->nCell =  (cellInfo->vCell[0]+cellInfo->nCellCache[0])*
		     	   (cellInfo->vCell[1]+cellInfo->nCellCache[1])*
		           (cellInfo->vCell[2]+cellInfo->nCellCache[2]);
	
	cellInfo->nBody = nBody;
	
	char msg[1000];
	sprintf(msg,"%d-Body",nBody);
	reportPrintStr("Cell information for: ",msg);
	reportPrintIntArr3("Owned        cell vector: ",cellInfo->vCell); 	
	reportPrintIntArr3("Cached       cell vector: ",cellInfo->nCellCache); 	
	reportPrintFixedDouble3("Cell dimension [Ang]: ",cellInfo->cellDimension[0]*BOHR,
							    cellInfo->cellDimension[1]*BOHR,
							    cellInfo->cellDimension[2]*BOHR); 	

	//return cellInfo;
}

CELL_LIST* makeCellList(CELL_LIST *cl,CELL_INFO *cellInfo,STATE* s,double* boxSize,double rcut, double rCache, int nlayer)
{
	//Allocate cell list structure	
	cl = initCellList(cl,s,boxSize,rcut/nlayer,rCache);

	double (*r)[3] = s->r;
	unsigned int *atomSCCacheMask = s->scCacheMask;
	int nAtomAll = s->nAtomOwn + s->nAtomCache;

	int vCellAll[3];
	double cellDimInv[3];
	int **cellAtomData = cl->cellAtomData;
	int *nAtomCell = cl->nAtomCell;
	unsigned int *cellSCCacheMask = cl->cellSCCacheMask;
 
	//copy cell structure info from cellInfo
	for (int a = 0;a < 3;a++) {
		cl->vCell[a] = cellInfo->vCell[a];
		cl->cellDimension[a] = cellInfo->cellDimension[a];
		cl->nCell = cellInfo->nCell;
	}

	//cell vector including caching cells.
	for (int a = 0;a < 3;a++)
		vCellAll[a] = cl->vCell[a] + cl->nCellCache[a];

	for (int a = 0;a < 3;a++)
		cellDimInv[a] = 1.0 / cl->cellDimension[a];

	int vCellX    = vCellAll[0];
	int vCellXY   = vCellAll[0] * vCellAll[1];

	//loop all atoms to build cell list
	for (int i = 0;i < nAtomAll;i++) {
		int c = 0; //index of cell

		c +=           ((int)(r[i][0] * cellDimInv[0])); //x dimension
		c += vCellX  * ((int)(r[i][1] * cellDimInv[1])); //y dimension
		c += vCellXY * ((int)(r[i][2] * cellDimInv[2])); //z dimension

		cellAtomData[c][nAtomCell[c]] = i;
		nAtomCell[c]++;
		atomSCCacheMask[i] = cellSCCacheMask[c];		
	}

	//print all cell information, if VERBOSE > 1
	#if (VERBOSE > 2)
		int nCount = 0;
		for (int i = 0;i < cl->nCell;i++){
			printf("cell [%5d]:",i);
			for (int j = 0;j < cl->nAtomCell[i]; j++){
				printf(" %4d",cl->cellAtomData[i][j]);
				nCount++;
			}
			printf("\n");
		}
		printf("Total n: %d\n",nCount);
	#endif

	//Check if cell data exceed capacity or not
	for (int i = 0;i < cl->nCell;i++){
		if (nAtomCell[i] > cl->nAtomCellMax[i]) {
			char msg[1000];
			sprintf(msg,"MPI RANK %d: Cell %d over capacity (%d/%d)",getMyId(),i,nAtomCell[i],cl->nAtomCellMax[i]);
			terminate(msg);
		}
	}


	return cl;

}

CELL_LIST* initCellList(CELL_LIST *cl,STATE* s,double* boxSize,double rcut, double rCache)
{
	int fact = 3;

	cl = (CELL_LIST*)scMalloc(sizeof(CELL_LIST));

	// Number of cell list
	for (int a = 0;a < 3;a++) {
		cl->vCell[a] = boxSize[a]/rcut;
		cl->cellDimension[a] = boxSize[a]/cl->vCell[a];
	}

	//How many cells are cached cell. i.e. cell that contain only atom imported from neighbor nodes.
	//For future improvement, nCellCache should double (*2) for 6-side caching.
	int nCellCache[3];
	for (int a = 0;a < 3;a++) 
		nCellCache[a] = (int)(rCache/cl->cellDimension[a]+1);	

	//Total number of cells
	int nCell =  (cl->vCell[0]+nCellCache[0])*
		     (cl->vCell[1]+nCellCache[1])*
		     (cl->vCell[2]+nCellCache[2]);

	//Estimate atom resident in each cell.
	//This will be used to allocate size of cell.
	int avgAtomPerCell = BASE_NATOM_PER_CELL+(s->nAtomOwn+s->nAtomCache)/nCell;

	//NOTES: cellAtomData use 2D array instead of arrays of array. This would data locality especially for GPU.
	cl->cellAtomData = (int**)scMalloc(sizeof(int*) * nCell);
	cl->nAtomCell    = (int*)scMalloc(sizeof(int)*nCell);
	cl->cellSCCacheMask = (unsigned int*)scMalloc(sizeof(unsigned int)*nCell);
	cl->nAtomCellMax = (int*)scMalloc(sizeof(int)*nCell);
	int* cellAtomData = (int*)scMalloc(sizeof(int) * nCell * avgAtomPerCell*fact);
	for (int i = 0;i < nCell;i++) {
		cl->nAtomCellMax[i] = avgAtomPerCell*fact;
		//cl->cellAtomData[i] = (int*)scMalloc(sizeof(int)*cl->nAtomCellMax[i]);
		cl->cellAtomData[i] = &cellAtomData[i * avgAtomPerCell * fact];
		cl->nAtomCell[i] = 0; //reset number of atom in cell to 0;
		cl->cellSCCacheMask[i] = SC_CACHE_MASK_NONE;
	}

	//store paremeters to cell_list
	for (int a = 0;a < 3;a++)	
		cl->nCellCache[a] = nCellCache[a];
	cl->nCell = nCell;


	//store cell's SC cache mask
	int vCellAll[3];

	//cell vector including caching cells.
	for (int a = 0;a < 3;a++)
		vCellAll[a] = cl->vCell[a] + cl->nCellCache[a];

	//variable use for calculating cell index
	int vCellAllX  = vCellAll[0];
	int vCellAllXY = vCellAll[0]*vCellAll[1];

	//Loop over all cell within domain.
	for (int cz = 0;cz < vCellAll[2];cz++)
	for (int cy = 0;cy < vCellAll[1];cy++)
	for (int cx = 0;cx < vCellAll[0];cx++) {

		//index of cell, c
		unsigned scMask = 0;
		int c = cx + cy*vCellAllX + cz*vCellAllXY;
		if (cx >= cl->vCell[0]) scMask |= SC_CACHE_MASK_X;
		if (cy >= cl->vCell[1]) scMask |= SC_CACHE_MASK_Y;
		if (cz >= cl->vCell[2]) scMask |= SC_CACHE_MASK_Z;

		cl->cellSCCacheMask[c] = scMask;

	}

	return cl;
}

CELL_LIST* freeCellListData(CELL_LIST *cl)
{
	//for (int i = 0;i < cl->nCell;i++) {
	//	scFree(cl->cellAtomData[i]);
	//}
	scFree(cl->nAtomCell);
	scFree(cl->nAtomCellMax);
	scFree(cl->cellAtomData[0]);
	scFree(cl->cellAtomData);
	scFree(cl->cellSCCacheMask);
	scFree(cl);
	cl = NULL;
	return cl;
}

void buildCellPattern(CELL_INFO *cellInfo, COMPUTE_PATTERN* pat)
{
	int nBody = pat->nBody;
	int nPattern = pat->nPattern;
	COMPUTE_PATTERN_CELL *patCell = &cellInfo->computePatternCell;

	patCell->cellPatternOffset = (int**)scMalloc(sizeof(int*)*nPattern);
	int* patCellArray = (int*)scMalloc(sizeof(int)*nBody*nPattern);
	for (int i = 0;i < nPattern;i++)
		//patCell->cellPatternOffset[i] = (int*)scMalloc(sizeof(int)*nBody);
		patCell->cellPatternOffset[i] = &patCellArray[i*nBody];

	patCell->nPattern = nPattern;
	patCell->nBody = nBody;

	int vCellAll[3];
	double cellDimInv[3];
	//cell vector including caching cells.
	for (int a = 0;a < 3;a++)
		vCellAll[a] = cellInfo->vCell[a] + cellInfo->nCellCache[a];

	for (int a = 0;a < 3;a++)
		cellDimInv[a] = 1.0 / cellInfo->cellDimension[a];

	int vCellX    = vCellAll[0];
	int vCellXY   = vCellAll[0] * vCellAll[1];
	//int vCellXYZ  = vCellAll[0] * vCellAll[1] * vCellAll[2];

	int **cellPatternOffset = patCell->cellPatternOffset;
	int (**pattern)[3] = pat->pattern;
	//loop all atoms to build cell list
	for (int i = 0;i < nPattern;i++) {
		for (int j = 0;j < nBody;j++) {

			cellPatternOffset[i][j] =    pattern[i][j][0]            //x dimension
		                                  + (pattern[i][j][1]*vCellX)    //y dimension
	                                          + (pattern[i][j][2]*vCellXY);  //z dimension
		
		}
	}
	reportPrintInt("Cell compute pattern built: number of pattern = ",nPattern);

	#if (DEBUG_LEVEL > 1)		
	char msg[1000];
	for (int i = 0;i < nPattern;i++){
		sprintf(msg,"Pattern [%5d]: (%5d)->(%5d)",i,cellPatternOffset[i][0],cellPatternOffset[i][1]);
	
		reportVerbose(msg);
	}

	#endif

}


void freeCellPattern(CELL_INFO *cellInfo)
{
	COMPUTE_PATTERN_CELL* patCell = &cellInfo->computePatternCell;

	int nPattern = patCell->nPattern;
	//for (int i = 0;i < nPattern;i++)
	//	scFree(patCell->cellPatternOffset[i]);

	scFree(patCell->cellPatternOffset[0]);
	scFree(patCell->cellPatternOffset);
}
/*


void sortCellPattern(COMPUTE_PATTERN_CELL* patCell)
{
	int nBody = pat->nBody;
	int nPattern = pat->nPattern;

	int **cellPatternOffset = patCell->cellPatternOffset;
	//loop all atoms to build cell list
	for (int i = 0;i < nPattern;i++) {
		for (int j = 0;j < nBody;j++) {

			cellPatternOffset[i][j] =    pattern[i][j][0]            //x dimension
		                                  + (pattern[i][j][1]*vCellX)    //y dimension
	                                          + (pattern[i][j][2]*vCellXY);  //z dimension
		
		}
	}
	reportPrintInt("Cell compute pattern built: number of pattern = ",nPattern);

	#if (DEBUG_LEVEL > 1)		
	char msg[1000];
	for (int i = 0;i < nPattern;i++){
		sprintf(msg,"Pattern [%5d]: (%5d)->(%5d)",i,cellPatternOffset[i][0],cellPatternOffset[i][1]);
	
		reportVerbose(msg);
	}

	#endif

}


int compare_2B(const void *a, const void *b)
{
	int *pat1src = (int*)a
	int *pat2src = (int*)b
	if (pat1src[0] < pat2src[0])
		return -1;
	else if (pat1src[0] == pat2src[0])
		return pat1src[1] - pat2src[1];
	else
		return 1;
}

int compare_3B(const void *a, const void *b)
{
	int *pat1src = (int*)a
	int *pat2src = (int*)b
	if (pat1src[0] < pat2src[0])
		return -1;
	else if (pat1src[0] == pat2src[0])
		if (pat1src[1] < pat2src[1])
			return -1;
		else if (pat1src[1] == pat2src[1])
			return pat1src[2] - pat2src[2];
		else
			return 1;
	else
		return 1;


}
*/
