#include "constants.h"
#include "simulate.h"
#include "state.h"
#include "params.h"
#include "cell.h"
#include "utils.h"
#include "input.h"
#include "memory.h"
#include <assert.h>
#include <stdlib.h>
#include "init.h"

void analysis_applyStrain(STATE* s,PARAMS* p)
{
	
	INPUT_PARAMS* inp = getInputObj();

	double (*r)[3]  = s->r;

	double scaleFactor[3];
	double scaleFactorInv[3];
	double refCoord[3];

	#if (VERBOSE > 0)
	reportPrintFixedDoubleArr3("Applying strain [Ang]:",inp->analyzeApplyStrain);
	#endif

	for (int a = 0;a < 3;a++){
		scaleFactor[a]    = p->hMatrix[a][a];
		scaleFactorInv[a] = 1.0/scaleFactor[a];
	}

	for (int a = 0;a < 3;a++){
		p->boxSize[a] *= scaleFactorInv[a];
		refCoord[a] = s->cornerCoord[a]*scaleFactor[a];
	}

	//Convert all position to global coordinate.
	int nAtom = s->nAtomOwn;
	for (int i = 0;i < nAtom;i++)
		for (int a = 0;a < 3;a++)
			r[i][a] = (r[i][a]+refCoord[a])*scaleFactorInv[a];


	//Apply strain to hMatrix:
	for (int a = 0;a < 3;a++)
		p->hMatrix[a][a] += inp->analyzeApplyStrain[a]/BOHR;
	
	//Re-calculate scaling factor from hMatrix:
	for (int a = 0;a < 3;a++){
		scaleFactor[a]    = p->hMatrix[a][a];
		scaleFactorInv[a] = 1.0/scaleFactor[a];
	}
	
	//Apply new scale
	for (int a = 0;a < 3;a++){
		p->boxSize[a] *= scaleFactor[a];
		refCoord[a] = s->cornerCoord[a]*scaleFactor[a];
	}


	//Convert all position to scaled-local coordinate.
	for (int i = 0;i < nAtom;i++)
		for (int a = 0;a < 3;a++)
			r[i][a] = (r[i][a]*scaleFactor[a])-refCoord[a];

	//Recompute cell system
	CELL_INFO* cellInfoSet = simulateGetCellInfoSet();
	initCellInfoSet(cellInfoSet);

	//Re-init topology
	initTopology(&p->commInfo,p->boxSize);

	#if (VERBOSE > 0)
	double boxSizeAng[3];
	for (int a = 0;a < 3;a++)
		boxSizeAng[a] = p->boxSize[a]*BOHR;
	
	reportPrintDoubleArr3("boxSize [Ang]  : ", boxSizeAng);
	reportPrintDoubleArr3("H-Matrix (a.u.): ", p->hMatrix[0]);
	reportPrintDoubleArr3("                 ", p->hMatrix[1]);
	reportPrintDoubleArr3("                 ", p->hMatrix[2]);
	reportPrint("");
	#endif
}
