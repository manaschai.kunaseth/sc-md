#include "atomType.h"
#include "constants.h"
#include "params.h"
#include "utils.h"
#include <stdio.h>
#include <string.h>
#include <assert.h>

#define N_ALLOW_SYMBOL 5
static char atomTypeTable[ATOM_TYPE_MAX][ATOM_TYPE_SYMBOL_MAX];  
static char nAtomType;

static const char ELEMENT_SYMBOL[N_ALLOW_SYMBOL][ATOM_TYPE_SYMBOL_MAX] = {
	"SI",
	"O",
	"P1",
	"P2",
	"AR"
};


void initAtomTypeTable(PARAMS *p)
{
	nAtomType = p->nAtomType;

	for (int i = 0;i < nAtomType;i++){
		
		int isFound = 0;
		for (int j = 0;j < N_ALLOW_SYMBOL;j++) {
			//reportPrint(p->atomTypeTable[i]);
			if (strcmp(ELEMENT_SYMBOL[j],p->atomTypeTable[i])==0)
				isFound = 1;
		}
		if (isFound==0)
			terminateSingle("Unknown atom type declared in input file",1);
		strcpy(atomTypeTable[i],p->atomTypeTable[i]);
	}
}

/*--------------------------------------------------------------------*/
int typeFromSymbol(const char* symbol) {
/*--------------------------------------------------------------------*/

  //Lookup table and solve atom type id
  for (int i = 0;i < nAtomType;i++)
  	if (strcmp(atomTypeTable[i],symbol)==0)
		return i;

  char msg[1000];
  sprintf(msg,"Incorrect atom symbol: (%s)",symbol);
  terminateSingle(msg,1);
  return -1;
}


/*--------------------------------------------------------------------*/
void symbolFromType(int typeId, char* symbol) {
/*--------------------------------------------------------------------*/
  
  assert((typeId < nAtomType) && (typeId >= 0));
  strcpy(symbol,atomTypeTable[typeId]);
}

