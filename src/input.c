#include "constants.h"
#include "params.h"
#include "state.h"
#include "input.h"
#include "mpi.h"
#include "fileUtils.h"
#include "utils.h"
#include "memory.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_INPUT_LINE 4096
#define OP_CHAR_LENGTH 40
#define ARGS_CHAR_LENGTH 1000

int splitInput(char* s,char* op, char* args);
int parseInput(char* s);

static INPUT_PARAMS* s_inp;

void initInput(INPUT_PARAMS* input)
{
	s_inp = input;
	//critical value will be set to negative such that it would halt program when trying to run.
	s_inp->nAtomType = 0;
	s_inp->unitCellAtomCount = 0;
	s_inp->stepLimit = -1;
	s_inp->potentialTableNBin = -1;
	sprintf(s_inp->computationMethodName,"DEFAULT"); //set default computation

	s_inp->parallelMode = PARMODE_MPI;
	for (int i = 0;i < MAX_NBODY;i++){
		s_inp->nThreads[i] = 0;
		s_inp->nGpus[i] = 0;
		s_inp->nlayer[i] = 1;
		s_inp->gpu_chunk[i] = 1;
	}


	for (int i = 0;i < 3;i++)
		s_inp->vProc[i] = -1;

	s_inp->mdMode = -1;
	s_inp->randomSeed = 13597;
	s_inp->deltaT = -1;
	s_inp->evalPropFreq = 100;
	s_inp->detailedEvalProp = 0;
	s_inp->initTemp = 0.0;
	s_inp->runtimeProfileRatio = 1;
	s_inp->runtimeProfileWriteFreq = 0;

	s_inp->analyzeVelocityScalingFreq = 0;
	s_inp->analyzeVelocityScalingTempBegin = 0;
	s_inp->analyzeVelocityScalingTempEnd = 0;

	s_inp->analyzeApplyStrainFreq = 0;
	s_inp->analyzeApplyStrain[0] = 0.0;
	s_inp->analyzeApplyStrain[1] = 0.0;
	s_inp->analyzeApplyStrain[2] = 0.0;

	s_inp->analyzeFixBoundary = 0;
	s_inp->analyzeFixBoundaryLength[0][0] = 0.0;
	s_inp->analyzeFixBoundaryLength[0][1] = 0.0;
	s_inp->analyzeFixBoundaryLength[1][0] = 0.0;
	s_inp->analyzeFixBoundaryLength[1][1] = 0.0;
	s_inp->analyzeFixBoundaryLength[2][0] = 0.0;
	s_inp->analyzeFixBoundaryLength[2][1] = 0.0;

	s_inp->analyzeReorder = 1;
	s_inp->analyzeReorderFreq = 100;

	s_inp->analyzeReorder = 1;

	s_inp->analyzeRdf = 0;
	s_inp->analyzeRdfFreq = 0;
	s_inp->analyzeRdfRCut = 10.0;

	s_inp->analyzeVacf = 0;
	s_inp->analyzeVacfFreq = 5;
	//s_inp->analyzeVacfNCollect = 2000;

	s_inp->writeAggregateOutputXYZ = 1;
	s_inp->writeAggregateOutputMTS = 1;
	s_inp->writeAggregateOutputAtomicForce = 1;

	s_inp->writeOutputXYZ = 0;
	s_inp->writeOutputXYZFreq = 0;
	s_inp->writeOutputXYZMode = WRITE_XYZ_GLOBAL;

	s_inp->writeOutputMTS = 1;
	s_inp->writeOutputMTSFreq = 0;
	s_inp->writeOutputMTSFormat = WRITE_MTS_BINARY;

	s_inp->writeOutputAtomicForce = 0;
	s_inp->writeOutputAtomicForceFreq = 0;
	s_inp->writeOutputAtomicForceDummy = WRITE_DUMMY;

	s_inp->readAsciiMTS = 0;
}

INPUT_PARAMS* getInputObj()
{
	return s_inp;
}

int splitInput(char* s,char* op, char* args)
{
	int len = strlen(s);
	int argOffset = 0;
	int equalPos = 0;
	int isFoundEqual = 0;

	for (int i = 0;(i < len) && (i < OP_CHAR_LENGTH) && (isFoundEqual == 0);i++) {
		if (s[i] != '=') {
			equalPos++;
		} else {
			strncpy(op,s,equalPos);
			op[equalPos] = '\0';
			inPlaceTrim(op);
			argOffset = i;	
			isFoundEqual = 1;
		}
	}


	if (!isFoundEqual)
		return 1;

		strncpy(args, &s[argOffset+1], len-argOffset+1);
		//for (int i = argOffset;i < len;i++)
		//	args[i-argOffset] = s[i];
		inPlaceTrim(args);

	return 0;
}

int parseInput(char* s)
{

	char  inputOp[OP_CHAR_LENGTH];
	char  inputArgs[ARGS_CHAR_LENGTH];

	strcpy(inputOp,"");
	strcpy(inputArgs,"");

	int isError = splitInput(s,inputOp,inputArgs);
	strToUpper(inputOp);
	if (isError)
		return isError;

#if (DEBUG_LEVEL > 0)
	char str[1000];
	sprintf(str,"OPS: %s ARGS:%s", inputOp,inputArgs);
	reportPrint(str);
#endif

	int isNotMatched = 1;
	if (strcmp(inputOp,"MD_MODE")==0) {
		int nArgs = sscanf(inputArgs,"%d",&s_inp->mdMode);
		if (nArgs < 1)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
	}

	else if (strcmp(inputOp,"STEP_LIMIT")==0) {
		int nArgs = sscanf(inputArgs,"%d",&s_inp->stepLimit);
		if (nArgs < 1)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
	}

	else if ((strcmp(inputOp,"TIME_STEP")==0) || (strcmp(inputOp,"DELTA_T")==0)) {
		int nArgs = sscanf(inputArgs,"%le",&s_inp->deltaT);
		if (nArgs < 1)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
		if (strcmp(inputOp,"DELTA_T")==0) 
			reportPrint("WARNING: DELTA_T OPS is depricated and will be removed. Please use TIME_STEP instead.");
	}

	else if (strcmp(inputOp,"POTENTIAL")==0) {
		strToUpper(inputArgs);
		int nArgs = sscanf(inputArgs,"%s",s_inp->potentialName);
		if (nArgs < 1)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
	}

	else if (strcmp(inputOp,"MTS_DIR")==0) {
		int nArgs = sscanf(inputArgs,"%s",s_inp->mtsDir);
		if (nArgs < 1)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
	}

	else if (strcmp(inputOp,"VELOCITY_SCALING")==0) {
		int nArgs = sscanf(inputArgs,"%d %le %le",&s_inp->analyzeVelocityScalingFreq,
						    &s_inp->analyzeVelocityScalingTempBegin,
						    &s_inp->analyzeVelocityScalingTempEnd);
		if (nArgs < 3)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
	}


	else if (strcmp(inputOp,"RUNTIME_PROFILE_RATIO")==0) {
		int nArgs = sscanf(inputArgs,"%d",&s_inp->runtimeProfileRatio);
		if (nArgs < 1)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
	}

	else if (strcmp(inputOp,"RUNTIME_PROFILE_WRITE_FREQ")==0) {
		int nArgs = sscanf(inputArgs,"%d",&s_inp->runtimeProfileWriteFreq);
		if (nArgs < 1)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
	}


	else if (strcmp(inputOp,"FIX_BOUNDARY")==0) {
		int nArgs = sscanf(inputArgs,"%d %le %le %le %le %le %le",&s_inp->analyzeFixBoundary,
						    &s_inp->analyzeFixBoundaryLength[0][0],
						    &s_inp->analyzeFixBoundaryLength[0][1],
						    &s_inp->analyzeFixBoundaryLength[1][0],
						    &s_inp->analyzeFixBoundaryLength[1][1],
						    &s_inp->analyzeFixBoundaryLength[2][0],
						    &s_inp->analyzeFixBoundaryLength[2][1]);
		if (nArgs < 7)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
	}


	else if (strcmp(inputOp,"COMPUTE_RDF")==0) {
		int nArgs = sscanf(inputArgs,"%d %d %le %le",&s_inp->analyzeRdf,
						    &s_inp->analyzeRdfFreq,
						    &s_inp->analyzeRdfRCut,
						    &s_inp->analyzeRdfBinLength);
		if (nArgs < 4)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
	}

	else if (strcmp(inputOp,"COMPUTE_VACF")==0) {
		int nArgs = sscanf(inputArgs,"%d %d",&s_inp->analyzeVacf,
						    &s_inp->analyzeVacfFreq);
		if (nArgs < 2)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
	}


	else if (strcmp(inputOp,"APPLY_STRAIN")==0) {
		int nArgs = sscanf(inputArgs,"%d %le %le %le",&s_inp->analyzeApplyStrainFreq,
						    &s_inp->analyzeApplyStrain[0],
						    &s_inp->analyzeApplyStrain[1],
						    &s_inp->analyzeApplyStrain[2]);
		if (nArgs < 4)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
	}


	else if (strcmp(inputOp,"WRITE_XYZ")==0) {
		int xyzMode;
		int nArgs = sscanf(inputArgs,"%d %d %d",&s_inp->writeOutputXYZ,
						    &s_inp->writeOutputXYZFreq,
						    &xyzMode);
		if (nArgs < 3)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
		if (xyzMode == 0)
			s_inp->writeOutputXYZMode = WRITE_XYZ_LOCAL;
		else
			s_inp->writeOutputXYZMode = WRITE_XYZ_GLOBAL;
	}

	else if (strcmp(inputOp,"WRITE_AGGREGATE_OUTPUT_MTS")==0) {
		int xyzMode;
		int nArgs = sscanf(inputArgs,"%d",&s_inp->writeAggregateOutputMTS);
		if (nArgs < 1)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
	}

	else if (strcmp(inputOp,"WRITE_AGGREGATE_OUTPUT_ATOMIC_FORCE")==0) {
		int xyzMode;
		int nArgs = sscanf(inputArgs,"%d",&s_inp->writeAggregateOutputAtomicForce);
		if (nArgs < 1)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
	}

	else if (strcmp(inputOp,"WRITE_AGGREGATE_OUTPUT_XYZ")==0) {
		int xyzMode;
		int nArgs = sscanf(inputArgs,"%d",&s_inp->writeAggregateOutputXYZ);
		if (nArgs < 1)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
	}


	else if (strcmp(inputOp,"DATA_REORDER")==0) {
		int nArgs = sscanf(inputArgs,"%d %d",&s_inp->analyzeReorder,
						    &s_inp->analyzeReorderFreq);
		if (nArgs < 2)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
	}

	else if (strcmp(inputOp,"WRITE_MTS")==0) {
		int mtsFormat;
		int nArgs = sscanf(inputArgs,"%d %d %d",&s_inp->writeOutputMTS,
						    &s_inp->writeOutputMTSFreq,
						    &mtsFormat);
		if (nArgs < 3)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
		if (mtsFormat == 0)
			s_inp->writeOutputMTSFormat = WRITE_MTS_BINARY;
		else
			s_inp->writeOutputMTSFormat = WRITE_MTS_ASCII;
			
	}

	else if (strcmp(inputOp,"WRITE_ATOMIC_FORCE")==0) {
		int nArgs = sscanf(inputArgs,"%d %d",&s_inp->writeOutputAtomicForce,
						    &s_inp->writeOutputAtomicForceFreq);
		if (nArgs < 2)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
			
	}

	else if (strcmp(inputOp,"CELL_LAYER")==0) {
		int nb;
		int nlayer;
		int nArgs = sscanf(inputArgs,"%d %d",&nb,&nlayer);
		if (nArgs < 2)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
		if (nb > 0){
			s_inp->nlayer[nb] = nlayer;
		} 
		
	}
	
	else if (strcmp(inputOp,"OPENMP_MODE")==0) {
		int omp;
		int nArgs = sscanf(inputArgs,"%d",&omp);
		if (nArgs < 1)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
		if (omp == 1)
			s_inp->parallelMode = PARMODE_MPI_OMP;
			
	}

	else if (strcmp(inputOp,"COMPUTATION_METHOD")==0) {
		strToUpper(inputArgs);
		int nArgs = sscanf(inputArgs,"%s",s_inp->computationMethodName);
		if (nArgs < 1)
			return ERRORCODE_FATAL;
		isNotMatched = 0;

	}

	else if (strcmp(inputOp,"GPU_MODE")==0) {
		int gpu_mode, nGpus;
		int nArgs = sscanf(inputArgs,"%d %d",&gpu_mode, &nGpus);
		if (nArgs < 2)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
		if (gpu_mode == 1) {
			s_inp->parallelMode = PARMODE_MPI_OMP_GPU;
			s_inp->nGpus[0] = nGpus;
			
		}
			
	}

	else if (strcmp(inputOp,"GPU_CHUNK")==0) {
		int nbody, gpu_chunk;
		int nArgs = sscanf(inputArgs,"%d %d",&nbody,&gpu_chunk);
		if (nArgs < 1)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
		s_inp->gpu_chunk[nbody] = gpu_chunk;
			
	}

	
	else if (strcmp(inputOp,"NGPUS")==0) {
		int nBody;
		int nGpus;
		int nArgs = sscanf(inputArgs,"%d %d",&nBody, &nGpus);
		if (nArgs < 2)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
		terminateSingle("ERROR: NGPUS OPS is currently disabled. Use GPU_MODE to enable/disable GPU mode and also set total number of GPUs instead.",1);
		if (nBody > 0){
			s_inp->nGpus[nBody] = nGpus;
			//sum total number of threads
			s_inp->nGpus[0] = 0;
			for (int i = 0;i < MAX_NBODY;i++)
				s_inp->nGpus[0] += s_inp->nGpus[i];
		}
	}
	
		
	else if (strcmp(inputOp,"MIC_MODE")==0) {
		int mic;
		int nMic;
		int nArgs = sscanf(inputArgs,"%d %d",&mic,&nMic);
		if (nArgs < 2)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
		if (mic == 1) {
			s_inp->parallelMode = PARMODE_MPI_OMP_MIC;
			s_inp->nMics = nMic;	
		}
	}

	else if (strcmp(inputOp,"NTHREADS")==0) {
		int nBody;
		int nThreads;
		int nArgs = sscanf(inputArgs,"%d %d",&nBody, &nThreads);
		if (nArgs < 2)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
		if (nBody > 0){
			s_inp->nThreads[nBody] = nThreads;
			//sum total number of threads
			s_inp->nThreads[0] = 0;
			for (int i = 0;i < MAX_NBODY;i++)
				s_inp->nThreads[0] += s_inp->nThreads[i];
		}
	}
	
	else if (strcmp(inputOp,"NTHREADS_MIC")==0) {
		int nBody;
		int nThreads_mic;
		int nArgs = sscanf(inputArgs,"%d %d",&nBody, &nThreads_mic);
		if (nArgs < 2)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
		if (nBody > 0){
			s_inp->nThreads_mic[nBody] = nThreads_mic;
			//sum total number of threads
			s_inp->nThreads_mic[0] = 0;
			for (int i = 0;i < MAX_NBODY;i++)
				s_inp->nThreads_mic[0] += s_inp->nThreads_mic[i];
		}
	}
	

	/*
	else if (strcmp(inputOp,"READ_ASCII_MTS")==0) {
		int nArgs = sscanf(inputArgs,"%d",&s_inp->readAsciiMTS);
		if (nArgs < 1)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
	}
	*/


	else if (strcmp(inputOp,"N_NODES")==0) {
		int nArgs = sscanf(inputArgs,"%d %d %d",&s_inp->vProc[0],
							&s_inp->vProc[1], 
							&s_inp->vProc[2]);
		if (nArgs < 3)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
	}

	else if (strcmp(inputOp,"INIT_UNIT_CELL")==0) {
		int nArgs = sscanf(inputArgs,"%d %d %d",&s_inp->initUCell[0],
							&s_inp->initUCell[1], 
							&s_inp->initUCell[2]);
		if (nArgs < 3)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
	}

	else if (strcmp(inputOp,"ATOM_TYPE")==0) {
		strToUpper(inputArgs);
		int nArgs = sscanf(inputArgs,"%s %le",s_inp->atomTypeTable[s_inp->nAtomType],
						      &s_inp->atomMass[s_inp->nAtomType]);
		if (nArgs < 2)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
		s_inp->nAtomType++;
	}
/*
	else if (strcmp(inputOp,"ATOM_IN_UNIT_CELL")==0) {
		int nArgs = sscanf(inputArgs,"%d",&s_inp->nAtomUCell);
		if (nArgs < 1)
			return 1;
		isNotMatched = 0;
		s_inp->unitCellAtomSymbol = (scMalloc(sizeof(char[ATOM_TYPE_SYMBOL_MAX])*s_inp->nAtomUCell));
		s_inp->unitCellAtomCoord = (scMalloc(sizeof(double[3])*s_inp->nAtomUCell));
	}
*/

	else if (strcmp(inputOp,"INIT_TEMP")==0) {
		int nArgs = sscanf(inputArgs,"%le",&s_inp->initTemp);
		if (nArgs < 1)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
	}

	else if (strcmp(inputOp,"RANDOM_SEED")==0) {
		int nArgs = sscanf(inputArgs,"%d",&s_inp->randomSeed);
		if (nArgs < 1)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
	}

	else if (strcmp(inputOp,"EVAL_PROP_FREQ")==0) {
		int nArgs = sscanf(inputArgs,"%d",&s_inp->evalPropFreq);
		if (nArgs < 1)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
	}

	else if (strcmp(inputOp,"DETAILED_EVAL_PROP")==0) {
		int nArgs = sscanf(inputArgs,"%d",&s_inp->detailedEvalProp);
		if (nArgs < 1)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
	}


	else if (strcmp(inputOp,"POTENTIAL_TABLE_SIZE")==0) {
		int nArgs = sscanf(inputArgs,"%d",&s_inp->potentialTableNBin);
		if (nArgs < 1)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
	}

	else if (strcmp(inputOp,"LATTICE_CONST")==0) {
		int nArgs = sscanf(inputArgs,"%le %le %le",&s_inp->latticeConst[0],
							&s_inp->latticeConst[1], 
							&s_inp->latticeConst[2]);
		if (nArgs < 3)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
	}

	else if (strcmp(inputOp,"UNIT_CELL_ATOM")==0) {
		int count = s_inp->unitCellAtomCount;
		int nArgs = sscanf(inputArgs,"%s %le %le %le",s_inp->unitCellAtomSymbol[count],
							&s_inp->unitCellAtomCoord[count][0],
							&s_inp->unitCellAtomCoord[count][1],
							&s_inp->unitCellAtomCoord[count][2]);
		if (nArgs < 3)
			return ERRORCODE_FATAL;
		isNotMatched = 0;

		count++;
		if (count > UNIT_CELL_ATOM_MAX)
			terminateSingle("Number of unit cell atoms exceeds maximum value.",1);
		s_inp->unitCellAtomCount=count;
	}

	else if (strcmp(inputOp,"INCLUDE")==0) {
		char includeFileName[1000];
		int nArgs = sscanf(inputArgs,"%s",includeFileName);
		if (nArgs < 1)
			return ERRORCODE_FATAL;
		isNotMatched = 0;
		
		//TAG "INCLUDE" is special. It will call splitLine and recursively parse input.
		
		inPlaceTrim(includeFileName);
		reportPrintStr("Reading include file:",includeFileName);
		char** inputLineStr;
		inputLineStr = (char**)(scMalloc(sizeof(char*)*MAX_INPUT_LINE));
		char* inputLineStr1d = (char*)scMalloc(sizeof(char) * MAX_INPUT_LINE * MAX_LINE_CHAR_LENGTH);
		//inputLineStr = (char**)(scMalloc(sizeof(char)*MAX_INPUT_LINE*MAX_LINE_CHAR_LENGTH));

		for (int i = 0;i < MAX_INPUT_LINE;i++)
			//inputLineStr[i] = (char*) scMalloc(sizeof(char[MAX_LINE_CHAR_LENGTH]));
			inputLineStr[i] = &inputLineStr1d[i*MAX_LINE_CHAR_LENGTH];
		int nLine;
		if (getMyId() == 0) {
			nLine = splitLine(includeFileName, inputLineStr, MAX_INPUT_LINE);
		}
		MPI_Bcast(&nLine,1,MPI_INT,0,MPI_COMM_WORLD);
		MPI_Bcast(inputLineStr1d,nLine*MAX_LINE_CHAR_LENGTH,MPI_CHAR,0,MPI_COMM_WORLD);

		#ifdef VERBOSE
		for (int i = 0;i < nLine;i++) {
			char str[1000];
			sprintf(str,"Read input [%s] (%s)",includeFileName,inputLineStr[i]);
			reportVerbose(str);
		}
		#endif

		for (int i = 0;i < nLine;i++) {
			int isError = parseInput(inputLineStr[i]);
			if (isError == ERRORCODE_FATAL) {
				//The following code force terminate of program if input argument unmatched.	
				char strErr[MAX_LINE_CHAR_LENGTH];
				sprintf(strErr,"Invalid input argument(s) in %s: %s\n",includeFileName,inputLineStr[i]);
				terminateSingle(strErr,1);	
				//terminate(strErr);
			}
		}

		reportPrintStr("Finish include file:",includeFileName);
		//for (int i = 0;i < MAX_INPUT_LINE;i++)
		//	scFree(inputLineStr[i]);
	
		scFree(inputLineStr1d);
		scFree(inputLineStr);
	}



	//In case no Input matching
	else 
	{
		char msg[1000];
		sprintf(msg,"No match for input parameter OP(%s), ARGS(%s)",inputOp,inputArgs);
		terminateSingle(msg,1);
	}


	
	return isNotMatched;

}

void readInput()
{
	
	char** inputLineStr;
   	inputLineStr = (char**)(scMalloc(sizeof(char*)*MAX_INPUT_LINE));
	char* inputLineStr1d = (char*)scMalloc(sizeof(char) * MAX_INPUT_LINE * MAX_LINE_CHAR_LENGTH);

	for (int i = 0;i < MAX_INPUT_LINE;i++)
		inputLineStr[i] = &inputLineStr1d[i*MAX_LINE_CHAR_LENGTH];
      		//inputLineStr[i] = (char*) scMalloc(sizeof(char[MAX_LINE_CHAR_LENGTH]));

	int nLine;
	if (getMyId() == 0) {
		nLine = splitLine("scmd.in", inputLineStr, MAX_INPUT_LINE);
	}
	MPI_Bcast(&nLine,1,MPI_INT,0,MPI_COMM_WORLD);
	MPI_Bcast(inputLineStr1d,nLine*MAX_LINE_CHAR_LENGTH,MPI_CHAR,0,MPI_COMM_WORLD);

	//initInput(input);

	#ifdef VERBOSE
	for (int i = 0;i < nLine;i++) {
		char str[1000];
		sprintf(str,"Read input [scmd.in] (%s)",inputLineStr[i]);
		reportVerbose(str);
	}
	#endif
	reportPrint("Read input from scmd.in completed.");

	for (int i = 0;i < nLine;i++) {
		int isError = parseInput(inputLineStr[i]);
      		if (isError == ERRORCODE_FATAL) {
			//The following code force terminate of program if input argument unmatched.	
			char strErr[MAX_LINE_CHAR_LENGTH];
			sprintf(strErr,"Invalid input argument(s): %s\n",inputLineStr[i]);
			terminateSingle(strErr,1);	
			//terminate(strErr);
		}
	}

	reportPrint("Parse input completed.");

	//for (int i = 0;i < MAX_INPUT_LINE;i++)
	//	scFree(inputLineStr[i]);

	scFree(inputLineStr1d);
	scFree(inputLineStr);

}

