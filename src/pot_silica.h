#ifndef __POT_SILICA_H__
#define __POT_SILICA_H__

#include "potential.h"
#include "input.h"
#include "params.h"

typedef struct potential_silica_3b_st {
	double* 	r0;
	double*		r02;
	double*		dl;
	double***	bb;
	double***	cosb;
	double***	c3b;
	
	double		rcut3b;

	double**	rcut2_2b; //cut-off^2 for each atom type of ij
	int		nType;
} POTENTIAL_SILICA_EXTENSION;

void initPotential_silica(POTENTIAL *pot);
#endif
