#ifndef __STATE_H__
#define __STATE_H__

#include "constants.h"

typedef struct state_st {
	/*	Atom coordinates	*/
	double (*r)[3];
	double (*rv)[3];
	double (*ra)[3];
	int *rType;
	unsigned long *rgid;
	unsigned int nMax;
	unsigned int nAtomCache;
	unsigned int nAtomOwn;
	unsigned long nAtomGlobal;

	//!Atom's SC cache mask
	unsigned int*	scCacheMask;
	
	//!atom position update flag (1 == update, 0 is not)
	double* rUpdateFlag;
	double* rdf;
	double kineticEnergy;
	double potentialEnergy[MAX_NBODY];
	
	//Corner Coordinate: Scaled global coordinate [0,1) at the lower corner of simulation box in each node.
	//i.e. Reference coordinate for each box.
	double cornerCoord[3];

	//in case each potential needs extra atom information.
	void*  extension;
} STATE;

void initState(STATE*,int nAtom);
void resetState(STATE* s);
void freeState(STATE *s);

#endif
