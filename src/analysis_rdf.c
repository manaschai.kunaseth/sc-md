#include "state.h"
#include "params.h"
#include "analysis_collection.h"
#include "utils.h"
#include "timing.h"
#include "memory.h"
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

void analysis_rdf_fill(STATE* s, PARAMS *p,double *sendbuffer, int recordLength,int recordOffset) {

	//convert own atoms' coordinate and pack into buffer
	double (*r)[3]  = s->r;
	int nAtomOwn = s->nAtomOwn;

	double scaleFactor[3];
	double scaleFactorInv[3];
	double refCoord[3];

	for (int a = 0;a < 3;a++){
		scaleFactor[a]    = p->hMatrix[a][a];
		scaleFactorInv[a] = 1.0/scaleFactor[a];
	}

	for (int a = 0;a < 3;a++){
		//p->boxSize[a] *= scaleFactorInv[a];
		refCoord[a] = s->cornerCoord[a]*scaleFactor[a];
	}

	//Convert all position to global coordinate.

	for (int i = 0; i < nAtomOwn;i++) {
		for (int a = 0;a < 3;a++) {
			sendbuffer[recordLength*i+recordOffset+a] = (r[i][a]+refCoord[a]);
		}
	}
}

void analysis_rdf_compute(STATE* s, PARAMS *p,ANALYSIS_STORE* ast,double *collVec, int collRowLength,int recordOffset) {
	


}


void analysis_rdf_writeOutput(ANALYSIS_STORE* ast) {


}


