
#ifndef __CUDAUTILS_H__
#define __CUDAUTILS_H__ 

void cudaSafeCall(cudaError_t error,const char* file, int line);
void check_kernelError(const char* file,int line);

#define CUDA_SAFE_CALL(ans) { cudaSafeCall(ans,__FILE__,__LINE__);}
#define CHECK_KERNEL_ERROR() { check_kernelError(__FILE__,__LINE__);}

#endif
