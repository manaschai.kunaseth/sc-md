#include "params.h"
#include "constants.h"

static PARAMS *s_p;

void initParams(PARAMS *p)
{
	//setup default value
	//critical value will set such that it would failed if not set by user.
	p->mdMode = -1;
	p->stepLimit = -1;
	p->evalFreq = 100;
	//init->deltaT = 1.0*1e-15/TIMEU;
	p->deltaT = -1.0*1e-15/TIMEU;
	p->rCacheRadius = -1.0;

	for (int i = 0;i < 3;i++)
		p->boxSize[i] = 0.0;

	for (int i = 0;i < 3;i++)
		for (int j = 0;j < 3;j++)
			p->hMatrix[i][j] = 0.0;

	p->nAtomType = 0;
	
	for (int i = 0;i < ATOM_TYPE_MAX;i++)
		p->atomMass[i] = 0.0;

	p->nBodyFlag = 0;
	p->isSelectiveCoordUpdate = 0;
	s_p = p;
}

PARAMS *getParamsObj()
{
	return s_p;
}

void paramsSetSeed(double seed)
{
	s_p->randomSeedNode = seed;

}

double paramsGetSeed()
{
	return s_p->randomSeedNode;
	//*seed = (double)s_p->randomSeedNode;
}

void paramsGetMass(double* aMass)
{
	for (unsigned int i = 0;i < s_p->nAtomType;i++)
		aMass[i] = s_p->atomMass[i];
}

int paramsGetNType(){
	return s_p->nAtomType;
}

double paramsGetDeltaT(){
	return s_p->deltaT;
}

double paramsGetCacheRadius(){
	return s_p->rCacheRadius;
}

void paramsSetCacheRadius(double r){
	s_p->rCacheRadius = r;	

}

void paramsSetNBodyFlag(int flag){
	s_p->nBodyFlag = flag;
}

int paramsGetNBodyFlag(){
	return s_p->nBodyFlag;
}

double paramsGetRCut(int nBody){
	return s_p->potential.rcut[nBody-1];
}


void paramsSetRCut(int nBody,double rcut){
	s_p->potential.rcut[nBody-1] = rcut;
}

void paramsSetPotentialType(POTENTIAL_TYPES potType){
	s_p->potentialType = potType;
}

POTENTIAL_TYPES paramsGetPotentialType(){
	return s_p->potentialType;
}

COMP_METHODS paramsGetComputationMethod(){
	return s_p->computationMethod;
}
