#ifndef __PARAMS_H__
#define __PARAMS_H__


#include "constants.h"
#include "input.h"
#include "potential.h"
#include "nodeComm.h"
#include "computePattern.h"
#include <string.h>

typedef struct params_st {
	POTENTIAL			potential;
	COMM_INFO			commInfo;
	COMP_METHODS    		computationMethod;
	//CELL_INFO			cellInfo;
	//COMPUTE_PATTERN   		computePattern;
	INPUT_PARAMS			input;

	int 				mdMode;
	unsigned long			startStep;
	unsigned long			stepLimit;
	int				evalFreq;
	double				deltaT;
   	//DOUBLE3				boxSize;
   	double				boxSize[3];
	double				hMatrix[3][3];

	int				randomSeedNode;

	unsigned int			nAtomType;

	//double*			atomMass;
	double				atomMass[ATOM_TYPE_MAX];
	
	//int* 				atomTypeId;
	//char				(*atomTypeName)[ATOM_TYPE_NAME_MAX];
	//char				(*atomTypeSymbol)[ATOM_TYPE_SYMBOL_MAX];
	char				atomTypeTable[ATOM_TYPE_MAX][ATOM_TYPE_SYMBOL_MAX];	

	int				nBodyFlag;
	int				isSelectiveCoordUpdate;
	int				aggregateMTS_read;

	double				rCacheRadius;
	int				cell_nlayer[MAX_NBODY];
	POTENTIAL_TYPES			potentialType;
}	PARAMS;

void initParams(PARAMS *p);
PARAMS* getParamsObj();
void paramsSetSeed(double seed);
double paramsGetSeed();
void paramsGetMass(double* am);
int    paramsGetNType();
double paramsGetDeltaT();
void paramsSetCacheRadius(double r);
double paramsGetCacheRadius();

void paramsSetNBodyFlag(int flag);
int paramsGetNBodyFlag();

double paramsGetRCut(int nBody);
void paramsSetRCut(int nBody,double rcut);


void paramsSetPotentialType(POTENTIAL_TYPES potType);
POTENTIAL_TYPES paramsGetPotentialType();
COMP_METHODS paramsGetComputationMethod();


#endif	
