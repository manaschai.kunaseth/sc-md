#include "constants.h"
#include "neighborList.h"
#include "cell.h"
#include "memory.h"
#include <stdlib.h>



NEIGHBOR_LIST* createNeighborList(NEIGHBOR_LIST* nbl, const int nAtom, const int nbMax) {
	nbl = (NEIGHBOR_LIST*) scMalloc(sizeof(NEIGHBOR_LIST));

	nbl->nbData = (int**)scMalloc(sizeof(int*)*nAtom);
	nbl->nNeighbor = (int*)scMalloc(sizeof(int)*nAtom);
	int *nblArray = (int*)scMalloc(sizeof(int)*nbMax*nAtom);
	for (int i = 0;i < nAtom;i++) {
		//nbl->nbData[i] = (int*)scMalloc(sizeof(int)*nbMax);
		nbl->nbData[i] = &nblArray[i*nbMax];
		nbl->nNeighbor[i] = 0;
	}

	nbl->nAtom = nAtom;
	nbl->nbMax = nbMax;
 
	return nbl;	
}


NEIGHBOR_LIST* freeNeighborList(NEIGHBOR_LIST* nbl) {

	//for (int i = 0;i < nbl->nAtom;i++) 
	//	free(nbl->nbData[i]);
	
	scFree(nbl->nNeighbor);
	scFree(nbl->nbData[0]);
	scFree(nbl->nbData);

	scFree(nbl);

	return nbl;
}

