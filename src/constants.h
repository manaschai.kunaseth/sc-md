#ifndef __CONSTANTS_H__
#define __CONSTANTS_H__

/*-----------------------------------------------------------------------
 UNIT CONVERSION FACTORS
-----------------------------------------------------------------------*/
#define BOHR 0.5291772                  // Bohr radius in angstrom
#define BOHR_INV 1.0/BOHR                  // Bohr radius in angstrom
#define ENGAU 4.35981534e-18    // Atomic unit energy in J
#define EAUEV 27.2116                   // Atomic unit energy in eV
#define AMU 1822.9                              // Atomic mass unit
#define TEMPAU 3.15785e5                // Atomic unit Temperature
#define TWOPI  6.28318530717959
#define TIMEU 2.418884326505e-17        //Atomic unit Time


#define ERRORCODE_SUCCESS 0
#define ERRORCODE_WARNING 1
#define ERRORCODE_FATAL   2

#define MAX_NBODY 10 //Maximum of 10-body potential

#define ATOM_TYPE_NAME_MAX 80
#define ATOM_TYPE_SYMBOL_MAX 10
#define ATOM_TYPE_MAX 10


#define UNIT_CELL_ATOM_MAX 2048


#define PATH_LENGTH 200
#define POTENTIAL_NAME_MAX 30

typedef enum {
	POT_UNKNOWN = 0,
	POT_LJ,
	POT_SIO2

} POTENTIAL_TYPES;

typedef enum {
	COMP_DEFAULT = 0,
	COMP_SC,
	COMP_SCNBL
} COMP_METHODS;

typedef enum {
	WRITE_MTS_ASCII = 1,
	WRITE_MTS_BINARY,
	WRITE_XYZ_LOCAL,	
	WRITE_XYZ_GLOBAL,
	WRITE_DUMMY	
} WRITE_MODES; 

typedef enum {
	NBODY_FLAG_2B = 2,
	NBODY_FLAG_3B = 4,
	NBODY_FLAG_4B = 8
} NBODY_FLAGS; 

typedef enum {
	INDEX_SET = 0,
	INDEX_2B = 1,
	INDEX_3B = 2,
	INDEX_4B = 3,
	INDEX_5B = 4,
	INDEX_6B = 5,
	INDEX_7B = 6,
	INDEX_8B = 7,
	INDEX_9B = 8,
	INDEX_10B = 9
} NBODY_INDICES; 

typedef enum {
	SC_CACHE_MASK_NONE = 0,
	SC_CACHE_MASK_X = 1,
	SC_CACHE_MASK_Y = 2,
	SC_CACHE_MASK_Z = 4,

	SC_CACHE_MASK_XY = 3, 
	SC_CACHE_MASK_YZ = 6,
	SC_CACHE_MASK_XZ = 5,

	SC_CACHE_MASK_XYZ = 7
} SC_CACHE_MASKS; 

typedef enum {
	ANALYSIS_COLLECTION_RDF,
	ANALYSIS_COLLECTION_VACF,
	ANALYSIS_COLLECTION_MSD,
	ANALYSIS_COLLECTION_MAX
} ANALYSIS_COLLECTIONS;

#endif
