#include <stdio.h>
#include "input.h"
#include "params.h"
#include "state.h"
#include "utils.h"
#include "atomType.h"
#include "analysis.h"
#include "simulate.h"
#include "mpi.h"
#include "memory.h"
#include "timing.h"

static void createAtomConfig(PARAMS* p, STATE* s);
static void readAtomConfig(PARAMS* p, STATE* s);
static void unpackAtomReadMTSBuffer(double* atomDataBuffer,PARAMS *p,STATE *s,int extensionType);

#define MTS_HEADER_COUNT 1+9+6
#define MTS_DOUBLE_PER_ATOM 8
#define MPI_TAG_AGGREGATE_READ_MTS 1101
#define MPI_TAG_AGGREGATE_READ_MTS_SIZE 1102

void initAtomConf(PARAMS* p, STATE* s)
{
	INPUT_PARAMS* inp = getInputObj();
	if (inp->mdMode == 1)
	{
		reportPrint("[MD_MODE=1]: Read atom configuration.");
		timerStart(TIMING_READ_ATOM_CONFIG);
		readAtomConfig(p,s);
		timerStop(TIMING_READ_ATOM_CONFIG);
		reportPrint("[MD_MODE=1]: Read atom configuration completed.");
	} 
	else if (inp->mdMode == 0)
	{
		reportPrint("[MD_MODE=0]: Create new atom configuration.");
		timerStart(TIMING_READ_ATOM_CONFIG);
		createAtomConfig(p,s);
		timerStop(TIMING_READ_ATOM_CONFIG);
		reportPrint("[MD_MODE=0]: Create atom configuration completed.");
	}
	else
	{
		terminateSingle("invalid MD mode.",1);
	}

	reportPrintDoubleArr3("H-Matrix (a.u.): ", p->hMatrix[0]);
	reportPrintDoubleArr3("                 ", p->hMatrix[1]);
	reportPrintDoubleArr3("                 ", p->hMatrix[2]);
	
	int nNodes;
 	long nAtomOwn_tmp = s->nAtomOwn;
  	MPI_Allreduce(&nAtomOwn_tmp, &s->nAtomGlobal, 1, MPI_LONG, MPI_SUM, MPI_COMM_WORLD);
	MPI_Comm_size(MPI_COMM_WORLD, &nNodes);

	reportPrintFixedDouble("Average number of atoms per nodes : ", ((double)s->nAtomGlobal)/nNodes);
	reportPrintInt("Total number of atoms in the system : ", s->nAtomGlobal);
	
	//Estimate size fraction of that need caching.


}

void createAtomConfig(PARAMS* p, STATE* s)
{
	INPUT_PARAMS* inp = getInputObj();
		
	//Check if current.in exists. This means checkpoint file exists and creating new configure may override existing checkpoint files.
	//Therefore, if "current.in" is existed, user must delete it first before continue.
	if (getMyId() == 0) {  
		FILE* fp;
		fp = fopen("current.in", "r");
		if (fp) //file exists
			terminateSingle("MD_MODE=0 but 'current.in' exists--Existing checkpoint may be overwritten. Delete 'current.in' before continue.",1);
		if (fp) 
			fclose(fp);
	}
	reportPrintInt("Number of atom in unit cell = ",inp->unitCellAtomCount);
	if (inp->unitCellAtomCount == 0)
		terminateSingle("No initial atom configuration found.",1);	
	reportVerbose("Atom coordinate in unit cell:");
	for (int j = 0; j < inp->unitCellAtomCount; j++) 
		reportVerboseDoubleArr3(inp->unitCellAtomSymbol[j], inp->unitCellAtomCoord[j]);	

	//Init boxSize and H-MATRIX
	for (int a = 0; a < 3; a++) {
		p->boxSize[a] = inp->initUCell[a] * inp->latticeConst[a]/BOHR;
		p->hMatrix[a][a] = p->boxSize[a] * inp->vProc[a];
	}
	
	
	reportPrintIntArr3   ("Unit cells per node in X, Y, Z     :",inp->initUCell);	
	reportPrintDoubleArr3("Lattice constant in X, Y, Z [Ang]  :",inp->latticeConst);	
	reportPrintDouble3   ("Box size of each node [Ang]        :",p->boxSize[0]*BOHR,p->boxSize[1]*BOHR, p->boxSize[2]*BOHR);	
	
	//Determine scaled global coordinate at the lower corner of simulation box in each node.
	//i.e. Reference coordinate for each box.
	
	for (int a = 0; a < 3;a++){
		s->cornerCoord[a] = (p->commInfo.myVid[a] * p->boxSize[a])/p->hMatrix[a][a]; 
	}
	#if (VERBOSE > 1)
		printf("Corner Coord[myId=%7d]: %10.7f %10.7f %10.7f\n",getMyId(),s->cornerCoord[0],s->cornerCoord[1],s->cornerCoord[2]);
	#endif

	//allocate memory for state data
	int nTotalAtom = inp->initUCell[0]*inp->initUCell[1]*inp->initUCell[2]*inp->unitCellAtomCount;
	initState(s,nTotalAtom);
	//reportPrintInt("Allocate atom memory (KB): ",s->nMax);
	
	/* Set initial configration */
	double (*r)[3] = s->r;
	int myId = p->commInfo.myId;
	int atomCount = 0;
	for (int nX = 0; nX < inp->initUCell[0]; nX++) {
		for (int nY = 0; nY < inp->initUCell[1]; nY++) {
			for (int nZ = 0; nZ < inp->initUCell[2]; nZ++) {
				for (int j = 0; j < inp->unitCellAtomCount; j++) {
						r[atomCount][0] = (nX + inp->unitCellAtomCoord[j][0]) *(inp->latticeConst[0]/BOHR);
						r[atomCount][1] = (nY + inp->unitCellAtomCoord[j][1]) *(inp->latticeConst[1]/BOHR);
						r[atomCount][2] = (nZ + inp->unitCellAtomCoord[j][2]) *(inp->latticeConst[2]/BOHR);
					s->rType[atomCount] = typeFromSymbol(inp->unitCellAtomSymbol[j]);
					s->rgid[atomCount] = myId*nTotalAtom+atomCount;
					s->rUpdateFlag[atomCount] = 1.0;
					atomCount++;
				}
			}
		}
	}
	
	//store number of atom.	
	s->nAtomOwn = atomCount;

	//start from step 0	
	simulateSetCurrentStep(0);
	p->startStep = 0;

	//initialized temperature
	analysis_velocityScaling(s,inp->initTemp / TEMPAU);	
}


void readAtomConfig(PARAMS* p, STATE* s)
{
	int myId = getMyId();
	int currentStep;
	int asciiFormat = 1;
	int extensionType = 0;
	int aggregateMTS = 1;
	FILE *fp;
	int buffer[4];
	int readStatus;
	//If root node, read current.in
	if (myId == 0) {  
		char mtsFormat[20];
		
		fp = fopen("current.in", "r");
		if (!fp)
			terminateSingle("Invalid current.in file.",1);
		fscanf(fp, "%d %s",&currentStep,mtsFormat);
		if (strcmp(mtsFormat,"ASCII") == 0)
			asciiFormat = 1;
		else if (strcmp(mtsFormat,"BINARY") == 0)
			asciiFormat = 0;
		else
			terminateSingle("Invalid current.in file.",1);
		fscanf(fp, "%d",&extensionType);
		readStatus = fscanf(fp, "%d",&aggregateMTS);
		if (readStatus == EOF) //No aggregateMTS written. This is possible due to an old MTS version. Use aggregateMTS = 1 if EOF.
			aggregateMTS = 1;
		fclose(fp);
		buffer[0] = currentStep;
		buffer[1] = asciiFormat;
		buffer[2] = extensionType;
		buffer[3] = aggregateMTS;
	}	

	//broadcast current step and mts format to all nodes.
	MPI_Bcast(buffer,4,MPI_INT,0,MPI_COMM_WORLD); 

	currentStep = buffer[0];
	asciiFormat = buffer[1];
	extensionType = buffer[2];
	aggregateMTS = buffer[3];

	
	p->aggregateMTS_read = aggregateMTS;
	simulateSetCurrentStep(currentStep);
	p->startStep = currentStep;

	if (extensionType == 1) {
		p->isSelectiveCoordUpdate = 1;
	}

	//read ASCII MTS format
	if (asciiFormat == 1) {
		//start reading file.
		char fullFname[100];
		int  nAtomTotal;

		if (p->aggregateMTS_read == 1) {
			sprintf(fullFname, "snapshot.%011d/%07d.mts", currentStep,myId);
			fp = fopen(fullFname, "r");
			if (!fp)
				terminateNode("Cannot open MTS file.",1);

			//read mts header
			fscanf(fp, "%d", &nAtomTotal);
			initState(s,nAtomTotal);
			s->nAtomOwn = nAtomTotal;

			double (*r)[3]  = s->r;
			double (*rv)[3] = s->rv;
			int     *rType  = s->rType;
			unsigned long *rgid  = s->rgid;

			fscanf(fp, "%le %le %le", &p->hMatrix[0][0], &p->hMatrix[0][1], &p->hMatrix[0][2]);
			fscanf(fp, "%le %le %le", &p->hMatrix[1][0], &p->hMatrix[1][1], &p->hMatrix[1][2]);
			fscanf(fp, "%le %le %le", &p->hMatrix[2][0], &p->hMatrix[2][1], &p->hMatrix[2][2]);
			fscanf(fp, "%le %le %le", &s->cornerCoord[0],&s->cornerCoord[1],&s->cornerCoord[2]);
			fscanf(fp, "%le %le %le", &p->boxSize[0],&p->boxSize[1],&p->boxSize[2]);

			double globCoord[3];
			double corner[3];
			char atomTypeStr[20];
			for (int a = 0;a < 3;a++)
				corner[a] = s->cornerCoord[a]*p->hMatrix[a][a];
				
			//read atom information
			for (int i = 0;i < nAtomTotal;i++) {
				fscanf(fp, "%s %ld %le %le %le %le %le %le",
						atomTypeStr, &rgid[i],
						&globCoord[0],&globCoord[1],&globCoord[2],
						&rv[i][0],&rv[i][1],&rv[i][2]);
				//convert coordinate to global
				for (int a = 0;a < 3;a++)
					r[i][a] = (globCoord[a]*p->hMatrix[a][a])-corner[a]; 
				rType[i] = typeFromSymbol(atomTypeStr);
			}
			//if extension type > 0 read updateFlags
			if (extensionType == 1) {
				for (int i = 0;i < nAtomTotal;i++) {
					fscanf(fp, "%le", &s->rUpdateFlag[i]);
				} //end for
			} else if (extensionType == 0) {
				for (int i = 0;i < nAtomTotal;i++) {
					s->rUpdateFlag[i] = 1.0;
				} //end for
			} else if (extensionType != 0) {
				terminateSingle("Wrong extension type.",1);
			} 
			fclose(fp);
		}
		else if (p->aggregateMTS_read > 1) { //AggregateMTS read
			int aggregateId = getMyId() % p->aggregateMTS_read;
			int masterId = getMyId() - aggregateId;
			if (aggregateId == 0) { //master node
				sprintf(fullFname, "snapshot.%011d/%07d.mts", currentStep,myId);
				fp = fopen(fullFname, "r");
				if (!fp)
					terminateNode("Cannot open MTS file.",1);

				for (int iNode = 0; iNode < p->aggregateMTS_read;iNode++) {	
					//read mts header
					fscanf(fp, "%d", &nAtomTotal);

					double *readAtomConfigBuffer = (double*)scMalloc(sizeof(double)*(nAtomTotal*MTS_DOUBLE_PER_ATOM+MTS_HEADER_COUNT));
					int offset = 0;
					//read header
					readAtomConfigBuffer[offset++] = nAtomTotal;
					for (int i = 0;i < MTS_HEADER_COUNT-1;i++)
						fscanf(fp,"%le",&readAtomConfigBuffer[offset++]);

					double globCoord[3];
					double corner[3];
					char atomTypeStr[20];
					unsigned long gid;	
					double rv_tmp[3];

					//read atom information
					for (int i = 0;i < nAtomTotal;i++) {
						fscanf(fp, "%s %ld %le %le %le %le %le %le",
								atomTypeStr, &gid,
								&globCoord[0],&globCoord[1],&globCoord[2],
								&rv_tmp[0],&rv_tmp[1],&rv_tmp[2]);
				
						int rType = typeFromSymbol(atomTypeStr);
						double atomdata = dataFromId(rType,gid);
						
						readAtomConfigBuffer[offset++] = atomdata;
						for (int a = 0;a < 3;a++) {
							readAtomConfigBuffer[offset  +a] = globCoord[a];
							readAtomConfigBuffer[offset+3+a] = rv_tmp[a];
						}
						offset+=6;
					}

					//if extension type > 0 read updateFlags
					if (extensionType == 1) {
						for (int i = 0;i < nAtomTotal;i++) {
							fscanf(fp, "%le", &readAtomConfigBuffer[offset]);
							offset++;
						} //end for
					}

					if (iNode == 0) {
						//printf("master: inode = %d, MPI_task = %5d, aggregateId = %5d, masterId = %5d\n",iNode,getMyId(),aggregateId,masterId);
						unpackAtomReadMTSBuffer(readAtomConfigBuffer,p,s,extensionType);
					} else {
						//printf("master: inode = %d, MPI_task = %5d, aggregateId = %5d, masterId = %5d\n",iNode,getMyId(),aggregateId,masterId);
						MPI_Send(&nAtomTotal,1,MPI_INT,getMyId()+iNode,MPI_TAG_AGGREGATE_READ_MTS_SIZE,MPI_COMM_WORLD); 
						MPI_Send(readAtomConfigBuffer,nAtomTotal*MTS_DOUBLE_PER_ATOM+MTS_HEADER_COUNT,
								MPI_DOUBLE,getMyId()+iNode,MPI_TAG_AGGREGATE_READ_MTS,MPI_COMM_WORLD);
					}		
					scFree(readAtomConfigBuffer);
				} //end iNode
 
				fclose(fp);
			} //end master
			else { //for slave IO task
				int nAtomTotal;
				MPI_Status status;
				//printf("slave: MPI_task = %5d, aggregateId = %5d, masterId = %5d\n",getMyId(),aggregateId,masterId);
				MPI_Recv(&nAtomTotal, 1, MPI_INT, masterId, MPI_TAG_AGGREGATE_READ_MTS_SIZE,MPI_COMM_WORLD,&status);
				double *readAtomConfigBuffer = (double*)scMalloc(sizeof(double)*(nAtomTotal*MTS_DOUBLE_PER_ATOM+MTS_HEADER_COUNT));
				MPI_Recv(readAtomConfigBuffer, nAtomTotal*MTS_DOUBLE_PER_ATOM+MTS_HEADER_COUNT, 
					MPI_DOUBLE, masterId, MPI_TAG_AGGREGATE_READ_MTS,MPI_COMM_WORLD,&status);

				unpackAtomReadMTSBuffer(readAtomConfigBuffer,p,s,extensionType);
				scFree(readAtomConfigBuffer);
			} //end slave IO task			
		}// end p->aggregateMTS_read > 1


		#if (VERBOSE > 1)
			printf("Corner Coord[myId=%7d]: %10.7f %10.7f %10.7f\n",getMyId(),s->cornerCoord[0],s->cornerCoord[1],s->cornerCoord[2]);
		#endif

	} //END read ASCII mts
	else if (asciiFormat == 0) {
		terminateSingle("Reading BINARY mts is not implemented yet.",1);
	} //END read BINARY mts 
	else
		terminateSingle("Invalid MTS format in current.in.",1);
	
}//End read atom config.


void unpackAtomReadMTSBuffer(double* atomDataBuffer,PARAMS *p,STATE *s,int extensionType) {

	int offset = 0;

	//unpack header
	int nAtomOwn = (int)atomDataBuffer[offset++];
	initState(s,nAtomOwn);
	s->nAtomOwn = nAtomOwn;
	
	double (*r)[3]  = s->r;
	double (*rv)[3] = s->rv;
	int     *rType  = s->rType;
	unsigned long *rgid  = s->rgid;

	for (int i = 0;i < 3;i++)
	for (int j = 0;j < 3;j++)
		p->hMatrix[i][j] = atomDataBuffer[offset++];

	for (int a = 0;a < 3;a++)
		s->cornerCoord[a] = atomDataBuffer[offset++];	

	for (int a = 0;a < 3;a++)
		p->boxSize[a] = atomDataBuffer[offset++];	


	double globCoord[3];
	double corner[3];
	char atomTypeStr[20];

	for (int a = 0;a < 3;a++)
		corner[a] = s->cornerCoord[a]*p->hMatrix[a][a];
		
	//unpack atom information
	for (int i = 0;i < nAtomOwn;i++) {
		
		double atomdata = atomDataBuffer[offset++];

		for (int a = 0;a < 3;a++)
			globCoord[a] = atomDataBuffer[offset++];

		for (int a = 0;a < 3;a++)
			rv[i][a] = atomDataBuffer[offset++];

		//convert coordinate to global
		for (int a = 0;a < 3;a++)
			r[i][a] = (globCoord[a]*p->hMatrix[a][a])-corner[a]; 

		rType[i] = typeFromData(atomdata);
		rgid[i] = idFromData(atomdata);
	} //end unpack atom 

	//if extension type > 0 read updateFlags
	if (extensionType == 1) {
		for (int i = 0;i < nAtomOwn;i++) {
			s->rUpdateFlag[i] = atomDataBuffer[offset++];
		} //end for
	} else if (extensionType == 0) {
		for (int i = 0;i < nAtomOwn;i++) {
			s->rUpdateFlag[i] = 1.0;
		} //end for
	} else if (extensionType != 0) {
		terminateSingle("Wrong extension type.",1);
	} 
}
