#include "nodeComm.h"
#include "state.h"
#include "mpi.h"
#include "utils.h"
#include "timing.h"
#include "input.h"
#include "memory.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define MOVED_OUT -1.0e10
 
#define MPI_TAG_ATOM_COPY 100
#define MPI_TAG_ATOM_MOVE 200
#define MPI_TAG_FORCE_RETURN 300

#define BASE_BUFFER_SIZE 5000

static int isAtomCacheNeighbor(double *ri, int ku, double rCache, double* boxSize);
static int isAtomMoveNeighbor(double *ri, int ku, double* boxSize);

void initCommunication(COMM_INFO* commInfo)
{
	int nNodes;
	int myId; 

	INPUT_PARAMS* inp = getInputObj();

	MPI_Comm_rank(MPI_COMM_WORLD, &myId);	
	MPI_Comm_size(MPI_COMM_WORLD, &nNodes);
	commInfo->myId = myId;
	commInfo->nNodes = nNodes;
	if (inp->parallelMode == PARMODE_MPI)
		reportPrint("Parallelization mode: MPI-only");
	else if (inp->parallelMode == PARMODE_MPI_OMP)
		reportPrint("Parallelization mode: Hybrid MPI/OpenMP");
	else if (inp->parallelMode == PARMODE_MPI_OMP_GPU)
		reportPrint("Parallelization mode: Hybrid MPI/OpenMP/GPU");
	else if (inp->parallelMode == PARMODE_MPI_OMP_MIC)
		reportPrint("Parallelization mode: Hybrid MPI/OpenMP/MIC");
	else
		reportPrint("Parallelization mode: Not recognized.");

	
	if (nNodes != (inp->vProc[0]*inp->vProc[1]*inp->vProc[2]))
	{
		terminate("Number of processors requested different from config file.");
	}	 
	reportPrintInt("Number of MPI tasks = ",nNodes); 
	reportPrintIntArr3("(NX, NY, NZ): ", inp->vProc);
	for (int i = 0;i < 3;i++)
		commInfo->vProc[i] = inp->vProc[i];

	//Printout number of threads if applicable
	if (inp->parallelMode == PARMODE_MPI_OMP)
	{
		reportPrintInt("Number of computing threads per node: ",inp->nThreads[0]);
		char msg[1000];
		for (int i = 1;i < MAX_NBODY;i++){
			if (inp->nThreads[i] > 0) {
				sprintf(msg,"#Thread for %d-body: ",i);	
				reportPrintInt(msg,inp->nThreads[i]);
			}
		}
	}
	if (inp->parallelMode == PARMODE_MPI_OMP_GPU) 
	{
		reportPrintInt("Number of computing threads per node: ",inp->nThreads[0]);
		reportPrintInt("Number of computing GPUs per node: ",inp->nGpus[0]);
		char msg[1000];
		for (int i = 1;i < MAX_NBODY;i++){
			if (inp->nGpus[i] > 0) {
				sprintf(msg,"#GPU for %d-body: ",i);	
				reportPrintInt(msg,inp->nGpus[i]);
				if (inp->nThreads[i] > 0) {
					sprintf(msg,"Error: non-zero nGPUs and nThreads for %d-body. Only either GPU or CPU calculation mode can handle %d-body calculation at a time.",i,i);	
					terminateSingle(msg,1);
				}
			}
			else if (inp->nThreads[i] > 0) {
				sprintf(msg,"#Thread for %d-body: ",i);	
				reportPrintInt(msg,inp->nThreads[i]);
			}
				
		}
	}

	if (inp->parallelMode == PARMODE_MPI_OMP_MIC) 
	{
		reportPrintInt("Number of computing threads per node: ",inp->nThreads[0]);
		reportPrintInt("Number of computing MICs per node: ",inp->nMics);
		reportPrintInt("Number of computing threads per each MIC unit: ",inp->nThreads_mic[0]);
		char msg[1000];
		for (int i = 1;i < MAX_NBODY;i++){
			if (inp->nThreads_mic[i] > 0) {
				sprintf(msg,"#MIC threads for %d-body: ",i);	
				reportPrintInt(msg,inp->nThreads_mic[i]);
				if (inp->nThreads[i] > 0) {
					sprintf(msg,"Error: non-zero nThreads_mic and nThreads for %d-body. Only either MIC or CPU calculation mode can handle %d-body calculation at a time.",i,i);	
					terminateSingle(msg,1);
				}
			}
			else if (inp->nThreads[i] > 0) {
				sprintf(msg,"#Thread for %d-body: ",i);	
				reportPrintInt(msg,inp->nThreads[i]);
			}
				
		}
	}
	//Init vector index of this processor
	commInfo->myVid[0] = myId/(commInfo->vProc[1] * commInfo->vProc[2]);
	commInfo->myVid[1] = (myId / commInfo->vProc[2]) % commInfo->vProc[1];
	commInfo->myVid[2] = myId % commInfo->vProc[2];

	//set import radius
	commInfo->rCacheRadius = paramsGetCacheRadius();	

	//set default copy/move size per atom
	commInfo->nCopySizePerAtom = 4;
	commInfo->extraCopyDataType = 0;

	if (inp->analyzeFixBoundary > 0) {
		commInfo->nMoveSizePerAtom = 8;
		commInfo->extraMoveDataType = 1;
	} else {
		commInfo->nMoveSizePerAtom = 7;
		commInfo->extraMoveDataType = 0;
	}

}



void atomCopy(STATE* s,ATOM_CACHE_DATA* ac,COMM_INFO* commInfo, double* boxSize)
{
/*----------------------------------------------------------------------
 Exchanges boundary-atom coordinates among neighbor nodes:  Makes 
 boundary-atom list, LSB, then sends & receives boundary atoms.
 ----------------------------------------------------------------------*/

	int **cacheList = ac->cacheList;
	int *dirCacheSend = ac->directionalCacheSendN;
	double (*sv)[3] = commInfo->shiftVector;
	double (*r)[3] = s->r;
	int *rType = s->rType;

	double fact = 13.0;
	
	unsigned long *rgid = s->rgid;
	unsigned int nAtomOwn = s->nAtomOwn;
	double rCache = commInfo->rCacheRadius;
	int copySizePerAtom = commInfo->nCopySizePerAtom;
	int kd, kdd, i, p, ku, inode, inoder, nSend, nReceived, a, newImi;
	int nbnew = 0;				/* # of "received" boundary atoms */
	double *bufferSend,*bufferReceived;
	MPI_Status status;
	forceTimingRelation(TIMING_ATOM_COPY,TIMING_MD_STEP);
	timerStart(TIMING_ATOM_COPY);

	assert(rCache > 0.0);

	bufferSend     = ((double*)scMalloc(BASE_BUFFER_SIZE*sizeof(double)+sizeof(double)*s->nAtomOwn*fact)); 
	bufferReceived = ((double*)scMalloc(BASE_BUFFER_SIZE*sizeof(double)+sizeof(double)*s->nAtomOwn*fact)); 
	s->nAtomCache = 0;
	
 	/* Main loop over x, y & z directions starts-------------------------- */
  	//com1 = MPI_Wtime();
			/* To calculate the communication time */
  	for (kd = 0; kd < 3; kd++) {

	/* Make a boundary-atom list, LSB--------------------------------- */

	/* Reset the # of to-be-copied atoms for lower&higher directions */
	for (kdd = 0; kdd < 2; kdd++)
		dirCacheSend[2*kd+kdd] = 0;
		//lsb[2 * kd + kdd][0] = 0;
	/* Scan all the residents & copies to identify boundary atoms */
	p = s->nAtomOwn + nbnew;
	//p = n + nbnew;
	for (i = 0; i < p; i++) {

	  for (kdd = 0; kdd < 1; kdd++) {	// only "lower" direction (0) for cache (3-way communication)

		ku = 2 * kd + kdd;		/* Neighbor ID */

		/* Add an atom to the boundary-atom list, LSB, for neighbor ku 
		   according to bit-condition function, isAtomCacheNeighbor */
		if (isAtomCacheNeighbor(r[i], ku, rCache, boxSize)) {
			cacheList[ku][dirCacheSend[ku]] = i;
			dirCacheSend[ku]++;
		  	//lsb[ku][++(lsb[ku][0])] = i;
		}

	  }
	}

	/* Message passing------------------------------------------------ */

	/* Loop over "lower" directions (3-way communication) */
	for (kdd = 0; kdd < 1; kdd++) {	//kdd = 0
		//inode = nn[ku = 2 * kd + kdd];	/* Neighbor node ID */
		//inoder = nnr[ku];
		ku = 2 * kd + kdd;
	  	inode = commInfo->myNN[ku];	/* Neighbor node ID */
		inoder = commInfo->myNNr[ku];

	  	/* Send & receive the # of boundary atoms----------------------- */

		nSend = dirCacheSend[ku];			/* # of atoms to be sent */
		/* Message buffering */
		for (i = 0; i < nSend; i++) {
			int iSize = copySizePerAtom*i;
			int cacheListIdx = cacheList[ku][i];
			for (a = 0; a < 3; a++)	/* Shift the coordinate origin */
				bufferSend[iSize+a] = r[cacheListIdx][a] - sv[ku][a];
			//add atomdata  Type info
			bufferSend[iSize+3] = dataFromId(rType[cacheListIdx],rgid[cacheListIdx]);
		//bufferSend[4 * (i - 1) + 3] = atomdata[lsb[ku][i]];
		}
		if (commInfo->myParity[kd] < 2) {

			//MPI_Sendrecv (bufferSend, 4 * nSend, MPI_DOUBLE, inode, 10, bufferReceived, 4 * (NEMAX - NMAX), MPI_DOUBLE, inoder, 10, MPI_COMM_WORLD, &status);
			MPI_Sendrecv (bufferSend, copySizePerAtom * nSend, MPI_DOUBLE, inode, MPI_TAG_ATOM_COPY, bufferReceived, BASE_BUFFER_SIZE + 4 * nAtomOwn*fact, MPI_DOUBLE, inoder, MPI_TAG_ATOM_COPY, MPI_COMM_WORLD, &status);
			MPI_Get_count (&status, MPI_DOUBLE, &nReceived);
			nReceived = nReceived / copySizePerAtom;                  //4 double data per one atom (x,y,z,atomdata)
		}
	  /* Single layer: Exchange information with myself */
		else {
			for (i = 0; i < copySizePerAtom * nSend; i++)
				bufferReceived[i] = bufferSend[i];
			nReceived = nSend;
		}
			  /* Message storing */
			int n = s->nAtomOwn;
			for (i = 0; i < nReceived; i++) 
			{
				newImi = n + nbnew + i;

				for (a = 0; a < 3; a++)
					r[newImi][a] = bufferReceived[copySizePerAtom * i + a];
				rType[newImi] = typeFromData(bufferReceived[copySizePerAtom * i + 3]);
				rgid[newImi] = idFromData(bufferReceived[copySizePerAtom * i + 3]);
				//atomdata[newImi] = bufferReceived[4 * i + 3];
				//atomtype[newImi] = (int) atomdata[newImi];
			}
			/* Increment the # of received boundary atoms */
			nbnew = nbnew + nReceived;
			//lrc[ku] = nReceived;
			ac->directionalCacheReceiveN[ku] = nReceived;

		}                                                       /* Endfor lower & higher directions, kdd */

	}                                                             /* Endfor x, y & z directions, kd */


	//comt_copy += MPI_Wtime () - com1;     /* Update communication time, COMT */

	/* Main loop over x, y & z directions ends-------------------------- */

	/* Update the # of received boundary atoms */
	//nb = nbnew;
	s->nAtomCache = nbnew;

	scFree(bufferSend);
	scFree(bufferReceived);
	#if (VERBOSE > 1)
	reportVerboseInt3("Finish atom copy routine: Atom (Own, Cache, X) = ",s->nAtomOwn,s->nAtomCache,0);
	#endif
	timerStop(TIMING_ATOM_COPY);
}


void atomMove(STATE* s, COMM_INFO* commInfo, double* boxSize)
{
	/*----------------------------------------------------------------------
	 Sends moved-out atoms to neighbor nodes and receives moved-in atoms 
	 from neighbor nodes.  Called with n, r[0:n-1] & rv[0:n-1], atom_move 
	 returns a new n' together with r[0:n'-1] & rv[0:n'-1].
	 ----------------------------------------------------------------------*/
  
	/* Local variables------------------------------------------------------

	   moveQueue[6][NBMAX]: moveQueue[ku][0] is the # of to-be-moved atoms to neighbor 
	   ku; MVQUE[ku][k>0] is the atom ID, used in r, of the k-th atom to be
	   moved.
	   ---------------------------------------------------------------------- */
	//int moveQueue[6][NBMAX];
	int** moveQueue;
	int moveQueueSize[6];
	int nImmigrantAtom = 0;	/* # of new immigrants */
	int nSend, nReceived;
	//double com1;
	double fact = 13.0;

	unsigned int nAtomOwn = s->nAtomOwn;
  	//unsigned int nAtomCache = s->nAtomCache;
	double (*sv)[3] = commInfo->shiftVector;
  	double (*r)[3] = s->r;
  	double (*rv)[3] = s->rv;
	int *rType = s->rType;
	unsigned long *rgid = s->rgid;
	double *bufferSend,*bufferReceived;
	int moveSizePerAtom = commInfo->nMoveSizePerAtom;
	int extraDataType = commInfo->extraMoveDataType;
	MPI_Status status;
	timerStart(TIMING_ATOM_MOVE);

	//reportVerbose("Starting atom move routine...");
	//reportVerboseInt("Begin atom move routine: nAtomOwn:",s->nAtomOwn);

	bufferSend     = ((double*)scMalloc(BASE_BUFFER_SIZE*sizeof(double)+sizeof(double)*s->nAtomOwn*fact)); 
	bufferReceived = ((double*)scMalloc(BASE_BUFFER_SIZE*sizeof(double)+sizeof(double)*s->nAtomOwn*fact)); 


	moveQueue = (int**)(scMalloc(sizeof(int*)*6));
 	for (int i = 0;i < 6;i++) moveQueue[i] = (int*)(scMalloc(BASE_BUFFER_SIZE*sizeof(double)+sizeof(int)*nAtomOwn*fact));

  /* Reset the # of to-be-moved atoms, MVQUE[][0] */
  for (int ku = 0; ku < 6; ku++){
	//moveQueue[ku][0] = 0;
	moveQueueSize[ku] = 0;
  }

  /* Main loop over x, y & z directions starts------------------------ */
  //com1 = MPI_Wtime ();

  for (int kd = 0; kd < 3; kd++) {

	/* Make a moved-atom list, moveQueue---------------------------------- */

	/* Scan all the residents & immigrants to list moved-out atoms */
	for (int i = 0; i < nAtomOwn + nImmigrantAtom; i++) {
	  int kul = 2 * kd;				/* Neighbor ID */
	  int kuh = 2 * kd + 1;
	  /* Register a to-be-copied atom in moveQueue[kul|kuh][] */
	  if (r[i][0] > MOVED_OUT) {	/* Don't scan moved-out atoms */
		/* Move to the lower direction */
		if (isAtomMoveNeighbor (r[i], kul,boxSize))
		  moveQueue[kul][moveQueueSize[kul]++] = i;
		/* Move to the higher direction */
		else if (isAtomMoveNeighbor (r[i],kuh,boxSize))
		  moveQueue[kuh][moveQueueSize[kuh]++] = i;
	  }
	}

	/* Message passing with neighbor nodes---------------------------- */

	/* Loop over the lower & higher directions------------------------ */


	for (int kdd = 0; kdd < 2; kdd++) {
		int ku = 2 * kd + kdd;
		int inode = commInfo->myNN[ku];	/* Neighbor node ID */
		int inoder = commInfo->myNNr[ku];
	  /* Send atom-number information--------------------------------- */

	  	nSend = moveQueueSize[ku];		/* # of atoms to-be-sent */

	  /* Send & receive information on boundary atoms----------------- */

	  /* Message buffering */
	  //bufferSend[0] = (double)nSend;
	  for (int i = 0; i < nSend; i++) {
		int moveIdx = moveQueue[ku][i];
		for (int a = 0; a < 3; a++) {
		  /* Shift the coordinate origin */
		  bufferSend[(moveSizePerAtom * i) + a]     = r[moveIdx][a] - sv[ku][a]; //Shifted coordinate
		  bufferSend[(moveSizePerAtom * i) + 3 + a] = rv[moveIdx][a];	//Velocities

		  /*Debug
		  //reportVerboseDoubleArr3("Atom move Before shift:",r[moveIdx]);
		  //reportVerboseDouble3   ("Atom move After  shift:",r[moveIdx][0]-sv[ku][0],
		  //				    r[moveIdx][1]-sv[ku][1],
		  //				    r[moveIdx][2]-sv[ku][2]);
		    End Debug */
		}
		bufferSend[(moveSizePerAtom * i) + 6]     = dataFromId(rType[moveIdx], rgid[moveIdx]); 	//AtomData (merged type + id)
		if (extraDataType == 1)
			bufferSend[(moveSizePerAtom * i) + 7]     = s->rUpdateFlag[moveIdx]; 	//Atom update flag
		r[moveIdx][0] = MOVED_OUT;	/* Mark the moved-out atom */
	  }
	if (commInfo->myParity[kd] < 2) {
		MPI_Sendrecv (bufferSend, moveSizePerAtom * nSend, MPI_DOUBLE, inode, MPI_TAG_ATOM_MOVE, bufferReceived, BASE_BUFFER_SIZE*sizeof(double)+moveSizePerAtom * nAtomOwn*fact, MPI_DOUBLE, inoder, MPI_TAG_ATOM_MOVE, MPI_COMM_WORLD, &status);
		MPI_Get_count (&status, MPI_DOUBLE, &nReceived);
		nReceived = nReceived / moveSizePerAtom;			// 7 double data per atom (x,y,z,vx,vy,vz,atomdata)
	  }
	  /* Single layer: Exchange information with myself */
	  else {
	  	for (int i = 0; i < moveSizePerAtom * nSend; i++)
			bufferReceived[i] = bufferSend[i];
		nReceived = nSend;
	  }

	  /* Message storing */

	  for (int i = 0; i < nReceived; i++) {
		int newImi = nAtomOwn + nImmigrantAtom + i;
		for (int a = 0; a < 3; a++) {
			r[newImi][a]  = bufferReceived[moveSizePerAtom * i + a];
			rv[newImi][a] = bufferReceived[moveSizePerAtom * i + a + 3];
		}
		rType[newImi] = typeFromData(bufferReceived[moveSizePerAtom * i + 6]);
		rgid[newImi]  = idFromData(bufferReceived[moveSizePerAtom * i + 6]);
		if (extraDataType == 1)
			s->rUpdateFlag[newImi] = bufferReceived[moveSizePerAtom * i + 7];
	  }

	  /* Increment the # of new immigrants */
	  nImmigrantAtom = nImmigrantAtom + nReceived;

	}							/* Endfor lower & higher directions, kdd */

  }								/* Endfor x, y & z directions, kd */

  /* Main loop over x, y & z directions ends-------------------------- */

  /* Compress resident arrays including new immigrants */

  int nAtomCount = 0;
  int countMoveOut = 0;
  for (int i = 0; i < nAtomOwn + nImmigrantAtom; i++) {
	if (r[i][0] > MOVED_OUT) {
	  for (int a = 0; a < 3; a++) {
		r[nAtomCount][a] = r[i][a];
		rv[nAtomCount][a] = rv[i][a];
	  }
	  rType[nAtomCount] = rType[i];
	  rgid[nAtomCount] = rgid[i];
	if (extraDataType == 1)
		s->rUpdateFlag[nAtomCount] = s->rUpdateFlag[i];
	  ++nAtomCount;
	} else countMoveOut++;
  }
  //comt_mov += MPI_Wtime () - com1;
  /* Update the compressed # of resident atoms */
  s->nAtomOwn = nAtomCount;
  s->nAtomCache = 0;
  /* Clean up dynamically allocated arrays */
	for (int i = 0;i < 6;i++)
		scFree(moveQueue[i]);
	scFree(moveQueue);

	scFree(bufferSend);
	scFree(bufferReceived);
  
   #if (VERBOSE > 1)
   reportVerboseInt3("Finish atom move routine: nAtomOwn, moveOut, Immigrant:",s->nAtomOwn,countMoveOut,nImmigrantAtom);
   #endif
   timerStop(TIMING_ATOM_MOVE);
}

void forceReturn(STATE* s,ATOM_CACHE_DATA* ac,COMM_INFO* commInfo)
{
  //int k, a, kd, oc, i, tmpidx, kdd, ku, kuc, nSend, nSend3, nReceive, nReceive3, nForceSendBack, ibkwd, ibkwdr;
  //double com1;
  //double f3r[NEMAX-NMAX][3]; // reaction force buffer

  	double (*ra)[3] = s->ra;
 	double *bufferSend,*bufferReceived;
	double fact = 13.0;
	MPI_Status status;
	forceTimingRelation(TIMING_FORCE_RETURN,TIMING_MD_STEP);
	timerStart(TIMING_FORCE_RETURN);

	//reportVerbose("Starting force return routine...");


	bufferSend     = ((double*)scMalloc(BASE_BUFFER_SIZE*sizeof(double)+sizeof(double)*s->nAtomOwn*fact)); 
	bufferReceived = ((double*)scMalloc(BASE_BUFFER_SIZE*sizeof(double)+sizeof(double)*s->nAtomOwn*fact)); 

 //com1 = MPI_Wtime ();

//-------Force return---------------
//-------Number of sent-back reactions
  int nForceSendBack = 0;

//-------Send back reactions in the reverse order, z, y, & x
  for (int kd = 2; kd >= 0; kd--) { // 2,1,0

//---------Only lower direction
//---------Change kdd to 1 if both direction are used
        for (int kdd = 0; kdd >= 0; kdd--) {        // only lower direction: kdd=0


// flip communication direction, ku={5,4,3,2,1,0}->kuc={4,5,2,3,0,1}
// for lower direction only, kuc={4,2,0}
          int ku = 2 * kd + kdd;
	  int kur;
          if (ku % 2 == 1) {
                kur = ku - 1;
          }                                                      
          else {
                kur = ku + 1;
          }

          //ibkwd = nn[kur];                      // Neighbor node ID 
          //ibkwdr = nnr[kur];            /* reverse-neighbor node ID */
	  int inode = commInfo->myNN[kur];	/* Neighbor node ID */
	  int inoder = commInfo->myNNr[kur];

// 'recv' becomes 'send', and 'send' becomes 'recv'
//-----------Number of reactions to neighbor -ku
          //int nSend = lrc[ku];
          int nSend = ac->directionalCacheReceiveN[ku];
          int nSend3 = nSend * 3;
//-----------Number of reactions coming from ku
          //int nReceive = lsb[ku][0];
          int nReceive = ac->directionalCacheSendN[ku];
          int nReceive3 = nReceive * 3;

//-----------Message buffering
          //int tmpidx = nb - nForceSendBack - nSend + n;
          int idxOffset = s->nAtomCache + s->nAtomOwn - nForceSendBack - nSend;
          for (int i = 0; i < nSend; i++) {
                //oc = nb-nForceSendBack-nSend+i+n;
                int raIdx = idxOffset + i;
                for (int a = 0; a < 3; a++)
                  bufferSend[(3 * i) + a] = ra[raIdx][a];
          }

//-----------Message passing

          if (commInfo->myParity[kd] < 2) {       //if # of nodes in kd direction is more than 1

                MPI_Sendrecv(bufferSend, nSend3, MPI_DOUBLE, inode, MPI_TAG_FORCE_RETURN, bufferReceived, nReceive3, MPI_DOUBLE, inoder, MPI_TAG_FORCE_RETURN, MPI_COMM_WORLD, &status);
          }
          else                                          //if # of nodes in kd direction is more than 1
                                                                                                    
                for (int i = 0; i < nSend3; i++)
                  bufferReceived[i] = bufferSend[i];


          //-----------Message storing

          for (int k = 0; k < nReceive; k++) {
                //i = lsb[ku][k + 1];
                int i = ac->cacheList[ku][k];
//-------------Reaction on all atoms
                ra[i][0] += bufferReceived[3 * k];
                ra[i][1] += bufferReceived[3 * k + 1];
                ra[i][2] += bufferReceived[3 * k + 2];
          }

//-----------Accumulate the # of already sent-back atoms
          nForceSendBack += nSend;

        }                                                       //---------Enddo the higher & lower directions
  }                                                             //-------Enddo z, y, & x directions

  //comt_ret += MPI_Wtime () - com1;

	scFree(bufferSend);
	scFree(bufferReceived);

	#if (VERBOSE > 1)
	reportVerbose("Finish force return routine.");
	#endif
	timerStop(TIMING_FORCE_RETURN);
}

void initAtomCache(ATOM_CACHE_DATA* cache,STATE* s)
{
	//Estimate atom that need caching.
	//Later job: For eadch direction, Use 9*system size (roughly maximum possible) for now...

	int nAtomGlobal = s->nAtomGlobal;
	int nNodes;
	int memNeed = 0;
	double fact = 9.0;
	
	MPI_Comm_size(MPI_COMM_WORLD, &nNodes);
	
	double avgAtom = nAtomGlobal/(double)nNodes;
	cache->cacheListNMax = BASE_BUFFER_SIZE+avgAtom*fact; 

	cache->cacheList = (int**)scMalloc(sizeof(int*)*6);
	for (int i = 0;i < 6;i++) {
		cache->cacheList[i] = (int*)scMalloc(sizeof(int)*cache->cacheListNMax);
		memNeed += (sizeof(int)*cache->cacheListNMax);
		//init all value to 0
		cache->directionalCacheSendN[i] = 0;
		cache->directionalCacheReceiveN[i] = 0;
	}
	
	reportPrintFixedDouble("Allocate memory for atom caching [MB]:", memNeed/(1024.0*1024));
}
/*----------------------------------------------------------------------
 Bit condition functions:
 
 1. isAtomCacheNeighbor(ri,ku) is .true. if coordinate ri[3] is in the boundary to 
 neighbor ku.
 2. isAtomMoveNeighbor(ri,ku) is .true. if an atom with coordinate ri[3] has moved out 
     to neighbor ku.
----------------------------------------------------------------------*/
int isAtomCacheNeighbor(double *ri, int ku, double rCache, double* boxSize) {
  int kd, kdd;
  kd = ku / 2;					/* x(0)|y(1)|z(2) direction */
  kdd = ku % 2;					/* Lower(0)|higher(1) direction */
  if (kdd == 0)
	return ri[kd] < rCache;
  //return ri[kd] < rc[kd]*rcsize;
  else
	//return la[kd] - rc[kd]*rcsize < ri[kd];
	return boxSize[kd] - rCache < ri[kd];
}

int isAtomMoveNeighbor(double *ri, int ku, double* boxSize) {
  int kd, kdd;
  kd = ku / 2;					/* x(0)|y(1)|z(2) direction */
  kdd = ku % 2;					/* Lower(0)|higher(1) direction */
  if (kdd == 0)
	return ri[kd] < 0.0;
  else
	return boxSize[kd] < ri[kd];
}


