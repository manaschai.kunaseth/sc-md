#include "params.h"
#include "simulate.h"
#include "input.h"
#include "potential.h"
#include "state.h"
#include "utils.h"
#include "atomType.h"
#include "cell.h"
#include "analysis.h"
#include "atomConfig.h"
#include "analysis_collection.h"
#include "mpi.h"
#include "memory.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "init.h"

static void parseParams(PARAMS *params);

#define BASE_BUFFER_SIZE 10000

void init(PARAMS* params,SIMULATE_DATA* simulate)
{
	//Pot > Comm, Topoloy > AtomConf
	initSimulate(simulate);
	initParams(params);
	initUtils(params);

	printBuildInfo();

	reportVLine("Begin initialization process."); 	

	initInput(&params->input);
	readInput();
	reportPrint("Read input configuration completed.");
	reportPrint("");

	parseParams(params);
	reportPrint("Parse parameter completed."); 	
	reportPrint("");

	initPotential(&params->potential);
	reportPrint("Init potential completed.");
	reportPrint("");

	initCommunication(&params->commInfo);
	reportPrint("Init Communication & parallel parameters completed."); 	
	reportPrint("");

	initAtomConf(params, &simulate->mainState);
	reportPrint("Init atom configuration completed.");
	reportPrint("");

	initTopology(&params->commInfo,params->boxSize);
	reportPrint("Init topology information completed."); 	
	reportPrint("");

	initAtomCache(&simulate->atomCache,&simulate->mainState);
	reportPrint("Communication buffer initialization completed."); 	
	reportPrint("");
	
	initCellInfoSet(simulate->cellInfoSet);
	reportPrint("Calculate cell list structure completed."); 	
	reportPrint("");

	analysis_collection_init(&simulate->mainState,params);
	reportPrint("Initialize collective analysis completed."); 	
	reportPrint("");

	reportVLine("End initialization process."); 	
}



void parseParams(PARAMS *params)
{
	char msg[1000];

	INPUT_PARAMS* inp = getInputObj();
	params->mdMode = inp->mdMode;
	params->stepLimit = inp->stepLimit;
	params->deltaT = inp->deltaT*(1e-15/TIMEU); //atomic unit
	params->nAtomType = inp->nAtomType;	
	params->randomSeedNode = inp->randomSeed+getMyId();	
	for (int i = 0;i < MAX_NBODY; i++) {
		params->cell_nlayer[i] = inp->nlayer[i];
		if (params->cell_nlayer[i] > 1 && i != 2)
			terminateSingle("Number of cell layers > 1 for non-2body is not supported.",1);
	}
	reportPrintInt("Number of cell layers in 2-body:",params->cell_nlayer[2]);

	reportPrintFixedDouble("MD time step (fs):",inp->deltaT);
	reportPrint("Atom ID mapping:");
	reportPrint("      TYPE     ID    MASS(STD unit):");
	for (int i = 0;i < params->nAtomType;i++)
	{
		strcpy(params->atomTypeTable[i],inp->atomTypeTable[i]);
		params->atomMass[i] = inp->atomMass[i]*AMU;
		sprintf(msg,"%10s    %3d   %10.6lf",params->atomTypeTable[i], i, inp->atomMass[i]);
		reportPrint(msg);
	}	
	
	if (inp->analyzeFixBoundary > 0) {
		params->isSelectiveCoordUpdate = 1;
		reportPrint("Fixed boundary: Selective atomic coordinate update is enabled");
	}

	//Determining computation method: SC-only / SC for 2body comp. and neighbor list for 3body comp.
	//There is no longer a default computation method.
	if (strcmp(inp->computationMethodName,"SCNBL") == 0) {
		params->computationMethod = COMP_SCNBL;
		reportPrint("Computation method: SC-NBL");
	}
	else if (strcmp(inp->computationMethodName,"SC2B_VERLET3B") == 0) {
		params->computationMethod = COMP_SCNBL;
		reportPrint("Warning: SC2B_VERLET3B keyword is depricated and has been changed to SCNBL.");
		reportPrint("Computation method: SC-NBL");
	}
	else if     (strcmp(inp->computationMethodName,"SC2B_SC3B") == 0) {
		params->computationMethod = COMP_SC;
		reportPrint("Computation method: SC-2B, SC-3B");
	} 
	else {
		terminateSingle("Error: Incorrect computation method.",1);
	}
	initAtomTypeTable(params);

}


void initTopology(COMM_INFO* comm, double* boxSize)
{
/*----------------------------------------------------------------------
Defines a logical network topology.  Prepares a neighbor-node ID table, 
nn, & a shift-vector table, sv, for internode message passing.  Also 
prepares the node parity table, myparity.
----------------------------------------------------------------------*/
  /* Integer vectors to specify the six neighbor nodes */
  /* -X, +X, -Y, +Y, -Z, +Z directions, respectively */

	int iv[6][3] = {
	 {-1, 0, 0}, {1, 0, 0}, {0, -1, 0}, {0, 1, 0}, {0, 0, -1}, {0, 0, 1}
	};
	int k1[3];

	      int* vid;
	      int* vproc;

	 vid   = (comm->myVid);
	 vproc = (comm->vProc);

	/* Set up neighbor tables, nn & sv */
	for (int ku = 0; ku < 6; ku++) 
	{
		 /* Vector index of neighbor ku */

		 for (int a = 0; a < 3; a++)
		 	k1[a] = (vid[a] + iv[ku][a] + vproc[a]) % vproc[a];
		 /* Scalar neighbor ID, nn */
		 comm->myNN[ku] = k1[0] * vproc[1] * vproc[2] + k1[1] * vproc[2] + k1[2];

		 for (int a = 0; a < 3; a++)
		   	k1[a] = (vid[a] - iv[ku][a] + vproc[a]) % vproc[a];
		 /* Scalar neighbor ID reversed, nnr */
		 comm->myNNr[ku] = k1[0] * vproc[1] * vproc[2] + k1[1] * vproc[2] + k1[2];

		 /* Shift vector, sv */
		 for (int a = 0; a < 3;a++)
		    	comm->shiftVector[ku][a] = boxSize[a] * iv[ku][a];
	}
	/* Set up the node parity table, myparity */
	for (int a = 0; a < 3; a++) {
		if (vproc[a] == 1) 		//Only 1 node in a-direction
			comm->myParity[a] = 2;
		else if (vid[a] % 2 == 0)	//Even node
			comm->myParity[a] = 0;
		else				//Odd node
			comm->myParity[a] = 1;
	}

	reportVerbose("Shift Vector: (a.u.)");
	for (int ku = 0;ku < 6;ku++)
		reportVerboseDoubleArr3("            :", comm->shiftVector[ku]);

}

