#include "computeAccel_silica.h"
#include "pot_silica.h"
#include "potential.h"
#include "computePattern.h"
#include "state.h"
#include "params.h"
#include "cell.h"
#include "utils.h"
#include "timing.h"
#include "omp.h"
#include <math.h>

#if (DEBUG_LEVEL > 1)
#include <stdio.h>
#endif

#define MAX(a,b) (a > b)? a : b

double computeAccel_silica2B_thread(STATE *s, CELL_LIST* cl, COMPUTE_PATTERN_CELL* patCell,int nThreads);
double computeAccel_silica3B_thread(STATE *s, CELL_LIST* cl, COMPUTE_PATTERN_CELL* patCell,int nThreads);

//force routine call return energy.
double computeAccel_silica_cell2BAcyclic_thread(int *listA, int *listB, int nListA, int nListB, 
					double (*r)[3], double (*ra)[3], int *rType);

double computeAccel_silica_cell2BCyclic_thread(int *listA, int nListA, 
					double (*r)[3], double (*ra)[3], int *rType);

//Three body as in A-B-C (B-center)
double computeAccel_silica_cell3BAcyclic_thread(int *listA, int *listB, int *listC, int nListA, int nListB, int nListC, 
					double (*r)[3], double (*ra)[3], int *rType);

//Three body as in A-B-A (B-Center)
double computeAccel_silica_cell3BCyclic_thread(int *listAC, int *listB, int nListAC, int nListB, 
					double (*r)[3], double (*ra)[3], int *rType);

//universal parameter for potential/force calculation.
static double dh2Inv;     //1.0/(dh^2)   : dh = pot_table spacing;
static double** rcut2_2B; //Cut-off^2 for each atom type
static POTENTIAL_SILICA_EXTENSION *potExt;
static double*** forceTable;
static double*** potentialTable;
static double rij2max;


//3Body
static double *r0_3B;
static double *r02_3B;
static double *dl_3B;
static double ***bb_3B;
static double ***cosb_3B;
static double ***c3b_3B;

void computeAccel_silica_thread(STATE* s, POTENTIAL* pot, PARAMS* p,CELL_INFO* cellInfoSet)
{

	//general parameter use within this file.
	dh2Inv = 1.0/(pot->dh2);
	rij2max = pot->rij2max; 
	potExt = (POTENTIAL_SILICA_EXTENSION*)pot->extension;
	rcut2_2B = potExt->rcut2_2b;
	forceTable = pot->forceTable;
	potentialTable = pot->potentialTable;

	r0_3B = potExt->r0;
	r02_3B = potExt->r02;
	dl_3B = potExt->dl;
	bb_3B = potExt->bb;
	cosb_3B = potExt->cosb;
	c3b_3B = potExt->c3b;
	//end defining general parameter within this file.

	INPUT_PARAMS* inp = getInputObj();
	int nThreads2B = inp->nThreads[2];
	int nThreads3B = inp->nThreads[3];
	int nThreadsGlobal = nThreads2B + nThreads3B;
	omp_set_num_threads(nThreadsGlobal);
	double potE2b = 0.0;
	double potE3b = 0.0;
	omp_set_nested(1);
        if (omp_get_nested() == 0)
        {
		terminateSingle("omp_get_nested() == 0. Nested OMP parallel feature is needed in OMP version.",1);
        }
	//printf("maxlvl=%d\n",omp_get_max_active_levels());

	#pragma omp parallel sections \
				default(none) \
				shared(p,s,cellInfoSet,nThreads2B,nThreads3B) \
				reduction(+:potE2b,potE3b) \
				num_threads(2) 
	{
		#pragma omp section 
		{
			if (nThreads2B > 0) {
				forceTimingRelation(TIMING_COMPUTE_ACCEL_2B_THREAD,TIMING_COMPUTE_ACCEL);
				timerStart(TIMING_COMPUTE_ACCEL_2B_THREAD);
				CELL_LIST *cl2b = NULL;
				cl2b = makeCellList(cl2b,&cellInfoSet[INDEX_2B],s,p->boxSize,paramsGetRCut(2),p->commInfo.rCacheRadius,p->cell_nlayer[2]);
				potE2b += computeAccel_silica2B_thread(s,cl2b,&cellInfoSet[INDEX_2B].computePatternCell,nThreads2B);
				freeCellListData(cl2b);
				timerStop(TIMING_COMPUTE_ACCEL_2B_THREAD);
			}
		}
		#pragma omp section
		{
			if (nThreads3B > 0) {
				forceTimingRelation(TIMING_COMPUTE_ACCEL_3B_THREAD,TIMING_COMPUTE_ACCEL);
				timerStart(TIMING_COMPUTE_ACCEL_3B_THREAD);
				CELL_LIST *cl3b = NULL;
				cl3b = makeCellList(cl3b,&cellInfoSet[INDEX_3B],s,p->boxSize,paramsGetRCut(3),p->commInfo.rCacheRadius,p->cell_nlayer[3]);
				potE3b += computeAccel_silica3B_thread(s,cl3b,&cellInfoSet[INDEX_3B].computePatternCell,nThreads3B);
				freeCellListData(cl3b);
				timerStop(TIMING_COMPUTE_ACCEL_3B_THREAD);
			}
		}
	}

	s->potentialEnergy[INDEX_2B] += potE2b;
	s->potentialEnergy[INDEX_3B] += potE3b;
}


double computeAccel_silica2B_thread(STATE *s, CELL_LIST* cl, COMPUTE_PATTERN_CELL* patCell,int nThreads)
{

	int nPattern = patCell->nPattern;
	int **cellOffset = patCell->cellPatternOffset;
	double potEnergy = 0.0;

	int **cellData = cl->cellAtomData;
	int *nAtomCell = cl->nAtomCell;

	double (*r)[3]  = s->r;
	double (*ra)[3] = s->ra;
	int  *rType   = s->rType;

	int vCellX = cl->vCell[0];
	int vCellY = cl->vCell[1];
	int vCellZ = cl->vCell[2];

	int vCellAll[3];
	//cell vector including caching cells.
	for (int a = 0;a < 3;a++)
		vCellAll[a] = cl->vCell[a] + cl->nCellCache[a];

	//variable use for calculating cell index
	int vCellAllX  = vCellAll[0];
	int vCellAllY  = vCellAll[1];
	int vCellAllXY = vCellAll[0]*vCellAll[1];
	int vCellAllXYZ= vCellAll[0]*vCellAll[1]*vCellAll[2];

	//Loop over all cell within domain.
	/*
	for (int cz = 0;cz < vCellZ;cz++)
	for (int cy = 0;cy < vCellY;cy++)
	for (int cx = 0;cx < vCellX;cx++) {
	*/

	//#pragma omp for schedule(dynamic,1) 
	#pragma omp parallel for \
			default(none)\
			shared(vCellAllX,vCellAllY,vCellAllXY,vCellX,vCellY,vCellZ,vCellAllXYZ,nPattern,\
				cellOffset,nAtomCell,cellData,r,ra,rType)\
			reduction(+:potEnergy)\
			schedule(dynamic,2) \
			num_threads(nThreads) 
	for (int c = 0; c < vCellAllXYZ;c++){

		int cx =  c % vCellAllX;
		int cy = (c / vCellAllX) % vCellAllY;
		int cz =  c / vCellAllXY;

		if ((cx >= vCellX) || (cy >= vCellY) || (cz >= vCellZ)) continue;

		for (int iPat = 0;iPat < nPattern;iPat++) {
			int cellChain1 = c + cellOffset[iPat][0];
			int cellChain2 = c + cellOffset[iPat][1];

			if (cellChain1 != cellChain2)
				potEnergy += computeAccel_silica_cell2BAcyclic_thread(cellData[cellChain1],cellData[cellChain2], 
							nAtomCell[cellChain1], nAtomCell[cellChain2], r, ra, rType);
			else
				potEnergy += computeAccel_silica_cell2BCyclic_thread(cellData[cellChain1],nAtomCell[cellChain1],
							r, ra, rType);
			#if (DEBUG_LEVEL > 1)
			printf("(%3d,%3d,%3d) c = %5d iPat = %5d: chain(%5d,%5d) done. PotE = %le\n",cx,cy,cz,c,iPat,cellChain1,cellChain2,potEnergy);
			#endif
			//printf("c = %5d iPat = %5d passed.\n",c,iPat);
		}
	}

	return potEnergy;

}

double computeAccel_silica_cell2BAcyclic_thread(int *listA, int *listB, int nListA, int nListB, 
					double (*r)[3],  double (*ra)[3], int *rType) {

	double potEnergyCell2B = 0.0;

	//loop over i-th atom
	for (int ia = 0;ia < nListA; ia++){
		//information for atom i
		double rix , riy,  riz; //temporary coord. of atom i
		double raix,raiy, raiz; //temporary accel. of atom i

		int i     = listA[ia];
		int iType = rType[i];

		rix = r[i][0];
		riy = r[i][1];
		riz = r[i][2];

		raix = 0.0;
		raiy = 0.0;
		raiz = 0.0;
		
		//loop over j-th atom
		for (int ib = 0;ib < nListB; ib++){
			int j = listB[ib];
			int jType = rType[j];

			double dx,dy,dz; //interatomic distance in x,y,z
			double rr; //interatomic distance ^2

			dx  = rix - r[j][0];
			rr  = dx*dx;
			dy  = riy - r[j][1];
			rr += dy*dy;
			dz  = riz - r[j][2];
			rr += dz*dz;
		
			//if r^2 less than rcut2B, then calculate force/energy
			if (rr < rcut2_2B[iType][jType]) {
				//rr = (rr > rij2max) ? rij2max : rr;
				//Lookup table:
				//We want to lookup value v(rr) from table TAB: v(rr) = TAB[idxRR]
				//For table with table spacing dh, index in the table that rr falls, idxRR = (rr/dh)
				//However, TAB keeps discrete value. We can't directly compute fraction of TAB[idxRR] direction.
				//v(rr) will be calculated using intepolation instead.
				//Let's assume that position of idxRR falls between TAB[idxBase] to TAB[idxBase+1], idxBase is integer.
				//idxBase can be calculated from: idxBase = (int)(tabRR - idxBase);
				//let idxdh = idxRR-idxBase, Then v(rr) = TAB[idxBase] + TAB[idxdh]
				//TAB[idxBase] can be calculated, but TAB[idxdh] will be interpolated from TAB[idxBase] to TAB[idxBase+1]
				//Therefore, dv = TAB[idxBase+1] - TAB[idxBase]
				//v(rr) = TAB[idxBase] + idxdh*dv

				double idxRR      = rr*dh2Inv;
				int    idxBase    = (int)idxRR; //Round down to the nearest integer

				double idxdh	  = idxRR-idxBase;
				double vBase	  = forceTable[iType][jType][idxBase];
				/*
				double dv	  = forceTable[iType][jType][idxBase+1] - vBase;
				double vrr	  = dv*idxdh + vBase;
				*/
				double vrr	  = (forceTable[iType][jType][idxBase+1] - vBase)*idxdh + vBase;
				//double vrr = (1.0 - idxdh)*vBase + idxdh*potentialTable[iType][jType][idxBase+1];
				//printf("vrr = %le: rr = %le: rcut2_2B= %le\n",vrr,rr,rcut2_2B[iType][jType]);
				
				//Calculate force in x, y, z
				double vForce;
				vForce    = vrr*dx;
				raix     += vForce;
				#pragma omp atomic 
				ra[j][0] -= vForce; 

				vForce    = vrr*dy;
				raiy     += vForce; 
				#pragma omp atomic 
				ra[j][1] -= vForce; 

				vForce    = vrr*dz;
				raiz     += vForce; 
				#pragma omp atomic 
				ra[j][2] -= vForce; 

				//calculate potential energy
				potEnergyCell2B += (1.0 - idxdh)*potentialTable[iType][jType][idxBase] + 
						           idxdh*potentialTable[iType][jType][idxBase+1];

			} // end if rr < rcut2
			
		} //end for atom j
		
		//Sum force acting on i back
		#pragma omp atomic 
		ra[i][0] += raix;
		#pragma omp atomic 
		ra[i][1] += raiy;
		#pragma omp atomic 
		ra[i][2] += raiz;
	} //end for atom i	
	return potEnergyCell2B;
}


double computeAccel_silica_cell2BCyclic_thread(int *listA, int nListA, 
					double (*r)[3], double (*ra)[3], int *rType) {
	
	double potEnergyCell2B = 0.0;
	for (int ia = 0;ia < nListA-1; ia++)	{
		//information for atom i
		double rix , riy,  riz; //temporary coord. of atom i
		double raix,raiy, raiz; //temporary accel. of atom i

		int i     = listA[ia];
		int iType = rType[i];

		rix = r[i][0];
		riy = r[i][1];
		riz = r[i][2];

		raix = 0.0;
		raiy = 0.0;
		raiz = 0.0;
		
		//loop over j-th atom
		for (int ib = ia+1;ib < nListA; ib++){
			int j = listA[ib];
			int jType = rType[j];

			double dx,dy,dz; //interatomic distance in x,y,z
			double rr; //interatomic distance ^2

			dx  = rix - r[j][0];
			rr  = dx*dx;
			dy  = riy - r[j][1];
			rr += dy*dy;
			dz  = riz - r[j][2];
			rr += dz*dz;
		
			//if r^2 less than rcut2B, then calculate force/energy
			if (rr < rcut2_2B[iType][jType]) {
				//rr = (rr > rij2max) ? rij2max : rr;
				//Lookup table:
				//We want to lookup value v(rr) from table TAB: v(rr) = TAB[idxRR]
				//For table with table spacing dh, index in the table that rr falls, idxRR = (rr/dh)
				//However, TAB keeps discrete value. We can't directly compute fraction of TAB[idxRR] direction.
				//v(rr) will be calculated using intepolation instead.
				//Let's assume that position of idxRR falls between TAB[idxBase] to TAB[idxBase+1], idxBase is integer.
				//idxBase can be calculated from: idxBase = (int)(tabRR - idxBase);
				//let idxdh = idxRR-idxBase, Then v(rr) = TAB[idxBase] + TAB[idxdh]
				//TAB[idxBase] can be calculated, but TAB[idxdh] will be interpolated from TAB[idxBase] to TAB[idxBase+1]
				//Therefore, dv = TAB[idxBase+1] - TAB[idxBase]
				//v(rr) = TAB[idxBase] + idxdh*dv

				double idxRR      = rr*dh2Inv;
				int    idxBase    = (int)idxRR; //Round down to the nearest integer

				double idxdh	  = idxRR-idxBase;
				double vBase	  = forceTable[iType][jType][idxBase];
				/*
				double dv	  = forceTable[iType][jType][idxBase+1] - vBase;
				double vrr	  = dv*idxdh + vBase;
				*/
				double vrr	  = (forceTable[iType][jType][idxBase+1] - vBase)*idxdh + vBase;

				
				//Calculate force in x, y, z
				double vForce;
				vForce    = vrr*dx;
				raix     += vForce; 
				#pragma omp atomic 
				ra[j][0] -= vForce; 

				vForce    = vrr*dy;
				raiy     += vForce; 
				#pragma omp atomic 
				ra[j][1] -= vForce; 

				vForce    = vrr*dz;
				raiz     += vForce; 
				#pragma omp atomic 
				ra[j][2] -= vForce; 

				//calculate potential energy
				potEnergyCell2B += (1.0 - idxdh)*potentialTable[iType][jType][idxBase] + 
						           idxdh*potentialTable[iType][jType][idxBase+1];

			} // end if rr < rcut2
			
		} //end for atom j
		
		//Sum force acting on i back
		#pragma omp atomic 
		ra[i][0] += raix;
		#pragma omp atomic 
		ra[i][1] += raiy;
		#pragma omp atomic 
		ra[i][2] += raiz;
	} //end for atom i
	return potEnergyCell2B;
}



double computeAccel_silica3B_thread(STATE *s, CELL_LIST* cl, COMPUTE_PATTERN_CELL* patCell,int nThreads)
{
	int nPattern = patCell->nPattern;
	int **cellOffset = patCell->cellPatternOffset;
	double potEnergy = 0.0;

	int **cellData = cl->cellAtomData;
	int *nAtomCell = cl->nAtomCell;

	double (*r)[3]  = s->r;
	double (*ra)[3] = s->ra;
	int  *rType   = s->rType;

	int vCellX = cl->vCell[0];
	int vCellY = cl->vCell[1];
	int vCellZ = cl->vCell[2];


	int vCellAll[3];
	//cell vector including caching cells.
	for (int a = 0;a < 3;a++)
		vCellAll[a] = cl->vCell[a] + cl->nCellCache[a];

	//variable use for calculating cell index
	int vCellAllX  = vCellAll[0];
	int vCellAllY  = vCellAll[1];
	int vCellAllXY = vCellAll[0]*vCellAll[1];
	int vCellAllXYZ= vCellAll[0]*vCellAll[1]*vCellAll[2];

	//Loop over all cell within domain.
	/*
	for (int cz = 0;cz < vCellZ;cz++)
	for (int cy = 0;cy < vCellY;cy++)
	for (int cx = 0;cx < vCellX;cx++) {
	*/

	//#pragma omp for schedule(dynamic,1) 
	#pragma omp parallel for \
			default(none)\
			shared(vCellAllX,vCellAllY,vCellAllXY,vCellX,vCellY,vCellZ,vCellAllXYZ,nPattern,\
				cellOffset,nAtomCell,cellData,r,ra,rType)\
			reduction(+:potEnergy)\
			schedule(dynamic,2) \
			num_threads(nThreads) 
	for (int c = 0; c < vCellAllXYZ;c++){

		int cx =  c % vCellAllX;
		int cy = (c / vCellAllX) % vCellAllY;
		int cz =  c / vCellAllXY;

		if ((cx >= vCellX) || (cy >= vCellY) || (cz >= vCellZ)) continue;

		for (int iPat = 0;iPat < nPattern;iPat++) {
			int cellChain1 = c + cellOffset[iPat][0];
			int cellChain2 = c + cellOffset[iPat][1];
			int cellChain3 = c + cellOffset[iPat][2];
			
			//if no atom in cell, move to the next one		
			if ((nAtomCell[cellChain1] == 0) || (nAtomCell[cellChain2] == 0) || (nAtomCell[cellChain3] == 0)) continue;		


			if (cellChain1 != cellChain3)
				potEnergy += computeAccel_silica_cell3BAcyclic_thread(cellData[cellChain1],cellData[cellChain2],cellData[cellChain3], 
							nAtomCell[cellChain1], nAtomCell[cellChain2], nAtomCell[cellChain3], r, ra, rType);
			else
				potEnergy += computeAccel_silica_cell3BCyclic_thread(cellData[cellChain1], cellData[cellChain2],
							nAtomCell[cellChain1], nAtomCell[cellChain2], r, ra, rType);

			//printf("(%3d,%3d,%3d) c = %5d iPat = %5d: chain(%5d,%5d,%5d) Finished.\n",cx,cy,cz,c,iPat,cellChain1,cellChain2,cellChain3);
			//printf("c = %5d iPat = %5d passed.\n",c,iPat);
		}

	}
	
	return potEnergy;

}


double computeAccel_silica_cell3BAcyclic_thread(int *listA, int *listB, int*listC, int nListA, int nListB, int nListC,
					double (*r)[3],  double (*ra)[3], int *rType) 
{

	double potEnergyCell3B = 0.0;

	//Choose central cell first
	for (int ib = 0;ib < nListB;ib++){
		int  i     = listB[ib];
		int  iType = rType[i];
		double rix = r[i][0];
		double riy = r[i][1];
		double riz = r[i][2];
		double r0iType = r0_3B[iType];
		double dliType = dl_3B[iType];
		double r02iType = r02_3B[iType];
		for (int ia = 0;ia < nListA;ia++){
			int j = listA[ia];
			int jType = rType[j];
			if (jType == iType) continue;

			double rr;
			double dx,dy,dz;
			dx = rix - r[j][0];
			rr =  dx*dx;
			dy =  riy - r[j][1];
			rr += dy*dy;
			dz =  riz - r[j][2];
			rr += dz*dz;

			if (rr >= r02iType) continue;
				
			double rij = sqrt(rr);
			double rijInv = 1.0/rij;
			double uijx = dx*rijInv;
			double uijy = dy*rijInv;
			double uijz = dz*rijInv;
			double argij = MAX(1.0 / (rij - r0iType),-100);

			for (int ic = 0;ic < nListC;ic++){
				int k = listC[ic];
				int kType = rType[k];
				if (kType == iType) continue;

				dx = rix - r[k][0];
				rr =  dx*dx;
				dy =  riy - r[k][1];
				rr += dy*dy;
				dz =  riz - r[k][2];
				rr += dz*dz;

				if (rr >= r02iType) continue;
			
				double rik = sqrt(rr);
				double rikInv = 1.0/rik;
				double uikx = dx*rikInv;
				double uiky = dy*rikInv;
				double uikz = dz*rikInv;
				double argik = MAX(1.0 / (rik - r0iType),-100);
				
				//int min = MIN(j%24,k%24);
				//int max = MAX(j%24,k%24);
				//printf("TRIPLET:i(cen)-j-k %03d %03d %03d   %3d %3d %3d\n",i%24,min,max,i,j,k);
				//compute 3-Body force for j-i-k triplet.
				double ee = bb_3B[jType][iType][kType] * exp(dliType*(argij+argik));

				double drij  = -dliType*argij*argij;
				double drik  = -dliType*argik*argik;
				/* unused
				double d2rij = -2.0*drij*argij; 
				double d2rik = -2.0*drik*argik;
				double rdrij = rij*drij;
				double rdrik = rik*drik;
				*/
				double tt = uijx*uikx + uijy*uiky + uijz*uikz;

				double xx = tt - cosb_3B[jType][iType][kType];
				double xx2 =  xx*xx;
				//double eexx = ee*xx;
				double yy = 1.0/(1.0+c3b_3B[jType][iType][kType]*xx2);
				double eexxyy = ee*xx*yy;

				double dvijx = eexxyy*(uijx*drij*xx+2.0*yy*(uikx-tt*uijx)*rijInv);
				double dvijy = eexxyy*(uijy*drij*xx+2.0*yy*(uiky-tt*uijy)*rijInv);
				double dvijz = eexxyy*(uijz*drij*xx+2.0*yy*(uikz-tt*uijz)*rijInv);

				double dvikx = eexxyy*(uikx*drik*xx+2.0*yy*(uijx-tt*uikx)*rikInv);
				double dviky = eexxyy*(uiky*drik*xx+2.0*yy*(uijy-tt*uiky)*rikInv);
				double dvikz = eexxyy*(uikz*drik*xx+2.0*yy*(uijz-tt*uikz)*rikInv);

				//Force on i-atom (central atom)				
				#pragma omp atomic 
				ra[i][0] -= (dvijx + dvikx);
				#pragma omp atomic 
				ra[i][1] -= (dvijy + dviky);
				#pragma omp atomic 
				ra[i][2] -= (dvijz + dvikz);
			
				//Force on j-atom 	
				#pragma omp atomic 
				ra[j][0] += dvijx;
				#pragma omp atomic 
				ra[j][1] += dvijy;
				#pragma omp atomic 
				ra[j][2] += dvijz;
	
				//Force on k-atom 	
				#pragma omp atomic 
				ra[k][0] += dvikx;
				#pragma omp atomic 
				ra[k][1] += dviky;
				#pragma omp atomic 
				ra[k][2] += dvikz;

				//potential energy
				potEnergyCell3B += eexxyy*xx*yy;		

			}// end atom k	

		} // end atom j

	} // end atom i
	

	return potEnergyCell3B;
}



double computeAccel_silica_cell3BCyclic_thread(int *listAC, int *listB, int nListAC, int nListB,
					double (*r)[3],  double (*ra)[3], int *rType) {

	double potEnergyCell3B = 0.0;

	//Choose central cell first
	for (int ib = 0;ib < nListB;ib++){
		int  i     = listB[ib];
		int  iType = rType[i];
		double rix = r[i][0];
		double riy = r[i][1];
		double riz = r[i][2];
		double r0iType  = r0_3B[iType];
		double dliType  = dl_3B[iType];
		double r02iType = r02_3B[iType];
		for (int ia = 0;ia < nListAC-1;ia++){
			int j = listAC[ia];
			int jType = rType[j];
			if (jType == iType) continue;

			double rr;
			double dx,dy,dz;
			dx = rix - r[j][0];
			rr =  dx*dx;
			dy =  riy - r[j][1];
			rr += dy*dy;
			dz =  riz - r[j][2];
			rr += dz*dz;

			//printf("r0iType = %lf\n",r02iType);
			if (rr >= r02iType) continue;
					
			double rij = sqrt(rr);
			double rijInv = 1.0/rij;
			double uijx = dx*rijInv;
			double uijy = dy*rijInv;
			double uijz = dz*rijInv;
			double argij = MAX(1.0 / (rij - r0iType),-100);

			for (int ic = ia+1;ic < nListAC;ic++){
				int k = listAC[ic];
				int kType = rType[k];
				if (kType == iType) continue;  //no need to check k = j, since ic = ia+1

				dx = rix - r[k][0];
				rr =  dx*dx;
				dy =  riy - r[k][1];
				rr += dy*dy;
				dz =  riz - r[k][2];
				rr += dz*dz;

				if (rr >= r02iType) continue;
			
				double rik = sqrt(rr);
				double rikInv = 1.0/rik;
				double uikx = dx*rikInv;
				double uiky = dy*rikInv;
				double uikz = dz*rikInv;
				double argik = MAX(1.0 / (rik - r0iType),-100);

				//int min = MIN(j%24,k%24);
				//int max = MAX(j%24,k%24);
				//printf("TRIPLET:i(cen)-j-k %03d %03d %03d   %3d %3d %3d\n",i%24,min,max,i,j,k);
				//compute 3-Body force for j-i-k triplet.
				double ee = bb_3B[jType][iType][kType] * exp(dliType*(argij+argik));

				double drij  = -dliType*argij*argij;
				double drik  = -dliType*argik*argik;
				/* unused
				double d2rij = -2.0*drij*argij; 
				double d2rik = -2.0*drik*argik;
				double rdrij = rij*drij;
				double rdrik = rik*drik;
				*/
				double tt = uijx*uikx + uijy*uiky + uijz*uikz;

				double xx = tt - cosb_3B[jType][iType][kType];
				double xx2 =  xx*xx;
				//double eexx = ee*xx;
				double yy = 1.0/(1.0+c3b_3B[jType][iType][kType]*xx2);
				double eexxyy = ee*xx*yy;

				double dvijx = eexxyy*(uijx*drij*xx+2.0*yy*(uikx-tt*uijx)*rijInv);
				double dvijy = eexxyy*(uijy*drij*xx+2.0*yy*(uiky-tt*uijy)*rijInv);
				double dvijz = eexxyy*(uijz*drij*xx+2.0*yy*(uikz-tt*uijz)*rijInv);

				double dvikx = eexxyy*(uikx*drik*xx+2.0*yy*(uijx-tt*uikx)*rikInv);
				double dviky = eexxyy*(uiky*drik*xx+2.0*yy*(uijy-tt*uiky)*rikInv);
				double dvikz = eexxyy*(uikz*drik*xx+2.0*yy*(uijz-tt*uikz)*rikInv);

				//Force on i-atom (central atom)				
				#pragma omp atomic 
				ra[i][0] -= (dvijx + dvikx);
				#pragma omp atomic 
				ra[i][1] -= (dvijy + dviky);
				#pragma omp atomic 
				ra[i][2] -= (dvijz + dvikz);
			
				//Force on j-atom 	
				#pragma omp atomic 
				ra[j][0] += dvijx;
				#pragma omp atomic 
				ra[j][1] += dvijy;
				#pragma omp atomic 
				ra[j][2] += dvijz;
	
				//Force on k-atom 	
				#pragma omp atomic 
				ra[k][0] += dvikx;
				#pragma omp atomic 
				ra[k][1] += dviky;
				#pragma omp atomic 
				ra[k][2] += dvikz;

				//potential energy
				potEnergyCell3B += eexxyy*xx*yy;		

			}// end atom k	

		} // end atom j

	} // end atom i
	



	return potEnergyCell3B;
}


