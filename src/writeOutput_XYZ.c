#include <stdio.h>
#include "simulate.h"
#include "state.h"
#include "input.h"
#include "mpi.h"
#include "params.h"
#include "utils.h"
#include "atomType.h"
#include "fileUtils.h"
#include "timing.h"
#include "memory.h"

#define XYZ_HEADER_COUNT 1
#define XYZ_DOUBLE_PER_ATOM 4
#define MPI_TAG_AGGREGATE_WRITE_XYZ 1200

static void packAtomWriteXYZBuffer(double* atomDataBuffer,int nAtomOwn,PARAMS *p,STATE *s);
static void writeAtomWriteXYZBuffer(double* atomDataBuffer,int dataBufferPerNode, int nNode, char* fullFilePath, PARAMS* p,unsigned long nAtomGlobal);


void writeOutput_xyz(STATE* s, PARAMS* p, WRITE_MODES mode)
{
	char dirName[1000];
	char fileName[100];
	char fullFilePath[1100];

	createOutputDir(simulateGetCurrentStep(),dirName);
	sprintf(fileName,"%07d.xyz",getMyId());
	sprintf(fullFilePath, "%s/%s",dirName,fileName);
	INPUT_PARAMS* inp = getInputObj();

	reportPrintInt("Writing output XYZ: Aggregate write for a group of MPI tasks = ",inp->writeAggregateOutputXYZ);

	FILE *fp;
	

	if (mode == WRITE_XYZ_GLOBAL)
	{
		if (inp->writeAggregateOutputXYZ == 1) {

			fp = fopen(fullFilePath, "w");
			if (getMyId() == 0){
				fprintf(fp, "%ld\n",s->nAtomGlobal);
				fprintf(fp, "SCMD XYZ File (Global format): Step %ld\n",simulateGetCurrentStep());
			}
			int nAtomOwn = s->nAtomOwn;
			double globCoord[3];
			double (*r)[3]     = s->r;
			int     *rType     = s->rType;
			unsigned long *rgid= s->rgid;
			double corner[3];
			char  atomTypeName[ATOM_TYPE_MAX][ATOM_TYPE_SYMBOL_MAX];
			//store atomType
			for (int i = 0;i < p->nAtomType;i++)
				symbolFromType(i,atomTypeName[i]);

			//calculating corner coordinate
			for (int a = 0;a < 3;a++)
				corner[a] = s->cornerCoord[a]*p->hMatrix[a][a];

			//write atom data
			for (int i = 0;i < nAtomOwn;i++) {
				//convert coordinate to global
				for (int a = 0;a < 3;a++)
					globCoord[a] = (r[i][a] + corner[a]) * BOHR; 
				fprintf(fp, "%3s %8.4f %8.4f %8.4f %ld\n", atomTypeName[rType[i]], globCoord[0],globCoord[1],globCoord[2],rgid[i]);
			}
			fclose(fp);
		} //end writeAggregateOutputXYZ == 1
		else if (inp->writeAggregateOutputXYZ > 1) { // for writeAggregateOutputXYZ > 1
			int nAtomOwn = s->nAtomOwn;
			int maxNatom = nAtomOwn; 
			MPI_Allreduce(&nAtomOwn,&maxNatom,1,MPI_INT,MPI_MAX,MPI_COMM_WORLD);

			int aggregateId = getMyId() % inp->writeAggregateOutputXYZ;
			int dataBufferPerNode = maxNatom*(XYZ_DOUBLE_PER_ATOM)+XYZ_HEADER_COUNT; //[nAtom]+nAtom*[atomdata,r*3]
			int masterId = getMyId()-(aggregateId);
			//printf("XYZ: MPI_task = %5d, aggregateId = %5d, masterId = %5d\n",getMyId(),aggregateId,masterId);
			//for master aggregate IO node
			if (aggregateId == 0) { 
				double* atomDataBuffer = (double*)scMalloc(sizeof(double)*inp->writeAggregateOutputXYZ*dataBufferPerNode);
				MPI_Request* req = (MPI_Request*)scMalloc(sizeof(MPI_Request)*inp->writeAggregateOutputXYZ);
				for (int i = 1;i < inp->writeAggregateOutputXYZ;i++) {
					MPI_Irecv(&atomDataBuffer[dataBufferPerNode*i], 
						   dataBufferPerNode, MPI_DOUBLE, getMyId()+i,MPI_TAG_AGGREGATE_WRITE_XYZ, MPI_COMM_WORLD, &req[i]);
				}
				
				packAtomWriteXYZBuffer(atomDataBuffer,nAtomOwn,p,s);
				MPI_Waitall(inp->writeAggregateOutputXYZ-1, &req[1], MPI_STATUSES_IGNORE); //Wait for req[1] to inp->writeAggregateOutputXYZ

				writeAtomWriteXYZBuffer(atomDataBuffer,dataBufferPerNode, inp->writeAggregateOutputXYZ, fullFilePath,p,s->nAtomGlobal);

				scFree(atomDataBuffer);
				scFree(req);

			} else { //for slave aggregate IO node 
				double* atomDataBuffer = (double*)scMalloc(sizeof(double)*dataBufferPerNode);
				packAtomWriteXYZBuffer(atomDataBuffer,nAtomOwn,p,s);
				MPI_Send(atomDataBuffer, nAtomOwn*(XYZ_DOUBLE_PER_ATOM)+XYZ_HEADER_COUNT, 
						MPI_DOUBLE, masterId, MPI_TAG_AGGREGATE_WRITE_XYZ,MPI_COMM_WORLD);

				scFree(atomDataBuffer);
			}

		} //end writeAggregateOutputXYZ > 1
	} //end WRITE_XYZ_GLOBAL
	else if (mode == WRITE_XYZ_LOCAL)
	{
		if (inp->writeAggregateOutputXYZ > 1) 
			terminateSingle("WRITE_XYZ_LOCAL does not support WRITE_AGGREGATE_OUTPUT > 1.",1);

		fp = fopen(fullFilePath, "w");

		fprintf(fp, "%d\n",s->nAtomOwn);
		fprintf(fp, "SCMD XYZ File (Local format): Step %ld\n",simulateGetCurrentStep());

		int nAtomOwn = s->nAtomOwn;
		double globCoord[3];
		double (*r)[3]     = s->r;
		int     *rType     = s->rType;
		unsigned long *rgid= s->rgid;
		char  atomTypeName[ATOM_TYPE_MAX][ATOM_TYPE_SYMBOL_MAX];
		//store atomType
		for (int i = 0;i < p->nAtomType;i++)
			symbolFromType(i,atomTypeName[i]);

		//write atom data
		for (int i = 0;i < nAtomOwn;i++) {
			//convert coordinate to global
			for (int a = 0;a < 3;a++)
				globCoord[a] = r[i][a] * BOHR; 
			fprintf(fp, "%3s %8.4f %8.4f %8.4f %ld\n", atomTypeName[rType[i]], globCoord[0],globCoord[1],globCoord[2],rgid[i]);
		}
		fclose(fp);
	} //end WRITE_XYZ_LOCAL

	
}

void packAtomWriteXYZBuffer(double* atomDataBuffer,int nAtomOwn,PARAMS *p,STATE *s) {
	double corner[3];
	double (*r)[3] = s->r;
	double (*rv)[3] = s->rv;
	int *rType = s->rType;
	unsigned long *rgid = s->rgid;

	//calculating corner coordinate
	for (int a = 0;a < 3;a++)
		corner[a] = s->cornerCoord[a]*p->hMatrix[a][a];

	int count = 0;

	//pack node header
	atomDataBuffer[count++] = nAtomOwn;
	//pack atom data
	for (int i = 0;i < nAtomOwn;i++) {
		atomDataBuffer[count++] = dataFromId(rType[i],rgid[i]); 

		//convert coordinate to global
		for (int a = 0;a < 3;a++) 
			atomDataBuffer[count  +a] = (r[i][a] + corner[a]) * BOHR;

		count+=3;
	}
	//printf("XYZ_pack: MPI_task %5d, count = %d\n",getMyId(),count);
}


void writeAtomWriteXYZBuffer(double* atomDataBuffer,int dataBufferPerNode, int nNode, char* fullFilePath, PARAMS* p,unsigned long nAtomGlobal) {

	char  atomTypeName[ATOM_TYPE_MAX][ATOM_TYPE_SYMBOL_MAX];
	//store atomType
	for (int i = 0;i < p->nAtomType;i++)
		symbolFromType(i,atomTypeName[i]);

	FILE *fp;
	fp = fopen(fullFilePath, "w");

	if (getMyId() == 0) {
		fprintf(fp, "%ld\n",nAtomGlobal);
		fprintf(fp, "SCMD XYZ File (Global format): Step %ld\n",simulateGetCurrentStep());
	}

	for (int iNode = 0;iNode < nNode;iNode++) {
		int offset = iNode*dataBufferPerNode;
		int nAtom = atomDataBuffer[offset];

		offset++;
		//write atom data
		for (int i = 0;i < nAtom;i++) {
			int rType = typeFromData(atomDataBuffer[offset]);
			unsigned long  rgid = idFromData(atomDataBuffer[offset]);
	
			fprintf(fp, "%3s %8.4f %8.4f %8.4f %ld\n", atomTypeName[rType], 
					atomDataBuffer[offset+1],atomDataBuffer[offset+2],atomDataBuffer[offset+3],
					rgid);
			offset+=4;
		} //end write atom record
	} //end iNode
	fclose(fp);
}




