#ifndef __INIT_H__
#define __INIT_H__

#include "simulate.h"
#include "params.h"

void init(PARAMS*,SIMULATE_DATA*);

void initTopology(COMM_INFO* comm, double* boxSize);


#endif
