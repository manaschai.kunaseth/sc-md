#include "state.h"
#include "params.h"
#include "cell.h"
#include "utils.h"
#include "timing.h"
#include "memory.h"
#include <assert.h>
#include <stdlib.h>

void analysis_reorderState(STATE* s,PARAMS *p, CELL_INFO* cellInfo,int nBody)
{
	timerStart(TIMING_REORDERING);
	//reordering atom data such that it would get performance benefit from better data locality.
	CELL_LIST *cl = NULL;
	cl = makeCellList(cl,cellInfo,s,p->boxSize,paramsGetRCut(nBody),p->commInfo.rCacheRadius,p->cell_nlayer[nBody]);

	STATE *s_tmp;
	s_tmp = (STATE*)scMalloc(sizeof(STATE));
	initState(s_tmp,s->nAtomOwn);
	int nCell = cl->nCell;
	double (*r)[3]  = s->r;
	double (*rv)[3] = s->rv;
	int    *rType   = s->rType;
	unsigned long *rgid    = s->rgid;

	double (*r_tmp)[3]  = s_tmp->r;
	double (*rv_tmp)[3] = s_tmp->rv;
	int    *rType_tmp   = s_tmp->rType;
	unsigned long *rgid_tmp    = s_tmp->rgid;
	
	int **cellAtomData = cl->cellAtomData;
	int atomCount = 0;
	for (int c = 0;c < nCell;c++)
	{
		int nAtomCell = cl->nAtomCell[c];
		for (int ia = 0;ia < nAtomCell;ia++){
			int i = cellAtomData[c][ia];
			r_tmp[atomCount][0] = r[i][0];
			r_tmp[atomCount][1] = r[i][1];
			r_tmp[atomCount][2] = r[i][2];


			rv_tmp[atomCount][0] = rv[i][0];
			rv_tmp[atomCount][1] = rv[i][1];
			rv_tmp[atomCount][2] = rv[i][2];

			
			rType_tmp[atomCount] = rType[i];
			rgid_tmp[atomCount] = rgid[i];
			atomCount++;
		}
	}
	
	assert(s->nAtomOwn == atomCount);

	//copy atom info back
	for (int i = 0;i < atomCount;i++){
		r[i][0] = r_tmp[i][0];
		r[i][1] = r_tmp[i][1];
		r[i][2] = r_tmp[i][2];


		rv[i][0] = rv_tmp[i][0];
		rv[i][1] = rv_tmp[i][1];
		rv[i][2] = rv_tmp[i][2];

		
		rType[i] = rType_tmp[i];
		rgid[i] = rgid_tmp[i];
	}
		
	#if (VERBOSE > 1)
	reportPrint("Data reordering completed.");
	#endif

	freeState(s_tmp);
	scFree(s_tmp);
	freeCellListData(cl);
	timerStop(TIMING_REORDERING);
}	
