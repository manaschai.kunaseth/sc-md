#include "computeAccel_lj.h"
#include "potential.h"
#include "pot_lj.h"
#include "computePattern.h"
#include "state.h"
#include "params.h"
#include "cell.h"
#include "timing.h"
#include <stdio.h>
#include <math.h>

#if (DEBUG_LEVEL > 1)
#include <stdio.h>
#endif

#define MAX(a,b) (a > b)? a : b

double computeAccel_lj_2B(STATE *s, CELL_LIST* cl, COMPUTE_PATTERN_CELL* patCell);
//double computeAccel_silica3B(STATE *s, CELL_LIST* cl, COMPUTE_PATTERN_CELL* patCell);

//force routine call return energy.
double computeAccel_lj_cell2BAcyclic(int *listA, int *listB, int nListA, int nListB, 
					double (*r)[3], double (*ra)[3], int *rType);

double computeAccel_lj_cell2BCyclic(int *listA, int nListA, 
					double (*r)[3], double (*ra)[3], int *rType);

//universal parameter for potential/force calculation.
static double dh2Inv;     //1.0/(dh^2)   : dh = pot_table spacing;
static double** rcut2_2B; //Cut-off^2 for each atom type
static POTENTIAL_LJ_EXTENSION *potExt;
static double*** forceTable;
static double*** potentialTable;
static double rij2max;


//DEBUG purpose
#if (DEBUG_LEVEL > 4)
static STATE *priv_s;
#endif


void computeAccel_lj(STATE* s, POTENTIAL* pot, PARAMS* p,CELL_INFO* cellInfoSet)
{
	CELL_LIST *cl2b = NULL;
	cl2b = makeCellList(cl2b,&cellInfoSet[INDEX_2B],s,p->boxSize,paramsGetRCut(2),p->commInfo.rCacheRadius,p->cell_nlayer[2]);
	//cl2b = makeCellList(cl2b,&cellInfoSet[INDEX_2B],s,p->boxSize,2.5,p->commInfo.rCacheRadius,p->cell_nlayer[2]);
	//cl2b = makeCellList(cl2b,&cellInfoSet[INDEX_2B],s,p->boxSize,2.5,2.5,p->cell_nlayer[2]);

	//general parameter use within this file.
	dh2Inv = 1.0/(pot->dh2);
	rij2max = pot->rij2max; 
	potExt = (POTENTIAL_LJ_EXTENSION*)pot->extension;
	rcut2_2B = potExt->rcut2_2b;
	forceTable = pot->forceTable;
	potentialTable = pot->potentialTable;

	//end defining general parameter within this file.

	timerStart(TIMING_COMPUTE_ACCEL_2B);
	s->potentialEnergy[INDEX_2B] += computeAccel_lj_2B(s,cl2b,&cellInfoSet[INDEX_2B].computePatternCell);
	timerStop(TIMING_COMPUTE_ACCEL_2B);
	//timerStart(TIMING_COMPUTE_ACCEL_3B);
	//s->potentialEnergy[INDEX_3B] += computeAccel_silica3B(s,cl3b,&cellInfoSet[INDEX_3B].computePatternCell);
	//timerStop(TIMING_COMPUTE_ACCEL_3B);

	freeCellListData(cl2b);
	//freeCellListData(cl3b);
}


double computeAccel_lj_2B(STATE *s, CELL_LIST* cl, COMPUTE_PATTERN_CELL* patCell)
{

	int nPattern = patCell->nPattern;
	int **cellOffset = patCell->cellPatternOffset;
	double potEnergy = 0.0;

	int **cellData = cl->cellAtomData;
	int *nAtomCell = cl->nAtomCell;

	double (*r)[3]  = s->r;
	double (*ra)[3] = s->ra;
	int  *rType   = s->rType;

	int vCellX = cl->vCell[0];
	int vCellY = cl->vCell[1];
	int vCellZ = cl->vCell[2];

	int vCellAll[3];
	//cell vector including caching cells.
	for (int a = 0;a < 3;a++)
		vCellAll[a] = cl->vCell[a] + cl->nCellCache[a];

	//variable use for calculating cell index
	int vCellAllX  = vCellAll[0];
	int vCellAllXY = vCellAll[0]*vCellAll[1];

	//Loop over all cell within domain.
	for (int cz = 0;cz < vCellZ;cz++)
	for (int cy = 0;cy < vCellY;cy++)
	for (int cx = 0;cx < vCellX;cx++) {

	//index of cell, c
		int c = cx + cy*vCellAllX + cz*vCellAllXY;

		for (int iPat = 0;iPat < nPattern;iPat++) {
			int cellChain1 = c + cellOffset[iPat][0];
			int cellChain2 = c + cellOffset[iPat][1];
			double patEnergy = 0.0;

			if (cellChain1 != cellChain2)
				patEnergy += computeAccel_lj_cell2BAcyclic(cellData[cellChain1],cellData[cellChain2], 
							nAtomCell[cellChain1], nAtomCell[cellChain2], r, ra, rType);
			else
				patEnergy += computeAccel_lj_cell2BCyclic(cellData[cellChain1],nAtomCell[cellChain1],
							r, ra, rType);
			potEnergy += patEnergy;
			#if (DEBUG_LEVEL > 1)
			printf("(%3d,%3d,%3d) c = %5d iPat = %5d: chain(%5d,%5d) done. PotE = %le\n",cx,cy,cz,c,iPat,cellChain1,cellChain2,patEnergy);
			#endif
			//printf("c = %5d iPat = %5d passed.\n",c,iPat);
		}


	}
	
	return potEnergy;

}

double computeAccel_lj_cell2BAcyclic(int *listA, int *listB, int nListA, int nListB, 
					double (*r)[3],  double (*ra)[3], int *rType) {

	double potEnergyCell2B = 0.0;

	//loop over i-th atom
	for (int ia = 0;ia < nListA; ia++){
		//information for atom i
		double rix , riy,  riz; //temporary coord. of atom i
		double raix,raiy, raiz; //temporary accel. of atom i

		int i     = listA[ia];
		int iType = rType[i];

		rix = r[i][0];
		riy = r[i][1];
		riz = r[i][2];

		raix = 0.0;
		raiy = 0.0;
		raiz = 0.0;
		
		//loop over j-th atom
		for (int ib = 0;ib < nListB; ib++){
			int j = listB[ib];
			int jType = rType[j];

			double dx,dy,dz; //interatomic distance in x,y,z
			double rr; //interatomic distance ^2

			dx  = rix - r[j][0];
			rr  = dx*dx;
			dy  = riy - r[j][1];
			rr += dy*dy;
			dz  = riz - r[j][2];
			rr += dz*dz;
			//printf("rr = %10.5f iType = %5d jType = %5d rcut2_2B = %10.5f\n",rr,iType,jType,rcut2_2B[iType][jType]);	
			//if r^2 less than rcut2B, then calculate force/energy
			if (rr < rcut2_2B[iType][jType]) {
				//rr = (rr > rij2max) ? rij2max : rr;
				//Lookup table:
				//We want to lookup value v(rr) from table TAB: v(rr) = TAB[idxRR]
				//For table with table spacing dh, index in the table that rr falls, idxRR = (rr/dh)
				//However, TAB keeps discrete value. We can't directly compute fraction of TAB[idxRR] direction.
				//v(rr) will be calculated using intepolation instead.
				//Let's assume that position of idxRR falls between TAB[idxBase] to TAB[idxBase+1], idxBase is integer.
				//idxBase can be calculated from: idxBase = (int)(tabRR - idxBase);
				//let idxdh = idxRR-idxBase, Then v(rr) = TAB[idxBase] + TAB[idxdh]
				//TAB[idxBase] can be calculated, but TAB[idxdh] will be interpolated from TAB[idxBase] to TAB[idxBase+1]
				//Therefore, dv = TAB[idxBase+1] - TAB[idxBase]
				//v(rr) = TAB[idxBase] + idxdh*dv

				double idxRR      = rr*dh2Inv;
				int    idxBase    = (int)idxRR; //Round down to the nearest integer

				double idxdh	  = idxRR-idxBase;
				double vBase	  = forceTable[iType][jType][idxBase];
				/*
				double dv	  = forceTable[iType][jType][idxBase+1] - vBase;
				double vrr	  = dv*idxdh + vBase;
				*/
				double vrr	  = (forceTable[iType][jType][idxBase+1] - vBase)*idxdh + vBase;
				//double vrr = (1.0 - idxdh)*vBase + idxdh*potentialTable[iType][jType][idxBase+1];
				//printf("vrr = %le: rr = %le: rcut2_2B= %le\n",vrr,rr,rcut2_2B[iType][jType]);
				//printf("FRC-Cyclic: %10.5f %10.6le %10.6le\n",rr, idxBase+idxRR,vrr);	
				//Calculate force in x, y, z
				double vForce;
				vForce    = vrr*dx;
				raix     += vForce; 
				ra[j][0] -= vForce; 

				vForce    = vrr*dy;
				raiy     += vForce; 
				ra[j][1] -= vForce; 

				vForce    = vrr*dz;
				raiz     += vForce; 
				ra[j][2] -= vForce; 

				//calculate potential energy
				potEnergyCell2B += (1.0 - idxdh)*potentialTable[iType][jType][idxBase] + 
						           idxdh*potentialTable[iType][jType][idxBase+1];

			} // end if rr < rcut2
			
		} //end for atom j
		
		//Sum force acting on i back
		ra[i][0] += raix;
		ra[i][1] += raiy;
		ra[i][2] += raiz;
	} //end for atom i	
	return potEnergyCell2B;
}


double computeAccel_lj_cell2BCyclic(int *listA, int nListA, 
					double (*r)[3], double (*ra)[3], int *rType) {
	
	double potEnergyCell2B = 0.0;
	for (int ia = 0;ia < nListA-1; ia++)	{
		//information for atom i
		double rix , riy,  riz; //temporary coord. of atom i
		double raix,raiy, raiz; //temporary accel. of atom i

		int i     = listA[ia];
		int iType = rType[i];

		rix = r[i][0];
		riy = r[i][1];
		riz = r[i][2];

		raix = 0.0;
		raiy = 0.0;
		raiz = 0.0;
		
		//loop over j-th atom
		for (int ib = ia+1;ib < nListA; ib++){
			int j = listA[ib];
			int jType = rType[j];

			double dx,dy,dz; //interatomic distance in x,y,z
			double rr; //interatomic distance ^2

			dx  = rix - r[j][0];
			rr  = dx*dx;
			dy  = riy - r[j][1];
			rr += dy*dy;
			dz  = riz - r[j][2];
			rr += dz*dz;
		
			//if r^2 less than rcut2B, then calculate force/energy
			if (rr < rcut2_2B[iType][jType]) {
				//rr = (rr > rij2max) ? rij2max : rr;
				//Lookup table:
				//We want to lookup value v(rr) from table TAB: v(rr) = TAB[idxRR]
				//For table with table spacing dh, index in the table that rr falls, idxRR = (rr/dh)
				//However, TAB keeps discrete value. We can't directly compute fraction of TAB[idxRR] direction.
				//v(rr) will be calculated using intepolation instead.
				//Let's assume that position of idxRR falls between TAB[idxBase] to TAB[idxBase+1], idxBase is integer.
				//idxBase can be calculated from: idxBase = (int)(tabRR - idxBase);
				//let idxdh = idxRR-idxBase, Then v(rr) = TAB[idxBase] + TAB[idxdh]
				//TAB[idxBase] can be calculated, but TAB[idxdh] will be interpolated from TAB[idxBase] to TAB[idxBase+1]
				//Therefore, dv = TAB[idxBase+1] - TAB[idxBase]
				//v(rr) = TAB[idxBase] + idxdh*dv

				double idxRR      = rr*dh2Inv;
				int    idxBase    = (int)idxRR; //Round down to the nearest integer

				double idxdh	  = idxRR-idxBase;
				double vBase	  = forceTable[iType][jType][idxBase];
				/*
				double dv	  = forceTable[iType][jType][idxBase+1] - vBase;
				double vrr	  = dv*idxdh + vBase;
				*/
				double vrr	  = (forceTable[iType][jType][idxBase+1] - vBase)*idxdh + vBase;
				//printf("FRC-NonCyclic: %10.5f %10.6le %10.6le\n",rr, idxBase+idxRR,vrr);	

				
				//Calculate force in x, y, z
				double vForce;
				vForce    = vrr*dx;
				raix     += vForce; 
				ra[j][0] -= vForce; 

				vForce    = vrr*dy;
				raiy     += vForce; 
				ra[j][1] -= vForce; 

				vForce    = vrr*dz;
				raiz     += vForce; 
				ra[j][2] -= vForce; 

				//calculate potential energy
				potEnergyCell2B += (1.0 - idxdh)*potentialTable[iType][jType][idxBase] + 
						           idxdh*potentialTable[iType][jType][idxBase+1];

			} // end if rr < rcut2
			
		} //end for atom j
		
		//Sum force acting on i back
		ra[i][0] += raix;
		ra[i][1] += raiy;
		ra[i][2] += raiz;
	} //end for atom i
	return potEnergyCell2B;
}


