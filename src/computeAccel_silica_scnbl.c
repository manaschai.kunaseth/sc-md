/*------------------------------------------------------------
2014-10-20
Force computation routine for MPI-only algorithm 
Using SC for 2 body calculation and neighbor list for 3 body calculation
-------------------------------------------------------------*/
#include "computeAccel_silica.h"
#include "pot_silica.h"
#include "potential.h"
#include "computePattern.h"
#include "state.h"
#include "params.h"
#include "cell.h"
#include "simulate.h"
#include "neighborList.h"
#include "memory.h"
#include "timing.h"
#include <math.h>
#include <stdlib.h>

#if (DEBUG_LEVEL > 1)
#include <stdio.h>
#endif

#define MAX(a,b) (a > b)? a : b
#define NATOM_CELL2B_MAX 64
#define NATOM_CELL3B_MAX 64

double computeAccel_silica2B_SC_buildNeighborList(STATE *s, CELL_LIST* cl, COMPUTE_PATTERN_CELL* patCell, NEIGHBOR_LIST* nb);
double computeAccel_silica3B_NeighborList(STATE *s, CELL_LIST* cl, NEIGHBOR_LIST* nb);
void   buildExtraNeighborList(STATE *s,CELL_LIST* cl, COMPUTE_PATTERN_CELL* patCell, NEIGHBOR_LIST* nb);

//force routine call return energy.
double computeAccel_silica_cell2BAcyclic_buildNeighborList(int *listA, int *listB, int nListA, int nListB, 
				double (*r)[3], double (*ra)[3], int *rType, NEIGHBOR_LIST* nb);

double computeAccel_silica_cell2BCyclic_buildNeighborList(int *listA, int nListA, 
				double (*r)[3], double (*ra)[3], int *rType, NEIGHBOR_LIST* nb);

double computeAccel_silica_NeighborList3B(int *listB, int nListB, int **nbData, int *nNeighbor,
				double (*r)[3],  double (*ra)[3], int *rType, unsigned int *cMask);

void buildNeighborList_Cyclic(int *listA, int nListA, 

				double (*r)[3], int *rType, NEIGHBOR_LIST* nbl);

void buildNeighborList_Acyclic(int *listA, int *listB, int nListA, int nListB, 
				double (*r)[3], int *rType, NEIGHBOR_LIST* nbl);

//universal parameter for potential/force calculation.
static double dh2Inv;     //1.0/(dh^2)   : dh = pot_table spacing;
static double** rcut2_2B; //Cut-off^2 for each atom type
static POTENTIAL_SILICA_EXTENSION *potExt;
static double*** forceTable;
static double*** potentialTable;
static double rij2max;


//3Body
static double *r0_3B;
static double *r02_3B;
static double *dl_3B;
static double ***bb_3B;
static double ***cosb_3B;
static double ***c3b_3B;

//DEBUG purpose
#if (DEBUG_LEVEL > 4)
static STATE *priv_s;
#endif

void computeAccel_silica_scnbl(STATE* s, POTENTIAL* pot, PARAMS* p,CELL_INFO* cellInfoSet)
{
	CELL_LIST *cl2b = NULL;
	cl2b = makeCellList(cl2b,&cellInfoSet[INDEX_2B],s,p->boxSize,paramsGetRCut(2),p->commInfo.rCacheRadius,p->cell_nlayer[2]);

	//allocation memory for verlet list
	NEIGHBOR_LIST *nb3B = NULL;
	nb3B = createNeighborList(nb3B,s->nAtomOwn+s->nAtomCache,NATOM_CELL3B_MAX);

	//general parameter use within this file.
	dh2Inv = 1.0/(pot->dh2);
	rij2max = pot->rij2max; 
	potExt = (POTENTIAL_SILICA_EXTENSION*)pot->extension;
	rcut2_2B = potExt->rcut2_2b;
	forceTable = pot->forceTable;
	potentialTable = pot->potentialTable;

	r0_3B = potExt->r0;
	r02_3B = potExt->r02;
	dl_3B = potExt->dl;
	bb_3B = potExt->bb;
	cosb_3B = potExt->cosb;
	c3b_3B = potExt->c3b;
	//end defining general parameter within this file.

		
	timerStart(TIMING_COMPUTE_ACCEL_2B);
	s->potentialEnergy[INDEX_2B] += computeAccel_silica2B_SC_buildNeighborList(s,cl2b,&cellInfoSet[INDEX_2B].computePatternCell,nb3B);
	timerStop(TIMING_COMPUTE_ACCEL_2B);
	timerStart(TIMING_BUILD_NEIGHBOR_LIST);
	//Calculate neighbor of atom in the cached cells that normally not generated via SC algorithm (i.e. duplicated pairs with the neighbor nodes)
	buildExtraNeighborList(s,cl2b, &cellInfoSet[INDEX_3B].computePatternCell,nb3B);
	timerStop(TIMING_BUILD_NEIGHBOR_LIST);
	timerStart(TIMING_COMPUTE_ACCEL_3B);
	s->potentialEnergy[INDEX_3B] += computeAccel_silica3B_NeighborList(s,cl2b,nb3B);
	timerStop(TIMING_COMPUTE_ACCEL_3B);

	//free cell list data
	freeCellListData(cl2b);

	//free neighbor list memory
	freeNeighborList(nb3B);

}



double computeAccel_silica2B_SC_buildNeighborList(STATE *s, CELL_LIST* cl, COMPUTE_PATTERN_CELL* patCell, NEIGHBOR_LIST* nb)
{

	int nPattern = patCell->nPattern;
	int **cellOffset = patCell->cellPatternOffset;
	double potEnergy = 0.0;

	int **cellData = cl->cellAtomData;
	int *nAtomCell = cl->nAtomCell;

	double (*r)[3]  = s->r;
	double (*ra)[3] = s->ra;
	int  *rType   = s->rType;

	int vCellX = cl->vCell[0];
	int vCellY = cl->vCell[1];
	int vCellZ = cl->vCell[2];

	int vCellAll[3];
	//cell vector including caching cells.
	for (int a = 0;a < 3;a++)
		vCellAll[a] = cl->vCell[a] + cl->nCellCache[a];

	//variable use for calculating cell index
	int vCellAllX  = vCellAll[0];
	int vCellAllXY = vCellAll[0]*vCellAll[1];


	//Loop over all cell within domain.
	for (int cz = 0;cz < vCellZ;cz++)
	for (int cy = 0;cy < vCellY;cy++)
	for (int cx = 0;cx < vCellX;cx++) {

	//index of cell, c
		int c = cx + cy*vCellAllX + cz*vCellAllXY;

		for (int iPat = 0;iPat < nPattern;iPat++) {
			int cellChain1 = c + cellOffset[iPat][0];
			int cellChain2 = c + cellOffset[iPat][1];
			double patEnergy = 0.0;

			if (cellChain1 != cellChain2)
				patEnergy += computeAccel_silica_cell2BAcyclic_buildNeighborList(cellData[cellChain1],cellData[cellChain2], 
							nAtomCell[cellChain1], nAtomCell[cellChain2], r, ra, rType, nb);
			else
				patEnergy += computeAccel_silica_cell2BCyclic_buildNeighborList(cellData[cellChain1],nAtomCell[cellChain1],
							r, ra, rType, nb);
			potEnergy += patEnergy;

			#if (DEBUG_LEVEL > 1)
			printf("(%3d,%3d,%3d) c = %5d iPat = %5d: chain(%5d,%5d) done. PotE = %le\n",cx,cy,cz,c,iPat,cellChain1,cellChain2,patEnergy);
			#endif
		}


	}

return potEnergy;

}

double computeAccel_silica_cell2BAcyclic_buildNeighborList(int *listA, int *listB, int nListA, int nListB, 
				double (*r)[3],  double (*ra)[3], int *rType, NEIGHBOR_LIST* nbl) {

	double potEnergyCell2B = 0.0;

	//loop over i-th atom
	for (int ia = 0;ia < nListA; ia++){
		//information for atom i
		double rix , riy,  riz; //temporary coord. of atom i
		double raix,raiy, raiz; //temporary accel. of atom i

		int i     = listA[ia];
		int iType = rType[i];
		int **nb  = nbl->nbData;
		int *nnb  = nbl->nNeighbor;


		rix = r[i][0];
		riy = r[i][1];
		riz = r[i][2];

		raix = 0.0;
		raiy = 0.0;
		raiz = 0.0;
		
		//loop over j-th atom
		for (int ib = 0;ib < nListB; ib++){
			int j = listB[ib];
			int jType = rType[j];

			double dx,dy,dz; //interatomic distance in x,y,z
			double rr; //interatomic distance ^2

			dx  = rix - r[j][0];
			rr  = dx*dx;
			dy  = riy - r[j][1];
			rr += dy*dy;
			dz  = riz - r[j][2];
			rr += dz*dz;
		
			//if r^2 less than rcut2B, then calculate force/energy
			if (rr < rcut2_2B[iType][jType]) {
				//rr = (rr > rij2max) ? rij2max : rr;
				//Lookup table:
				//We want to lookup value v(rr) from table TAB: v(rr) = TAB[idxRR]
				//For table with table spacing dh, index in the table that rr falls, idxRR = (rr/dh)
				//However, TAB keeps discrete value. We can't directly compute fraction of TAB[idxRR] direction.
				//v(rr) will be calculated using intepolation instead.
				//Let's assume that position of idxRR falls between TAB[idxBase] to TAB[idxBase+1], idxBase is integer.
				//idxBase can be calculated from: idxBase = (int)(tabRR - idxBase);
				//let idxdh = idxRR-idxBase, Then v(rr) = TAB[idxBase] + TAB[idxdh]
				//TAB[idxBase] can be calculated, but TAB[idxdh] will be interpolated from TAB[idxBase] to TAB[idxBase+1]
				//Therefore, dv = TAB[idxBase+1] - TAB[idxBase]
				//v(rr) = TAB[idxBase] + idxdh*dv

				double idxRR      = rr*dh2Inv;
				int    idxBase    = (int)idxRR; //Round down to the nearest integer

				double idxdh	  = idxRR-idxBase;
				double vBase	  = forceTable[iType][jType][idxBase];
				/*
				double dv	  = forceTable[iType][jType][idxBase+1] - vBase;
				double vrr	  = dv*idxdh + vBase;
				*/
				double vrr	  = (forceTable[iType][jType][idxBase+1] - vBase)*idxdh + vBase;
				//double vrr = (1.0 - idxdh)*vBase + idxdh*potentialTable[iType][jType][idxBase+1];
				//printf("vrr = %le: rr = %le: rcut2_2B= %le\n",vrr,rr,rcut2_2B[iType][jType]);
				
				//Calculate force in x, y, z
				double vForce;
				vForce    = vrr*dx;
				raix     += vForce; 
				ra[j][0] -= vForce; 

				vForce    = vrr*dy;
				raiy     += vForce; 
				ra[j][1] -= vForce; 

				vForce    = vrr*dz;
				raiz     += vForce; 
				ra[j][2] -= vForce; 

				//calculate potential energy
				potEnergyCell2B += (1.0 - idxdh)*potentialTable[iType][jType][idxBase] + 
							   idxdh*potentialTable[iType][jType][idxBase+1];

				//if r^2 less than rcut3B, then store neighbor list
				if (rr < r02_3B[iType]) {
					nb[i][nnb[i]++] = j;
					nb[j][nnb[j]++] = i;
				}
				//if (rr < r02_3B[jType]) {
				//	nb[j][nnb[j]++] = i;
			 	//}	

			} // end if rr < rcut2
			
		} //end for atom j
		
		//Sum force acting on i back
		ra[i][0] += raix;
		ra[i][1] += raiy;
		ra[i][2] += raiz;
	} //end for atom i	
	return potEnergyCell2B;
}


double computeAccel_silica_cell2BCyclic_buildNeighborList(int *listA, int nListA, 
				double (*r)[3], double (*ra)[3], int *rType, NEIGHBOR_LIST* nbl) {

	double potEnergyCell2B = 0.0;
	for (int ia = 0;ia < nListA-1; ia++)	{
		//information for atom i
		double rix , riy,  riz; //temporary coord. of atom i
		double raix,raiy, raiz; //temporary accel. of atom i

		int i     = listA[ia];
		int iType = rType[i];
		int **nb  = nbl->nbData;
		int *nnb  = nbl->nNeighbor;

		rix = r[i][0];
		riy = r[i][1];
		riz = r[i][2];

		raix = 0.0;
		raiy = 0.0;
		raiz = 0.0;
		
		//loop over j-th atom
		for (int ib = ia+1;ib < nListA; ib++){
			int j = listA[ib];
			int jType = rType[j];

			double dx,dy,dz; //interatomic distance in x,y,z
			double rr; //interatomic distance ^2

			dx  = rix - r[j][0];
			rr  = dx*dx;
			dy  = riy - r[j][1];
			rr += dy*dy;
			dz  = riz - r[j][2];
			rr += dz*dz;
		
			//if r^2 less than rcut2B, then calculate force/energy
			if (rr < rcut2_2B[iType][jType]) {
				//rr = (rr > rij2max) ? rij2max : rr;
				//Lookup table:
				//We want to lookup value v(rr) from table TAB: v(rr) = TAB[idxRR]
				//For table with table spacing dh, index in the table that rr falls, idxRR = (rr/dh)
				//However, TAB keeps discrete value. We can't directly compute fraction of TAB[idxRR] direction.
				//v(rr) will be calculated using intepolation instead.
				//Let's assume that position of idxRR falls between TAB[idxBase] to TAB[idxBase+1], idxBase is integer.
				//idxBase can be calculated from: idxBase = (int)(tabRR - idxBase);
				//let idxdh = idxRR-idxBase, Then v(rr) = TAB[idxBase] + TAB[idxdh]
				//TAB[idxBase] can be calculated, but TAB[idxdh] will be interpolated from TAB[idxBase] to TAB[idxBase+1]
				//Therefore, dv = TAB[idxBase+1] - TAB[idxBase]
				//v(rr) = TAB[idxBase] + idxdh*dv

				double idxRR      = rr*dh2Inv;
				int    idxBase    = (int)idxRR; //Round down to the nearest integer

				double idxdh	  = idxRR-idxBase;
				double vBase	  = forceTable[iType][jType][idxBase];
				/*
				double dv	  = forceTable[iType][jType][idxBase+1] - vBase;
				double vrr	  = dv*idxdh + vBase;
				*/
				double vrr	  = (forceTable[iType][jType][idxBase+1] - vBase)*idxdh + vBase;

				
				//Calculate force in x, y, z
				double vForce;
				vForce    = vrr*dx;
				raix     += vForce; 
				ra[j][0] -= vForce; 

				vForce    = vrr*dy;
				raiy     += vForce; 
				ra[j][1] -= vForce; 

				vForce    = vrr*dz;
				raiz     += vForce; 
				ra[j][2] -= vForce; 

				//calculate potential energy
				potEnergyCell2B += (1.0 - idxdh)*potentialTable[iType][jType][idxBase] + 
							   idxdh*potentialTable[iType][jType][idxBase+1];

				//if r^2 less than rcut3B, then store neighbor list
				if (rr < r02_3B[iType]) {
					nb[i][nnb[i]++] = j;
					nb[j][nnb[j]++] = i;
				}
				//if (rr < r02_3B[jType]) {
				//	nb[j][nnb[j]++] = i;
				//}

			} // end if rr < rcut2
			
		} //end for atom j
		
		//Sum force acting on i back
		ra[i][0] += raix;
		ra[i][1] += raiy;
		ra[i][2] += raiz;
	} //end for atom i
	return potEnergyCell2B;
}


//Iterate all cells and call a Nearest-neighbor 3B-force computing routine within the cell  
double computeAccel_silica3B_NeighborList(STATE *s, CELL_LIST *cl, NEIGHBOR_LIST* nbl)
{
	double potEnergy = 0.0;

	int **nbData = nbl->nbData; //neighbor list data structure
	int *nnb  = nbl->nNeighbor; //number of neighbor per atoms

	int **cellData = cl->cellAtomData;
	int *nAtomCell = cl->nAtomCell;

	double (*r)[3]  = s->r;
	double (*ra)[3] = s->ra;
	int  *rType   = s->rType;
	unsigned int *rSCCacheMask = s->scCacheMask; //SC cache mask of each atom

	#if (DEBUG_LEVEL > 4)
	priv_s = s;
	#endif

	int vCellAll[3];
	//cell vector including caching cells.
	for (int a = 0;a < 3;a++)
		vCellAll[a] = cl->vCell[a] + cl->nCellCache[a];

	//variable use for calculating cell index
	int vCellAllX  = vCellAll[0];
	int vCellAllY  = vCellAll[1];
	int vCellAllZ  = vCellAll[2];
	int vCellAllXY = vCellAll[0]*vCellAll[1];

	//Loop over all cells
	for (int cz = 0;cz < vCellAllZ;cz++)
	for (int cy = 0;cy < vCellAllY;cy++)
	for (int cx = 0;cx < vCellAllX;cx++) {

		//index of cell, c
		int c = cx + cy*vCellAllX + cz*vCellAllXY;

		potEnergy += computeAccel_silica_NeighborList3B(cellData[c],nAtomCell[c], nbData, nnb, r, ra, rType, rSCCacheMask);
	}

	return potEnergy;

}


//Iterate atoms in a particular cell and compute 3 body forces from its neighbor list
double computeAccel_silica_NeighborList3B(int *listB, int nListB, int **nbData, int *nNeighbor,
				double (*r)[3],  double (*ra)[3], int *rType, unsigned int *cMask) {

	double potEnergyCell3B_vList = 0.0;

	//Choose central cell first
	for (int ib = 0;ib < nListB;ib++){
		int  i     = listB[ib];
		int  iType = rType[i];
		int *nbList = nbData[i];
		int nnbList = nNeighbor[i];
		unsigned int scMaskb = cMask[i];
		double rix = r[i][0];
		double riy = r[i][1];
		double riz = r[i][2];
		double r0iType  = r0_3B[iType];
		double dliType  = dl_3B[iType];
		
		double r02iType = r02_3B[iType];

		for (int ia = 0;ia < nnbList-1;ia++){
			int j = nbList[ia];
			int jType = rType[j];
			if (jType == iType) continue;

			unsigned int scMaskab = (scMaskb & cMask[j]);
			double rr;
			double dx,dy,dz;
			dx = rix - r[j][0];
			rr =  dx*dx;
			dy =  riy - r[j][1];
			rr += dy*dy;
			dz =  riz - r[j][2];
			rr += dz*dz;

			//No need to check interatomic radius, since it was already checked during the neighbor list building
			if (rr >= r02iType) continue;
					
			double rij = sqrt(rr);
			double rijInv = 1.0/rij;
			double uijx = dx*rijInv;
			double uijy = dy*rijInv;
			double uijz = dz*rijInv;
			double argij = MAX(1.0 / (rij - r0iType),-100);

			for (int ic = ia+1;ic < nnbList;ic++){
				int k = nbList[ic];
				int kType = rType[k];
				if ((kType == iType) || ((scMaskab & cMask[k]) != 0)) continue;  //no need to check k = j, since ic = ia+1
				
				dx = rix - r[k][0];
				rr =  dx*dx;
				dy =  riy - r[k][1];
				rr += dy*dy;
				dz =  riz - r[k][2];
				rr += dz*dz;

				//No need to check interatomic radius, since it was already checked during the neighbor list building
				if (rr >= r02iType) continue;
			
				double rik = sqrt(rr);
				double rikInv = 1.0/rik;
				double uikx = dx*rikInv;
				double uiky = dy*rikInv;
				double uikz = dz*rikInv;
				double argik = MAX(1.0 / (rik - r0iType),-100);

				//int min = MIN(j%24,k%24);
				//int max = MAX(j%24,k%24);
				//printf("TRIPLET:i(cen)-j-k %03d %03d %03d   %3d %3d %3d\n",i%24,min,max,i,j,k);
				//compute 3-Body force for j-i-k triplet.
				double ee = bb_3B[jType][iType][kType] * exp(dliType*(argij+argik));

				double drij  = -dliType*argij*argij;
				double drik  = -dliType*argik*argik;
				/* unused
				double d2rij = -2.0*drij*argij; 
				double d2rik = -2.0*drik*argik;
				double rdrij = rij*drij;
				double rdrik = rik*drik;
				*/
				double tt = uijx*uikx + uijy*uiky + uijz*uikz;

				double xx = tt - cosb_3B[jType][iType][kType];
				double xx2 =  xx*xx;
				//double eexx = ee*xx;
				double yy = 1.0/(1.0+c3b_3B[jType][iType][kType]*xx2);
				double eexxyy = ee*xx*yy;

				double dvijx = eexxyy*(uijx*drij*xx+2.0*yy*(uikx-tt*uijx)*rijInv);
				double dvijy = eexxyy*(uijy*drij*xx+2.0*yy*(uiky-tt*uijy)*rijInv);
				double dvijz = eexxyy*(uijz*drij*xx+2.0*yy*(uikz-tt*uijz)*rijInv);

				double dvikx = eexxyy*(uikx*drik*xx+2.0*yy*(uijx-tt*uikx)*rikInv);
				double dviky = eexxyy*(uiky*drik*xx+2.0*yy*(uijy-tt*uiky)*rikInv);
				double dvikz = eexxyy*(uikz*drik*xx+2.0*yy*(uijz-tt*uikz)*rikInv);

				//Force on i-atom (central atom)				
				ra[i][0] -= (dvijx + dvikx);
				ra[i][1] -= (dvijy + dviky);
				ra[i][2] -= (dvijz + dvikz);
			
				//Force on j-atom 	
				ra[j][0] += dvijx;
				ra[j][1] += dvijy;
				ra[j][2] += dvijz;
	
				//Force on k-atom 	
				ra[k][0] += dvikx;
				ra[k][1] += dviky;
				ra[k][2] += dvikz;

				//potential energy
				potEnergyCell3B_vList += eexxyy*xx*yy;
								
				#if (DEBUG_LEVEL > 4)
				//print out all triplets
				int tmpj = j;
				int tmpk = k;
				if (j > k) {
					tmpj = k;
					tmpk = j;
				}
				//printf("(%3d,%3d,%3d) c = %5d iPat = %5d: chain(%5d,%5d) done. \n",cx,cy,cz,c,iPat,cellChain1,cellChain2);
				printf("3B( %3ld , %3ld , %3ld ) potEn = %le\n", priv_s->rgid[i],priv_s->rgid[tmpj],priv_s->rgid[tmpk],eexxyy*xx*yy);
				#endif
	
			}// end atom k	

		} // end atom j

	} // end atom i
	
	return potEnergyCell3B_vList;
}

//Build neighbor list for atoms in the cached cells. These computing patterns are not typically generated via SC algorithm.
void buildExtraNeighborList(STATE *s,CELL_LIST* cl, COMPUTE_PATTERN_CELL* patCell,NEIGHBOR_LIST* nbl) {

	int nPattern = patCell->nPattern;
	unsigned int*	cMask = cl->cellSCCacheMask;

	int **cellData = cl->cellAtomData;
	int *nAtomCell = cl->nAtomCell;

	COMPUTE_PATTERN *compPat = simulateGetComputePattern(2);
	int (**pat)[3] = compPat->pattern;
	double (*r)[3]  = s->r;
	int  *rType   = s->rType;

	int vCell[3];
	for (int a = 0;a < 3;a++)
		vCell[a] = cl->vCell[a];

	int vCellAll[3];
	//cell vector including caching cells.
	for (int a = 0;a < 3;a++)
		vCellAll[a] = cl->vCell[a] + cl->nCellCache[a];

	//variable use for calculating cell index
	int vCellAllX  = vCellAll[0];
	int vCellAllY  = vCellAll[1];
	int vCellAllZ  = vCellAll[2];
	int vCellAllXY = vCellAll[0]*vCellAll[1];

	//Loop over all cells
	for (int cz = 0;cz < vCellAllZ;cz++)
	for (int cy = 0;cy < vCellAllY;cy++)
	for (int cx = 0;cx < vCellAllX;cx++) {

		//index of cell, c
		int c = cx + cy*vCellAllX + cz*vCellAllXY;

		//cached-cell: self calculation

		if (cMask[c] > 0) {
			//buildNeighborList_Cyclic(cellData[c], nAtomCell[c], r, rType, nbl);
			//border-cell: anti pattern calculation
			for (int iPat = 0;iPat < nPattern;iPat++) {
				int cAx = cx + pat[iPat][0][0];
				int cAy = cy + pat[iPat][0][1];
				int cAz = cz + pat[iPat][0][2];

				int cBx = cx + pat[iPat][1][0];
				int cBy = cy + pat[iPat][1][1];
				int cBz = cz + pat[iPat][1][2];
			
				if (   (cAx >= vCellAll[0]) || (cBx >= vCellAll[0])
				     ||(cAy >= vCellAll[1]) || (cBy >= vCellAll[1])
				     ||(cAz >= vCellAll[2]) || (cBz >= vCellAll[2])) continue;

				int cA = cAx + cAy*vCellAllX + cAz*vCellAllXY;
				int cB = cBx + cBy*vCellAllX + cBz*vCellAllXY;

				//buildNeighborList_Acyclic(cellData[cellChain1],cellData[cellChain2],
				//				nAtomCell[cellChain1], nAtomCell[cellChain2], r, rType, nbl);
				if (cA == cB)
					buildNeighborList_Cyclic(cellData[cA], nAtomCell[cB], r, rType, nbl);
				else
					buildNeighborList_Acyclic(cellData[cA],cellData[cB],
							nAtomCell[cA], nAtomCell[cB], r, rType, nbl);
								
				#if (DEBUG_LEVEL > 1)
				printf("(%3d,%3d,%3d) c = %5d iPat = %5d: chain(%5d,%5d) done. \n",cx,cy,cz,c,iPat,cA,cB);
				#endif
			} //end iPat

		} //end elseif
		
	}// end loop c


	#if (DEBUG_LEVEL > 2)
	for (int i = 0;i < s->nAtomOwn+s->nAtomCache;i++) {
		printf("atom %5d:",i);
		for (int j = 0;j < nbl->nNeighbor[i];j++)
			printf(" %5d", nbl->nbData[i][j]);
		printf("\n");
	}

	printf("SC cache mask:\n");

	double cellDimInv[3];

	for (int a = 0;a < 3;a++)
		cellDimInv[a] = 1.0 / cl->cellDimension[a];

	int vCellX    = vCellAll[0];
	int vCellXY   = vCellAll[0] * vCellAll[1];

	//loop all atoms to build cell list
	for (int i = 0;i < s->nAtomOwn+s->nAtomCache;i++) {
		int c = 0; //index of cell

		c +=           ((int)(r[i][0] * cellDimInv[0])); //x dimension
		c += vCellX  * ((int)(r[i][1] * cellDimInv[1])); //y dimension
		c += vCellXY * ((int)(r[i][2] * cellDimInv[2])); //z dimension

		printf("atom %5d: Cell = %5d Mask = %3d\n",i,c,s->scCacheMask[i]);

	}

	#endif

}


void buildNeighborList_Acyclic(int *listA, int *listB, int nListA, int nListB, 
				double (*r)[3], int *rType, NEIGHBOR_LIST* nbl) {


	//loop over i-th atom
	for (int ia = 0;ia < nListA; ia++){
		//information for atom i
		double rix , riy,  riz; //temporary coord. of atom i

		int i     = listA[ia];
		int iType = rType[i];
		int **nb  = nbl->nbData;
		int *nnb  = nbl->nNeighbor;

		rix = r[i][0];
		riy = r[i][1];
		riz = r[i][2];

		//loop over j-th atom
		for (int ib = 0;ib < nListB; ib++){
			int j = listB[ib];
			int jType = rType[j];

			double dx,dy,dz; //interatomic distance in x,y,z
			double rr; //interatomic distance ^2

			dx  = rix - r[j][0];
			rr  = dx*dx;
			dy  = riy - r[j][1];
			rr += dy*dy;
			dz  = riz - r[j][2];
			rr += dz*dz;

			//if r^2 less than rcut3B, then store neighbor list
			if (rr < r02_3B[iType]) {
				nb[i][nnb[i]++] = j;
				nb[j][nnb[j]++] = i;
			}
			//if (rr < r02_3B[jType]) {
			//	nb[j][nnb[j]++] = i;
			//}

		} //end for atom j
	} //end for atom i	

}


void buildNeighborList_Cyclic(int *listA, int nListA, 
				double (*r)[3], int *rType, NEIGHBOR_LIST* nbl) {

	for (int ia = 0;ia < nListA-1; ia++)	{
		//information for atom i
		double rix , riy,  riz; //temporary coord. of atom i

		int i     = listA[ia];
		int iType = rType[i];
		int **nb  = nbl->nbData;
		int *nnb  = nbl->nNeighbor;

		rix = r[i][0];
		riy = r[i][1];
		riz = r[i][2];

		//loop over j-th atom
		for (int ib = ia+1;ib < nListA; ib++){
			int j = listA[ib];
			int jType = rType[j];

			double dx,dy,dz; //interatomic distance in x,y,z
			double rr; //interatomic distance ^2

			dx  = rix - r[j][0];
			rr  = dx*dx;
			dy  = riy - r[j][1];
			rr += dy*dy;
			dz  = riz - r[j][2];
			rr += dz*dz;
		
			//if r^2 less than rcut3B, then store neighbor list
			if (rr < r02_3B[iType]) {
				nb[i][nnb[i]++] = j;
				nb[j][nnb[j]++] = i;
			}
			//if (rr < r02_3B[jType]) {
			//	nb[j][nnb[j]++] = i;
			//}

		} //end for atom j
		
	} //end for atom i
}


