#include "computeAccel_silica.h"
#include "computeAccel_lj.h"
#include "computePattern.h"
#include "state.h"
#include "potential.h"
#include "params.h"
#include "utils.h"
#include "cell.h"
#include "input.h"
#include "timing.h"
#include "mpi.h"
#include <stdio.h>


void computeAccel(STATE* s, POTENTIAL* pot, PARAMS* p, CELL_INFO *cellInfoSet)
{
	forceTimingRelation(TIMING_BARRIER0,TIMING_MD_STEP);	
	timerStart(TIMING_BARRIER0);
	MPI_Barrier(MPI_COMM_WORLD);
	timerStop(TIMING_BARRIER0);

	forceTimingRelation(TIMING_COMPUTE_ACCEL,TIMING_MD_STEP);	
	timerStart(TIMING_COMPUTE_ACCEL);

	//reset aceleration to 0
	resetState(s);

	INPUT_PARAMS* inp = getInputObj();
	
	//for silica potential
	if (strcmp(pot->name,"SIO2") == 0) {

#ifdef GPU
	        if (inp->parallelMode == PARMODE_MPI_OMP_GPU) {
			if (p->computationMethod != COMP_SC)
				terminateSingle("GPU parallelization mode does not support non-sc algorithm.",1);
			computeAccel_silica_thread_gpu(s,pot,p,cellInfoSet);
		}
		else
#endif 
		if (inp->parallelMode == PARMODE_MPI_OMP) {
			if (p->computationMethod != COMP_SC)
				terminateSingle("Hybrid parallelization mode does not support non-sc algorithm.",1);
			computeAccel_silica_thread(s,pot,p,cellInfoSet);
		} 
#ifdef MIC
		else if (inp->parallelMode == PARMODE_MPI_OMP_MIC)  {
			if (p->computationMethod != COMP_SC)
				terminateSingle("Mic parallelization mode does not support non-sc algorithm.",1);
			computeAccel_silica_thread_mic(s,pot,p,cellInfoSet);
		} 
#endif
		else if (inp->parallelMode == PARMODE_MPI) {
			//Determining computation method for mpi-only silica computation.
			if (p->computationMethod == COMP_SCNBL)
				computeAccel_silica_scnbl(s,pot,p,cellInfoSet);
			else if (p->computationMethod == COMP_SC)
				computeAccel_silica(s,pot,p,cellInfoSet);
			else
				terminateSingle("Computation method was not entered or incorrected.",1);
		} else
			terminateSingle("Parallelization mode is invalid or not entered.",1);
	}
	//for LJ potential
	else if (strcmp(pot->name,"LJ") == 0) {

#ifdef GPU
		//terminateSingle("GPU parallelization mode is not supported for LJ potential.",1);
	        if (inp->parallelMode == PARMODE_MPI_OMP_GPU) {
			if (p->computationMethod != COMP_SC)
				terminateSingle("GPU parallelization mode does not support non-sc algorithm.",1);
			else
				computeAccel_lj_thread_gpu(s,pot,p,cellInfoSet);
		}
		else
#endif 
		if (inp->parallelMode == PARMODE_MPI_OMP) {
			terminateSingle("Hybrid MPI/OpenMP parallelization mode is not supported for LJ potential.",1);
			if (p->computationMethod != COMP_SC)
				terminateSingle("Hybrid parallelization mode does not support non-sc algorithm.",1);
			//computeAccel_silica_thread(s,pot,p,cellInfoSet);
		} 
#ifdef MIC
		else if (inp->parallelMode == PARMODE_MPI_OMP_MIC)  {
			terminateSingle("Mic parallelization mode is not supported for LJ potential.",1);
			if (p->computationMethod != COMP_SC)
				terminateSingle("Mic parallelization mode does not support non-sc algorithm.",1);
			//computeAccel_silica_thread_mic(s,pot,p,cellInfoSet);
		} 
#endif
		else if (inp->parallelMode == PARMODE_MPI) {
			//Determining computation method for mpi-only silica computation.
			if (p->computationMethod == COMP_SCNBL) {
				terminateSingle("SCNBL is not support for 2-body potentials.",1);
				//computeAccel_silica_scnbl(s,pot,p,cellInfoSet);
			}
			else if (p->computationMethod == COMP_SC)
				computeAccel_lj(s,pot,p,cellInfoSet);
			else
				terminateSingle("Computation method was not entered or incorrected.",1);
		} else
			terminateSingle("Parallelization mode is invalid or not entered.",1);
	}


	else
		terminateSingle("Potential name unmatched in ComputeAcel.",1);


	timerStop(TIMING_COMPUTE_ACCEL);

	forceTimingRelation(TIMING_BARRIER1,TIMING_MD_STEP);	
	timerStart(TIMING_BARRIER1);
	MPI_Barrier(MPI_COMM_WORLD);
	timerStop(TIMING_BARRIER1);
}

