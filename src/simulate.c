#include "simulate.h"
#include "constants.h"
#include "computePattern.h"
#include "cell.h"
#include "memory.h"
#include <stdlib.h>

static SIMULATE_DATA *s_sim;

void initSimulate(SIMULATE_DATA *sim)
{
	s_sim = sim;
	s_sim->computePatternSet = (COMPUTE_PATTERN*)scMalloc(sizeof(COMPUTE_PATTERN)*MAX_NBODY);
	s_sim->cellInfoSet = (CELL_INFO*)scMalloc(sizeof(CELL_INFO)*MAX_NBODY);
}

void simulateSetCurrentStep(unsigned long step)
{
	s_sim->currentStep = step;
}

unsigned long  simulateGetCurrentStep()
{
	return s_sim->currentStep;
}

COMPUTE_PATTERN* simulateGetComputePattern(int nBody)
{
	return &s_sim->computePatternSet[nBody-1];
}

CELL_INFO* simulateGetCellInfo(int nBody)
{
	return &s_sim->cellInfoSet[nBody-1];
}

CELL_INFO* simulateGetCellInfoSet()
{
	return s_sim->cellInfoSet;
}
/*
void simulateSetComputePattern(COMPUTE_PATTERN *pat, int nBody)
{
	s_sim->computePatternSet[nBody-1] = pat;
}
*/

/*
void simulateSetComputePattern(COMPUTE_PATTERN *pat, int nBody)
{
	s_sim->computePatternSet[nBody-1] = pat;
}
*/

