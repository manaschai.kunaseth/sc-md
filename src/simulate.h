#ifndef __SIMULATE_H__
#define __SIMULATE_H__

#include "constants.h"
#include "state.h"
#include "nodeComm.h"
#include "cell.h"
#include "computePattern.h"

typedef struct simulate_data_st {
	STATE 				mainState;
	ATOM_CACHE_DATA			atomCache;
	//COMPUTE_PATTERN**		computePatternSet;
	COMPUTE_PATTERN*		computePatternSet;
	CELL_INFO*			cellInfoSet;

	unsigned long			currentStep;

} SIMULATE_DATA;

void initSimulate(SIMULATE_DATA*);
void simulateSetCurrentStep(unsigned long step);
unsigned long simulateGetCurrentStep();
//void simulateGetComputePatternSet(COMPUTE_PATTERN_SET* cpSet);
COMPUTE_PATTERN* simulateGetComputePattern(int nBody);
void simulateSetComputePattern(COMPUTE_PATTERN*, int nBody);
CELL_INFO* simulateGetCellInfo(int nBody);
CELL_INFO* simulateGetCellInfoSet();

#endif
