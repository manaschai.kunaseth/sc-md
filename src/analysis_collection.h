#ifndef __ANALYSIS_COLLECTION_H__
#define __ANALYSIS_COLLECTION_H__

#define ANALYSIS_COLLECTION_SIZE_RDF 3
#define ANALYSIS_COLLECTION_SIZE_VACF 3
#define ANALYSIS_COLLECTION_SIZE_MSD 3

#define ANALYSIS_VACF_BUFFER 20
#define ANALYSIS_VACF_INTERVAL 50
#define ANALYSIS_VACF_NCOLLECT 2000

#include "constants.h"
#include "state.h"
#include "params.h"

typedef struct analysis_store_st {

	int data_per_atom;
	int enabled_flag[ANALYSIS_COLLECTION_MAX];

	double *rdfHist;
	int rdfHist_nbin;
	double rdfHist_rcut;
	double rdfHist_binLength;

	double *vacfAcc;
	double **vacfBuffer;
	double (**vOrig)[3];
	int vacf_nBuffer;
	int vacf_nCollect;
	int vacf_accumulateCount;
	int vacf_collectCall;
	int vacf_freq;

	//double (*msd_rOrig)[3]; 

} ANALYSIS_STORE;

void analysis_collection_init(STATE* s,PARAMS *p);
void analysis_collection_eval(STATE* s,PARAMS *p);
void analysis_collection_collect(STATE* s,PARAMS *p, int* enabledFlag,int dataRecordLength); 
void analysis_collection_writeOutput();

void analysis_rdf_fill(STATE* s, PARAMS *p,double *sendbuffer, int recordLength,int recordOffset); 
void analysis_vacf_fill(STATE* s, PARAMS *p,double *sendbuffer, int recordLength,int recordOffset); 

void analysis_rdf_compute(STATE* s, PARAMS *p,ANALYSIS_STORE*, double *collVec, int collRowLength,int recordOffset);
void analysis_vacf_compute(STATE* s, PARAMS *p,ANALYSIS_STORE*,double *collVec, int collRowLength,int recordOffset);

void analysis_rdf_writeOutput(ANALYSIS_STORE*);
void analysis_vacf_writeOutput(ANALYSIS_STORE*);

#endif
