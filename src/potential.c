#include "constants.h"
#include "potential.h"
#include "pot_silica.h"
#include "pot_lj.h"
#include "params.h"
#include "utils.h"
#include "computePattern.h"
#include "simulate.h"
#include <string.h>



void initPotential(POTENTIAL* pot)
{
	//CAUTION: every potential have to initialize atom import radius corresponding to their cutoff.
	//         This can be done by calling paramsSetCacheRadius(double r);

	INPUT_PARAMS* inp = getInputObj();

	pot->tableNBin = inp->potentialTableNBin;
	
	//If potential is SIO2
	if (strcmp(inp->potentialName,"SIO2") == 0){
		reportPrint("Potential name matched. Initializing SIO2 potential parameters...");
		strcpy(pot->name,"SIO2");
		paramsSetPotentialType(POT_SIO2);
		initPotential_silica(pot);
	
		//initilizing computation pattern
		//COMPUTE_PATTERN* pattern2B = NULL;
		//COMPUTE_PATTERN* pattern3B = NULL;
		
		//initializing computation pattern for SC algorithm for both 2 body and 3 body calculations 
		if (paramsGetComputationMethod() == COMP_SC) {
			COMPUTE_PATTERN* pattern2B = simulateGetComputePattern(2);
			COMPUTE_PATTERN* pattern3B = simulateGetComputePattern(3);
			
			if (inp->nlayer[2] == 1) {	
				pattern2B  = initPattern(pattern2B,COMPUTE_PATTERN_FLAG_2B);
			} else if (inp->nlayer[2] == 2) {	
				pattern2B  = initPattern(pattern2B,COMPUTE_PATTERN_FLAG_2B_2LAYER);
			} else { 
				terminateSingle("Invalid number of cell layer.",1);
			}
			//pattern2B  = initPattern(pattern2B,COMPUTE_PATTERN_FLAG_2B);
			pattern3B  = initPattern(pattern3B,COMPUTE_PATTERN_FLAG_3B);
			//simulateSetComputePattern(pattern2B,2);
			//simulateSetComputePattern(pattern3B,3);
			paramsSetNBodyFlag(NBODY_FLAG_2B | NBODY_FLAG_3B);
		} else if (paramsGetComputationMethod() == COMP_SCNBL) {
 			COMPUTE_PATTERN* pattern2B = simulateGetComputePattern(2);
			COMPUTE_PATTERN* pattern3B = simulateGetComputePattern(3);

			pattern2B  = initPattern(pattern2B,COMPUTE_PATTERN_FLAG_2B);
			//pattern3B  = initPattern(pattern3B,COMPUTE_PATTERN_FLAG_ANTI2B);
			pattern3B  = initPattern(pattern3B,COMPUTE_PATTERN_FLAG_2B);
			//simulateSetComputePattern(pattern2B,2);
			//simulateSetComputePattern(pattern3B,3);
			paramsSetNBodyFlag(NBODY_FLAG_2B | NBODY_FLAG_3B);
		}

	} //end if pot=SIO2
	//If potential is SIO2
	else if (strcmp(inp->potentialName,"LJ") == 0){
		reportPrint("Potential name matched. Initializing Lennard-Jones potential parameters...");
		strcpy(pot->name,"LJ");
		paramsSetPotentialType(POT_LJ);
		initPotential_lj(pot);
	
		//initilizing computation pattern
		//COMPUTE_PATTERN* pattern2B = NULL;
		//COMPUTE_PATTERN* pattern3B = NULL;
		
		//initializing computation pattern for SC algorithm for both 2 body and 3 body calculations 
		if (paramsGetComputationMethod() == COMP_SC) {
			COMPUTE_PATTERN* pattern2B = simulateGetComputePattern(2);
			//COMPUTE_PATTERN* pattern3B = simulateGetComputePattern(3);
			
			if (inp->nlayer[2] == 1) {	
				pattern2B  = initPattern(pattern2B,COMPUTE_PATTERN_FLAG_2B);
			} else if (inp->nlayer[2] == 2) {	
				pattern2B  = initPattern(pattern2B,COMPUTE_PATTERN_FLAG_2B_2LAYER);
			} else { 
				terminateSingle("Invalid number of cell layer.",1);
			}
			//pattern2B  = initPattern(pattern2B,COMPUTE_PATTERN_FLAG_2B);
			//pattern3B  = initPattern(pattern3B,COMPUTE_PATTERN_FLAG_3B);
			//simulateSetComputePattern(pattern2B,2);
			//simulateSetComputePattern(pattern3B,3);
			//paramsSetNBodyFlag(NBODY_FLAG_2B | NBODY_FLAG_3B);
			paramsSetNBodyFlag(NBODY_FLAG_2B);
		} 
		/*No 3-body in LJ
		else if (paramsGetComputationMethod() == COMP_SCNBL) {
 			COMPUTE_PATTERN* pattern2B = simulateGetComputePattern(2);
			COMPUTE_PATTERN* pattern3B = simulateGetComputePattern(3);

			pattern2B  = initPattern(pattern2B,COMPUTE_PATTERN_FLAG_2B);
			//pattern3B  = initPattern(pattern3B,COMPUTE_PATTERN_FLAG_ANTI2B);
			pattern3B  = initPattern(pattern3B,COMPUTE_PATTERN_FLAG_2B);
			//simulateSetComputePattern(pattern2B,2);
			//simulateSetComputePattern(pattern3B,3);
			paramsSetNBodyFlag(NBODY_FLAG_2B | NBODY_FLAG_3B);
		}
		*/

	} //end if pot=LJ
	else 
	{
		terminateSingle("Unknown potential specified.",1);
	}
	
	
}

