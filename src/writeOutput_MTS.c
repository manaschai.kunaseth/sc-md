#include <stdio.h>
#include "simulate.h"
#include "state.h"
#include "input.h"
#include "mpi.h"
#include "params.h"
#include "utils.h"
#include "atomType.h"
#include "fileUtils.h"
#include "timing.h"
#include "memory.h"

#define MTS_HEADER_COUNT 1+9+6
#define MTS_DOUBLE_PER_ATOM 8
#define MPI_TAG_AGGREGATE_WRITE_MTS 1100

static void packAtomWriteMTSBuffer(double* atomDataBuffer,int nAtomOwn,PARAMS *p,STATE *s);
static void writeAtomWriteMTSBuffer(double* atomDataBuffer,int dataBufferPerNode, int nNode, char* fullFilePath, PARAMS* p);


void writeOutput_mts(STATE* s, PARAMS* p, WRITE_MODES mode)
{

	/*Format: (int:myId), (int:nAtom), (int:timestep), ([double]*3*3:hMatrix),(double*3:boxSize)
		  
		  (int:nAtomType), 
		For each atom type  :([char*5][int])
		For each atom record:(double:atomdata) (double*3:x,y,z Coord) (double*3:x,y,z velocity)
	*/
		
	char dirName[1000];
	char fileName[100];
	char fullFilePath[1100];

	createOutputDir(simulateGetCurrentStep(),dirName);
	sprintf(fileName,"%07d.mts",getMyId());
	sprintf(fullFilePath, "%s/%s",dirName,fileName);
	INPUT_PARAMS* inp = getInputObj();

	reportPrintInt("Writing checkpoint MTS: Aggregate write for a group of MPI tasks = ",inp->writeAggregateOutputMTS);

	FILE *fp;
	
	if (mode == WRITE_MTS_ASCII)
	{
		if (inp->writeAggregateOutputMTS == 1) {
			double globCoord[3];
			double corner[3];
			double (*r)[3] = s->r;
			double (*rv)[3] = s->rv;
			int *rType = s->rType;
			unsigned long *rgid = s->rgid;
			int   nAtomOwn = s->nAtomOwn;
			char  atomTypeName[ATOM_TYPE_MAX][ATOM_TYPE_SYMBOL_MAX];
			//store atomType
			for (int i = 0;i < p->nAtomType;i++)
				symbolFromType(i,atomTypeName[i]);

			//calculating corner coordinate
			for (int a = 0;a < 3;a++)
				corner[a] = s->cornerCoord[a]*p->hMatrix[a][a];

			fp = fopen(fullFilePath, "w");

			fprintf (fp, "%d\n", s->nAtomOwn);
			//fprintf (fp, "%ld\n", simulateGetCurrentStep());
			fprintf (fp, "%20.12le %20.12le %20.12le\n", p->hMatrix[0][0], p->hMatrix[0][1], p->hMatrix[0][2]);
			fprintf (fp, "%20.12le %20.12le %20.12le\n", p->hMatrix[1][0], p->hMatrix[1][1], p->hMatrix[1][2]);
			fprintf (fp, "%20.12le %20.12le %20.12le\n", p->hMatrix[2][0], p->hMatrix[2][1], p->hMatrix[2][2]);
			fprintf (fp, "%20.12le %20.12le %20.12le\n", s->cornerCoord[0],s->cornerCoord[1],s->cornerCoord[2]);
			fprintf (fp, "%20.12le %20.12le %20.12le\n", p->boxSize[0],p->boxSize[1],p->boxSize[2]);
			//write atom data
			for (int i = 0;i < nAtomOwn;i++) {
				//convert coordinate to global
				for (int a = 0;a < 3;a++)
					globCoord[a] = (r[i][a] + corner[a])/p->hMatrix[a][a]; 
				fprintf(fp, "%5s %012ld %20.12le %20.12le %20.12le %20.12le %20.12le %20.12le\n",
						atomTypeName[rType[i]], rgid[i],
						globCoord[0],globCoord[1],globCoord[2],
						rv[i][0],rv[i][1],rv[i][2]);
			}
			if (p->isSelectiveCoordUpdate == 1) {
				for (int i = 0;i < nAtomOwn;i++) {
					fprintf(fp, "%d\n", (int)s->rUpdateFlag[i]);
				} 
			} //end if selective coord update
			fclose(fp);
		} // end writeAggregateOutputMTS == 1
		else { // for writeAggregateOutputMTS > 1
			int nAtomOwn = s->nAtomOwn;
			int maxNatom = nAtomOwn; 
			MPI_Allreduce(&nAtomOwn,&maxNatom,1,MPI_INT,MPI_MAX,MPI_COMM_WORLD);

			int aggregateId = getMyId() % inp->writeAggregateOutputMTS;
			int dataBufferPerNode = maxNatom*MTS_DOUBLE_PER_ATOM+MTS_HEADER_COUNT;
			int masterId = getMyId()-(aggregateId);
			//printf("MPI_task = %5d, aggregateId = %5d, masterId = %5d\n",getMyId(),aggregateId,masterId);
			//for master aggregate IO node
			if (aggregateId == 0) { 
				double* atomDataBuffer = (double*)scMalloc(sizeof(double)*inp->writeAggregateOutputMTS*dataBufferPerNode);
				MPI_Request* req = (MPI_Request*)scMalloc(sizeof(MPI_Request)*inp->writeAggregateOutputMTS);
				for (int i = 1;i < inp->writeAggregateOutputMTS;i++) {
					MPI_Irecv(&atomDataBuffer[dataBufferPerNode*i], 
						   dataBufferPerNode, MPI_DOUBLE, getMyId()+i,MPI_TAG_AGGREGATE_WRITE_MTS, MPI_COMM_WORLD, &req[i]);
				}
				
				packAtomWriteMTSBuffer(atomDataBuffer,nAtomOwn,p,s);
				MPI_Waitall(inp->writeAggregateOutputMTS-1, &req[1], MPI_STATUSES_IGNORE); //Wait for req[1] to inp->writeAggregateOutputMTS

				writeAtomWriteMTSBuffer(atomDataBuffer,dataBufferPerNode, inp->writeAggregateOutputMTS, fullFilePath,p);

				scFree(atomDataBuffer);
				scFree(req);

			} else { //for slave aggregate IO node 
				double* atomDataBuffer = (double*)scMalloc(sizeof(double)*dataBufferPerNode);
				packAtomWriteMTSBuffer(atomDataBuffer,nAtomOwn,p,s);
				MPI_Send(atomDataBuffer, nAtomOwn*(MTS_DOUBLE_PER_ATOM)+MTS_HEADER_COUNT, 
						MPI_DOUBLE, masterId, MPI_TAG_AGGREGATE_WRITE_MTS,MPI_COMM_WORLD);

				scFree(atomDataBuffer);
			}

		} //end writeAggregateOutputMTS > 1
	}
	else if (mode == WRITE_MTS_BINARY)
	{
		terminateSingle("WRITE_MTS_BINARY has not been implemented yet.",1);
		fp = fopen(fullFilePath,"wb");

		fwrite(p,sizeof(PARAMS),1,fp);
		fclose(fp);
	/*
	fwrite(getMyId(),sizeof(int),1,fp);
	fwrite(s->nAtomOwn,sizeof(int),1,fp);
	fwrite(simulateGetCurrentStep(),sizeof(int),1,fp);
	for (int i = 0;i < 3;i++)
		for (int j = 0;j < 3;j++)
			fwrite(p->hMatrix[i][j],sizeof(double),1,fp);

	fwrite(p->boxSize,sizeof(double),3,fp);

	fwrite(s->nAtomOwn,sizeof(int),1,fp);


	fwrite(x, sizeof(x[0]), sizeof(x)/sizeof(x[0]), fp);
	*/
	}
	else {
		terminateSingle("Unrecognized MTS_WRITE_MODES",1);
	}


	//write current.in file: Tell number of current step.
	if (getMyId() == 0) {
		//write 2 times: 1. to the snapshot directory
		//		 2. to the current directory
		sprintf(fullFilePath, "%s/current.in",dirName);
		fp = fopen(fullFilePath, "w");
		fprintf (fp, "%ld\n", simulateGetCurrentStep());
		if (mode == WRITE_MTS_ASCII)
			fprintf(fp,"ASCII\n");
		else
			fprintf(fp,"BINARY\n");
		fprintf(fp, "%d\n",p->isSelectiveCoordUpdate);
		fprintf(fp, "%d\n", inp->writeAggregateOutputMTS);
		fclose(fp);


		sprintf(fullFilePath, "current.in");
		fp = fopen(fullFilePath, "w");
		fprintf (fp, "%ld\n", simulateGetCurrentStep());
		if (mode == WRITE_MTS_ASCII)
			fprintf(fp,"ASCII\n");
		else
			fprintf(fp,"BINARY\n");
		fprintf(fp, "%d\n", p->isSelectiveCoordUpdate);
		fprintf(fp, "%d\n", inp->writeAggregateOutputMTS);
		fclose(fp);
	}
}

void packAtomWriteMTSBuffer(double* atomDataBuffer,int nAtomOwn,PARAMS *p,STATE *s) {
	double globCoord[3];
	double corner[3];
	double (*r)[3] = s->r;
	double (*rv)[3] = s->rv;
	int *rType = s->rType;
	unsigned long *rgid = s->rgid;

	//calculating corner coordinate
	for (int a = 0;a < 3;a++)
		corner[a] = s->cornerCoord[a]*p->hMatrix[a][a];

	int count = 0;

	//pack node header
	atomDataBuffer[count++] = nAtomOwn;
	for (int i = 0;i < 3;i++)
	for (int j = 0;j < 3;j++) 
		atomDataBuffer[count+3*i+j] = p->hMatrix[i][j];
	count+=9;
	
	for (int a = 0;a < 3;a++) {
		atomDataBuffer[count  +a] = s->cornerCoord[a];
		atomDataBuffer[count+3+a] = p->boxSize[a];
	}
	count+=6;


	//pack atom data
	for (int i = 0;i < nAtomOwn;i++) {
		//convert coordinate to global
		atomDataBuffer[count++] = dataFromId(rType[i],rgid[i]); 
		for (int a = 0;a < 3;a++) {
			atomDataBuffer[count  +a] = (r[i][a] + corner[a])/p->hMatrix[a][a];
			atomDataBuffer[count+3+a] = rv[i][a];
		}
		count+=6;
	}
	for (int i = 0;i < nAtomOwn;i++) {
		atomDataBuffer[count++] = s->rUpdateFlag[i];
	}
}

void writeAtomWriteMTSBuffer(double* atomDataBuffer,int dataBufferPerNode, int nNode, char* fullFilePath, PARAMS* p) {

	char  atomTypeName[ATOM_TYPE_MAX][ATOM_TYPE_SYMBOL_MAX];
	//store atomType
	for (int i = 0;i < p->nAtomType;i++)
		symbolFromType(i,atomTypeName[i]);

	FILE *fp;
	fp = fopen(fullFilePath, "w");
	for (int iNode = 0;iNode < nNode;iNode++) {
		int offset = iNode*dataBufferPerNode;
		int nAtom = atomDataBuffer[offset];

		fprintf (fp, "%d\n", nAtom);
		//fprintf (fp, "%ld\n", simulateGetCurrentStep());
		fprintf (fp, "%20.12le %20.12le %20.12le\n", atomDataBuffer[offset+ 1], atomDataBuffer[offset+ 2], atomDataBuffer[offset+ 3]); //hMatrix
		fprintf (fp, "%20.12le %20.12le %20.12le\n", atomDataBuffer[offset+ 4], atomDataBuffer[offset+ 5], atomDataBuffer[offset+ 6]); //hMatrix
		fprintf (fp, "%20.12le %20.12le %20.12le\n", atomDataBuffer[offset+ 7], atomDataBuffer[offset+ 8], atomDataBuffer[offset+ 9]); //hMatrix
		fprintf (fp, "%20.12le %20.12le %20.12le\n", atomDataBuffer[offset+10], atomDataBuffer[offset+11], atomDataBuffer[offset+12]); //corner
		fprintf (fp, "%20.12le %20.12le %20.12le\n", atomDataBuffer[offset+13], atomDataBuffer[offset+14], atomDataBuffer[offset+15]); //boxSize
		offset += MTS_HEADER_COUNT;

		//write atom data
		for (int i = 0;i < nAtom;i++) {
			int rType = typeFromData(atomDataBuffer[offset]);
			unsigned long  rgid = idFromData(atomDataBuffer[offset]);

			fprintf(fp, "%5s %012ld %20.12le %20.12le %20.12le %20.12le %20.12le %20.12le\n",
					atomTypeName[rType], rgid,
					atomDataBuffer[offset+1],atomDataBuffer[offset+2],atomDataBuffer[offset+3],  //r
					atomDataBuffer[offset+4],atomDataBuffer[offset+5],atomDataBuffer[offset+6]); //rv
			offset+=7;
		} //end write atom record
		if (p->isSelectiveCoordUpdate == 1) {
			for (int i = 0;i < nAtom;i++) {
				fprintf(fp, "%d\n", (int)atomDataBuffer[offset++]);
			} 
		} //end if selective coord update
		fprintf(fp,"\n");
	} //end iNode
	fclose(fp);
}


