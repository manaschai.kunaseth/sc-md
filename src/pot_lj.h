#ifndef __POT_LJ_H__
#define __POT_LJ_H__

#include "potential.h"
#include "input.h"
#include "params.h"

typedef struct potential_lj_st {
	double**	rcut2_2b; //cut-off^2 for each atom type of ij
} POTENTIAL_LJ_EXTENSION;


void initPotential_lj(POTENTIAL *pot);
#endif
