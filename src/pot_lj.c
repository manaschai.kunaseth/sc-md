#include "potential.h"
#include "pot_lj.h"
#include "constants.h"
#include "params.h"
#include "atomType.h"
#include "utils.h"
#include "memory.h"
#include <math.h>
#include <stdlib.h>

// 2 types: silicon (Si) & oxygen (O)
#define NTYPE  2
// To avoid table lookup overflow, we allocate a little more than we need.
#define EXPANDED_BUFFER 10
//const int NC = 6;
const int NC = NTYPE;

double Ac[NC]={1.0, 1.0}, Sg[NC]={1.0, 1.0}; 
double RCUT = 2.5;

void initPotential_lj(POTENTIAL *pot)
{

  double *vtab0[NC][NC];
  double *vtab1[NC][NC];
  double *vtab2[NC][NC];

  double atomMass[NC];
 
  INPUT_PARAMS* inp = getInputObj();

  //set 2- and 3-body cutoff
  //paramsSetRCut(2,RCUT/BOHR);
  paramsSetRCut(2,RCUT);
  //paramsSetRCut(3,r0_sio/BOHR);

  pot->tableNBin = inp->potentialTableNBin;
  int nBin = pot->tableNBin;

  int idp1 = typeFromSymbol("P1");
  int idp2 = typeFromSymbol("P2");


  paramsGetMass(atomMass);

  double deltaT = paramsGetDeltaT();
  if (paramsGetNType() != NC)
	terminateSingle("Specified atom type(s) are not not matched with potential",1);  

  reportPrint("Allocating temporary variables...");
  for (int i = 0;i < NC;i++)
	for (int j = 0;j < NC;j++){
		vtab0[i][j] = (double*)scMalloc(sizeof(double)*(pot->tableNBin+1));
		vtab1[i][j] = (double*)scMalloc(sizeof(double)*(pot->tableNBin+1));
		vtab2[i][j] = (double*)scMalloc(sizeof(double)*(pot->tableNBin+1));
	}


//start LJ

    double ri, ri2, ri6, ri12;
    double currentr, currentr2, voffset, foffset;
    double dr2,rcab;
    double aa[NC][NC],ss[NC][NC];

// V(r)[i][j] = aa[i][j]*( (ss[i][j]/r)^12 - (ss[i][j]/r)^6 )

    for (int ia=0; ia<NC; ia++) {
        for (int ib=0; ib<NC; ib++) {
            aa[ia][ib]=0.5*(Ac[ia]+Ac[ib]);   // Energy coefficient
            ss[ia][ib]=0.5*(Sg[ia]+Sg[ib]);   // Sigma
        }
    }

    /*
    //aa&ss values taken from Srikanth et al., Nature, p554, vol. 398 (1998)
        aa[0][0]=1.0;
        aa[0][1]=1.5;
        aa[1][0]=1.5;
        aa[1][1]=0.5;
        ss[0][0]=1.0;
        ss[0][1]=0.8;
        ss[1][0]=0.8;
        ss[1][1]=0.88;
    */

// For each sample point, calculate the value of potential table elements
// First creat the tables without the energy cofficient, then multiply aa[][].
//dr2=RCUT*RCUT/NTMAX;
    //double tab_scale = 111111.0;
    for (int ia=0; ia<NC; ia++) {
        for (int ib=0; ib<NC; ib++) {

            rcab=RCUT*ss[ia][ib];
            //dr2=rcab*rcab/NTMAX;
            dr2=rcab*rcab/nBin;
            // Compute potential energy at RCUT
            ri = ss[ia][ib] / rcab;
            ri2 = ri * ri;
            ri6 = ri2 * ri2 * ri2;
            ri12 = ri6 * ri6;
            voffset = 4.0 * (ri12 - ri6);
            foffset = (48.0 * ri12 - 24.0 * ri6) * ri;

            for(int i = 1; i<=nBin; i++) {
            //for(int i = 1; i<=nBin; i++) {
                currentr2 = (double)i * dr2;
                currentr = sqrt(currentr2);
                ri = ss[ia][ib] / currentr;
                ri2 = ri * ri;
                ri6 = ri2 * ri2 * ri2;
                ri12 = ri6 * ri6;
                vtab0[ia][ib][i] = 4.0 * (ri12 - ri6) - voffset + foffset * (currentr - rcab);
                vtab1[ia][ib][i] = (48.0 * ri12 - 24.0 * ri6) * ri2 - foffset * ri;
                vtab0[ia][ib][i] *= aa[ia][ib];
                vtab1[ia][ib][i] *= aa[ia][ib];
                //vtab0[ia][ib][i] *= tab_scale*aa[ia][ib];
                //vtab1[ia][ib][i] *= tab_scale*aa[ia][ib];
		//if (ia == 0 && ib == 0)
		//	printf("Table: %5d %10.5f %12.6le %12.6le\n",i,ri,vtab0[ia][ib][i],vtab1[ia][ib][i]);
            }

            //Take care of the last point
            vtab0[ia][ib][0] = 0.0;
            vtab1[ia][ib][0] = 0.0;
            //vtab0[ia][ib][nBin-1] = 0.0;
            //vtab1[ia][ib][nBin-1] = 0.0;
            //vtab0[ia][ib][nBin] = 0.0;
            //vtab1[ia][ib][nBin] = 0.0;
        }
    }

  double rc2 = RCUT*RCUT;
  //double rc2 = RCUT*RCUT/BOHR/BOHR;
  dr2 = rc2 / nBin;
  //double dr2i = 1.0 / dr2;
  //printf("dr2i = %10.5f\n",dr2i);
  double rij2max = rc2 - dr2;			// To avoid segmentation fault of potential arrays



	//initializing potential table
	reportPrint("Allocating 2-body potential/force table...");
 	pot->forceTable     = (double***)(scMalloc(sizeof(double**)*NC));
	pot->potentialTable = (double***)(scMalloc(sizeof(double**)*NC));
 	for(int ia = 0;ia < NC;ia++) {
		pot->forceTable[ia]     = (double**)(scMalloc(sizeof(double*)*NC));
		pot->potentialTable[ia] = (double**)(scMalloc(sizeof(double*)*NC));
  		for(int ib = 0;ib < NC;ib++){
			pot->forceTable[ia][ib]     = (double*)(scMalloc(sizeof(double)*(nBin+1+EXPANDED_BUFFER)));
			pot->potentialTable[ia][ib] = (double*)(scMalloc(sizeof(double)*(nBin+1+EXPANDED_BUFFER)));
		}
	}

	//reset all value
	for (int ia = 0;ia < NC;ia++){
		for (int ib = 0;ib < NC;ib++){
			double *ftab = pot->forceTable[ia][ib];
			double *ptab = pot->potentialTable[ia][ib];
			for (int k = 0;k < nBin+1+EXPANDED_BUFFER;k++){
				ptab[k] = 0.0;
				ftab[k] = 0.0;
			}
		}
	}
		
	// copy potential table data
	reportPrint("Saving potential/force table...");
	for (int ia = 0;ia < NC;ia++){
		for (int ib = 0;ib < NC;ib++){
			double *ftab = pot->forceTable[ia][ib];
			double *ptab = pot->potentialTable[ia][ib];
			for (int k = 1;k <= nBin;k++){
				ptab[k] = vtab0[ia][ib][k];
				ftab[k] = vtab1[ia][ib][k];
			}
		}
	}
	
/*	
	for (int i = 0;i < pot->tableNBin+1;i++)
		printf("%6d  %12.7lf: %12.7lf %12.7lf %12.7lf %12.7lf %12.7lf %12.7lf\n",i,rc1/nBin*i*BOHR,pot->forceTable[idSi][idSi][i],pot->forceTable[idSi][idO][i],pot->forceTable[idO][idO][i],pot->potentialTable[idSi][idSi][i],pot->potentialTable[idSi][idO][i],pot->potentialTable[idO][idO][i]);
	exit(1);
*/
	
	/*
	for (int i = 0;i < pot->tableNBin+1;i++)
		printf("%6d  %12.7lf: %12.7lf %12.7lf %12.7lf %12.7lf %12.7lf %12.7lf\n",i,rc1/nBin*i,vtab1[idSi][idSi][i],vtab1[idSi][idO][i],vtab1[idO][idO][i],vtab0[idSi][idSi][i],vtab0[idSi][idO][i],vtab0[idO][idO][i]);
	exit(1);
	*/

	// setup some potential parameters
	pot->dh2 = rc2/nBin;
	pot->rij2max = rij2max;
	POTENTIAL_LJ_EXTENSION * pot_lj_ext;
	pot_lj_ext = (POTENTIAL_LJ_EXTENSION*)scMalloc(sizeof(POTENTIAL_LJ_EXTENSION));
	
	//these parameters is not exactly for 3 body. But due to it specificity, we'll leave it here.
	pot_lj_ext->rcut2_2b = (double**)scMalloc(sizeof(double*)*NC);
	for (int ia = 0; ia < NC; ia++)
		pot_lj_ext->rcut2_2b[ia] = (double*)scMalloc(sizeof(double)*NC);

	for (int ia = 0; ia < NC; ia++)
		for (int ib = 0; ib < NC; ib++)
			//pot_lj_ext->rcut2_2b[ia][ib] = RCUT*RCUT/BOHR/BOHR;
			pot_lj_ext->rcut2_2b[ia][ib] = RCUT*RCUT;

	#if (VERBOSE > 0)
	//reportVerboseFixedDouble("2-body cutoff for P1-P1:",sqrt(pot_lj_ext->rcut2_2b[idp1][idp1]*BOHR*BOHR));
	//reportVerboseFixedDouble("2-body cutoff for P1-P2:",sqrt(pot_lj_ext->rcut2_2b[idp1][idp2]*BOHR*BOHR));
	//reportVerboseFixedDouble("2-body cutoff for P2-P1:",sqrt(pot_lj_ext->rcut2_2b[idp2][idp1]*BOHR*BOHR));
	//reportVerboseFixedDouble("2-body cutoff for P2-P2:",sqrt(pot_lj_ext->rcut2_2b[idp2][idp2]*BOHR*BOHR));
	reportVerboseFixedDouble("2-body cutoff for P1-P1:",sqrt(pot_lj_ext->rcut2_2b[idp1][idp1]));
	reportVerboseFixedDouble("2-body cutoff for P1-P2:",sqrt(pot_lj_ext->rcut2_2b[idp1][idp2]));
	reportVerboseFixedDouble("2-body cutoff for P2-P1:",sqrt(pot_lj_ext->rcut2_2b[idp2][idp1]));
	reportVerboseFixedDouble("2-body cutoff for P2-P2:",sqrt(pot_lj_ext->rcut2_2b[idp2][idp2]));
	#endif



	// initializing 3-body - parameters
	/*
	reportPrint("Allocate 3-body parameters...");
	POTENTIAL_SILICA_EXTENSION * pot_sio2_ext;
	pot_sio2_ext = (POTENTIAL_SILICA_EXTENSION*)scMalloc(sizeof(POTENTIAL_SILICA_EXTENSION));

	pot_sio2_ext->r0   = (double*)scMalloc(sizeof(double)*NC);
	pot_sio2_ext->r02  = (double*)scMalloc(sizeof(double)*NC);
	pot_sio2_ext->dl   = (double*)scMalloc(sizeof(double)*NC);

	pot_sio2_ext->bb   = (double***)scMalloc(sizeof(double**)*NC);
	pot_sio2_ext->cosb = (double***)scMalloc(sizeof(double**)*NC);
	pot_sio2_ext->c3b  = (double***)scMalloc(sizeof(double**)*NC);
	for (int ia = 0;ia < NC;ia++){
		pot_sio2_ext->bb[ia]   = (double**)scMalloc(sizeof(double*)*NC);
		pot_sio2_ext->cosb[ia] = (double**)scMalloc(sizeof(double*)*NC);
		pot_sio2_ext->c3b[ia]  = (double**)scMalloc(sizeof(double*)*NC);
		for (int ib = 0;ib < NC;ib++){
			pot_sio2_ext->bb[ia][ib]   = (double*)scMalloc(sizeof(double)*NC);
			pot_sio2_ext->cosb[ia][ib] = (double*)scMalloc(sizeof(double)*NC);
			pot_sio2_ext->c3b[ia][ib]  = (double*)scMalloc(sizeof(double)*NC);
		}
	}

	
	//these parameters is not exactly for 3 body. But due to it specificity, we'll leave it here.
	pot_sio2_ext->rcut2_2b = (double**)scMalloc(sizeof(double*)*NC);
	for (int ia = 0; ia < NC; ia++)
		pot_sio2_ext->rcut2_2b[ia] = (double*)scMalloc(sizeof(double)*NC);

	for (int ia = 0; ia < NC; ia++)
		for (int ib = 0; ib < NC; ib++)
			pot_sio2_ext->rcut2_2b[ia][ib] = rcij2[ia][ib];

	#if (VERBOSE > 0)
	reportVerboseFixedDouble("2-body cutoff for SI-SI [Ang]:",sqrt(pot_sio2_ext->rcut2_2b[idSi][idSi]*BOHR*BOHR));
	reportVerboseFixedDouble("2-body cutoff for  SI-O [Ang]:",sqrt(pot_sio2_ext->rcut2_2b[idSi][idO]*BOHR*BOHR));
	reportVerboseFixedDouble("2-body cutoff for  O-SI [Ang]:",sqrt(pot_sio2_ext->rcut2_2b[idO][idSi]*BOHR*BOHR));
	reportVerboseFixedDouble("2-body cutoff for   O-O [Ang]:",sqrt(pot_sio2_ext->rcut2_2b[idO][idO]*BOHR*BOHR));
	#endif

	// copy potential parameter for 3-body potential
	reportPrint("Saving 3-body parameters...");
	for (int ia = 0;ia < NC;ia++){
		for (int ib = 0;ib < NC;ib++){
			for (int ic = 0;ic < NC;ic++){
				pot_sio2_ext->bb[ia][ib][ic] = bb[ia][ib][ic];
				pot_sio2_ext->cosb[ia][ib][ic] = cosb[ia][ib][ic];
				pot_sio2_ext->c3b[ia][ib][ic]  = c3b[ia][ib][ic];
			}
		}
	}

	for (int ia = 0; ia < NC; ia++){
		pot_sio2_ext->r0[ia] = r0[ia];
		pot_sio2_ext->r02[ia] = r02[ia];
		pot_sio2_ext->dl[ia] = dl[ia];
		
	}

	pot_sio2_ext->nType = NC;

	pot->extension = (void*) pot_sio2_ext;
	*/
	//determine an import radius from MAX(rcut2body,max(rcut3b)) 
	pot->extension = (void*) pot_lj_ext;
	double rCache = pot->rcut[INDEX_2B];

	/*
	for (int ic = 0; ic < NC; ic++){
          	if (rCache < (r0[ic]+r0[ic]))
			rCache = r0[ic]+r0[ic];
	}
	*/
	paramsSetCacheRadius(rCache);
	//paramsSetCacheRadius(2*rCache);


	reportPrintInt("Atom type ID for P1:",idp1);
	reportPrintInt("Atom type ID for P2:",idp2);
	//reportPrintFixedDouble("2-Body cutoff radius [Ang]:", paramsGetRCut(2)*BOHR);  	
	reportPrintFixedDouble("2-Body cutoff radius [Ang]:", paramsGetRCut(2));  	
	//reportPrintFixedDouble("3-Body cutoff radius for Si center [Ang]:", r0[idSi]*BOHR);  	
	//reportPrintFixedDouble("3-Body cutoff radius for O  center [Ang]:", r0[idO]*BOHR);  	
	reportPrintInt("Size of potential/force table:", nBin);  	
	//reportPrintFixedDouble("Import radius [Ang]  :",rCache*BOHR);
	reportPrintFixedDouble("Import radius [Ang]  :",rCache);

	
  //free up temp vtab
	reportPrint("Free up temporary variables...");
  for (int i = 0;i < NC;i++)
	for (int j = 0;j < NC;j++){
		scFree(vtab0[i][j]);
		scFree(vtab1[i][j]);
		scFree(vtab2[i][j]);
	}

}
