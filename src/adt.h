#ifndef __ADT_H__
#define __ADT_H__
typedef struct stack_int_st {
	int	isInit;
	int	head;
	int*	stack;
	int	capacity;
} STACK_INT;


//Binary tree adapted from http://www.cprogramming.com/tutorial/c/lesson18.html
typedef struct node
{
  int key_value;
  struct node *left;
  struct node *right;
} B_NODE;

typedef struct btree
{
  B_NODE *root;

} B_TREE;


STACK_INT* stackIntInit(STACK_INT*,int size);
STACK_INT* stackIntDestroy(STACK_INT*);
void stackIntPush(STACK_INT*,int data);
int stackIntPop(STACK_INT*);
int stackIntPeek(STACK_INT*);
int stackIntSize(STACK_INT*);
int stackIntCapacity(STACK_INT* st);

#endif

