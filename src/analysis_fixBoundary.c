#include "constants.h"
#include "simulate.h"
#include "state.h"
#include "params.h"
#include "cell.h"
#include "utils.h"
#include "input.h"
#include "memory.h"
#include <assert.h>
#include <stdlib.h>
#include "init.h"
#include "mpi.h"

void analysis_applyFixBoundary(STATE* s,PARAMS* p)
{
	
	INPUT_PARAMS* inp = getInputObj();

	double (*r)[3]  = s->r;
	double *rUpdateFlag = s->rUpdateFlag;

	double scaleFactor[3];
	double scaleFactorInv[3];
	double refCoord[3];

	double fixThreshold[3][2];

	reportPrint("Analyze: Fixing atom near boundary...");

	for (int a = 0;a < 3;a++) {
		fixThreshold[a][0] = (inp->analyzeFixBoundaryLength[a][0]/BOHR)/p->hMatrix[a][a];
		fixThreshold[a][1] = 1.0-(inp->analyzeFixBoundaryLength[a][1]/BOHR)/p->hMatrix[a][a];
	}

	for (int a = 0;a < 3;a++){
		scaleFactor[a]    = p->hMatrix[a][a];
		scaleFactorInv[a] = 1.0/scaleFactor[a];
	}
	/*
	for (int a = 0;a < 3;a++){
		p->boxSize[a] *= scaleFactorInv[a];
		refCoord[a] = s->cornerCoord[a]*scaleFactor[a];
	}
	*/
	for (int a = 0;a < 3;a++)
		refCoord[a] = s->cornerCoord[a];

	//Convert all position to global coordinate.
	long nFixedCount = 0;
	int nAtom = s->nAtomOwn;
	for (int i = 0;i < nAtom;i++) {
		for (int a = 0;a < 3;a++) {
			if (((r[i][a]*scaleFactorInv[a]+refCoord[a]) < fixThreshold[a][0])  ||
			    ((r[i][a]*scaleFactorInv[a]+refCoord[a]) > fixThreshold[a][1])) {
					rUpdateFlag[i] = 0.0;
					nFixedCount++;	
			} //end if
		} //end for direction
	} //end for nAtom
	long nFixedCountGlobal = 0;
  	MPI_Allreduce(&nFixedCount, &nFixedCountGlobal, 1, MPI_LONG, MPI_SUM, MPI_COMM_WORLD);
	reportPrintInt("Analyze: Total number of fixed atoms = ", (int)nFixedCountGlobal);

}
