#include "scmd.h"
#include "simulate.h"
#include "params.h"
#include "init.h"
#include "MDLoop.h"
#include "mpi.h"
#include "timing.h"
#include "memory.h"
#include <stdio.h>

int main(int argc, char **argv)
{

	MPI_Init(&argc, &argv);
	double sim_start = MPI_Wtime();
	timerInit();
	timerStart(TIMING_ALL);
	timerStart(TIMING_INIT);
	init(&params,&simulateData);
	timerStop(TIMING_INIT);
	timerStart(TIMING_LOOPS);
	MDLoop(&simulateData,&params);
	timerStop(TIMING_LOOPS);
	timerStop(TIMING_ALL);
	timerWriteProfile();
	writeMemUsage();
	double sim_end = MPI_Wtime();
	
	int myId;
	MPI_Comm_rank(MPI_COMM_WORLD, &myId);
	if (myId == 0)
		printf("Elapsed time:%f s\n",sim_end-sim_start);
	MPI_Finalize();
}
