#ifndef __COMPUTE_ACCEL_H__
#define __COMPUTE_ACCEL_H__

#include "state.h"
#include "potential.h"
#include "params.h"
#include "cell.h"

void computeAccel(STATE* s, POTENTIAL* pot, PARAMS* p, CELL_INFO*);

#endif

