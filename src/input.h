#ifndef __INPUT_H__
#define __INPUT_H__

#include "constants.h"

typedef enum {
	POT_SILICA,
	POT_SILICA_WATER
} POT_TYPES; 

typedef enum {
	PARMODE_SEQUENTIAL,
	PARMODE_MPI = 0,
	PARMODE_MPI_OMP = 1,
	PARMODE_MPI_OMP_GPU = 2,
	PARMODE_MPI_OMP_MIC = 3
} PARALLEL_MODES; 


typedef struct input_params_st {
	//simulation parameter settings	
	char		potentialName[30];
	POTENTIAL_TYPES	potentialType;
	char		computationMethodName[50];
	int		potentialTableNBin;

	double		deltaT;
	int		evalPropFreq;
	int		detailedEvalProp;

	int		nAtomType;
	char		atomTypeTable[ATOM_TYPE_MAX][ATOM_TYPE_SYMBOL_MAX];	
	double		atomMass[ATOM_TYPE_MAX];	
	int		randomSeed;
	
	//simulation settings	
	int		stepLimit;

	//paralel settings
	PARALLEL_MODES	parallelMode;
	int		vProc[3];
	int		nGpus[MAX_NBODY];
	int		gpu_chunk[MAX_NBODY];
	int		nThreads[MAX_NBODY];
	int		nThreads_mic[MAX_NBODY];
	int		nMics;
	int		globalNThreads;

	//initial config settings
	int		mdMode;
//	int		nAtomUCell;
/*
	char		(*unitCellAtomSymbol)[ATOM_TYPE_SYMBOL_MAX];
	double		(*unitCellAtomCoord)[3];
*/
	char		unitCellAtomSymbol[UNIT_CELL_ATOM_MAX][ATOM_TYPE_SYMBOL_MAX];
	double		unitCellAtomCoord[UNIT_CELL_ATOM_MAX][3];
	int		unitCellAtomCount;

	int		initUCell[3];
	double		latticeConst[3];
	double		initTemp;
	unsigned long	startStep;
	char		mtsDir[PATH_LENGTH];
	int		nlayer[MAX_NBODY];

	//analyze settings
	int		analyzeVelocityScalingFreq;
	double		analyzeVelocityScalingTempBegin;
	double		analyzeVelocityScalingTempEnd;

	int		analyzeGrFreq;
	int		analyzeMsdFreq;

	int		analyzeReorder;
	int		analyzeReorderFreq;

	int		analyzeApplyStrainFreq;
	double		analyzeApplyStrain[3];

	int		analyzeFixBoundary;
	double		analyzeFixBoundaryLength[3][2]; //[x,y,z][min,max]

	int		analyzeRdf;
	int		analyzeRdfFreq;
	double		analyzeRdfRCut;
	double		analyzeRdfBinLength;

	int		analyzeVacf;
	int		analyzeVacfFreq;
	//int		analyzeVacfNCollect;
	//Output settings
	int		writeAggregateOutputXYZ;
	int		writeAggregateOutputMTS;
	int		writeAggregateOutputAtomicForce;

	int		writeOutputXYZ;
	int		writeOutputXYZFreq;
	WRITE_MODES	writeOutputXYZMode;	

	int		writeOutputMTS;
	int		writeOutputMTSFreq;
	WRITE_MODES	writeOutputMTSFormat;	

	int		writeOutputAtomicForce;
	int		writeOutputAtomicForceFreq;
	WRITE_MODES	writeOutputAtomicForceDummy;	

	int		readAsciiMTS;
	int		runtimeProfileRatio;
	int		runtimeProfileWriteFreq;
} INPUT_PARAMS;


void initInput(INPUT_PARAMS* input);
void readInput();
INPUT_PARAMS* getInputObj();

#endif
