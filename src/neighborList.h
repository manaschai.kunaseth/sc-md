#ifndef __NEIGHBOR_LIST_H__
#define __NEIGHBOR_LIST_H__

typedef struct neighbor_list_st {
	//!number of atoms
	int		nAtom;

	//!number of neighbors for each atom
	int*		nNeighbor;

	//!Neighbor list data structure
	int**		nbData;

	//!Maximum number of neighbor list data structure (define at allocation time from passed variable)
	int 		nbMax;

} NEIGHBOR_LIST;

NEIGHBOR_LIST* createNeighborList(NEIGHBOR_LIST* nbl, int nAtom, int nbMax);
NEIGHBOR_LIST* freeNeighborList(NEIGHBOR_LIST* nbl);

#endif
