#include <stdio.h>
#include "computeAccel_lj.h"
#include "computeAccel_lj_gpu.h"
#include "pot_lj.h"
#include "potential.h"
#include "computePattern.h"
#include "state.h"
#include "params.h"
#include "cell.h"
#include "timing.h"
#include "cudaUtils.h"
#include "utils.h"
#include <math.h>
#include <cuda.h>
#include <cuda_runtime.h>

#define MAX_NTYPE 2
#define MAX_NTAB_INDEX 3

#define NATOM_CELL2B_MAX 32
#define CONST_NATOM_CELL3B_MAX 5

#define NEIGHBOR_COUNT 27

#define atomIdx(i,xyz) (i*3)+xyz
#define dataIdx(c,i,xyz) c*NATOM_CELL2B_MAX*4+(i*4)+xyz
//#define data3BIdx(c,i,xyz) c*NATOM_CELL3B_MAX*4+(i*4)+xyz

//#define CHUNK_COUNT_2B 1
//#define CHUNK_COUNT_3B 3
#define PIPELINE_SIZE_3B 1
#define LANE_SIZE 32

#define USE_WARP_PRIMITIVE_2B
#define USE_VECTOR_TYPE_2B
//#define USE_PTX_2B

#define INIT_WARP_COUNT_3B 2
#define FINAL_WARP_COUNT_3B 4
#define USE_WARP_VOTING_INIT_3B
#define USE_VECTOR_GL_3B
//#define USE_WARP_PRIMITIVE_3B_I
//#define USE_WARP_PRIMITIVE_3B_J

//#define CUDA_9
#define FULL_MASK 0xffffffff

//2-body constants
__constant__ int con_vCellAllXYZ_2b;
__constant__ int con_vCell_2b[3];
__constant__ int con_vCellAll_2b[3];
__constant__ int con_nPattern_2b;
__constant__ int con_tabIdx_2b[MAX_NTYPE][MAX_NTYPE];
__constant__ float con_dh2Inv_2b;
__constant__ float con_rij2max_2b;
texture <float, cudaTextureType2DLayered> tex_force;
texture <float, cudaTextureType2DLayered> tex_pot;

//3-body constants
__constant__ int NATOM_CELL3B_MAX;
__constant__ int NATOM_CELL3B_PITCH;
__constant__ int con_vCellAllXYZ;
__constant__ int con_vCell[3];
__constant__ int con_vCellAll[3];
__constant__ int con_nPattern;
__constant__ float con_r0_3B[MAX_NTYPE];
__constant__ float con_r02_3B[MAX_NTYPE];
__constant__ float con_bb_3B[MAX_NTYPE][MAX_NTYPE][MAX_NTYPE];
__constant__ float con_cosb_3B[MAX_NTYPE][MAX_NTYPE][MAX_NTYPE];
__constant__ float con_c3b_3B[MAX_NTYPE][MAX_NTYPE][MAX_NTYPE];
__constant__ float con_dl_3B[MAX_NTYPE];
__constant__ float con_dh[MAX_NTYPE];

//GPU init 
static double 	*gl_dev_ra;
static double 	*gl_dev_r;
static int 		*gl_dev_rType;

#if !defined(__CUDA_ARCH__) || __CUDA_ARCH__ >= 600
#else
__device__ double atomicAdd(double* address, float val);
__device__ double atomicAdd(double* address, double val);
#endif


__global__ void computeAccel_lj_cell2B_gpu(	int *cellData, 
												int *nAtomCell,
												double *r, 
												int *rType,
												double *ra,
												float *cellEn, 
												int *cellOffset, 
												int nAtomCellMax,
												int chunk_id,
												int chunk_size,
												int gpuId, 
												int nGpus2B);


__global__ void computeAccel_lj_cell3B_gpu(	
					#ifdef USE_VECTOR_GL_3B
						float4 *dev_cellCoord,
					#else
						float *dev_cellCoord_x, 
						float *dev_cellCoord_y, 
						float *dev_cellCoord_z ,
					#endif
						float *dev_cellRa_x, 
						float *dev_cellRa_y,
						float *dev_cellRa_z,
						float *dev_cellEn, 
					#ifndef USE_VECTOR_GL_3B
						int *dev_cellRType,
					#endif
						unsigned int *dev_cellRSCMask, 
						int *dev_cellAtomCount,
						int *dev_cellAllAtomCount,
						int chunk_id,
						int chunk_size, 
						int gpuId, 
						int nGpus3B);

void initTables2B_gpu_lj(POTENTIAL* pot,int gpuId)
{
  	CUDA_SAFE_CALL(cudaSetDevice(gpuId)); 
	
	float *host_tabForce = (float *)malloc(sizeof(float) * pot->tableNBin * MAX_NTAB_INDEX);
	float *host_tabPotential = (float *)malloc(sizeof(float) * pot->tableNBin * MAX_NTAB_INDEX);

	//table index
	//(0,0): index 0
	//(1,1): index 1
	//(0,1): index 2
	//(1,0): index 2
	
	//type-index 0
	for (int i = 0;i < pot->tableNBin;i++)
	{
		host_tabForce[i] = (float)pot->forceTable[0][0][i];
		host_tabPotential[i] = (float)pot->potentialTable[0][0][i];
	}
	
	//type-index 1
	for (int i = 0;i < pot->tableNBin;i++)
	{
		host_tabForce[pot->tableNBin+i] = pot->forceTable[1][1][i];
		host_tabPotential[pot->tableNBin+i] = pot->potentialTable[1][1][i];
	}

	//type-index 2
	for (int i = 0;i < pot->tableNBin;i++)
	{
		host_tabForce[2*pot->tableNBin+i] = pot->forceTable[1][0][i];
		host_tabPotential[2*pot->tableNBin+i] = pot->potentialTable[1][0][i];
	}

	//preparing texture information
	cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc(32,0,0,0,cudaChannelFormatKindFloat);
	cudaArray *cu_3darrayForce;
	cudaArray *cu_3darrayPotential;
	cudaExtent extent = make_cudaExtent(pot->tableNBin,1,MAX_NTAB_INDEX);
	CUDA_SAFE_CALL(cudaMalloc3DArray(&cu_3darrayForce,&channelDesc,extent,cudaArrayLayered));
	CUDA_SAFE_CALL(cudaMalloc3DArray(&cu_3darrayPotential,&channelDesc,extent,cudaArrayLayered));


	cudaMemcpy3DParms pm = {0};
	pm.srcPos = make_cudaPos(0,0,0);
	pm.dstPos = make_cudaPos(0,0,0);
	pm.srcPtr = make_cudaPitchedPtr(host_tabForce,sizeof(float)*pot->tableNBin,MAX_NTAB_INDEX,1);
	pm.dstArray = cu_3darrayForce;
	pm.extent = extent;
	pm.kind = cudaMemcpyHostToDevice;
	CUDA_SAFE_CALL(cudaMemcpy3D(&pm));	

	tex_force.addressMode[0] = cudaAddressModeClamp;
	tex_force.addressMode[1] = cudaAddressModeClamp;
	tex_force.filterMode = cudaFilterModeLinear;
	tex_force.normalized = false;

	pm.srcPos = make_cudaPos(0,0,0);
	pm.dstPos = make_cudaPos(0,0,0);
	pm.srcPtr = make_cudaPitchedPtr(host_tabPotential,sizeof(float)*pot->tableNBin,MAX_NTAB_INDEX,1);
	pm.dstArray = cu_3darrayPotential;
	pm.extent = extent;
	pm.kind = cudaMemcpyHostToDevice;
	CUDA_SAFE_CALL(cudaMemcpy3D(&pm));

	tex_pot.addressMode[0] = cudaAddressModeClamp;
	tex_pot.addressMode[1] = cudaAddressModeClamp;
	tex_pot.filterMode = cudaFilterModeLinear;
	tex_pot.normalized = false;

	CUDA_SAFE_CALL(cudaBindTextureToArray(tex_force,cu_3darrayForce,channelDesc));
	CUDA_SAFE_CALL(cudaBindTextureToArray(tex_pot,cu_3darrayPotential,channelDesc));

	free(host_tabForce);
	free(host_tabPotential);
}

__global__ void computeAccel_lj_cell2B_gpu(	int *cellData, //float *cellData,
												int *nAtomCell, //float *ra, 
												double *r, 
												int *rType,
												double *ra,
												float *cellEn, 
												int *cellOffset, 
												int nAtomCellMax,
												int chunk_id, 
												int chunk_size, 
												int gpuId, 
												int nGpus2B
){	
	double potEnergyCell2B = 0.0;
	int cell_idx = blockIdx.x + blockIdx.y * con_vCellAll_2b[0] + (blockIdx.z + chunk_id * chunk_size) * con_vCellAll_2b[0] * con_vCellAll_2b[1];
	//int cell_idx = blockIdx.x + blockIdx.y * con_vCellAll_2b[0] + blockIdx.z * con_vCellAll_2b[0] * con_vCellAll_2b[1];

#ifdef CUDA_9
#ifdef USE_WARP_PRIMITIVE_2B
	int warp_group = threadIdx.y % (32 / blockDim.x);
	unsigned int warp_mask = ((1 << blockDim.x) - 1) << (warp_group * blockDim.x);
#endif
#endif

	double3* r_d = (double3*)r;

	if((blockIdx.z + chunk_id * chunk_size) >= con_vCell_2b[2] || blockIdx.y >= con_vCell_2b[1] || blockIdx.x >= con_vCell_2b[0]) return;

	for (int iPatt = 0;iPatt < con_nPattern_2b;iPatt++)
	{
		#ifdef USE_VECTOR_TYPE_2B
			int2 *patterns = (int2*)cellOffset;
			int ci = cell_idx + patterns[iPatt].x;
			int cj = cell_idx + patterns[iPatt].y;
		#else 
		#ifdef USE_PTX_2B
			unsigned long long *ptr = (unsigned long long*)&cellOffset[iPatt * 2];
			long long pattern_arr = ptr[iPatt];

			asm ("ldu.global.u64 %0, [%1];" : "=l"(pattern_arr) : "l"(ptr));
			int* pattern = (int*)&pattern_arr;
			
			int ci = cell_idx + pattern[0];
			int cj = cell_idx + pattern[1];
		#else
			int ci = cell_idx + cellOffset[iPatt*2  ];
			int cj = cell_idx + cellOffset[iPatt*2+1];
		#endif
		#endif
	
		for (int atom_i_idx = threadIdx.y; atom_i_idx < nAtomCell[ci]; atom_i_idx += blockDim.y){
			int atom_i = cellData[ci * nAtomCellMax + atom_i_idx];

			double3 ai 		= r_d[atom_i];
			int   	ai_type = rType[atom_i];

		#ifdef USE_WARP_PRIMITIVE_2B
			float ra_x = 0;
			float ra_y = 0;
			float ra_z = 0;
		#endif 

			for (int atom_j_idx = threadIdx.x; atom_j_idx < nAtomCell[cj]; atom_j_idx += blockDim.x){
				int atom_j = cellData[cj * nAtomCellMax + atom_j_idx];
				
				double3 aj 		= r_d[atom_j];
				int   	aj_type = rType[atom_j];

				float dx = ai.x - aj.x;
				float dy = ai.y - aj.y;
				float dz = ai.z - aj.z;
				float rr = dx*dx + dy*dy + dz*dz;

				if	((rr >= con_rij2max_2b && atom_i != atom_j) ||
					((ci == cj) && (atom_i >= atom_j))) continue;

				int interactionTypeIdx = con_tabIdx_2b[ai_type][aj_type];
				float idxRF = rr * con_dh2Inv_2b;

				potEnergyCell2B += tex2DLayered(tex_pot,idxRF+0.5f,0.0f,interactionTypeIdx); 
				//potEnergyCell2B /= 111111.f;
				float vrr = tex2DLayered(tex_force,idxRF+0.5f,0.0f,interactionTypeIdx); 
				//vrr /= 111111.f;


			#ifdef USE_WARP_PRIMITIVE_2B
				ra_x += vrr * dx;
				ra_y += vrr * dy;
				ra_z += vrr * dz;
			#else
				atomicAdd(&ra[atom_i * 3 + 0], vrr * dx);
				atomicAdd(&ra[atom_i * 3 + 1], vrr * dy);
				atomicAdd(&ra[atom_i * 3 + 2], vrr * dz);
			#endif 

				atomicAdd(&ra[atom_j * 3 + 0], -vrr * dx);
				atomicAdd(&ra[atom_j * 3 + 1], -vrr * dy);
				atomicAdd(&ra[atom_j * 3 + 2], -vrr * dz);
			}

		#ifdef USE_WARP_PRIMITIVE_2B
			for (int offset = blockDim.x / 2; offset > 0; offset /= 2) 
			{
			#ifdef CUDA_9
				ra_x += __shfl_down_sync(warp_mask, ra_x, offset);
				ra_y += __shfl_down_sync(warp_mask, ra_y, offset);
				ra_z += __shfl_down_sync(warp_mask, ra_z, offset);			
			#else
    			ra_x += __shfl_down(ra_x, offset, blockDim.x);
				ra_y += __shfl_down(ra_y, offset, blockDim.x);
				ra_z += __shfl_down(ra_z, offset, blockDim.x);			
			#endif
			}

			if(threadIdx.x == 0)
			{
				atomicAdd(&ra[atom_i * 3 + 0], ra_x);
				atomicAdd(&ra[atom_i * 3 + 1], ra_y);
				atomicAdd(&ra[atom_i * 3 + 2], ra_z);
			}
		#endif
		}
	}
	float potEn_f = potEnergyCell2B;
	atomicAdd(&cellEn[cell_idx], potEn_f);
}

double computeAccel_lj2B_gpu(STATE *s, CELL_LIST* cl,POTENTIAL* pot,COMPUTE_PATTERN_CELL* patCell,int gpuId, int nGpus2B)
{
	INPUT_PARAMS* inp = getInputObj();
	int 	CHUNK_COUNT_2B = inp->gpu_chunk[2];

	int nPattern_2b= patCell->nPattern;
	int **cellOffset_2b = patCell->cellPatternOffset;
	int tabIdx[MAX_NTYPE][MAX_NTYPE];
	//Cell data
	int **cellData = cl->cellAtomData;
	int *nAtomCell = cl->nAtomCell;
	int nAtomCellMax = cl->nAtomCellMax[0];

	//universal parameter for potential/force calculation.
	double dh2;
	float dh2Inv;     //1.0/(dh^2)   : dh = pot_table spacing;
	float rij2max;

	//general parameter use within this file.
	dh2 = pot->dh2;
	dh2Inv = 1.0/(dh2);
	rij2max = pot->rij2max; 
	
	//Cell data in GPU laid in 1D. Including atom data
	//int *host_cellOffset_2b;

	double (*r)[3]  = s->r;
	double (*ra)[3] = s->ra;
	int  *rType   	= s->rType;

	int vCellAll[3];
	//cell vector including caching cells.
	for (int a = 0;a < 3;a++)
		vCellAll[a] = cl->vCell[a] + cl->nCellCache[a];
	
	int vCellAllXYZ= vCellAll[0]*vCellAll[1]*vCellAll[2];
	
	int nAtom = s->nAtomOwn+s->nAtomCache;

	static float 	*host_cellEn_2b;

	static int		*dev_cellData_2b;
	static int 		*dev_nAtomCell_2b;
	static float 	*dev_cellEn_2b;
	static int 		*dev_cellOffset_2b;

	static cudaStream_t *stream;

	static int		initialized_2b = 0;
	double potEnergy = 0.0;
	
	tabIdx[0][0] = 0;
	tabIdx[1][1] = 1;
	tabIdx[0][1] = 2;
	tabIdx[1][0] = 2;

	//GPU INIT_TEST
  	CUDA_SAFE_CALL(cudaSetDevice(gpuId));

	if(initialized_2b == 0){
		CUDA_SAFE_CALL(cudaMalloc( (void**)&dev_cellData_2b, sizeof(int) * vCellAllXYZ * nAtomCellMax));
		CUDA_SAFE_CALL(cudaMalloc( (void**)&dev_nAtomCell_2b, sizeof(int) * vCellAllXYZ));
	
		CUDA_SAFE_CALL(cudaMemcpyToSymbol(con_dh2Inv_2b, &dh2Inv, sizeof(float), 0, cudaMemcpyHostToDevice));
		CUDA_SAFE_CALL(cudaMemcpyToSymbol(con_rij2max_2b, &rij2max, sizeof(float), 0, cudaMemcpyHostToDevice));

		int *host_cellOffset_2b;
		host_cellOffset_2b = (int(*))(malloc(sizeof(int[2])*nPattern_2b));
		CUDA_SAFE_CALL(cudaMalloc( (void**)&dev_cellOffset_2b , sizeof(int[2])*nPattern_2b));
	
		for (int iPatt = 0;iPatt < nPattern_2b;iPatt++) {
			host_cellOffset_2b[iPatt*2  ] = cellOffset_2b[iPatt][0];
			host_cellOffset_2b[iPatt*2+1] = cellOffset_2b[iPatt][1];
		}

		CUDA_SAFE_CALL(cudaMemcpy(dev_cellOffset_2b ,host_cellOffset_2b ,nPattern_2b*sizeof(int[2]),cudaMemcpyHostToDevice));
		CUDA_SAFE_CALL(cudaMemcpyToSymbol(con_nPattern_2b, &nPattern_2b, sizeof(int), 0, cudaMemcpyHostToDevice));

		for (int i = 0;i < MAX_NTYPE;i++){
			CUDA_SAFE_CALL(cudaMemcpyToSymbol(con_tabIdx_2b, tabIdx[i], sizeof(int[MAX_NTYPE]), sizeof(int)*(i*MAX_NTYPE), cudaMemcpyHostToDevice));
		}

		free(host_cellOffset_2b);
		//host_cellEn_2b = (float*)(malloc(sizeof(float) * vCellAllXYZ));
		CUDA_SAFE_CALL(cudaMallocHost((void**)&host_cellEn_2b, sizeof(float) * vCellAllXYZ));
		
		stream = (cudaStream_t*)malloc(sizeof(cudaStream_t) * CHUNK_COUNT_2B);
		for(int i = 0; i < CHUNK_COUNT_2B; i++){
			CUDA_SAFE_CALL(cudaStreamCreateWithFlags(&stream[i], cudaStreamDefault));
			//CUDA_SAFE_CALL(cudaStreamCreateWithFlags(&stream[i], cudaStreamNonBlocking));
		}

		initialized_2b = 1;
	}

	CUDA_SAFE_CALL(cudaMalloc( (void**)&dev_cellEn_2b , sizeof(float)*vCellAllXYZ));
	CUDA_SAFE_CALL(cudaMalloc( (void**)&gl_dev_ra, sizeof(double[3]) * nAtom ));
	CUDA_SAFE_CALL(cudaMalloc( (void**)&gl_dev_r, sizeof(double[3]) * nAtom ));
	CUDA_SAFE_CALL(cudaMalloc( (void**)&gl_dev_rType, sizeof(int) * nAtom ));
	CUDA_SAFE_CALL(cudaMemcpy(dev_cellData_2b ,cellData[0] ,sizeof(int) * vCellAllXYZ * nAtomCellMax,cudaMemcpyHostToDevice));	
	CUDA_SAFE_CALL(cudaMemcpy(dev_nAtomCell_2b ,nAtomCell ,sizeof(int) * vCellAllXYZ, cudaMemcpyHostToDevice));	

	CUDA_SAFE_CALL(cudaMemset(dev_cellEn_2b,0.0,vCellAllXYZ*sizeof(float)));	

	CUDA_SAFE_CALL(cudaMemcpy(gl_dev_ra ,ra ,sizeof(double[3]) * nAtom,cudaMemcpyHostToDevice));
	CUDA_SAFE_CALL(cudaMemcpy(gl_dev_r ,r ,sizeof(double[3]) * nAtom,cudaMemcpyHostToDevice));	
	CUDA_SAFE_CALL(cudaMemcpy(gl_dev_rType ,rType ,sizeof(int) * nAtom,cudaMemcpyHostToDevice));	

	CUDA_SAFE_CALL(cudaMemcpyToSymbol(con_vCell_2b, cl->vCell, sizeof(int[3]), 0, cudaMemcpyHostToDevice));
	CUDA_SAFE_CALL(cudaMemcpyToSymbol(con_vCellAll_2b, vCellAll, sizeof(int[3]), 0, cudaMemcpyHostToDevice));
	CUDA_SAFE_CALL(cudaMemcpyToSymbol(con_vCellAllXYZ_2b, &vCellAllXYZ, sizeof(int), 0, cudaMemcpyHostToDevice));

	int chunk_size = ((cl->vCell[2] % CHUNK_COUNT_2B) == 0) ? (cl->vCell[2] / CHUNK_COUNT_2B) : (cl->vCell[2] / CHUNK_COUNT_2B + 1);
	
	for(int chunk_id = 0; chunk_id < CHUNK_COUNT_2B; chunk_id++){
		//dim3 ownedCell(cl->vCell[0],cl->vCell[1],cl->vCell[2]);
		dim3 gridsize_cell2b(cl->vCell[0],cl->vCell[1], chunk_size);
		dim3 blocksize_cell2b(4,16,1);
		
		computeAccel_lj_cell2B_gpu<<<gridsize_cell2b,blocksize_cell2b,0, stream[chunk_id]>>>(	
																		dev_cellData_2b,
																		dev_nAtomCell_2b,
																		gl_dev_r, 
																		gl_dev_rType,
																		gl_dev_ra, 
																		dev_cellEn_2b,
																		dev_cellOffset_2b,
																		nAtomCellMax,
																		chunk_id,
																		chunk_size,
																		gpuId,
																		nGpus2B);
	}
	cudaDeviceSynchronize();

	CUDA_SAFE_CALL(cudaMemcpy(host_cellEn_2b,dev_cellEn_2b,sizeof(float) * vCellAllXYZ, cudaMemcpyDeviceToHost));
	
	for (int i = 0;i < vCellAllXYZ;i++)
	{
		potEnergy += host_cellEn_2b[i];
	}
	
	CUDA_SAFE_CALL(cudaMemcpy(ra ,gl_dev_ra ,sizeof(double[3]) * nAtom,cudaMemcpyDeviceToHost));	

	CUDA_SAFE_CALL(cudaFree(dev_cellEn_2b));
	//CUDA_SAFE_CALL(cudaFree(dev_cellOffset_2b));
	//CUDA_SAFE_CALL(cudaFree(dev_cellData_2b));
	//CUDA_SAFE_CALL(cudaFree(dev_nAtomCell_2b));
	CUDA_SAFE_CALL(cudaFree(gl_dev_ra));
	CUDA_SAFE_CALL(cudaFree(gl_dev_r));
	CUDA_SAFE_CALL(cudaFree(gl_dev_rType));

	//free(host_cellEn_2b);
	return potEnergy;
}


__global__ void init_lj3D_data_parallel_pruning( 
								#ifdef USE_VECTOR_GL_3B
									float4 *cellCoord, 
								#else
									float *cellCoord_x, 
									float *cellCoord_y, 
									float *cellCoord_z, 
									int   *cellRType,
								#endif
									unsigned int *cellRSCMask,  
									int   *cellAtomIdx,
									int   *cellAtomCount,
									int   *cellAllAtomCount,
									int   maxAtomCountPerCell,
									int   *cellData,
									int   nAtomCellMax,
									int   *nAtomCell,
									double *r,
									int   *rType,
									//unsigned int *rSCCacheMask,
									int   chunk_id,
									int   chunk_size, 
									int   gpuId, 
									int   nGpus3B
){
	int warp_id = threadIdx.x >> 5; // threadIdx.x / 32
	int lane_id = threadIdx.x & 31; // threadIdx.x % 32

	int i = (blockIdx.z + chunk_id * chunk_size);
	int j = blockIdx.y;
	int k = (blockIdx.x * INIT_WARP_COUNT_3B + warp_id);

	int cell_idx = k + j * con_vCellAll[0] + i * con_vCellAll[0] * con_vCellAll[1];
	int cell_idx_chuck = (blockIdx.x * INIT_WARP_COUNT_3B + warp_id) + blockIdx.y * con_vCellAll[0] + (blockIdx.z)* con_vCellAll[0] * con_vCellAll[1];

	unsigned int cell_mask = (k >= con_vCell[0]) | ((j >= con_vCell[1]) << 1) | ((i >= con_vCell[2]) << 2);

#ifdef USE_VECTOR_GL_3B
	__shared__ float4 shr_coord[INIT_WARP_COUNT_3B][16];
#else
	__shared__ float shr_coord_x[INIT_WARP_COUNT_3B][16];
	__shared__ float shr_coord_y[INIT_WARP_COUNT_3B][16];
	__shared__ float shr_coord_z[INIT_WARP_COUNT_3B][16];
	__shared__ int shr_r_type[INIT_WARP_COUNT_3B][16];
#endif
	double3 *r_d = (double3*)r;

#ifdef USE_WARP_VOTING_INIT_3B
	int atom_count;
#else
	__shared__ int shr_atom_count[INIT_WARP_COUNT_3B];
#endif

	if(i >= con_vCellAll[2] || j >= con_vCellAll[1] || k >= con_vCellAll[0]) return;

	for (int atom_idx = lane_id; atom_idx < nAtomCell[cell_idx]; atom_idx += LANE_SIZE) {
		int atom = cellData[cell_idx * nAtomCellMax + atom_idx];

		double3 r_tmp = r_d[atom];

	#ifdef USE_VECTOR_GL_3B
		shr_coord[warp_id][atom_idx] = make_float4(r_tmp.x, r_tmp.y, r_tmp.z, rType[atom]);
		cellCoord[cell_idx_chuck * maxAtomCountPerCell + atom_idx] = make_float4(r_tmp.x, r_tmp.y, r_tmp.z, rType[atom]);
	#else
		shr_coord_x[warp_id][atom_idx] = r_tmp.x;//r[atom * 3 + 0];
		shr_coord_y[warp_id][atom_idx] = r_tmp.y;//r[atom * 3 + 1];
		shr_coord_z[warp_id][atom_idx] = r_tmp.z;//r[atom * 3 + 2];
		shr_r_type[warp_id][atom_idx]  = rType[atom];

		cellCoord_x[cell_idx_chuck * maxAtomCountPerCell + atom_idx] = r_tmp.x;//r[atom * 3 + 0];
		cellCoord_y[cell_idx_chuck * maxAtomCountPerCell + atom_idx] = r_tmp.y;//r[atom * 3 + 1];
		cellCoord_z[cell_idx_chuck * maxAtomCountPerCell + atom_idx] = r_tmp.z;//r[atom * 3 + 2];
		cellRType[  cell_idx_chuck * maxAtomCountPerCell + atom_idx] = rType[atom];
	#endif
		cellRSCMask[cell_idx_chuck * maxAtomCountPerCell + atom_idx] = cell_mask;
		cellAtomIdx[cell_idx_chuck * maxAtomCountPerCell + atom_idx] = atom;
	}

#ifdef USE_WARP_VOTING_INIT_3B
	atom_count = nAtomCell[cell_idx];
	if (lane_id == 0 ) 
		cellAtomCount[cell_idx_chuck] = atom_count;
#else
	if(lane_id == 0){
		shr_atom_count[warp_id] = nAtomCell[cell_idx];
		cellAtomCount[cell_idx_chuck] = shr_atom_count[warp_id]; 
	}
#endif
	__syncthreads();

	for(int off_i = -1; off_i < 2; off_i++){
		for(int off_j = -1; off_j < 2; off_j++){
			for(int off_k = -1; off_k < 2; off_k++){

				if( !((off_i == 0 && off_j == 0 && off_k == 0)) &&
				(( ((i + off_i) >= 0) && ((i + off_i) < con_vCellAll[2])) &&
				( ((j + off_j) >= 0) && ((j + off_j) < con_vCellAll[1])) &&
				( ((k + off_k) >= 0) && ((k + off_k) < con_vCellAll[0])))) {

					int 	neighbor_idx = (i+off_i) * con_vCellAll[0] * con_vCellAll[1] + (j+off_j) * con_vCellAll[0] + (k+off_k);
							cell_mask 	 = ((k + off_k) >= con_vCell[0]) | (((j + off_j) >= con_vCell[1]) << 1) | (((i + off_i) >= con_vCell[2]) << 2);
				#ifdef USE_VECTOR_GL_3B
					float4 	ai;
					int 	ai_type;
				#else
					float 	ai_x; 
					float 	ai_y; 
					float 	ai_z;
					int 	ai_type;
				#endif
					int 	atom;

				#ifdef USE_WARP_VOTING_INIT_3B
					int   use     = 0;

					//assume that neighbor cell doens not contain more than 32
					for (int atom_idx = lane_id; atom_idx < 32; atom_idx += LANE_SIZE) {
						if(atom_idx < nAtomCell[neighbor_idx]){
							atom 	= cellData[neighbor_idx * nAtomCellMax + atom_idx];
						#ifdef USE_VECTOR_GL_3B
							ai 		= make_float4(r[atom * 3 + 0], r[atom * 3 + 1], r[atom * 3 + 2], rType[atom]);
							ai_type = (int)ai.w;
						#else
							ai_x    = r[atom * 3 + 0];
							ai_y    = r[atom * 3 + 1];
							ai_z    = r[atom * 3 + 2];
							ai_type = rType[atom];
						#endif
							
							// check if atom will interact with any atoms inside a cell 
							for(int a = 0; a < nAtomCell[cell_idx]; a++)
							{
							#ifdef USE_VECTOR_GL_3B
								float dx = shr_coord[warp_id][a].x - ai.x;
								float dy = shr_coord[warp_id][a].y - ai.y;
								float dz = shr_coord[warp_id][a].z - ai.z;
								float rr = dx*dx + dy*dy + dz*dz;

								if(ai_type != (int)shr_coord[warp_id][a].w || rr < con_r02_3B[(int)shr_coord[warp_id][a].w] ){
									use = 1;
									break;
								}
							#else
								float dx = shr_coord_x[warp_id][a] - ai_x;
								float dy = shr_coord_y[warp_id][a] - ai_y;
								float dz = shr_coord_z[warp_id][a] - ai_z;
								float rr = dx*dx + dy*dy + dz*dz;

								if(ai_type != shr_r_type[warp_id][a] || rr < con_r02_3B[shr_r_type[warp_id][a]] ){
									use = 1;
									break;
								}
							#endif
							}
						}

					#ifdef CUDA_9
						int votes = __ballot_sync(FULL_MASK, use);
					#else
						int votes = __ballot(use);
					#endif
						
						if(use == 1){
							// Mask for computing atom offset.
							// For example, if votes = 0x1101 then for thread in lane 2, the mask is 0x0111
							// then the offset is __popc(0x1101 & 0x0111)-1 = 1. 
							// __popc() returns sum of '1' in a 32-bit word.
							unsigned int mask = ((lane_id) < 31) ? ((1 << ((lane_id) + 1)) - 1) : 0xffffffff;

							int atom_offset =  __popc( mask & votes) - 1;
						#ifdef USE_VECTOR_GL_3B
							cellCoord[cell_idx_chuck * maxAtomCountPerCell + atom_count +  atom_offset] 	= ai;
						#else
							cellCoord_x[cell_idx_chuck * maxAtomCountPerCell + atom_count +  atom_offset] = ai_x;
							cellCoord_y[cell_idx_chuck * maxAtomCountPerCell + atom_count +  atom_offset] = ai_y;
							cellCoord_z[cell_idx_chuck * maxAtomCountPerCell + atom_count +  atom_offset] = ai_z;
							cellRType[  cell_idx_chuck * maxAtomCountPerCell + atom_count +  atom_offset] = ai_type;
						#endif	
							cellRSCMask[cell_idx_chuck * maxAtomCountPerCell + atom_count +  atom_offset] = cell_mask;
							cellAtomIdx[cell_idx_chuck * maxAtomCountPerCell + atom_count +  atom_offset] = atom;
						}
						
						atom_count += __popc(votes);
				#else
					for (int atom_idx = lane_id; atom_idx < nAtomCell[neighbor_idx]; atom_idx += LANE_SIZE) {
				
						atom 	= cellData[neighbor_idx * nAtomCellMax + atom_idx];
					#ifdef USE_VECTOR_GL_3B
						ai 		= make_float4(r[atom * 3 + 0], r[atom * 3 + 1], r[atom * 3 + 2], rType[atom]);
						ai_type = (int)ai.w;
					#else
						ai_x    = r[atom * 3 + 0];
						ai_y    = r[atom * 3 + 1];
						ai_z    = r[atom * 3 + 2];
						ai_type = rType[atom];
					#endif

						// check if atom will interact with any atoms inside a cell 
						for(int a = 0; a < nAtomCell[cell_idx]; a++)
						{
						#ifdef USE_VECTOR_GL_3B
							float dx = shr_coord[warp_id][a].x - ai.x;
							float dy = shr_coord[warp_id][a].y - ai.y;
							float dz = shr_coord[warp_id][a].z - ai.z;
						#else
							float dx = shr_coord_x[warp_id][a] - ai_x;
							float dy = shr_coord_y[warp_id][a] - ai_y;
							float dz = shr_coord_z[warp_id][a] - ai_z;
						#endif
							float rr = dx*dx + dy*dy + dz*dz;
							
						#ifdef USE_VECTOR_GL_3B
							if(ai_type != (int)shr_coord[warp_id][a].w || rr < con_r02_3B[(int)shr_coord[warp_id][a].w] ){
						#else
							if(ai_type != shr_r_type[warp_id][a] || rr < con_r02_3B[shr_r_type[warp_id][a]] ){
						#endif
								int atom_location = atomicAdd(&shr_atom_count[warp_id], 1);
								
							#ifdef USE_VECTOR_GL_3B
								cellCoord[cell_idx_chuck * maxAtomCountPerCell + atom_location] 	= ai;
							#else
								cellCoord_x[cell_idx_chuck * maxAtomCountPerCell + atom_location] = ai_x;
								cellCoord_y[cell_idx_chuck * maxAtomCountPerCell + atom_location] = ai_y;
								cellCoord_z[cell_idx_chuck * maxAtomCountPerCell + atom_location] = ai_z;
								cellRType[  cell_idx_chuck * maxAtomCountPerCell + atom_location] = ai_type;
							#endif
								cellRSCMask[cell_idx_chuck * maxAtomCountPerCell + atom_location] = cell_mask;
								cellAtomIdx[cell_idx_chuck * maxAtomCountPerCell + atom_location] = atom;
								break;
							}
						}
					#endif
					}
				}
	}}}
	
#ifdef USE_WARP_VOTING_INIT_3B
	if (lane_id == 0 ) cellAllAtomCount[cell_idx_chuck] = atom_count;
#else
	__syncthreads();
	if (lane_id == 0 ) cellAllAtomCount[cell_idx_chuck] = shr_atom_count[warp_id];
#endif
	
}

__global__ void finalized_lj3D_data( 	float* cellRa_x,
										 	float* cellRa_y,
											float* cellRa_z,
											int*   cellAtomIdx,
											int*   cellAllAtomCount,
											int    maxAtomCountPerCell,
											double* ra,
											int chunk_id,
											int chunk_size,
											int   gpuId, 
											int   nGpus3B	
){
	int warp_id = threadIdx.x / LANE_SIZE;
	int lane_id = threadIdx.x % LANE_SIZE;

	//int bid = blockIdx.x + blockIdx.y * con_vCellAll[0] + blockIdx.z * con_vCellAll[0] * con_vCellAll[1];
	//int cell_idx = (blockIdx.x * FINAL_WARP_COUNT_3B + warp_id) + blockIdx.y * con_vCellAll[0] + (blockIdx.z + chunk_id * chunk_size)* con_vCellAll[0] * con_vCellAll[1];
	int cell_idx_chunk = (blockIdx.x * FINAL_WARP_COUNT_3B + warp_id) + blockIdx.y * con_vCellAll[0] + (blockIdx.z)* con_vCellAll[0] * con_vCellAll[1];

	int i = (blockIdx.z + chunk_id * chunk_size);
	int j = blockIdx.y;
	int k = (blockIdx.x * FINAL_WARP_COUNT_3B + warp_id);

	if(i >= con_vCellAll[2] || j >= con_vCellAll[1] || k >= con_vCellAll[0]) return;
	
	for (int atom_idx = lane_id; atom_idx < cellAllAtomCount[cell_idx_chunk]; atom_idx += LANE_SIZE) {
		int atom = cellAtomIdx[cell_idx_chunk * maxAtomCountPerCell + atom_idx];
		
		atomicAdd(&ra[atom * 3 + 0], 0.5 * cellRa_x[cell_idx_chunk * maxAtomCountPerCell + atom_idx]); 
		atomicAdd(&ra[atom * 3 + 1], 0.5 * cellRa_y[cell_idx_chunk * maxAtomCountPerCell + atom_idx]);
		atomicAdd(&ra[atom * 3 + 2], 0.5 * cellRa_z[cell_idx_chunk * maxAtomCountPerCell + atom_idx]);
	}
}

__global__ void computeAccel_lj_cell3B_gpu(	
					#ifdef USE_VECTOR_GL_3B
						float4 *cellCoord, 
					#else
						float *cellCoord_x, 
						float *cellCoord_y, 
						float *cellCoord_z, 
					#endif
						float *ra_x,
						float *ra_y,
						float *ra_z, 
						float *cellEn, 
					#ifndef USE_VECTOR_GL_3B
						int   *cellRType,
					#endif
						unsigned int *cellRSCMask,  
						int   *cellAtomCount,
						int   *cellAllAtomCount,
						int   chunk_id,
						int   chunk_size,
						int   gpuId, 
						int   nGpus3B) {


	//int cellIdx = blockIdx.x + blockIdx.y * con_vCellAll[0] + (blockIdx.z + chunk_id * chunk_size) * con_vCellAll[0] * con_vCellAll[1];
	int cell_idx_chuck = blockIdx.x + blockIdx.y * con_vCellAll[0] + (blockIdx.z) * con_vCellAll[0] * con_vCellAll[1];
	int cell_offset = cell_idx_chuck * NATOM_CELL3B_MAX * NEIGHBOR_COUNT;

#ifdef CUDA_9
#if defined(USE_WARP_PRIMITIVE_3B_I) || defined(USE_WARP_PRIMITIVE_3B_J)
	int warp_group = threadIdx.y % (32 / blockDim.x);
	unsigned int warp_mask = ((1 << blockDim.x) - 1) << (warp_group * blockDim.x);
#endif
#endif

	if(blockIdx.x >= con_vCellAll[0] || blockIdx.y >= con_vCellAll[1] || (blockIdx.z + chunk_id * chunk_size) >= con_vCellAll[2]) return;

	if((threadIdx.x >= cellAllAtomCount[cell_idx_chuck]) || (threadIdx.y >= cellAllAtomCount[cell_idx_chuck])) return;

	float potEnergyCell3B = 0.0;

	//choose center atom
	for(int i = 0; i < cellAtomCount[cell_idx_chuck]; i++){
		#ifdef USE_VECTOR_GL_3B
			float4 ai 		= cellCoord[cell_offset + i];
			int    ai_type 	= (int)ai.w;
		#else
			float ai_x    = cellCoord_x[cell_offset + i];
			float ai_y    = cellCoord_y[cell_offset + i];
			float ai_z    = cellCoord_z[cell_offset + i];
			int   ai_type = cellRType[cell_offset + i];
		#endif

		//float r0iType  = con_r0_3B[ai_type];
		//float dliType  = con_dl_3B[ai_type];
		//float r02iType = con_r02_3B[ai_type];
	
	#ifdef USE_WARP_PRIMITIVE_3B_I
		float ra_i_x = 0.0f;
		float ra_i_y = 0.0f;
		float ra_i_z = 0.0f;
	#endif 

		for(int j = threadIdx.y; j < cellAllAtomCount[cell_idx_chuck]; j += blockDim.y)
		{
		#ifdef USE_VECTOR_GL_3B
			float4 	aj = cellCoord[cell_offset + j];
			int aj_type = (int)aj.w;
		#else
			int aj_type = cellRType[cell_offset + j];
		#endif
			float dx, dy, dz;
			float rr;

		#ifdef USE_VECTOR_GL_3B
			dx = ai.x - aj.x;
			dy = ai.y - aj.y;
			dz = ai.z - aj.z;	
		#else
			dx = ai_x - cellCoord_x[cell_offset + j];
			dy = ai_y - cellCoord_y[cell_offset + j];
			dz = ai_z - cellCoord_z[cell_offset + j];	
		#endif
			rr = dx*dx + dy*dy + dz*dz;

			if ((ai_type == aj_type) || (rr >= con_r02_3B[ai_type])) continue;

			float rij = sqrtf(rr);
			float rijInv = 1.0f/rij;

			float3 uij;
			uij.x = dx * rijInv;
			uij.y = dy * rijInv;
			uij.z = dz * rijInv;
			float arg_ij = fmaxf(1.0f / (rij - con_r0_3B[ai_type]),-100);

		#ifdef USE_WARP_PRIMITIVE_3B_J	
			float ra_j_x = 0.0f;
			float ra_j_y = 0.0f;
			float ra_j_z = 0.0f;
		#endif

			for(int k = threadIdx.x; k < cellAllAtomCount[cell_idx_chuck]; k += blockDim.x)
			{				

			#ifdef USE_VECTOR_GL_3B
				float4 	ak = cellCoord[cell_offset + k];
				int ak_type = (int)ak.w;
			#else
				int ak_type = cellRType[cell_offset + k];
			#endif

			#ifdef USE_VECTOR_GL_3B
				dx = ai.x - ak.x;
				dy = ai.y - ak.y;
				dz = ai.z - ak.z;
			#else
				dx = ai_x - cellCoord_x[cell_offset + k];
				dy = ai_y - cellCoord_y[cell_offset + k];
				dz = ai_z - cellCoord_z[cell_offset + k];
			#endif

				rr = dx*dx + dy*dy + dz*dz;

				if 	(((j == k) || (ai_type == ak_type)) ||
					((cellRSCMask[cell_offset + i] & cellRSCMask[cell_offset + j] & cellRSCMask[cell_offset + k]) != 0) ||
					(rr >= con_r02_3B[ai_type])) continue;

				float rik = sqrtf(rr);
				float rikInv = 1.0f/rik;
				
				float3 uik;
				uik.x = dx * rikInv;
				uik.y = dy * rikInv;
				uik.z = dz * rikInv;
				float arg_ik = fmaxf(1.0f / (rik - con_r0_3B[ai_type]),-100);
				
				float ee = con_bb_3B[aj_type][ai_type][ak_type] * expf(con_dl_3B[ai_type]*(arg_ij+arg_ik));
				float dr_ij  = -con_dl_3B[ai_type] * arg_ij * arg_ij;
				float dr_ik  = -con_dl_3B[ai_type] * arg_ik * arg_ik;
				float tt = uij.x * uik.x + uij.y * uik.y + uij.z * uik.z;
				float xx = tt - con_cosb_3B[aj_type][ai_type][ak_type];
				float yy = 1.0f/(1.0f + (con_c3b_3B[aj_type][ai_type][ak_type] * xx * xx));
				float eexxyy = ee * xx * yy;
			
				float3 dvij;
				dvij.x = eexxyy * (uij.x * dr_ij * xx + 2.0 * yy*(uik.x - tt * uij.x) * rijInv);
				dvij.y = eexxyy * (uij.y * dr_ij * xx + 2.0 * yy*(uik.y - tt * uij.y) * rijInv);
				dvij.z = eexxyy * (uij.z * dr_ij * xx + 2.0 * yy*(uik.z - tt * uij.z) * rijInv);

				float3 dvik;
				dvik.x = eexxyy * (uik.x * dr_ik * xx + 2.0 * yy*(uij.x - tt * uik.x) * rikInv);
				dvik.y = eexxyy * (uik.y * dr_ik * xx + 2.0 * yy*(uij.y - tt * uik.y) * rikInv);
				dvik.z = eexxyy * (uik.z * dr_ik * xx + 2.0 * yy*(uij.z - tt * uik.z) * rikInv);

			#ifdef USE_WARP_PRIMITIVE_3B_I
				ra_i_x += -(dvij.x + dvik.x);
				ra_i_y += -(dvij.y + dvik.y);
				ra_i_z += -(dvij.z + dvik.z);
			#else
			 	atomicAdd(&ra_x[cell_offset + i], -(dvij.x + dvik.x));
				atomicAdd(&ra_y[cell_offset + i], -(dvij.y + dvik.y));
				atomicAdd(&ra_z[cell_offset + i], -(dvij.z + dvik.z));
			#endif 

			#ifdef USE_WARP_PRIMITIVE_3B_J		
				ra_j_x += dvij.x;
				ra_j_y += dvij.y;
				ra_j_z += dvij.z;
			#else
				atomicAdd(&ra_x[cell_offset + j], dvij.x);
				atomicAdd(&ra_y[cell_offset + j], dvij.y);
				atomicAdd(&ra_z[cell_offset + j], dvij.z);
			#endif

				atomicAdd(&ra_x[cell_offset + k], dvik.x);
				atomicAdd(&ra_y[cell_offset + k], dvik.y);
				atomicAdd(&ra_z[cell_offset + k], dvik.z);

				potEnergyCell3B += eexxyy * xx * yy;
			}

		#ifdef USE_WARP_PRIMITIVE_3B_J
			for (int offset = blockDim.x / 2; offset > 0; offset /= 2) 
			{
			#ifdef CUDA_9
				ra_j_x += __shfl_down_sync(warp_mask, ra_j_x, offset);
				ra_j_y += __shfl_down_sync(warp_mask, ra_j_y, offset);
				ra_j_z += __shfl_down_sync(warp_mask, ra_j_z, offset);			
			#else
    			ra_j_x += __shfl_down(ra_j_x, offset, blockDim.x);
				ra_j_y += __shfl_down(ra_j_y, offset, blockDim.x);
				ra_j_z += __shfl_down(ra_j_z, offset, blockDim.x);			
			#endif
			}
			
			if(threadIdx.x == 0)
			{
				atomicAdd(&ra_x[cell_offset + j], ra_j_x);
				atomicAdd(&ra_y[cell_offset + j], ra_j_y);
				atomicAdd(&ra_z[cell_offset + j], ra_j_z);
			}
		#endif 
		}

	#ifdef USE_WARP_PRIMITIVE_3B_I
		for (int offset = blockDim.x * blockDim.y / 2; offset > 0; offset /= 2) 
		{
		#ifdef CUDA_9
			ra_i_x += __shfl_down_sync(FULL_MASK, ra_i_x, offset);
			ra_i_y += __shfl_down_sync(FULL_MASK, ra_i_y, offset);
			ra_i_z += __shfl_down_sync(FULL_MASK, ra_i_z, offset);			
		#else
			ra_i_x += __shfl_down(ra_i_x, offset, blockDim.x * blockDim.y);
			ra_i_y += __shfl_down(ra_i_y, offset, blockDim.x * blockDim.y);
			ra_i_z += __shfl_down(ra_i_z, offset, blockDim.x * blockDim.y);			
		#endif
		}
		
		if(threadIdx.x == 0 && threadIdx.y == 0)
		{
			atomicAdd(&ra_x[cell_offset + i], ra_i_x);
			atomicAdd(&ra_y[cell_offset + i], ra_i_y);
			atomicAdd(&ra_z[cell_offset + i], ra_i_z);
		}
	#endif
	}
	atomicAdd(&cellEn[cell_idx_chuck], (float)potEnergyCell3B * 0.5 );
}	
	

#if !defined(__CUDA_ARCH__) || __CUDA_ARCH__ >= 600
#else
__device__ double atomicAdd(double* address, float val)
{
  double double_val = (double)val;
  return atomicAdd(address, double_val);
}

__device__ double atomicAdd(double* address, double val)
{
    unsigned long long int* address_as_ull =
                          (unsigned long long int*)address;
    unsigned long long int old = *address_as_ull, assumed;
    do {
        assumed = old;
            old = atomicCAS(address_as_ull, assumed,__double_as_longlong(val +
                               __longlong_as_double(assumed)));
    } while (assumed != old);
    return __longlong_as_double(old);
}
#endif

