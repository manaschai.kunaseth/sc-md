#ifndef __FILE_UTILS_H__
#define __FILE_UTILS_H__

int splitLine(char* fileName, char** output, int maxLine);
void createOutputDir(int step, char* dirName);

#define MAX_LINE_CHAR_LENGTH 2048

#endif

