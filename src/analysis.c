#include "constants.h"
#include "params.h"
#include "state.h"
#include "input.h"
#include "analysis.h"
#include "analysis_collection.h"
#include "cell.h"
#include "simulate.h"
#include "timing.h"
#include "utils.h"

void checkPreLoopAnalysis(unsigned long stepCount, STATE* s)
{
	timerStart(TIMING_ANALYSIS);
	INPUT_PARAMS* inp = getInputObj();
	PARAMS* p = getParamsObj();
	if (inp->analyzeFixBoundary > 0) {
		analysis_applyFixBoundary(s,p);
	}
	//analysis_collection_init(s,p);
	timerStop(TIMING_ANALYSIS);
}


void checkPreUpdateCoordAnalysis(unsigned long stepCount, STATE* s)
{
	timerStart(TIMING_ANALYSIS);
	INPUT_PARAMS* inp = getInputObj();
	PARAMS* p = getParamsObj();

	//Just test
	analysis_collection_eval(s,p);
	/* 
	/ Wrong: should not check every step
	if (inp->analyzeFixBoundary > 0) {
		analysis_applyFixBoundary(s,p);
	}
	*/
	timerStop(TIMING_ANALYSIS);
}


void checkPreStepAnalysis(unsigned long stepCount, STATE* s)
{
	timerStart(TIMING_ANALYSIS);
	INPUT_PARAMS* inp = getInputObj();
	PARAMS* p = getParamsObj();
	if ((inp->analyzeReorder > 0) && (stepCount % inp->analyzeReorderFreq == 0)) {
		CELL_INFO *cellInfo = simulateGetCellInfo(2);
		analysis_reorderState(s,p,cellInfo,2);
	}
	if ((inp->analyzeApplyStrainFreq > 0) && (stepCount % inp->analyzeApplyStrainFreq == 0)) {
		analysis_applyStrain(s,p);
	}
	timerStop(TIMING_ANALYSIS);
}

void checkPostStepAnalysis(unsigned long stepCount, STATE* s)
{
	timerStart(TIMING_ANALYSIS);
	INPUT_PARAMS* inp = getInputObj();
	if ((inp->analyzeVelocityScalingFreq > 0) && (stepCount % inp->analyzeVelocityScalingFreq == 0)) { 
		PARAMS* p = getParamsObj();
		unsigned long startStep = p->startStep;
		double stepRatio = ((double)(stepCount-startStep))/(inp->stepLimit);
		double reqTemp = inp->analyzeVelocityScalingTempBegin+
					stepRatio*(inp->analyzeVelocityScalingTempEnd-inp->analyzeVelocityScalingTempBegin);
		analysis_velocityScaling(s,reqTemp/TEMPAU);	
	}
	timerStop(TIMING_ANALYSIS);
}


void checkPostSimulationAnalysis(STATE* s)
{
	timerStart(TIMING_ANALYSIS);
	analysis_collection_writeOutput();
	timerStop(TIMING_ANALYSIS);
}


