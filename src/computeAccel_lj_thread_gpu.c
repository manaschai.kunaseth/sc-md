#include <stdio.h>
#include "computeAccel_lj.h"
#include "computeAccel_lj_gpu.h"
#include "pot_lj.h"
#include "potential.h"
#include "computePattern.h"
#include "state.h"
#include "params.h"
#include "cell.h"
#include "timing.h"
#include "omp.h"
#include "utils.h"
#include "mpi.h"

//#define NUM_GPUS 1

#define MAX_NTYPE 2
#define MAX_NTAB_INDEX 3

#define NATOM_CELL2B_MAX 32
#define CONST_NATOM_CELL3B_MAX 5

#define atomIdx(i,xyz) (i*3)+xyz
#define dataIdx(c,i,xyz) c*NATOM_CELL2B_MAX*4+(i*4)+xyz
//#define data3BIdx(c,i,xyz) c*NATOM_CELL3B_MAX*4+(i*4)+xyz

//universal parameter for potential/force calculation.
static double dh2;
//static double dh2Inv;     //1.0/(dh^2)   : dh = pot_table spacing;
static float dh2Inv;     //1.0/(dh^2)   : dh = pot_table spacing;
static double** rcut2_2B; //Cut-off^2 for each atom type
static POTENTIAL_LJ_EXTENSION *potExt;
static double*** forceTable;
static double*** potentialTable;
//static double rij2max;
static float rij2max;

static int isInitTexture = 0;

void computeAccel_lj_thread_gpu(STATE* s, POTENTIAL* pot, PARAMS* p,CELL_INFO* cellInfoSet)
{

	INPUT_PARAMS* inp = getInputObj();
	int nGpus2B = inp->nGpus[2];
	int nGpus3B = inp->nGpus[3];
	//int nGpus = nGpus2B + nGpus3B;
	//int nGpus = NUM_GPUS;
	int nGpus = inp->nGpus[0];
	int nThreads2B = inp->nThreads[2];
	int nThreads3B = inp->nThreads[3];
	int nThreadsGlobal = nThreads2B + nThreads3B;
	omp_set_num_threads(nGpus);
	double potE2b = 0.0;
	double potE3b = 0.0;

	int mpi_rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);

	#ifndef GPU
	//if GPU is not defined, code will not compile correctly for GPU calls.
		terminateSingle("Error: Code is compiled without -DGPU flag enabled but GPU_MODE was set to 1. Terminated.",1);
		
	#endif

	if (isInitTexture == 0)
	{
		#ifdef GPU
		//init static parameters on GPU for 2B calculation
		//for (int i = 0;i < nGpus2B;i++) {
			//initTables2B_gpu(pot,i);
			//reportVerboseInt("Initialize GPU table for 2-body calculation on GPU #%d\n",i);
		//}
		int threadId = mpi_rank % nGpus;
		initTables2B_gpu_lj(pot,threadId);
		#endif		
		isInitTexture = 1;
	}
	//end defining general parameter within this file.

	omp_set_nested(1);


        if (omp_get_nested() == 0)
        {
		terminateSingle("omp_get_nested() == 0. Nested OMP parallel feature is needed in OMP version.",1);
        }

	gpu_return ret;
	//#pragma omp parallel sections \
				default(none) \
				shared(ret, p,s,pot,cellInfoSet,nThreads2B,nThreads3B,nGpus2B,nGpus3B) \
				reduction(+:potE2b,potE3b) \
				num_threads(3) 
	{
	//	#pragma omp section 
		{
			//if (nGpus2B > 0) {
				forceTimingRelation(TIMING_COMPUTE_ACCEL_2B_GPU,TIMING_COMPUTE_ACCEL);
				timerStart(TIMING_COMPUTE_ACCEL_2B_GPU);
				CELL_LIST *cl2b = NULL;

				cl2b = makeCellList(cl2b,&cellInfoSet[INDEX_2B],s,p->boxSize,paramsGetRCut(2),p->commInfo.rCacheRadius,p->cell_nlayer[2]);
//				#pragma omp parallel \
					default(none) \
					shared(p,s,pot,cl2b,cellInfoSet,nGpus2B,nGpus3B) \
					reduction(+:potE2b,potE3b) \
					num_threads(nGpus2B) 
					{
						//int threadId = omp_get_thread_num();
						int threadId = mpi_rank % nGpus;
						//int threadId = 0;
						//printf("myid = %d, running GPU 2B\n",threadId);
						#ifdef GPU
						potE2b += computeAccel_lj2B_gpu(s,cl2b,pot,&cellInfoSet[INDEX_2B].computePatternCell,threadId,nGpus2B);
						#endif
					}
			freeCellListData(cl2b);
			timerStop(TIMING_COMPUTE_ACCEL_2B_GPU);
			//}
		}
		#if 0
		double rsum[3] = {0.0};
		for (int i = 0;i < s->nAtomOwn+s->nAtomCache;i++)
			for (int a = 0;a < 3;a++)
				rsum[a] += abs(s->ra[i][a]);
		
		printf("RA: %10.6le %10.6le %10.6le\n",rsum[0],rsum[1],rsum[2]);

		printf("-------------\n");
		for (int i = 0;i < s->nAtomOwn+s->nAtomCache;i++)
			printf("RA %5d: %10.6le %10.6le %10.6le\n",s->rgid[i],s->ra[i][0],s->ra[i][1],s->ra[i][2]);
		printf("\n");
		#endif

	//	#pragma omp section
		/*
		{
			//if (nGpus3B > 0) {
				forceTimingRelation(TIMING_COMPUTE_ACCEL_3B_GPU,TIMING_COMPUTE_ACCEL);
				timerStart(TIMING_COMPUTE_ACCEL_3B_GPU);
				CELL_LIST *cl3b = NULL;
				cl3b = makeCellList(cl3b,&cellInfoSet[INDEX_3B],s,p->boxSize,paramsGetRCut(3),p->commInfo.rCacheRadius,p->cell_nlayer[3]);
	//				#pragma omp parallel \
					default(none) \
					shared(ret,p,s,pot,cl3b,cellInfoSet,nGpus2B,nGpus3B) \
					reduction(+:potE2b,potE3b) \
					num_threads(nGpus3B)
					{
						//int threadId = omp_get_thread_num()+nGpus2B;
						//int threadId = 0;
						int threadId = mpi_rank % nGpus;
						//printf("myid+nGpu2B = %d, running GPU 3B\n",threadId);
						#ifdef GPU
						ret = computeAccel_silica3B_gpu(s,cl3b,pot,&cellInfoSet[INDEX_3B].computePatternCell,threadId,nGpus3B);
						potE3b += ret.potEnergy;
						//potE3b += computeAccel_silica3B_gpu(s,cl3b,pot,&cellInfoSet[INDEX_3B].computePatternCell,threadId,nGpus3B);
						#endif
					}	
				freeCellListData(cl3b);
				timerStop(TIMING_COMPUTE_ACCEL_3B_GPU);
			//}
		}
		*/
	//	#pragma omp section 
		{
			/*
			if (nThreads2B+nThreads3B > 0)
			{
				computeAccel_silica_thread(s,pot,p,cellInfoSet);
			}
			*/
		}
	}
	//#pragma omp barrier

	s->potentialEnergy[INDEX_2B] += potE2b;
	//s->potentialEnergy[INDEX_3B] += potE3b;
}

/*
void threadSafeSum_ra(double (*ra)[3], float* host_ra,int vCellAllXYZ,int* nAtomCell,int **cellData, int cellSize)
{
	for (int c = 0; c < vCellAllXYZ;c++){
		for (int i = 0;i < nAtomCell[c];i++) {
			int idx = cellData[c][i];
			for (int a = 0;a < 3;a++){
				#pragma omp atomic
				//ra[idx][a] += host_ra[c*CONST_NATOM_CELL3B_MAX*3+3*i+a];
				ra[idx][a] += host_ra[c*cellSize*3+3*i+a];
				
			}
		}
	}
}

void threadSafeSum_ra_gpu(double (*ra)[3], double (*host_ra)[3],int vCellAllXYZ,int* nAtomCell,int **cellData, int cellSize)
{
	for (int c = 0; c < vCellAllXYZ;c++){
		for (int i = 0;i < nAtomCell[c];i++) {
			int idx = cellData[c][i];
			for (int a = 0;a < 3;a++){
				#pragma omp atomic
				//ra[idx][a] += host_ra[c*CONST_NATOM_CELL3B_MAX*3+3*i+a];
				ra[idx][a] += host_ra[idx][a];
				
			}
		}
	}
}
*/	
