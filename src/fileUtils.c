#include "fileUtils.h"
#include "utils.h"
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

int splitLine(char* fName, char** out, int maxLine)
{
	//char str[MAX_LINE_CHAR_LENGTH];

	FILE* fp = fopen(fName, "r");

	 // terminate if file not found
	if(!fp) {
		char msg[1000];
		sprintf(msg,"Cannot find %s",fName);
		terminateSingle(msg,1);	
	}

	int lineCount = 0;
	while(fgets(out[lineCount],MAX_LINE_CHAR_LENGTH,fp) != NULL)
	{
		inPlaceTrim(out[lineCount]);

		int len = strlen(out[lineCount]);
		//if empty line, don't store
		if(len == 0) continue;
		
		//if comment line, don't store
		if (out[lineCount][0] == '#') continue;
	
		//No longer convert to upper char. This will be done later.
		//strToUpper(out[lineCount]);     

		lineCount++;

		//Assert: 
		//if (lineCount >= maxLine) Error:#of line exceed maximum number of line
		assert(lineCount < maxLine);
	}

	fclose(fp);

	return lineCount;
}


void createOutputDir(int step, char* dirName)
{
	sprintf(dirName,"snapshot.%011d",step);
	/*
	int isFound = 0;
	struct stat st;
	if(stat(dirName,&st) == 0) {
		if(((st.st_mode) & S_IFMT) == S_IFDIR){
			isFound = 1;
		}
	}

	//I'm not sure if umask is needed. May have to check more careful if permission failed.
	if (!isFound) {
		mode_t process_mask = umask(0);
		int result_code = mkdir(dirName, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
		umask(process_mask);
		reportPrintStr("Creating snapshot dir: ",dirName);
	}
	*/
	int result_code = mkdir(dirName, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
}


