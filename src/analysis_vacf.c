#include "state.h"
#include "params.h"
#include "simulate.h"
#include "analysis_collection.h"
#include "utils.h"
#include "timing.h"
#include "memory.h"
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include "fileUtils.h"

void analysis_vacf_fill(STATE* s, PARAMS *p,double *sendbuffer, int recordLength,int recordOffset) {

	//convert own atoms' coordinate and pack into buffer
	double (*rv)[3]  = s->rv;
	int nAtomOwn = s->nAtomOwn;

	for (int i = 0; i < nAtomOwn;i++) {
		for (int a = 0;a < 3;a++) {
			sendbuffer[recordLength*i+recordOffset+a] = rv[i][a];
		}
	}
}

void analysis_vacf_compute(STATE* s, PARAMS *p,ANALYSIS_STORE* ast,double *collVec, int collRowLength,int recordOffset) {

	/*
	-------------------------------------------
	Short description for VACF compute routine
	-------------------------------------------
	atomic velocities were collected
	There are ANALYSIS_VACF_BUFFER number of buffers 
	The buffer is replaced in a round robin fashion
	For every ANALYSIS_VACF_INTERVAL step, store atomic velocities in the buffers
	
	The life time of each buffer frame is ANALYSIS_VACF_INTERVAL*ANALYSIS_VACFBUFFER
	When buffer complete collecting result, VACF are added to the accumulator (vacfAcc
	*/

	int nCollect = ast->vacf_nCollect;
	unsigned long nAtomGlobal = s->nAtomGlobal;

	double *vacfAcc = ast->vacfAcc;
	double **vacfBuffer = ast->vacfBuffer;
	int collectCall = ast->vacf_collectCall;
	collectCall++;


	int offset = (collectCall % ANALYSIS_VACF_INTERVAL);
	int firstBufferCollect = (collectCall / ANALYSIS_VACF_INTERVAL);
	int lastBufferCollect = firstBufferCollect - ANALYSIS_VACF_BUFFER;
	if (lastBufferCollect < 0)
		lastBufferCollect = 0;

	double (**vOrig)[3] = ast->vOrig;
	int fIdx = firstBufferCollect % ANALYSIS_VACF_BUFFER;
	if ((collectCall % ANALYSIS_VACF_INTERVAL) == 0) {
		for (int i = 0;i < nAtomGlobal; i++) {
			for (int a = 0;a < 3;a++)
				vOrig[fIdx][i][a] = collVec[collRowLength*i+recordOffset+a];	
		}
	}

	int bufferCount = 0;
	for (int k = firstBufferCollect;k >= lastBufferCollect;k--) {
		double vAcc[4] = {0.0,0.0,0.0,0.0};
		int kIdx = k % ANALYSIS_VACF_BUFFER;
		for (int i = 0;i < nAtomGlobal; i++) {
			double vt[3];
			for (int a = 0;a < 3;a++) {
				vt[a] = vOrig[kIdx][i][a]*collVec[collRowLength*i+recordOffset+a];
				vAcc[a] += vt[a];
			}	
			vAcc[3] += vt[0]+vt[1]+vt[2];
		}
		for (int a = 0;a < 4;a++)
			vAcc[a] /= nAtomGlobal;	

		vacfBuffer[kIdx][bufferCount*ANALYSIS_VACF_INTERVAL+offset] = vAcc[3];
		bufferCount++;
	}

	if (collectCall >= ANALYSIS_VACF_INTERVAL*ANALYSIS_VACF_BUFFER) {
		if ((collectCall % ANALYSIS_VACF_INTERVAL) == 0) {
			int idxCollect = (firstBufferCollect+1)% ANALYSIS_VACF_BUFFER;
			for (int i = 0;i < nCollect;i++) 
				vacfAcc[i] += vacfBuffer[idxCollect][i];

			ast->vacf_accumulateCount++;
		}
	}

	//printf("collectCall = %d\n",collectCall);
	ast->vacf_collectCall = collectCall;

}


void analysis_vacf_writeOutput(ANALYSIS_STORE* ast) {

	double *vacfAcc = ast->vacfAcc;
	int accumulateCount = ast->vacf_accumulateCount;
	int nCollect = ast->vacf_nCollect;
		
	char dirName[1000];
	char fileName[100];
	char fullFilePath[1100];

	createOutputDir(simulateGetCurrentStep(),dirName);
	sprintf(fileName,"%07d.vacf.out",getMyId());
	sprintf(fullFilePath, "%s/%s",dirName,fileName);
	
	PARAMS* p = getParamsObj();
	double timeStep = p->deltaT*(TIMEU/1e-15);
	FILE* fp = fopen(fullFilePath, "w");
	fprintf(fp,"#Bin t(fs) VACF(t)\n");
	//for (int i = 0;i < nCollect;i++) { 
	for (int i = 0;i < ANALYSIS_VACF_INTERVAL*ANALYSIS_VACF_BUFFER;i++) { 
		fprintf(fp,"%d %le %le\n",i,i*timeStep*ast->vacf_freq,vacfAcc[i]/accumulateCount);
	}
	fclose(fp);



}


