#include "timing.h"
#include "utils.h"
#include "fileUtils.h"
#include "adt.h"
#include "simulate.h"
#include "input.h"
#include "mpi.h"
#include <math.h>
#include <stdio.h>

//section description must be in accordance with timingSection enum in timing.h
static char sectionDescription[TIMING_SECTIONS_MAX][MAX_DESCRIPTION_LENGTH] = 
{	
	"SC-MD",
	"Init",
	"Read_Atom_Config",
	"MD_Loops",
	"MD_Step",
	"Comm_Cache",
	"Comm_Move",
	"Comm_Force_Return",
	"ComputeAccel",
	"ComputeAccel_2Body",
	"ComputeAccel_3Body",
	"Analysis",
	"Analysis_Collection",
	"Write_Output_XYZ",
	"Write_Output_MTS",
	"Write_Output_AtomicForce",
	"Reordering",
	"Load_Imbalance0",
	"Load_Imbalance1",
	"BuildNeighbor",
	"ComputeAccel_2Body_Thread",
	"ComputeAccel_3Body_Thread",
	"ComputeAccel_2Body_GPU",
	"ComputeAccel_3Body_GPU",
	"ComputeAccel_2Body_MIC",
	"ComputeAccel_3Body_MIC"
};

static ELAPSE_TIMER timer[TIMING_SECTIONS_MAX];
static STACK_INT sectionStack;

static void resetTimer(int ts);
static double getTimerAverage(int ts);
static double getTimerStdv(int ts);
static void DFS(int node,int *visited, FILE* fp, int lvl);


void timerInit()
{
	for (int i = 0;i < TIMING_SECTIONS_MAX;i++)
	{
		resetTimer(i);
	}
	stackIntInit(&sectionStack,(int)TIMING_SECTIONS_MAX);
}

void resetTimer(int ts)
{
	int tIdx = (int)ts;
	timer[tIdx].root = -1;
	timer[tIdx].isStart = 0;
	timer[tIdx].sumInterval	= 0.0;
	timer[tIdx].sumInterval2 = 0.0;
	timer[tIdx].sumChildren = 0.0;
	timer[tIdx].children[0] = 0;
	strcpy(timer[tIdx].description,sectionDescription[tIdx]);
}

void resetTimer_collection()
{

	for (int tIdx = 0;tIdx < TIMING_SECTIONS_MAX;tIdx++)
	{
		timer[tIdx].sumInterval	= 0.0;
		timer[tIdx].sumInterval2 = 0.0;
		timer[tIdx].sumChildren = 0.0;
	}
}



void timerStart(TIMING_SECTIONS ts)
{
	int tIdx = (int)ts;
	if (timer[tIdx].isStart != 0)
	{
		char msg[1000];
		sprintf(msg,"Timer has already been started: %s",timer[tIdx].description);
		terminateNode(msg,1);
	}

	if ((timer[tIdx].root < 0) && (tIdx > 0))
	{
		int peek;
		peek = stackIntPeek(&sectionStack);
		timer[tIdx].root = peek;
		timer[peek].children[++timer[peek].children[0]] = tIdx;
	}
	stackIntPush(&sectionStack,tIdx);
	timer[tIdx].isStart = 1;
	timer[tIdx].startMark = MPI_Wtime();
}



void timerStop(TIMING_SECTIONS ts) 
{
	int tIdx = (int)ts;
	if (timer[tIdx].isStart != 1)
	{
		char msg[1000];
		sprintf(msg,"Timer has not been started: %s",timer[tIdx].description);
		terminateNode(msg,1);
	}
	double interval = MPI_Wtime() - timer[tIdx].startMark;
	timer[tIdx].sumInterval  += interval;
	timer[tIdx].sumInterval2 += interval*interval;
	timer[tIdx].count++;

	stackIntPop(&sectionStack);
	int priorNode;
	priorNode = stackIntPeek(&sectionStack);
	timer[tIdx].isStart = 0;

	if (stackIntSize(&sectionStack) > 0)
		timer[priorNode].sumChildren += interval;
	
}

void forceTimingRelation(TIMING_SECTIONS tsChild, TIMING_SECTIONS tsParent)
{
	int tChild = (int)tsChild;
	int tParent = (int)tsParent;
	if (timer[tChild].root < 0)
	{
		#pragma omp critical
		{
			timer[tChild].root = (int)tsParent;
			//++timer[tParent].children[0];
			timer[tParent].children[++timer[tParent].children[0]] = tChild;
		}
	}
}

void timerWriteProfile() 
{
	
	char dirName[1000];
	char fileName[100];
	char fullFilePath[1100];

	createOutputDir(simulateGetCurrentStep(),dirName);
	sprintf(fileName,"profile-%07d.txt",getMyId());
	sprintf(fullFilePath, "%s/%s",dirName,fileName);

	reportPrint("Generating runtime profiles...");
	INPUT_PARAMS* inp = getInputObj();

	//Write runtime profile:
	//  To reduce number of output file created, only some MPI ranks will write profile.
	if ((getMyId()%inp->runtimeProfileRatio) == 0)
	{
		FILE *fp;
		fp = fopen(fullFilePath,"w");

		int visited[TIMING_SECTIONS_MAX];
		for (int i = 0;i < TIMING_SECTIONS_MAX;i++)
			visited[i] = 0;

		char vline[200] = "|";
		for (int i = 0;i < 125;i++)
			strcat(vline,"-");
		strcat(vline,"|");

		fprintf(fp,"nodeID: %d\n",getMyId());

		fprintf(fp,"%s\n",vline);
		fprintf(fp,"|%-30s |%10s |%13s |%13s |%13s |%13s | %8s | %8s |\n",
			    "Sections","Count","Average(s)","STDV","Total(s)","Self(s)","%Total","%Self");
		fprintf(fp,"%s\n",vline);
		DFS(0,visited,fp,0);
		fprintf(fp,"%s\n",vline);

		fclose(fp);
	}
}

double getTimerAverage(int ts)
{
	int tIdx = (int)ts;
	return timer[tIdx].sumInterval/timer[tIdx].count;
}

double getTimerStdv(int ts)
{
	int tIdx = (int)ts;
	double avg = getTimerAverage(ts);
	double avg2 = timer[tIdx].sumInterval2/timer[tIdx].count;
	return sqrt(avg2-(avg*avg));
}



void DFS(int node,int *visited, FILE* fp, int lvl)
{
	visited[node] = 1;
	
	char desc[50] = "";
	for (int i = 0;i < lvl;i++)
		strcat(desc," ");

	strcat(desc,timer[node].description);
	double timeTotal = timer[0].sumInterval;
	double selfTime = timer[node].sumInterval - timer[node].sumChildren;

	fprintf(fp,"|%-30s |%10ld |%13.6lf |%13.6lf |%13.6lf |%13.6lf | %8.3lf | %8.3lf |\n",
			desc,
			timer[node].count,
			getTimerAverage(node),
			getTimerStdv(node),
			timer[node].sumInterval,
			selfTime,	
			100.0*timer[node].sumInterval/timeTotal,
			100.0*selfTime/timeTotal);

	for (int i = 1;i <= timer[node].children[0];i++)
		DFS(timer[node].children[i],visited,fp,lvl+1);	

}



