#include <stdio.h>
#include "cudaUtils.h"


void cudaSafeCall(cudaError_t error,const char* file, int line)
{
	if (error!=cudaSuccess) {
		fprintf(stderr, "CUDA ERROR (%s:%d): %s\n",file,line,cudaGetErrorString(error));
		exit(-1);
	}


}

void check_kernelError(const char* file, int line)
{
	//printf("Checking error from kernel...");	
	cudaError_t error = cudaGetLastError();
	cudaSafeCall(error,file,line);
	//printf("PASS\n");	
}
