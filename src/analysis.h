#ifndef __ANALYSIS_H__
#define __ANALYSIS_H__

void checkPreLoopAnalysis(unsigned long stepCount, STATE* s);
void checkPreUpdateCoordAnalysis(unsigned long stepCount, STATE* s);
void checkPreStepAnalysis(unsigned long stepCount, STATE* s);
void checkPostStepAnalysis(unsigned long stepCount, STATE* s);
void checkPostSimulationAnalysis(STATE* s);

void analysis_velocityScaling(STATE* s, double reqTemp);
void analysis_reorderState(STATE* s,PARAMS *p, CELL_INFO* cellInfo,int nBody);
void analysis_applyStrain(STATE* s,PARAMS* p);
void analysis_applyFixBoundary(STATE* s,PARAMS* p);

#endif
