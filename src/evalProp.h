#ifndef __EVAL_PROP_H__
#define __EVAL_PROP_H__

void printEvalHeader();
void evalProperties(unsigned long stepCount, STATE* s);

void velocityScaling(STATE* s, double reqTemp);

#endif
