#include "analysis_collection.h"
#include "state.h"
#include "params.h"
#include "simulate.h"
#include "utils.h"
#include "timing.h"
#include "mpi.h"
#include "input.h"
#include "memory.h"
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

ANALYSIS_STORE* analysis_data;
static void analyze_fill_sendbuffer(STATE* s, PARAMS *p, double* sendBuffer,int* enabledFlag, int bufferRowLength);
static void analyze_compute(STATE* s, PARAMS *p, double* collVec,int* enabledFlag, int collRowLength);
static double* analyze_compact_collectionVector(double* recvBuffer, unsigned long*rgid,int *rType,double *collVec,int nNodes, int* nAtomPerNode,int bufferRowLength,int nAtomNodeMax, unsigned long nAtomGlobal);

int compareGlobalId (const void * a, const void * b)
{
   double a_val = idFromData(*(double*)a);	
   double b_val = idFromData(*(double*)b);	

   return (a_val-b_val);
}

//init data collection: create atom data array to store data
void analysis_collection_init(STATE* s,PARAMS *p)
{

	//timerStart(TIMING_REORDERING);

	
	INPUT_PARAMS* inp = getInputObj();

	analysis_data = (ANALYSIS_STORE*)scMalloc(sizeof(ANALYSIS_STORE));
	analysis_data->data_per_atom = 0;

	if (inp->analyzeRdf == 1) {
	
		reportPrint("Analysis Enabled: Radial distribution function (RDF)");

		double binLength = inp->analyzeRdfBinLength;
		double rdfRCut = inp->analyzeRdfRCut/BOHR;
		int nBin = (rdfRCut/binLength)+1;

		analysis_data->rdfHist = (double(*))(scMalloc(sizeof(double)*nBin));
		analysis_data->rdfHist_nbin = nBin;
		analysis_data->rdfHist_binLength = binLength;
		analysis_data->rdfHist_rcut = rdfRCut;
		reportPrintFixedDouble("RDF analysis frequency (MD Step):",inp->analyzeRdfFreq);
		reportPrintFixedDouble("RDF cutoff radius (Ang):",rdfRCut*BOHR);
		
		double* rdfHist = analysis_data->rdfHist;
		for (int i = 0;i < nBin;i++){ 
			rdfHist[i] = 0.0;
		}
		analysis_data->data_per_atom += 3;
		//data_size += 3; //true r(x,y,z) for calculating RDF
	}

	if (inp->analyzeVacf == 1) {
	
		reportPrint("Analysis Enabled: Velocity autocorrelation function (VACF)");

		//int nCollect = inp->analyzeVacfNCollect;
		int nCollect = ANALYSIS_VACF_NCOLLECT;
		unsigned long nAtomGlobal = s->nAtomGlobal;

		analysis_data->vacfAcc = (double(*))(scMalloc(sizeof(double)*nCollect));
		analysis_data->vacfBuffer = (double(**))(scMalloc(sizeof(double*)*ANALYSIS_VACF_BUFFER));
		for (int i = 0;i < ANALYSIS_VACF_BUFFER;i++)
			analysis_data->vacfBuffer[i] = (double(*))(scMalloc(sizeof(double)*nCollect));
		
		//s->r     = (double(*)[3])(scMalloc(sizeof(double[3])*nAtomReal))

		analysis_data->vOrig = (double(**)[3])(scMalloc(sizeof(double(*)[3])*ANALYSIS_VACF_BUFFER));
		for (int i = 0;i < ANALYSIS_VACF_BUFFER;i++)
			analysis_data->vOrig[i] = (double(*)[3])(scMalloc(sizeof(double[3])*nAtomGlobal));

		for (int k = 0;k < ANALYSIS_VACF_BUFFER; k++) {
			for (int i = 0;i < nAtomGlobal; i++) {
				for (int a = 0;a < 3;a++)
					analysis_data->vOrig[k][i][a] = 0.0;	
			}
		}
		analysis_data->vacf_nCollect = nCollect;
		analysis_data->vacf_accumulateCount = 0;
		analysis_data->vacf_collectCall = -1;
		analysis_data->vacf_freq = inp->analyzeVacfFreq;
		
		double* vacfAcc = analysis_data->vacfAcc;
		for (int i = 0;i < nCollect;i++){ 
			vacfAcc[i] = 0.0;
		}
		
		reportPrintFixedDouble("VACF analysis frequency (MD Step):",analysis_data->vacf_freq);
		analysis_data->data_per_atom += 3;
		//data_size += 3; //true r(x,y,z) for calculating RDF
	}


	//timerStop(TIMING_REORDERING);
}

void analysis_collection_writeOutput() 
{
	INPUT_PARAMS* inp = getInputObj();
	if (inp->analyzeRdf == 1) {
		analysis_rdf_writeOutput(analysis_data);
	}
	if (inp->analyzeVacf == 1) {
		PARAMS* p = getParamsObj();
		COMM_INFO* commInfo = &p->commInfo;
		int myId = commInfo->myId;
		if (myId == 0) 	
			analysis_vacf_writeOutput(analysis_data);
	}
}


void analysis_collection_eval(STATE* s,PARAMS *p) 
{
	unsigned long currentStep = simulateGetCurrentStep();	
	int enabledFlag[ANALYSIS_COLLECTION_MAX];
	int data_length = 0;

	for (int i = 0;i < ANALYSIS_COLLECTION_MAX;i++)
		enabledFlag[i] = 0;

	INPUT_PARAMS* inp = getInputObj();
	if (inp->analyzeRdf == 1) {
		if ((currentStep % inp->analyzeRdfFreq) == 0) {
			enabledFlag[ANALYSIS_COLLECTION_RDF] = 1;
			data_length += ANALYSIS_COLLECTION_SIZE_RDF;
		} 
	}
	if (inp->analyzeVacf == 1) {
		if ((currentStep % inp->analyzeVacfFreq) == 0) {
			enabledFlag[ANALYSIS_COLLECTION_VACF] = 1;
			data_length += ANALYSIS_COLLECTION_SIZE_VACF;
		} 
	}

	if (data_length > 0)
		analysis_collection_collect(s,p,enabledFlag,data_length);

}

void analysis_collection_collect(STATE* s,PARAMS *p, int* enabledFlag,int dataRecordLength) 
{
	unsigned long nAtomNodeMax = 0;
	unsigned long nAtomOwn = s->nAtomOwn;
	unsigned long nGlob = s->nAtomGlobal;	
	COMM_INFO* commInfo = &p->commInfo;
	int myId = commInfo->myId;
	int nNodes = commInfo->nNodes;

	timerStart(TIMING_ANALYSIS_COLLECTION);

	//get maximum number of atoms per node
	MPI_Allreduce(&nAtomOwn, &nAtomNodeMax, 1, MPI_LONG, MPI_MAX, MPI_COMM_WORLD);
	int data_per_atom = analysis_data->data_per_atom;

	int totalBufferRowLength = dataRecordLength+1;

	//get actual number of atoms per node	
	int *nAtomPerNode = (int*)scMalloc(sizeof(int)*nNodes);
	MPI_Gather(&nAtomOwn, 1, MPI_INT, nAtomPerNode, 1, MPI_INT, 0, MPI_COMM_WORLD);

	double *recvBuffer = (double*)scMalloc(sizeof(double)*totalBufferRowLength*nNodes*nAtomNodeMax);
	double *sendBuffer = (double*)scMalloc(sizeof(double)*totalBufferRowLength*nAtomNodeMax);

	analyze_fill_sendbuffer(s,p,sendBuffer,enabledFlag,totalBufferRowLength);

	//Each node perform quicksort to sort sendbuffer based on atom global ID
	qsort(sendBuffer,nAtomOwn , sizeof(double)*totalBufferRowLength, compareGlobalId);

	MPI_Gather(sendBuffer, nAtomOwn*totalBufferRowLength, MPI_DOUBLE, recvBuffer,totalBufferRowLength*nAtomNodeMax , MPI_DOUBLE, 0, MPI_COMM_WORLD);
	//record format: atomdata,collectionVector

	unsigned long *rgid = (unsigned long*)scMalloc(sizeof(unsigned long)*nGlob);
	int *rType = (int*)scMalloc(sizeof(int)*nGlob);
	double *collectionVec = (double*)scMalloc(sizeof(double)*nGlob*dataRecordLength);
	
	if (myId == 0) //currently only node 0 perform analysis
		collectionVec = analyze_compact_collectionVector(recvBuffer,rgid,rType,collectionVec,nNodes,nAtomPerNode,totalBufferRowLength,nAtomNodeMax,nGlob);

	analyze_compute(s,p,collectionVec,enabledFlag,dataRecordLength);

	scFree(recvBuffer);
	scFree(sendBuffer);
	scFree(nAtomPerNode);
	scFree(rgid);
	scFree(rType);
	scFree(collectionVec);
	timerStop(TIMING_ANALYSIS_COLLECTION);
}

double* analyze_compact_collectionVector(double* recvBuffer, unsigned long*rgid,int *rType,double *collVec,int nNodes, int* nAtomPerNode,int bufferRowLength,int nAtomNodeMax,unsigned long nAtomGlobal)
{
	unsigned long count = 0;
	int ndataNode = bufferRowLength*nAtomNodeMax;
	int collVecRowLength = bufferRowLength-1;

	/*
	for (int iNode = 0;iNode < nNodes;iNode++) { //loop through all nodes
		for (int k = 0;k < nAtomPerNode[iNode];k++) { //for each node, loop through all atoms in nodes

			int idx = iNode*ndataNode+bufferRowLength*k; //index of the first row in recv buffer
			double atomData = recvBuffer[idx]; 
			rgid[count] = idFromData(atomData);	
			rType[count] = typeFromData(atomData);	
			
			for (int a = 0;a < bufferRowLength;a++) { //loop through all collectionVector (skip the first one = atom data
				recvBuffer[count*bufferRowLength+a] = recvBuffer[idx+a];
			}
			count++;
		}
	}
	*/


	//In-place merge sort
	int* pnt = (int*)(scMalloc(sizeof(int)*nNodes));

	for (int a = 0; a < nNodes;a++)
		pnt[a] = 0;

	do {
		int nodeMin = 0;
		while (pnt[nodeMin] >= nAtomPerNode[nodeMin]) {
			nodeMin++;
			continue;
		}

		int minIdx = nodeMin*ndataNode+bufferRowLength*pnt[nodeMin];
		double minAtomData = recvBuffer[minIdx];
		unsigned long rgidMin = idFromData(minAtomData);

		for (int jNode = 1;jNode < nNodes;jNode++) { //loop through all nodes
			if (pnt[jNode] >= nAtomPerNode[jNode])
				continue;
			int jIdx = jNode*ndataNode+bufferRowLength*pnt[jNode];
			double jAtomData = recvBuffer[jIdx];
			unsigned long rgidj = idFromData(jAtomData);
			if (rgidj < rgidMin) {
				rgidMin = rgidj;
				nodeMin = jNode;
				minAtomData = jAtomData;
				minIdx = jIdx;
			}

		}

		rgid[count] = idFromData(minAtomData);	
		rType[count] = typeFromData(minAtomData);	
		
		for (int a = 1;a < bufferRowLength;a++) { //Add the data of the smallest rgid into the collection vector.
			collVec[count*collVecRowLength+(a-1)] = recvBuffer[minIdx+a];
		}

		pnt[nodeMin]++;
		count++;

	} while (count < nAtomGlobal);


	#if (DEBUG_LEVEL > 0)
	for (int i = 0;i < nAtomGlobal-1;i++) {
		if (rgid[i] > rgid[i+1]) {
			//printf("%d %ld %ld\n",i,rgid[i],rgid[i+1]);
			terminateSingle("Data is not sorted.",1);
		}	

	}
	#endif

	scFree(pnt);

	return collVec;
}

static void analyze_fill_sendbuffer(STATE* s, PARAMS *p, double* sendBuffer,int* enabledFlag, int bufferRowLength) {

	int offset = 0;

	int    *rType   = s->rType;
	unsigned long *rgid    = s->rgid;
	int nAtomOwn = s->nAtomOwn;

	
	for (int i = 0; i < nAtomOwn;i++) {
			sendBuffer[bufferRowLength*i] = dataFromId(rType[i],rgid[i]);
			//printf("i = %d buffer idx = %d data = %le\n",i,bufferRowLength*i,dataFromId(rType[i],rgid[i]));
	}
	offset += 1; //atomdata

	if (enabledFlag[ANALYSIS_COLLECTION_RDF] == 1) {
		analysis_rdf_fill(s,p,sendBuffer, bufferRowLength,offset);
		#if (DEBUG_LEVEL > 2)	
			int myId = p->commInfo.myId;
			int nAtomOwn = s->nAtomOwn;
			for (int i = 0;i < nAtomOwn;i++) {
				printf("myID = %5d %le ",myId,sendBuffer[i*bufferRowLength]);
				for (int a = 0;a < 3;a++)
					printf(" %le",sendBuffer[i*bufferRowLength+a+1]);
				printf("\n");
			}
		#endif
		offset += ANALYSIS_COLLECTION_SIZE_RDF; //true r(x,y,z) for calculating RDF
	}

	if (enabledFlag[ANALYSIS_COLLECTION_VACF] == 1) {
		analysis_vacf_fill(s,p,sendBuffer, bufferRowLength,offset);
		#if (DEBUG_LEVEL > 2)	
			int myId = p->commInfo.myId;
			int nAtomOwn = s->nAtomOwn;
			for (int i = 0;i < nAtomOwn;i++) {
				printf("myID = %5d %le ",myId,sendBuffer[i*bufferRowLength]);
				for (int a = 0;a < 3;a++)
					printf(" %le",sendBuffer[i*bufferRowLength+a+1]);
				printf("\n");
			}
		#endif
		offset += ANALYSIS_COLLECTION_SIZE_VACF; //velocities for calculating r(x,y,z) for calculating VACF
	}


}



static void analyze_compute(STATE* s, PARAMS *p, double* collVec,int* enabledFlag, int collRowLength) {

	int offset = 0;

	int    *rType   = s->rType;
	unsigned long *rgid    = s->rgid;
	int nAtomOwn = s->nAtomOwn;

	if (enabledFlag[ANALYSIS_COLLECTION_RDF] == 1) {
		analysis_rdf_compute(s,p,analysis_data,collVec, collRowLength,offset);
		offset += ANALYSIS_COLLECTION_SIZE_RDF; //true r(x,y,z) for calculating RDF
	}

	if (enabledFlag[ANALYSIS_COLLECTION_VACF] == 1) {
		analysis_vacf_compute(s,p,analysis_data,collVec, collRowLength,offset);
		offset += ANALYSIS_COLLECTION_SIZE_VACF; //velocities for calculating r(x,y,z) for calculating VACF
	}


}

