#include "params.h"
#include "constants.h"
#include "state.h"
#include "utils.h"
#include "input.h"
#include "utils.h"
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "mpi.h"
#include "cell.h"
#include "simulate.h"

static double sumTotalEnergy, sumTotalEnergy2;
static double sumKineticEnergy, sumKineticEnergy2;
static double sumPotentialNBodyEnergy[MAX_NBODY], sumPotentialNBodyEnergy2[MAX_NBODY];
static double sumPotentialEnergy, sumPotentialEnergy2;
static double sumTemp, sumTemp2;
static int nStatCount;

static double globTotalEnergy = 0.0, globTotalEnergy2 = 0.0;
static int nStatCountGlobal = 0;


static double evalKineticEnergy(STATE* s);
static void resetEval();
static FILE* energyPrintFile = NULL;


void printEvalHeader()
{
	INPUT_PARAMS* inp = getInputObj();
	char msg[1000];
	if ((inp->detailedEvalProp == 0)) {
		if (getMyId() == 0){
			energyPrintFile = fopen("e.print","a");
			sprintf(msg,"     MD_Step     Time(ns)    Total_Energy(eV)  Pot_Energy(eV)   Kin_Energy(eV)   Temp(K)\n"); 
			printf("%s",msg); 
			fprintf(energyPrintFile,"#%s",msg);
			fflush(energyPrintFile);
		}
	} else if ((inp->detailedEvalProp == 1) && (strcmp(inp->potentialName,"SIO2")==0)) {
		if (getMyId() == 0){
			energyPrintFile = fopen("e.print","a");
			sprintf(msg,"     MD_Step     Time(ns)        Total_Energy(eV)          Pot_Energy(eV)   Kin_Energy(eV)   2B_Energy(eV)    3B_Energy(eV)       Temp(K)\n");  
			printf("%s",msg); 
			fprintf(energyPrintFile,"#%s",msg);
			fflush(energyPrintFile);
		}

	}
	resetEval();
	//nStatCount = 0;
}


void resetEval()
{
	sumTotalEnergy  = 0.0;
	sumTotalEnergy2 = 0.0;

	for (int i = 0;i < MAX_NBODY;i++) {
		sumPotentialNBodyEnergy[i]  = 0.0;
		sumPotentialNBodyEnergy2[i] = 0.0;
	}

	sumPotentialEnergy  = 0.0;
	sumPotentialEnergy2 = 0.0;

	sumKineticEnergy  = 0.0;
	sumKineticEnergy2 = 0.0;

	sumTemp  = 0.0;
	sumTemp2 = 0.0;

	nStatCount = 0;


}


void evalProperties(unsigned long stepCount, STATE* s)
{
	
	char msg[1000];
	INPUT_PARAMS* inp = getInputObj();
	evalKineticEnergy(s);
	double globalEnergy[MAX_NBODY+1], localEnergy[MAX_NBODY+1]; //0 -> Kinetic, 1 -> Potential NBODY
	localEnergy[0] = s->kineticEnergy; 
	for (int i = 0;i < MAX_NBODY;i++)
		localEnergy[i+1] = s->potentialEnergy[i]; 

	MPI_Allreduce(localEnergy, globalEnergy, MAX_NBODY+1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	//convert to per atom energy
	s->kineticEnergy   = globalEnergy[0]/s->nAtomGlobal;
	double potentialEnergy = 0.0;
	for (int i = 0;i < MAX_NBODY;i++){
		 s->potentialEnergy[i]  = globalEnergy[i+1]/s->nAtomGlobal;
		 potentialEnergy    += s->potentialEnergy[i];
	}
	double totEnergy  = potentialEnergy + s->kineticEnergy;
	
	sumTotalEnergy  += totEnergy;
	sumTotalEnergy2 += totEnergy*totEnergy;


	for (int i = 0;i < MAX_NBODY;i++){
		sumPotentialNBodyEnergy[i]  += s->potentialEnergy[i];
		sumPotentialNBodyEnergy2[i] += s->potentialEnergy[i]*s->potentialEnergy[i];

	}

	sumPotentialEnergy  += potentialEnergy;
	sumPotentialEnergy2 += potentialEnergy*potentialEnergy;

	sumKineticEnergy  += s->kineticEnergy;
	sumKineticEnergy2 += s->kineticEnergy*s->kineticEnergy;

	double temp = s->kineticEnergy*(2.0/3.0);
	sumTemp += temp;
	sumTemp2 += temp*temp;

	nStatCount++;

	globTotalEnergy += totEnergy;
	globTotalEnergy2+= totEnergy*totEnergy;
	
	nStatCountGlobal++;

	#if (DEBUG_LEVEL > 0)
	double atomMass[3];
  	paramsGetMass(atomMass);
	double sumMomentum[3] = {0.0};
	double (*rv)[3] = s->rv;
	int *rType = s->rType;
	for (int i = 0;i < s->nAtomOwn;i++){
		double mass = atomMass[rType[i]];
		for (int a = 0;a < 3;a++)
			sumMomentum[a] += mass*rv[i][a];
	}
	reportPrintDoubleArr3("Total momentum:",sumMomentum);
	#endif



	if ((inp->evalPropFreq > 0) && (stepCount % inp->evalPropFreq == 0) && (inp->detailedEvalProp == 0)) {
		double avgTotEnergy = EAUEV*sumTotalEnergy/nStatCount;
		//double avgTotEnergy2 = EAUEV*EAUEV*sumTotalEnergy2/nStatCount;
		//double stdTotEnergy = sqrt(avgTotEnergy2 - (avgTotEnergy*avgTotEnergy));
		//double percentStdTotEnergy = 100.0*stdTotEnergy/avgTotEnergy;
		double avgTemp  = TEMPAU*sumTemp/nStatCount;
		//double avgTemp2 = TEMPAU*TEMPAU*sumTemp2/nStatCount;
		//double stdTemp = sqrt(avgTemp2 - (avgTemp*avgTemp));
		//double percentStdTemp = 100.0*stdTemp/avgTemp;

	
		if (getMyId() == 0){
			//energyPrintFile = fopen("e.print","a");
        		sprintf(msg,"%12ld  %12.6lf  %15.6le  %15.6le  %15.6le  %8.3f\n", stepCount, 
									  stepCount*paramsGetDeltaT()*TIMEU*1e+9, 
									  avgTotEnergy, 
									  (sumPotentialEnergy*EAUEV)/nStatCount, 
									  (sumKineticEnergy*EAUEV)/nStatCount, 
									  avgTemp);

			printf("%s",msg); 
			fprintf(energyPrintFile,"%s",msg);
			fflush(energyPrintFile);
		}
		resetEval();
	}
	else if ((inp->evalPropFreq > 0) && (stepCount % inp->evalPropFreq == 0) && (inp->detailedEvalProp == 1)) {
		double avgTotEnergy = EAUEV*sumTotalEnergy/nStatCount;
		double avgTotEnergy2 = EAUEV*EAUEV*sumTotalEnergy2/nStatCount;
		//double stdTotEnergy = sqrt(avgTotEnergy2 - (avgTotEnergy*avgTotEnergy));
		double avgGlobEnergy  = EAUEV*globTotalEnergy/nStatCountGlobal;
		double avgGlobEnergy2 = EAUEV*EAUEV*globTotalEnergy2/nStatCountGlobal;
		double stdGlobEnergy = sqrt(avgGlobEnergy2 - (avgGlobEnergy*avgGlobEnergy));
		//double percentStdTotEnergy = 100.0*stdTotEnergy/avgTotEnergy;
		double avgTemp  = TEMPAU*sumTemp/nStatCount;
		double avgTemp2 = TEMPAU*TEMPAU*sumTemp2/nStatCount;
		double stdTemp = sqrt(avgTemp2 - (avgTemp*avgTemp));
		//double percentStdTemp = 100.0*stdTemp/avgTemp;
		if (getMyId() == 0){
			//energyPrintFile = fopen("e.print","a");
        		sprintf(msg,"%12ld  %12.6lf  %15.6le (%9.6f)  %15.6le  %15.6le  %15.6le  %15.6le  %8.3f (%5.1f)\n", stepCount, 
									  stepCount*paramsGetDeltaT()*TIMEU*1e+9, 
									  avgTotEnergy, stdGlobEnergy, 
									  //avgTotEnergy, stdTotEnergy, 
									  (sumPotentialEnergy*EAUEV)/nStatCount, 
									  (sumKineticEnergy*EAUEV)/nStatCount, 
									  (sumPotentialNBodyEnergy[INDEX_2B]*EAUEV)/nStatCount, 
									  (sumPotentialNBodyEnergy[INDEX_3B]*EAUEV)/nStatCount, 
									  avgTemp,stdTemp);
			printf("%s",msg); 
			fprintf(energyPrintFile,"%s",msg);
			fflush(energyPrintFile);
		}
		resetEval();
	}



}


/*--------------------------------------------------------------------*/
double evalKineticEnergy(STATE* s) {
/*----------------------------------------------------------------------*/
  	//reqTemp in A.U.
  double atomMass[3];
  paramsGetMass(atomMass);
  double nodeKineticEnergy = 0.0;
  
  double (*rv)[3] = s->rv;
  int     *rType  = s->rType;
  int     nAtomOwn = s->nAtomOwn;
  /* Total kinetic energy */
  for (int i = 0; i < nAtomOwn; i++) {
        int iType = rType[i];
	double vv = 0.0;
        for (int a = 0; a < 3; a++)
        	vv += atomMass[iType] * rv[i][a] * rv[i][a];
        nodeKineticEnergy += vv;
  }
  nodeKineticEnergy *= 0.5;


  /* Energy per atom */
  //s->kineticEnergy = globalKineticEnergy/s->nAtomGlobal;
  s->kineticEnergy = nodeKineticEnergy;
  //temperature = kinEnergy * 2.0 / 3.0;


  return s->kineticEnergy;
}

	
