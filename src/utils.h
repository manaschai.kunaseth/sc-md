#ifndef __UTILS_H__
#define __UTILS_H__

#include "params.h"

//utility function
void initUtils(PARAMS*);
void terminate(const char*);
void terminateSingle(const char *s,int errorCode);
void terminateNode(const char *s,int errorCode);
int getMyId();

//report utility
void printBuildInfo();
void reportVLine(const char *s);
void reportPrint(const char *s);
void reportPrintStr(const char *s, const char *val);
void reportPrintInt(const char *s,int val);
void reportPrintInt3(const char *s,int val1, int val2, int val3);
void reportPrintIntArr3(const char *s,int* ar);
void reportPrintDouble(const char *s,double val);
void reportPrintDouble3(const char *s,double val1, double val2, double val3);
void reportPrintDoubleArr3(const char *s,double* ar);
void reportPrintFixedDouble(const char *s,double val);
void reportPrintFixedDouble3(const char *s,double val1, double val2, double val3);
void reportPrintFixedDoubleArr3(const char *s,double* ar);

void reportVerbose(const char *s);
void reportVerboseInt(const char *s,int val);
void reportVerboseInt3(const char *s,int val1, int val2, int val3);
void reportVerboseIntArr3(const char *s,int* ar);
void reportVerboseDouble(const char *s,double val);
void reportVerboseFixedDouble(const char *s,double val);
void reportVerboseDouble3(const char *s,double val1, double val2, double val3);
void reportVerboseDoubleArr3(const char *s,double* ar);

//atom type & id manipulation
double dataFromId(int isi, unsigned long idi);
unsigned long idFromData(double adata);
int typeFromData(double adata);

//string manipulation
void strToUpper(char* s);
void strToLower(char* s);
void inPlaceTrim(char* s);

//Random number generator
void randomVec3(double *p, double *seed);
double randomDouble(double *seed);

#endif


