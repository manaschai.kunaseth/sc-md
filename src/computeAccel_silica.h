#ifndef __COMPUTE_ACCEL_SILICA_H__
#define __COMPUTE_ACCEL_SILICA_H__

#include "state.h"
#include "potential.h"
#include "params.h"
#include "cell.h"

void computeAccel_silica(STATE* s, POTENTIAL* pot, PARAMS* p, CELL_INFO *cellInfo);
void computeAccel_silica_scnbl(STATE* s, POTENTIAL* pot, PARAMS* p, CELL_INFO *cellInfo);

void computeAccel_silica_thread(STATE* s, POTENTIAL* pot, PARAMS* p, CELL_INFO *cellInfo);
void computeAccel_silica_thread_gpu(STATE* s, POTENTIAL* pot, PARAMS* p, CELL_INFO *cellInfo);
void computeAccel_silica_thread_mic(STATE* s, POTENTIAL* pot, PARAMS* p, CELL_INFO *cellInfo);

#endif

