#include <stdio.h>
#include <stdlib.h>
#include <libgen.h>
#include "input.h"
#include "memory.h"
#include "utils.h"
#include "fileUtils.h"
#include "simulate.h"

static int addBlock(void* ptr, size_t size, char* location);
//static int updateBlock(void* old_ptr, void* new_ptr, size_t size, char* location);
static int freeBlock(void* ptr);
static int findBlock(void* ptr);

typedef struct memBlock_st
{
   size_t size;
   void*  ptr;
   char   location[50];
} MEMBLOCK;


#define MAX_MEM_BLOCK 4096
static MEMBLOCK _block[MAX_MEM_BLOCK];
static int      _freeBlock[MAX_MEM_BLOCK];
static int      _nextBlock = MAX_MEM_BLOCK +1;
static size_t   _memUsed = 0;
static size_t   _peakUsed = 0;
static int      _blocksUsed = 0;
const double    b2mb=1024*1024;

int _verboseTask = -1;


void* _scMalloc(size_t size, char* location)
{
	if (size == 0)
		size = sizeof(void*);

	void* ptr = malloc(size);
	if (!ptr)
	{
		printf("mem: ddcMalloc failed on task %d (%zu bytes at %s)\n"
		  "                                 memUsed=%8.2f\n",
		  getMyId(), size, location, _memUsed/b2mb);
		//printHeapInfo(stdout);
	}
	else
	{
		int b = addBlock(ptr, size, location);   
		if (_verboseTask == getMyId())
		{
			printf("mem: task %d block %d  malloc %10p  (%zu bytes at %s) total %8.2f\n",
			  getMyId(), b, ptr, size, location, _memUsed/b2mb);
			//printHeapInfo(stdout);
		}
	}

	return ptr;
}

char* _scLine(const char* file, int lineNum)
{
	static char buffer[256];
	sprintf(buffer, "%s:%d", file, lineNum);
	return buffer;
}

void  _scFree(void* ptr, const char* location)
{
	free(ptr);

	int b = freeBlock(ptr);

	if (_verboseTask == getMyId())
		printf("mem:  task %d block %d  free %10p at %s total %8.2f\n",
	    		getMyId(), b, ptr, location, _memUsed/b2mb);
}



int addBlock(void* ptr, size_t size, char* location)
{
	//assert(ptr != NULL);
	int here;
	#pragma omp critical (malloc_addBlock)
	{
		if (_nextBlock == MAX_MEM_BLOCK+1)
			scMemInit();
		
		here = _freeBlock[_nextBlock];
		_block[here].ptr = ptr;
		_block[here].size = size;
		_block[here].location[0] = '\0';
		strncat(_block[here].location, basename(location),49);
		
		++_blocksUsed;
		_memUsed += size;
		if (_memUsed > _peakUsed) _peakUsed = _memUsed;
		++_nextBlock;
		if (_nextBlock == MAX_MEM_BLOCK)
		{
			printf("Block storage exhausted on task %d in addBlock.\n" 
		     		"%s\n"
		     		"Try increasing MAX_BLOCK\n", getMyId(),location);
		   	exit(3);
		}
	} 
	return here;
}

int freeBlock(void* ptr)
{
	if (ptr == NULL)
		return -2;
	int here = findBlock(ptr);

	#pragma omp critical (ddcMalloc_freeBlock)
	{
		if (here >= 0)
		{
		_blocksUsed--;
		_memUsed -= _block[here].size;
		_block[here].ptr = NULL;
		_block[here].size = 0;
		_block[here].location[0] = '\0';
		_nextBlock--;
		//assert (_nextBlock >= 0);
		_freeBlock[_nextBlock] = here;
		}
		/*    else */
		/*       printf("mem: Error on Task %d. Request to free ptr 0x%08x.\n" */
		/* 	     "     Pointer cannot be found in block list.\n", */
		/* 	     getMyId(), ptr); */
	}
	return here;
}

int findBlock(void*ptr)
{
	for (unsigned ii=0; ii<MAX_MEM_BLOCK; ++ii)
		if (_block[ii].ptr == ptr)
			return ii;
	return -1;
}



void scMemInit(void)
{
	for (unsigned ii=0; ii<MAX_MEM_BLOCK; ++ii)
	{
		_freeBlock[ii] = ii;
		_block[ii].size = 0;
		_block[ii].ptr = NULL;
		_block[ii].location[0] = '\0';
	}

	_nextBlock = 0;
}


void scMemReport(FILE* file)
{
	size_t totalSize = 0;

	fprintf(file, "Memory report for node ID: %d\n\n",  getMyId());
	fprintf(file, "Block  | ptr             | size (kb)     | location\n");
	fprintf(file, "=======================================================================\n");

	for (unsigned ii=0; ii<MAX_MEM_BLOCK; ++ii)
	{
		if (_block[ii].ptr == NULL)
			continue;
		fprintf(file, "%5d: | %15p | %12zuk | %s\n", ii, _block[ii].ptr,
			_block[ii].size/1024, _block[ii].location);
		totalSize += _block[ii].size;
	}
	fprintf(file, "\nTotal size = %f MB\n", totalSize/b2mb);
	fprintf(file, "Peak size = %f MB\n\n", _peakUsed/b2mb);
	//printHeapInfo(file);
}

void writeMemUsage() 
{
	
	char dirName[1000];
	char fileName[100];
	char fullFilePath[1100];

	createOutputDir(simulateGetCurrentStep(),dirName);
	sprintf(fileName,"memory-%07d.txt",getMyId());
	sprintf(fullFilePath, "%s/%s",dirName,fileName);

	reportPrint("Generating memory usage profiles...");
	INPUT_PARAMS* inp = getInputObj();

	//Write memory usage profile:
	//  To reduce number of output file created, only some MPI ranks will write profile.
	if ((getMyId()%inp->runtimeProfileRatio) == 0)
	{
		FILE *fp;
		fp = fopen(fullFilePath,"w");
		scMemReport(fp);	
		fclose(fp);
	}
}


