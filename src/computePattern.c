#include "computePattern.h"
#include "utils.h"
#include "simulate.h"
#include "memory.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

#define REMOVED -100000

#define MIN(a,b) (a < b)?a:b;

static COMPUTE_PATTERN* allocPattern(COMPUTE_PATTERN* pat, int nBody);

static int iteratePattern2B(COMPUTE_PATTERN* pat,int nlayer);
static int iteratePattern3B(COMPUTE_PATTERN* pat);
static int iteratePattern4B(COMPUTE_PATTERN* pat);

static int removeNegativeSuperVector(int (**pattern)[3], int pCount, int nBody);
static void positiveShift(int (**pattern)[3], int pCount, int nBody); //shift to positive quadrant
static void PatternShift(int (**pattern)[3], int pCount, int nBody, int shiftVec[3]); //shift by an arbitary vector
static void inversePattern(int (**pattern)[3], int pCount, int nBody); //flip pattern by scaling with -1
static int substractPattern(int (**pattern1)[3], int pCount,int (**pattern2)[3], int pCount2, int nBody2); //subtract intersecting computation paths from pattern1
static int mergePattern(int (**pattern1)[3], int pCount,int (**pattern2)[3], int pCount2, int nBody2); //merge pattern
static int removeSelfPath(int (**pattern)[3], int pCount, int nBody);
static void sortComputePattern3B(int (**pattern)[3], int pCount, int nBody);

COMPUTE_PATTERN* initPattern(COMPUTE_PATTERN* pat, COMPUTE_PATTERN_FLAGS cpType)
{
	if (cpType == COMPUTE_PATTERN_FLAG_2B)
	{
		int nBody = 2;
		reportPrintStr("init cell's computation pattern: ","2-Body");
		pat = allocPattern(pat,2);
		reportPrintFixedDouble("Allocate computation pattern [MB]: ",
								pat->nPatternMax*sizeof(int[3])*nBody/(1024.0*1024));
		pat->nPattern = iteratePattern2B(pat,1);
		pat->nPattern = removeNegativeSuperVector(pat->pattern, pat->nPattern, 2);
		positiveShift(pat->pattern, pat->nPattern, 2);

	}
	else if (cpType == COMPUTE_PATTERN_FLAG_3B)
	{
		int nBody = 3;
		reportPrintStr("init cell's computation pattern: ","3-Body");
		pat = allocPattern(pat,3);
		reportPrintFixedDouble("Allocate computation pattern [MB]: ",
								pat->nPatternMax*sizeof(int[3])*nBody/(1024.0*1024));
		pat->nPattern = iteratePattern3B(pat);
		pat->nPattern = removeNegativeSuperVector(pat->pattern, pat->nPattern, 3);
		positiveShift(pat->pattern, pat->nPattern, 3);
		sortComputePattern3B(pat->pattern, pat->nPattern, 3);
	}
	else if (cpType == COMPUTE_PATTERN_FLAG_4B)
	{
		int nBody = 4;
		reportPrintStr("init cell's computation pattern: ","4-Body");
		pat = allocPattern(pat,4);
		reportPrintFixedDouble("Allocate computation pattern [MB]: ",
								pat->nPatternMax*sizeof(int[3])*nBody/(1024.0*1024));
		pat->nPattern = iteratePattern4B(pat);
		pat->nPattern = removeNegativeSuperVector(pat->pattern, pat->nPattern, 4);
		positiveShift(pat->pattern, pat->nPattern, 4);
	}
	else if (cpType == (COMPUTE_PATTERN_FLAG_ANTI2B))
	{
		int nBody = 2;
		reportPrintStr("init cell's computation pattern: ","Anti 2-Body");
		pat = allocPattern(pat,2);
		reportPrintFixedDouble("Allocate computation pattern [MB]: ",
								pat->nPatternMax*sizeof(int[3])*nBody/(1024.0*1024));
		pat->nPattern = iteratePattern2B(pat,1);
		pat->nPattern = removeNegativeSuperVector(pat->pattern, pat->nPattern, 2);
		positiveShift(pat->pattern, pat->nPattern, 2);
		inversePattern(pat->pattern, pat->nPattern, 2);
		int oneVec[3] = {1,1,1};
		PatternShift(pat->pattern, pat->nPattern, 2, oneVec);
		
		COMPUTE_PATTERN *pat2 = simulateGetComputePattern(2);
		//pat->nPattern = mergePattern(pat->pattern, pat->nPattern, pat2->pattern, pat2->nPattern, 2);
		pat->nPattern = substractPattern(pat->pattern, pat->nPattern, pat2->pattern, pat2->nPattern, 2);
		pat->nPattern = removeSelfPath(pat->pattern, pat->nPattern, 2);
		
	}
	else if (cpType == COMPUTE_PATTERN_FLAG_2B_2LAYER)
	{
		int nBody = 2;
		reportPrintStr("init cell's computation pattern: ","2-Body (2 layers)");
		pat = allocPattern(pat,3);
		reportPrintFixedDouble("Allocate computation pattern [MB]: ",
								pat->nPatternMax*sizeof(int[3])*nBody/(1024.0*1024));
		pat->nPattern = iteratePattern2B(pat,2);
		reportPrintInt("nPattern: ",pat->nPattern);
		pat->nPattern = removeNegativeSuperVector(pat->pattern, pat->nPattern, 2);
		reportPrintInt("nPattern: ",pat->nPattern);
		positiveShift(pat->pattern, pat->nPattern, 2);

	}
	else
	{
		terminateSingle("Unknown N-body computation pattern.",1);
	}

	reportPrint("init cell's computation pattern completed.");
	return pat;
}

COMPUTE_PATTERN* freePattern(COMPUTE_PATTERN* pat)
{
	//for (int i = 0;i < pat->nPatternMax;i++)
	//	scFree(pat->pattern[i]);
	
	scFree(pat->pattern[0]);
	scFree(pat->pattern);

	//scFree(pat);
	pat = NULL;
	return pat;
}

COMPUTE_PATTERN* allocPattern(COMPUTE_PATTERN* pat, int nBody)
{

	//pat = (COMPUTE_PATTERN*)scMalloc(sizeof(COMPUTE_PATTERN));

	//Pattern can be at most #NNCell^(nbody-1).
	//For 1st-order NN, #NNCell = 3x3x3 = 27. 
	//e.g. For 2 body, nPatternMax = 27^(2-1) = 27	
	//     For 3 body, nPatternMax = 27^(3-1) = 729 	
	//Memory will be allocated for 2x of the calculated size
	int  nPatternMax = 2.0*pow(27,nBody-1);
	pat->pattern = (int(**)[3])scMalloc(sizeof(int**)*nPatternMax);
	int (*patternAlloc)[3] = (int(*)[3])scMalloc(sizeof(int[3])*nBody*nPatternMax);
	for (int i = 0;i < nPatternMax;i++)
		//pat->pattern[i] = (int(*)[3])scMalloc(sizeof(int[3])*nBody);
		pat->pattern[i] = &patternAlloc[i*nBody];
	

	pat->nPatternMax = nPatternMax;	
	pat->nPattern = 0;
	pat->nBody = nBody;

	return pat;
}

int iteratePattern2B(COMPUTE_PATTERN* pat,int nlayer)
{
	int pCount = 0;
	int (**pattern)[3] = pat->pattern;

	//Iterate all pattern	
	//First dimension c   =      (0,0,0)
	//Second dimension c1 = c  + (+-1,+-1,+-1)
	for (int i = nlayer;i >= -nlayer;i--)
	for (int j = nlayer;j >= -nlayer;j--)
	for (int k = nlayer;k >= -nlayer;k--){

		//Set all source cell offset to (0,0,0)
		pattern[pCount][0][0] = 0;
		pattern[pCount][0][1] = 0;
		pattern[pCount][0][2] = 0;
	
		pattern[pCount][1][0] = i;
		pattern[pCount][1][1] = j;
		pattern[pCount][1][2] = k;
	
		pCount++;
	
	}
	
	reportPrintInt("Iterated pattern: ",pCount);

	#if (DEBUG_LEVEL > 1)		
	char msg[1000];
	for (int i = 0;i < pCount;i++){
		sprintf(msg,"Pattern [%5d]: (%5d %5d %5d)->(%5d %5d %5d)",i,
								  pattern[i][0][0],pattern[i][0][1],pattern[i][0][2],
								  pattern[i][1][0],pattern[i][1][1],pattern[i][1][2]);
	
		reportVerbose(msg);
	}
	#endif

	return pCount;
}


int iteratePattern3B(COMPUTE_PATTERN* pat)
{
	int pCount = 0;
	int (**pattern)[3] = pat->pattern;

	//Iterate all pattern	
	//First dimension c   =      (0,0,0)
	//Second dimension c1 = c  + (+-1,+-1,+-1)
	//Third  dimension c2 = c1 + (+-1,+-1,+-1)
	for (int i = 1;i >= -1;i--)
	for (int j = 1;j >= -1;j--)
	for (int k = 1;k >= -1;k--){

		for (int i1 = i+1;i1 >= i-1;i1--)
		for (int j1 = j+1;j1 >= j-1;j1--)
		for (int k1 = k+1;k1 >= k-1;k1--){

			//Set all source cell offset to (0,0,0)
			pattern[pCount][0][0] = 0;
			pattern[pCount][0][1] = 0;
			pattern[pCount][0][2] = 0;
		
			pattern[pCount][1][0] = i;
			pattern[pCount][1][1] = j;
			pattern[pCount][1][2] = k;
	
			pattern[pCount][2][0] = i1;
			pattern[pCount][2][1] = j1;
			pattern[pCount][2][2] = k1;
			
			pCount++;
		}
	}

	reportPrintInt("Iterated pattern: ",pCount);

	#if (DEBUG_LEVEL > 1)		
	char msg[1000];
	for (int i = 0;i < pCount;i++){
		sprintf(msg,"Pattern [%5d]: (%5d %5d %5d)->(%5d %5d %5d)->(%5d %5d %5d)",i,
								  pattern[i][0][0],pattern[i][0][1],pattern[i][0][2],
								  pattern[i][1][0],pattern[i][1][1],pattern[i][1][2],
								  pattern[i][2][0],pattern[i][2][1],pattern[i][2][2]);
	
		reportVerbose(msg);
	}
	#endif

	return pCount;
}

int iteratePattern4B(COMPUTE_PATTERN* pat)
{
	int pCount = 0;
	int (**pattern)[3] = pat->pattern;

	//Iterate all pattern	
	//First dimension c   =      (0,0,0)
	//Second dimension c1 = c  + (+-1,+-1,+-1)
	//Third  dimension c2 = c1 + (+-1,+-1,+-1)
	for (int i = 1;i >= 1;i--)
	for (int j = 1;j >= 1;j--)
	for (int k = 1;k >= 1;k--){

		for (int i1 = i+1;i1 >= i-1;i1--)
		for (int j1 = j+1;j1 >= j-1;j1--)
		for (int k1 = k+1;k1 >= k-1;k1--){


			for (int i2 = i1+1;i2 >= i1-1;i2--)
			for (int j2 = j1+1;j2 >= j1-1;j2--)
			for (int k2 = k1+1;k2 >= k1-1;k2--){


				//Set all source cell offset to (0,0,0)
				pattern[pCount][0][0] = 0;
				pattern[pCount][0][1] = 0;
				pattern[pCount][0][2] = 0;
			
				pattern[pCount][1][0] = i;
				pattern[pCount][1][1] = j;
				pattern[pCount][1][2] = k;
		
				pattern[pCount][2][0] = i1;
				pattern[pCount][2][1] = j1;
				pattern[pCount][2][2] = k1;
			
				pattern[pCount][3][0] = i2;
				pattern[pCount][3][1] = j2;
				pattern[pCount][3][2] = k2;
				
				pCount++;
			}
		}
	}

	reportPrintInt("Iterated pattern: ",pCount);

	#if (DEBUG_LEVEL > 1)		
	char msg[1000];
	for (int i = 0;i < pCount;i++){
		sprintf(msg,"Pattern [%5d]: (%5d %5d %5d)->(%5d %5d %5d)->(%5d %5d %5d)->(%5d %5d %5d)",i,
								  pattern[i][0][0],pattern[i][0][1],pattern[i][0][2],
								  pattern[i][1][0],pattern[i][1][1],pattern[i][1][2],
								  pattern[i][2][0],pattern[i][2][1],pattern[i][2][2],
								  pattern[i][3][0],pattern[i][3][1],pattern[i][3][2]);
	
		reportVerbose(msg);
	}
	#endif

	return pCount;
}


#if 0
// For arbitary pattern i,j: Remove pattern j with negative-reflected supervector of i
// for i = negativeReflect(j): i and j will be duplicated in force computation. Thus, only one is needed.
// Here, we remove pattern j out from pattern matrix, if i = negativeReflect(j).
// This function return the remaining number of patterns.
int removeNegativeSuperVector(int (**pattern)[3], int pCount, int nBody)
{	
	int patternVecSrc[3];
	int patternVecDes[3];
	int nBody1 = nBody-1;

	for (int i = 0;i < pCount-1;i++){
		//if already found duplicated, skip to the next one
		if (pattern[i][0][0] == REMOVED) continue;

		for (int j = i+1;j < pCount;j++){
			//if already found duplicated, skip to the next one
			if (pattern[j][0][0] == REMOVED) continue;

			//if i == j then skip to the next one
			if (i == j) continue; 

			int isVecDiff = 0;
			//Traverse all vector component of pattern i & j's super vector
			for (int chain = 0; (chain < nBody1) && (isVecDiff == 0); chain++) {
				
				//calculater pattern vector of i's SV:
				for (int a = 0;a < 3;a++)
					patternVecSrc[a] = pattern[i][chain+1][a] - pattern[i][chain][a];

				//calculater pattern vector of j's SV:
				for (int a = 0;a < 3;a++)
					patternVecDes[a] = pattern[j][chain+1][a] - pattern[j][chain][a];

				//if patternVec != patternVec^-1, then pattern i-j are not a duplicate-inverse.
				if ((patternVecSrc[0] != -patternVecDes[0]) ||
				    (patternVecSrc[1] != -patternVecDes[1]) || 
				    (patternVecSrc[2] != -patternVecDes[2])) {
					//One of pattern vector is different, then i-j are different.
					//No need to check further chains.
					isVecDiff = 1;
				}
			} //Next chain
			
			//if supervector is the same, then i and j pattern are duplicated. Marked for remove later.
			if (isVecDiff == 0) {	
				#if (DEBUG_LEVEL > 1)		
					reportVerboseInt("Pattern removed: ", j);
				#endif
				pattern[j][0][0] = REMOVED;
			}
		} //next j
	} //next i

	//Compact remaining pattern:
	int newPCount=0;
	for (int i = 0;i < pCount;i++){
		if (pattern[i][0][0] > REMOVED){
			for (int j = 0;j < nBody;j++) {
				for (int a = 0;a < 3;a++){
					pattern[newPCount][j][a] = pattern[i][j][a];
				}
			}
			newPCount++;
		}
	}
	
	pCount = newPCount;
	
	reportPrintInt("Non-duplicate pattern vector: ", pCount);

	#if (DEBUG_LEVEL > 1)		
	char msg[1000];
	for (int i = 0;i < pCount;i++){
		sprintf(msg,"Pattern [%5d]: (%5d %5d %5d)->(%5d %5d %5d)->(%5d %5d %5d)",i,
								  pattern[i][0][0],pattern[i][0][1],pattern[i][0][2],
								  pattern[i][1][0],pattern[i][1][1],pattern[i][1][2],
								  pattern[i][2][0],pattern[i][2][1],pattern[i][2][2]);
	
		reportVerbose(msg);
	}
	#endif

	return pCount;
} //End removeNegativeSuperVectorSuperVector 
#endif

// For arbitary pattern i,j: Remove pattern j with negative-reflected supervector of i
// for i = negativeReflect(j): i and j will be duplicated in force computation. Thus, only one is needed.
// Here, we remove pattern j out from pattern matrix, if i = negativeReflect(j).
// This function return the remaining number of patterns.
int removeNegativeSuperVector(int (**pattern)[3], int pCount, int nBody)
{	
	int patternVecSrc[3];
	int patternVecDes[3];
	int nBody1 = nBody-1;
	int (*sVecSrc)[3];
	int (*sVecDes)[3];

	int *patternDuplicate = (int*)scMalloc(sizeof(int)*pCount);
	for (int i = 0;i < pCount;i++)
		patternDuplicate[i] = 0;

	sVecSrc = (int(*)[3])scMalloc(sizeof(int[3])*(nBody-1));
	sVecDes = (int(*)[3])scMalloc(sizeof(int[3])*(nBody-1));
	for (int i = 0;i < pCount-1;i++){
		//if already found duplicated, skip to the next one
		if (pattern[i][0][0] == REMOVED) continue;

		//compute super-vector source
		for (int k = 0;k < nBody - 1;k++)
			for (int a = 0;a < 3;a++)
				sVecSrc[k][a] = pattern[i][k+1][a] - pattern[i][k][a];

		for (int j = i+1;j < pCount;j++){
			//if already found duplicated, skip to the next one
			if (pattern[j][0][0] == REMOVED) continue;

			//if i == j then skip to the next one
			if (i == j) continue; 

			//compute super-vector source
			for (int k = 0;k < nBody - 1;k++)
				for (int a = 0;a < 3;a++)
					sVecDes[k][a] = pattern[j][nBody-2-k][a] - pattern[j][nBody-1-k][a];

			int isVecDiff = 0;
			//Traverse all vector component of pattern i & j's super vector
			for (int chain = 0; (chain < nBody1) && (isVecDiff == 0); chain++) {
			//for (int chain = 0; (chain < nBody1) ; chain++) {
				
				//calculater pattern vector of i's SV:
				for (int a = 0;a < 3;a++)
					patternVecSrc[a] = sVecSrc[chain][a]; 

				//calculater pattern vector of j's SV:
				for (int a = 0;a < 3;a++)
					patternVecDes[a] = sVecDes[chain][a];

				//if patternVec != patternVec^-1, then pattern i-j are not a duplicate-inverse.
				if ((patternVecSrc[0] != patternVecDes[0]) ||
				    (patternVecSrc[1] != patternVecDes[1]) || 
				    (patternVecSrc[2] != patternVecDes[2])) {
					//One of pattern vector is different, then i-j are different.
					//No need to check further chains.
					isVecDiff = 1;
				} 

				
			} //Next chain
			
			//if supervector is the same, then i and j pattern are duplicated. Marked for remove later.
			if (isVecDiff == 0) {	
				#if (DEBUG_LEVEL > 1)		
					reportVerboseInt("Pattern removed: ", j);
				#endif
				pattern[j][0][0] = REMOVED;
				patternDuplicate[i]++;
				patternDuplicate[j]++;
			}
		} //next j
	} //next i

	for (int i = 0;i < pCount;i++)
		if (patternDuplicate[i] != 1)
			reportVerboseInt3("Pattern has different duplicate:",i,patternDuplicate[i],0);

	//Compact remaining pattern:
	int newPCount=0;
	for (int i = 0;i < pCount;i++){
		if (pattern[i][0][0] > REMOVED){
			for (int j = 0;j < nBody;j++) {
				for (int a = 0;a < 3;a++){
					pattern[newPCount][j][a] = pattern[i][j][a];
				}
			}
			newPCount++;
		}
	}
	
	pCount = newPCount;
	
	scFree(sVecSrc);
	scFree(sVecDes);
	scFree(patternDuplicate);

	reportPrintInt("Non-duplicate pattern vector: ", pCount);

	#if (DEBUG_LEVEL > 1)		
	char msg[1000];
	for (int i = 0;i < pCount;i++){
		sprintf(msg,"Pattern [%5d]: (%5d %5d %5d)->(%5d %5d %5d)->(%5d %5d %5d)",i,
								  pattern[i][0][0],pattern[i][0][1],pattern[i][0][2],
								  pattern[i][1][0],pattern[i][1][1],pattern[i][1][2],
								  pattern[i][2][0],pattern[i][2][1],pattern[i][2][2]);
	
		reportVerbose(msg);
	}
	#endif

	return pCount;
} //End removeNegativeSuperVector


// Remove pattern i in PatSrc if i (or inverse of i) is also found in PatDes (intersected)
// This function return the remaining number of patterns.
int substractPattern(int (**patternSrc)[3], int pCountSrc,int (**patternDes)[3], int pCountDes, int nBody) {	

	int patternVecSrc[3];
	int patternVecDes[3];
	int (*sVecSrc)[3];
	int (*sVecDes)[3];

	int *patternDuplicate = (int*)scMalloc(sizeof(int)*pCountSrc);
	for (int i = 0;i < pCountSrc;i++)
		patternDuplicate[i] = 0;

	sVecSrc    = (int(*)[3])scMalloc(sizeof(int[3])*(nBody));
	sVecDes    = (int(*)[3])scMalloc(sizeof(int[3])*(nBody));
	for (int i = 0;i < pCountSrc;i++){
		//if already found duplicated, skip to the next one
		//if (patternSrc[i][0][0] == REMOVED) continue;

		//compute super-vector source
		for (int k = 0;k < nBody;k++)
			for (int a = 0;a < 3;a++)
				sVecSrc[k][a] = patternSrc[i][k][a];

		for (int j = 0;j < pCountDes;j++){
			//if already found duplicated, skip to the next one
			if ((patternDes[j][0][0] == REMOVED) || (patternSrc[i][0][0] == REMOVED)) continue;

			//if i == j then skip to the next one
			//if (i == j) continue; 

			//compute super-vector source
			for (int k = 0;k < nBody;k++)
				for (int a = 0;a < 3;a++)
					sVecDes[k][a] = patternDes[j][nBody-1-k][a];

			int isVecDiff = 0;
			//Traverse all vector component of pattern i & j's super vector
			for (int chain = 0; (chain < nBody) && (isVecDiff == 0); chain++) {
			//for (int chain = 0; (chain < nBody1) ; chain++) {
				
				//calculater pattern vector of i's SV:
				for (int a = 0;a < 3;a++)
					patternVecSrc[a] = sVecSrc[chain][a]; 

				//calculater pattern vector of j's SV:
				for (int a = 0;a < 3;a++)
					patternVecDes[a] = sVecDes[chain][a];

				//if patternVec != patternVec^-1, then pattern i-j are not a duplicate-inverse.
				if ((patternVecSrc[0] != patternVecDes[0]) ||
				    (patternVecSrc[1] != patternVecDes[1]) || 
				    (patternVecSrc[2] != patternVecDes[2])) {
					//One of pattern vector is different, then i-j are different.
					//No need to check further chains.
					isVecDiff = 1;
				} 

				
			} //Next chain
			
			if (isVecDiff == 1) {
				isVecDiff = 0;
				//compute super-vector source
				for (int k = 0;k < nBody;k++)
					for (int a = 0;a < 3;a++) 
						sVecDes[k][a] = patternDes[j][k][a];

				for (int chain = 0; (chain < nBody) && (isVecDiff == 0); chain++) {
					
					//calculater pattern vector of i's SV:
					for (int a = 0;a < 3;a++)
						patternVecSrc[a] = sVecSrc[chain][a]; 

					//calculater pattern vector of j's SV:
					for (int a = 0;a < 3;a++)
						patternVecDes[a] = sVecDes[chain][a];

					//if patternVec != patternVec^-1, then pattern i-j are not a duplicate-inverse.
					if ((patternVecSrc[0] != patternVecDes[0]) ||
					    (patternVecSrc[1] != patternVecDes[1]) || 
					    (patternVecSrc[2] != patternVecDes[2])) {
						//One of pattern vector is different, then i-j are different.
						//No need to check further chains.
						isVecDiff = 1;
					} 

					
				} //Next chain
			} 

			//if supervector is the same, then i and j pattern are duplicated. Marked for remove later.
			if (isVecDiff == 0) {	
				#if (DEBUG_LEVEL > 1)		
					reportVerboseInt("Intersected computation path removed: ", j);
				#endif
				patternSrc[i][0][0] = REMOVED;
				//patternDes[j][0][0] = REMOVED;
				patternDuplicate[i]++;
				//patternDuplicate[j]++;
			}
		} //next j
	} //next i
	
	for (int i = 0;i < pCountSrc;i++)
		if (patternDuplicate[i] != 1)
			reportVerboseInt3("Pattern has different duplicate:",i,patternDuplicate[i],0);

	//Compact remaining pattern:
	int newPCount=0;
	for (int i = 0;i < pCountSrc;i++){
		if (patternSrc[i][0][0] > REMOVED){
			for (int j = 0;j < nBody;j++) {
				for (int a = 0;a < 3;a++){
					patternSrc[newPCount][j][a] = patternSrc[i][j][a];
				}
			}
			newPCount++;
		}
	}
	
	pCountSrc = newPCount;
	
	scFree(sVecSrc);
	scFree(sVecDes);
	scFree(patternDuplicate);

	reportPrintInt("Non-duplicate pattern vector: ", pCountSrc);

	#if (DEBUG_LEVEL > 1)		
	char msg[1000];
	for (int i = 0;i < pCountSrc;i++){
		sprintf(msg,"Pattern [%5d]: (%5d %5d %5d)->(%5d %5d %5d)->(%5d %5d %5d)",i,
								  patternSrc[i][0][0],patternSrc[i][0][1],patternSrc[i][0][2],
								  patternSrc[i][1][0],patternSrc[i][1][1],patternSrc[i][1][2],
								  patternSrc[i][2][0],patternSrc[i][2][1],patternSrc[i][2][2]);
	
		reportVerbose(msg);
	}
	#endif

	return pCountSrc;
} //End removeNegativeSuperVector


int mergePattern(int (**patternSrc)[3], int pCountSrc, int (**patternDes)[3], int pCountDes, int nBody)
{

	//shift pattern by shift vector
	for (int i = 0;i < pCountDes;i++){
		for (int j = 0;j < nBody;j++){
			for (int a = 0;a < 3;a++) {
				patternSrc[pCountSrc+i][j][a] = patternDes[i][j][a]; 
			}
		}
	}

	return pCountSrc+pCountDes;	

}

				

void positiveShift(int (**pattern)[3], int pCount, int nBody)
{
	//Shift pattern in region [-(Nbody-1) 0] to region [0 (Nbody-1)]
	int shiftVec[3];
	for (int i = 0;i < pCount;i++){

		//reset shift vector
		for (int a = 0;a < 3;a++)
			shiftVec[a] = 0;

		//calculate shift vector: find the lowest value of negative pattern offset. 
		for (int j = 0;j < nBody;j++){
			for (int a = 0;a < 3;a++) {
				shiftVec[a] = MIN(shiftVec[a],pattern[i][j][a]);
			}
		}

		//shift pattern by shift vector
		for (int j = 0;j < nBody;j++){
			for (int a = 0;a < 3;a++) {
				pattern[i][j][a] -= shiftVec[a]; 
			}
		}
	}

	reportPrintInt("Positive-shifted patterns: ",pCount);

	#if (DEBUG_LEVEL > 1)		
	char msg[1000];
	for (int i = 0;i < pCount;i++){
			sprintf(msg,"Pattern [%5d]: (%5d %5d %5d)->(%5d %5d %5d)->(%5d %5d %5d)",i,
								  pattern[i][0][0],pattern[i][0][1],pattern[i][0][2],
								  pattern[i][1][0],pattern[i][1][1],pattern[i][1][2],
								  pattern[i][2][0],pattern[i][2][1],pattern[i][2][2]);
		reportVerbose(msg);
	}
	#endif

}

int removeSelfPath(int (**pattern)[3], int pCount, int nBody)
{
	
	//remove path if computation path pointed to the same cell
	for (int i = 0;i < pCount;i++){
		
		int isDiff = 0;
		for (int j = 0;(j < nBody-1) && (isDiff == 0);j++){
			if ((pattern[i][j][0] != pattern[i][j+1][0]) ||
			    (pattern[i][j][1] != pattern[i][j+1][1]) ||
			    (pattern[i][j][2] != pattern[i][j+1][2])) {
				//One of pattern vector is different, then i-j are different.
				//No need to check further chains.
				isDiff = 1;
			} 


		}
	

		if (isDiff == 0) {	
			#if (DEBUG_LEVEL > 1)		
				reportVerboseInt("Self computation path removed: ", i);
			#endif
			pattern[i][0][0] = REMOVED;
		}
	}

	//Compact remaining pattern:
	int newPCount=0;
	for (int i = 0;i < pCount;i++){
		if (pattern[i][0][0] > REMOVED){
			for (int j = 0;j < nBody;j++) {
				for (int a = 0;a < 3;a++){
					pattern[newPCount][j][a] = pattern[i][j][a];
				}
			}
			newPCount++;
		}
	}
	
	pCount = newPCount;

	reportPrintInt("After self-path removed: ",pCount);

	#if (DEBUG_LEVEL > 1)		
	char msg[1000];
	for (int i = 0;i < pCount;i++){
			sprintf(msg,"Pattern [%5d]: (%5d %5d %5d)->(%5d %5d %5d)->(%5d %5d %5d)",i,
								  pattern[i][0][0],pattern[i][0][1],pattern[i][0][2],
								  pattern[i][1][0],pattern[i][1][1],pattern[i][1][2],
								  pattern[i][2][0],pattern[i][2][1],pattern[i][2][2]);
		reportVerbose(msg);
	}
	#endif

	return pCount;

}


void PatternShift(int (**pattern)[3], int pCount, int nBody, int *shiftVec)
{
	//Shift pattern in region [-(Nbody-1) 0] to region [0 (Nbody-1)]
	for (int i = 0;i < pCount;i++){

		//shift pattern by shift vector
		for (int j = 0;j < nBody;j++){
			for (int a = 0;a < 3;a++) {
				pattern[i][j][a] += shiftVec[a]; 
			}
		}
	}

	reportPrintIntArr3("Pattern-shifted by: ",shiftVec);

	#if (DEBUG_LEVEL > 1)		
	char msg[1000];
	for (int i = 0;i < pCount;i++){
			sprintf(msg,"Pattern [%5d]: (%5d %5d %5d)->(%5d %5d %5d)->(%5d %5d %5d)",i,
								  pattern[i][0][0],pattern[i][0][1],pattern[i][0][2],
								  pattern[i][1][0],pattern[i][1][1],pattern[i][1][2],
								  pattern[i][2][0],pattern[i][2][1],pattern[i][2][2]);
		reportVerbose(msg);
	}
	#endif

}

void inversePattern(int (**pattern)[3], int pCount, int nBody)
{
	//Shift pattern in region [-(Nbody-1) 0] to region [0 (Nbody-1)]
	for (int i = 0;i < pCount;i++){

		//shift pattern by shift vector
		for (int j = 0;j < nBody;j++){
			for (int a = 0;a < 3;a++) {
				pattern[i][j][a] *= -1; 
			}
		}
	}

	reportPrintInt("Pattern inversed: ",pCount);

	#if (DEBUG_LEVEL > 1)		
	char msg[1000];
	for (int i = 0;i < pCount;i++){
			sprintf(msg,"Pattern [%5d]: (%5d %5d %5d)->(%5d %5d %5d)->(%5d %5d %5d)",i,
								  pattern[i][0][0],pattern[i][0][1],pattern[i][0][2],
								  pattern[i][1][0],pattern[i][1][1],pattern[i][1][2],
								  pattern[i][2][0],pattern[i][2][1],pattern[i][2][2]);
		reportVerbose(msg);
	}
	#endif

}

//temporary sorting to help improve 3B computation
void sortComputePattern3B(int (**pattern)[3], int pCount, int nBody)
{
	int (*patTmp)[3] = (int(*)[3])scMalloc(nBody*sizeof(int[3]));
	for (int i = 0;i-1 < pCount;i++)
	for (int j = i+1;j < pCount;j++){

		int jLess = 0;
		int nx = 2*nBody;
		int nxy = 4*nBody*nBody;
		for (int k = 0;k < nBody;k++){
			int iOffset = pattern[i][k][0]+pattern[i][k][1]*nx+pattern[i][k][2]*nxy;
			int jOffset = pattern[j][k][0]+pattern[j][k][1]*nx+pattern[j][k][2]*nxy;
			if (iOffset > jOffset){
				jLess = 1;
				//reset counter to terminate loop
				k = nBody;
			}
			else if (iOffset < jOffset) {
				jLess = 0;
				//reset counter to terminate loop
				k = nBody;
			}
		}

		if (jLess == 1){
			for (int k = 0;k < nBody;k++)
			for (int a = 0;a < 3;a++)
				patTmp[k][a] = pattern[i][k][a];

			for (int k = 0;k < nBody;k++)
			for (int a = 0;a < 3;a++)
				pattern[i][k][a] = pattern[j][k][a];

			for (int k = 0;k < nBody;k++)
			for (int a = 0;a < 3;a++)
				pattern[j][k][a] = patTmp[k][a];
		}
	}


/*
	reportPrintInt("Positive-shifted patterns: ",pCount);

	#if (DEBUG_LEVEL > 1)		
	char msg[1000];
	for (int i = 0;i < pCount;i++){
			sprintf(msg,"Pattern [%5d]: (%5d %5d %5d)->(%5d %5d %5d)->(%5d %5d %5d)",i,
								  pattern[i][0][0],pattern[i][0][1],pattern[i][0][2],
								  pattern[i][1][0],pattern[i][1][1],pattern[i][1][2],
								  pattern[i][2][0],pattern[i][2][1],pattern[i][2][2]);
		reportVerbose(msg);
	}
	#endif
*/

scFree(patTmp);

}




//Keep for reference. Currently unused
#if 0
void initPattern_2B(COMPUTE_PATTERN* pat)
{
	int nBody = pat->nBody;
	reportPrintStr("initializing cell's computation pattern: ","2-Body");
	int pCount = 0;
	int (**pattern)[3] = pat->pattern;


	//Iterate all pattern	
	for (int i = 0;i < 3;i++)
	for (int j = 0;j < 3;j++)
	for (int k = 0;k < 3;k++){
		//Set all source cell offset to (0,0,0)
		for (int a = 0; a < 3;a++)
			pattern[pCount][0][a] = 0;
	
		pattern[pCount][1][0] = i-1;
		pattern[pCount][1][1] = j-1;
		pattern[pCount][1][2] = k-1;
		pCount++;
	}

	char msg[1000];
	reportVerboseInt("Iterated pattern: ",pCount);
	for (int i = 0;i < pCount;i++){
		sprintf(msg,"Pattern [%5d]: (%5d %5d %5d)->(%5d %5d %5d)",i,
								  pattern[i][0][0],pattern[i][0][1],pattern[i][0][2],
								  pattern[i][1][0],pattern[i][1][1],pattern[i][1][2]);
	
		reportVerbose(msg);
	}

	//Mark pattern with similar pattern vector. Keep only 1.
	int patternVecSrc[3];
	int patternVecDes[3];
	for (int i = 0;i < pCount-1;i++){
		//if already found duplicated, skip to the next one
		if (pattern[i][0][0] == REMOVED) continue;

		//calculater pattern vector
		for (int a = 0;a < 3;a++)
			patternVecSrc[a] = pattern[i][1][a] - pattern[i][0][a];
			
		for (int j = i+1;j < pCount;j++){
			//if already found duplicated, skip to the next one
			if (pattern[j][0][0] == REMOVED) continue;

			//if i == j then skip to the next one
			if (i == j) continue; 
			//calculater pattern vector
			for (int a = 0;a < 3;a++)
				patternVecDes[a] = pattern[j][1][a] - pattern[j][0][a];
	
			//if sum for each dimension of both vector = 0, then, it's duplicate.
			int sum[3];
			for (int a = 0;a < 3;a++)
				sum[a] = patternVecSrc[a] + patternVecDes[a];
	
			if ((sum[0] == 0) && (sum[1] == 0) && (sum[2] == 0)){
				reportVerboseInt("Pattern removed: ", j);
				pattern[j][0][0] = REMOVED;
			}
		} //next j
	} //next i

	int newPCount=0;
	//Compact remaining pattern:
	for (int i = 0;i < pCount;i++){
		if (pattern[i][0][0] > REMOVED){
			for (int j = 0;j < nBody;j++) {
				for (int a = 0;a < 3;a++){
					pattern[newPCount][j][a] = pattern[i][j][a];
				}
			}
			newPCount++;
		}
	}
	
	pCount = newPCount;
	reportVerboseInt("Non-duplicate pattern vector: ", pCount);
	for (int i = 0;i < pCount;i++){
		sprintf(msg,"Pattern [%5d]: (%5d %5d %5d)->(%5d %5d %5d)",i,
								  pattern[i][0][0],pattern[i][0][1],pattern[i][0][2],
								  pattern[i][1][0],pattern[i][1][1],pattern[i][1][2]);
	
		reportVerbose(msg);
	}


				

	//Shift pattern in region [-1 0] to region [0 1]
	int shiftVec[3];
	for (int i = 0;i < pCount;i++){

		//reset shift vector
		for (int a = 0;a < 3;a++)
			shiftVec[a] = 0;

		//calculate shift vector
		for (int j = 0;j < nBody;j++){
			for (int a = 0;a < 3;a++) {
				if (pattern[i][j][a] < 0)
					shiftVec[a] -= pattern[i][j][a]; //all shift vector should be >= 0
			}
		}

		//shift pattern by shift vector
		for (int j = 0;j < nBody;j++){
			for (int a = 0;a < 3;a++) {
				pattern[i][j][a] += shiftVec[a]; 
			}
		}
	}

	reportVerboseInt("Positive-shifted pattern: ",pCount);
	for (int i = 0;i < pCount;i++){
		sprintf(msg,"Pattern [%5d]: (%5d %5d %5d)->(%5d %5d %5d)",i,
								  pattern[i][0][0],pattern[i][0][1],pattern[i][0][2],
								  pattern[i][1][0],pattern[i][1][1],pattern[i][1][2]);
	
		reportVerbose(msg);
	}
}
#endif

