#include <stdio.h>
#include "simulate.h"
#include "writeOutput.h"
#include "state.h"
#include "input.h"
#include "mpi.h"
#include "params.h"
#include "utils.h"
#include "atomType.h"
#include "fileUtils.h"
#include "timing.h"
#include "memory.h"

/* Header MTS for aggregate write:
 * [nAtom] [corner*3] [boxSize*3]
 * Record format per atom:
 * [atomdata] [r*3] [rv*3] [rUpdateFlag]
*/ 


/*NOTE:
 * Output folder format: snapshot.0x11. 0x11 number denotes MD step. For example, snapshot.00000000000
 * File format: 0x7.[extension]. 0x7 number denotes MPI_ID. For example, 0000001.xyz
*/


void writeOutput(STATE* s, PARAMS* p)
{
	INPUT_PARAMS* inp = getInputObj();
	//reportPrint("Writing output...");
	if (inp->writeOutputXYZ == 1) {
		timerStart(TIMING_WRITE_OUTPUT_XYZ);	
		writeOutput_xyz(s,p,inp->writeOutputXYZMode);
		timerStop(TIMING_WRITE_OUTPUT_XYZ);	
	}

	if (inp->writeOutputMTS == 1) {
		timerStart(TIMING_WRITE_OUTPUT_MTS);	
		writeOutput_mts(s,p,inp->writeOutputMTSFormat);
		timerStop(TIMING_WRITE_OUTPUT_MTS);	
	}

	if (inp->writeOutputAtomicForce == 1) {
		timerStart(TIMING_WRITE_OUTPUT_ATOMIC_FORCE);	
		writeOutput_xyz(s,p,inp->writeOutputAtomicForceDummy);
		timerStop(TIMING_WRITE_OUTPUT_ATOMIC_FORCE);	
	}

}

void evalWriteOutput(int step, STATE* s, PARAMS* p)
{
	INPUT_PARAMS* inp = getInputObj();
	if ((inp->writeOutputXYZFreq > 0) && (step % inp->writeOutputXYZFreq == 0) && (inp->writeOutputXYZ == 1)) {
		timerStart(TIMING_WRITE_OUTPUT_XYZ);	
		writeOutput_xyz(s,p,inp->writeOutputXYZMode);
		timerStop(TIMING_WRITE_OUTPUT_XYZ);	
	}
	if ((inp->writeOutputMTSFreq > 0) && (step % inp->writeOutputMTSFreq == 0) && (inp->writeOutputMTS == 1)) {
		timerStart(TIMING_WRITE_OUTPUT_MTS);	
		writeOutput_mts(s,p,inp->writeOutputMTSFormat);
		timerStop(TIMING_WRITE_OUTPUT_MTS);	
	}
	if ((inp->writeOutputAtomicForceFreq > 0) && (step % inp->writeOutputAtomicForceFreq == 0) && (inp->writeOutputAtomicForce == 1)) {
		timerStart(TIMING_WRITE_OUTPUT_ATOMIC_FORCE);	
		writeOutput_atomicForce(s,p,inp->writeOutputAtomicForceDummy);
		timerStop(TIMING_WRITE_OUTPUT_ATOMIC_FORCE);	
	}
	if ((inp->runtimeProfileWriteFreq > 0) && (step % inp->runtimeProfileWriteFreq == 0)){
		timerWriteProfile();
		writeMemUsage();
		//resetTimer_collection();
	}
}





