#ifndef __WRITE_OUTPUT_H__
#define __WRITE_OUTPUT_H__

#include "constants.h"
#include "input.h"
#include "state.h"
#include "params.h"



void writeOutput(STATE* s, PARAMS* p);
void evalWriteOutput(int step, STATE* s, PARAMS* p);

void writeOutput_mts(STATE* s, PARAMS* p, WRITE_MODES mode);
void writeOutput_xyz(STATE* s, PARAMS* p,WRITE_MODES mode);
void writeOutput_atomicForce(STATE* s, PARAMS* p,WRITE_MODES mode);

#endif
