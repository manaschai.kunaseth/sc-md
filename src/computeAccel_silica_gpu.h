#ifndef __COMPUTE_ACCEL_SILICA_GPU_H__
#define __COMPUTE_ACCEL_SILICA_GPU_H__

#include "state.h"
#include "potential.h"
#include "params.h"
#include "cell.h"

typedef struct {
    double potEnergy;
    double (*ra)[3];
}gpu_return;

double computeAccel_silica2B_gpu(STATE *s, CELL_LIST* cl,POTENTIAL* pot, COMPUTE_PATTERN_CELL* patCell,int gpuID,int nGpu2B);
gpu_return computeAccel_silica3B_gpu(STATE *s, CELL_LIST* cl,POTENTIAL* pot, COMPUTE_PATTERN_CELL* patCell,int gpuID,int nGpu3B);
void initTables2B_gpu(POTENTIAL* pot,int gpuId);
void threadSafeSum_ra(double (*ra)[3], float* host_ra,int vCellAllXYZ,int* nAtomCell,int **cellData, int cellSize);
void threadSafeSum_ra_gpu(double (*ra)[3], double (*host_ra)[3],int vCellAllXYZ,int* nAtomCell,int **cellData, int cellSize);

#endif

