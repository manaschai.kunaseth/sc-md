#include "state.h"
#include "simulate.h"
#include "params.h"
#include "nodeComm.h"
#include "computeAccel.h"
#include "writeOutput.h"
#include "utils.h"
#include "analysis.h"
#include "evalProp.h"
#include "timing.h"
#include <stdlib.h>
#include "memory.h"
#include "mpi.h"
#if (DEBUG_LEVEL > 1)
#include <stdio.h>
#endif

//void updateCoord(double (*)[3], double (*)[3], int n, double dt);
static void halfKick(STATE*);
static void updateCoord(STATE*, double deltaT);
static void scaledAccelation(STATE* s, double dt);

static void singleStep(SIMULATE_DATA*, PARAMS*);
static void preLoop(SIMULATE_DATA*, PARAMS*);
static void postLoop(SIMULATE_DATA*, PARAMS*);

void MDLoop(SIMULATE_DATA* sim, PARAMS* params)
{
	reportVLine("Begin MD loop");
	preLoop(sim, params);
	printEvalHeader();
	evalProperties(sim->currentStep, &sim->mainState);
	int lastStep = sim->currentStep + params->stepLimit;
	while (sim->currentStep < lastStep)
	{
		timerStart(TIMING_MD_STEP);	
		singleStep(sim, params);
		sim->currentStep++;	
		evalProperties(sim->currentStep, &sim->mainState);
		checkPostStepAnalysis(sim->currentStep, &sim->mainState);
		evalWriteOutput(sim->currentStep, &sim->mainState, params);
		timerStop(TIMING_MD_STEP);	
	}
	postLoop(sim, params);
	checkPostStepAnalysis(sim->currentStep, &sim->mainState);
	writeOutput(&sim->mainState, params);
	reportVLine("End MD loop");
}

void preLoop(SIMULATE_DATA* sim, PARAMS* p)
{
	checkPreLoopAnalysis(sim->currentStep, &sim->mainState);
	atomCopy(&sim->mainState,&sim->atomCache,&p->commInfo,p->boxSize);
	computeAccel(&sim->mainState, &p->potential, p,sim->cellInfoSet);
	forceReturn(&sim->mainState,&sim->atomCache,&p->commInfo);
	scaledAccelation(&sim->mainState, p->deltaT);
}

void postLoop(SIMULATE_DATA* sim, PARAMS* p)
{
	
	checkPostSimulationAnalysis(&sim->mainState);

}



void singleStep(SIMULATE_DATA* sim, PARAMS* p)
{
	halfKick(&sim->mainState);
	checkPreUpdateCoordAnalysis(sim->currentStep, &sim->mainState);
	updateCoord(&sim->mainState,p->deltaT);
	atomMove(&sim->mainState,&p->commInfo,p->boxSize);
	checkPreStepAnalysis(sim->currentStep, &sim->mainState);
	atomCopy(&sim->mainState,&sim->atomCache,&p->commInfo,p->boxSize);
	computeAccel(&sim->mainState, &p->potential, p,sim->cellInfoSet);
	forceReturn(&sim->mainState,&sim->atomCache,&p->commInfo);
	scaledAccelation(&sim->mainState, p->deltaT);
	halfKick(&sim->mainState);
}

void halfKick(STATE* s)
{
	double (*rv)[3] = s->rv;
	double (*ra)[3] = s->ra;
	unsigned int n = s->nAtomOwn;
	double *rUpdateFlag = s->rUpdateFlag;

	#if (DEBUG_LEVEL > 0)
	long cntFixed = 0;
	for (int i = 0; i < n; i++)
		if (rUpdateFlag[i] == 0.0)
			cntFixed++;

	long cntFixedGlob = 0;
  	MPI_Allreduce(&cntFixed, &cntFixedGlob, 1, MPI_LONG, MPI_SUM, MPI_COMM_WORLD);
	reportPrintFixedDouble("# of fixed atoms:", (double)cntFixedGlob);
	#endif

	for (int i=0; i<n; i++)
	{
   		for (int a=0; a<3; a++) 
		{	
			//rv[i][a]=(rv[i][a]+ra[i][a])*rUpdateFlag[i];
			rv[i][a]=(rv[i][a]+ra[i][a]);
		}
	}

}

void updateCoord(STATE* s, double dt)
{
	double (*r)[3]  = s->r;
	double (*rv)[3] = s->rv;
	unsigned int n = s->nAtomOwn;
	double *rUpdateFlag = s->rUpdateFlag;

	for (int i = 0; i < n; i++)
	{               /* Update atomic coordinates to r(t+Dt) */
        	for (int a = 0; a < 3; a++)
		{        
			//r[i][a] = r[i][a] + dt * rv[i][a]*rUpdateFlag[i];
			r[i][a] = r[i][a] + dt * rv[i][a];
		}
	}
}

void scaledAccelation(STATE* s, double dt)
{
	//- Scaled acceration by deltaT/atomicMass. atomicMass is depend on each atom type. 

	double dtMassCoeff[ATOM_TYPE_MAX];
	double (*ra)[3]  = s->ra;
	int    	*rType = s->rType;
	double	dtHalf = 0.5*dt;
	double atomMass[3];
	int nType = paramsGetNType();

	unsigned int nAtomOwn = s->nAtomOwn;


	#if (DEBUG_LEVEL > 1)

	unsigned int nAtomCache = s->nAtomCache;
	double rsum[3];
	rsum[0] = 0.0;
	rsum[1] = 0.0;
	rsum[2] = 0.0;
	
	reportPrintFixedDouble("Half time-step:",dtHalf);

	for (int i = 0; i < nAtomOwn; i++)
		for (int a = 0;a < 3;a++)
			rsum[a] += ra[i][a];

	reportPrintDoubleArr3("      sum ra:",rsum);
	#endif

	paramsGetMass(atomMass);

	//dtMassCoeff = (double*)(malloc(sizeof(double)*nType));

	for (int i = 0;i < nType;i++)
		dtMassCoeff[i] = dtHalf/atomMass[i];
	
	for (int i = 0; i < nAtomOwn; i++) {
		
		double vami = dtMassCoeff[rType[i]];
		ra[i][0] *= vami;
		ra[i][1] *= vami;
		ra[i][2] *= vami;

	}

	#if (DEBUG_LEVEL > 1)
	rsum[0] = 0.0;
	rsum[1] = 0.0;
	rsum[2] = 0.0;

	for (int i = 0;i < nType;i++)
		printf("dtMassCoeff[%d]=%le\n",i,dtMassCoeff[i]);
	for (int i = 0; i < nAtomOwn; i++)
		for (int a = 0;a < 3;a++)
			rsum[a] += ra[i][a];

	reportPrintDoubleArr3("scaled-sum ra:",rsum);
	#endif
	//free(dtMassCoeff);

}
