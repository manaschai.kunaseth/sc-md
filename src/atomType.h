#ifndef __ATOM_TYPE_H__
#define __ATOM_TYPE_H__

#include "params.h"

int typeFromSymbol(const char* symbol);
void symbolFromType(int typeId, char* symbol);
void initAtomTypeTable(PARAMS*);

#endif

