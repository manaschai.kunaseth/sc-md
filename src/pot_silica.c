#include "potential.h"
#include "pot_silica.h"
#include "constants.h"
#include "params.h"
#include "atomType.h"
#include "utils.h"
#include "memory.h"
#include <math.h>
#include <stdlib.h>

// 2 types: silicon (Si) & oxygen (O)
#define NTYPE  2
// To avoid table lookup overflow, we allocate a little more than we need.
#define EXPANDED_BUFFER 10
//const int NC = 6;
const int NC = NTYPE;

// For Al2O3/SiC/SiO2
double RCANG = 5.6;				// = Max(RC_GAAS,RC_INA2,RC_SIO) + skinang (0.1)
double RPANG = 2.6;				// = r00_gaas (2.5) + skinang (0.1)
double AM_Ga = 26.98154, AM_As = 16.00;
double AM_In = 28.09, AM_A2 = 12.01;

  // SiO2 from Lu (species 4 & 5) 
double aa_sisi = 1.242e-19;		// Steric repulsion in J
double aa_sio = 1.242e-19;
double aa_oo = 1.242e-19;
double sg_si = 0.47;			// Atomic radius in angstrom
double sg_o = 1.2;
double et_sisi = 11.0;			// Steric repulsion power
double et_sio = 9.0;
double et_oo = 7.0;
double z_si = 1.20;				// Charge in atomic unit
double z_o = -0.60;
double al_si = 0.0;				// Polarizability in (angstrom)**3
double al_o = 2.4;
double ww_sisi = 0.0;			// vdW strength in J*(angstrom)**6
double ww_sio = 0.0;
double ww_oo = 0.0;
double r1s_sio = 4.43;			// Screening length in angstrom
double r4s_sio = 2.5;
double bb_si = 0.8e-18;			// 3-body strength in J
double bb_o = 3.2e-18;
double c3b_si = 0.0;			// 3-body saturation
double c3b_o = 0.0;
double dl_sio = 1.0;			// 3-body length scale in angstrom
double r0_sio = 2.6;			// 3-body cut-off in angstrom
double cosb_si = 109.47;		// Bond angle
double cosb_o = 141.0;
double rp_sio = 2.6;			// Primary & 3body interaction cut-off in angstrom
double rc_sio = 5.5;			// Potential (secondary) cut-off in angstrom
double r00_sio = 2.5;			// Neighbor list + MTS cut-off in angstrom 
double skinang = 0.1;			// Neighbor list + MTS cutoff in angstrom


void initPotential_silica(POTENTIAL *pot)
{
  double aa[NC][NC], sg[NC], et[NC][NC], hh[NC][NC], zz[NC], al[NC];
  double am[NC], ami[NC];
  double r1si[NC][NC], r4si[NC][NC];
  double ww[NC][NC];
  double vrc[NC][NC][2];
  double angfac;
  double rr, ri, r1siv, r4siv, det, dhh, facc, facp, facw, rep, col, pol, vdw, dr2;
  double ri2, ri3, ri4, ri5, ri6, ri7, ri8, ri9, ri10;
  double rcv, r1si2, r4si2, exbd, ex4s, rhc;
  double bohr3, bohr6;
  double rc1, rc2, rp1, rp2;

  double acon[NC],rcij[NC][NC],rcij2[NC][NC], rpij[NC][NC], rpij2[NC][NC];
  double bb[NC][NC][NC];
  double cosb[NC][NC][NC];
  double c3b[NC][NC][NC];
  double dl[NC];
  double r0[NC];
  double r02[NC];

  double *vtab0[NC][NC];
  double *vtab1[NC][NC];
  double *vtab2[NC][NC];

  double atomMass[NC];
 
  INPUT_PARAMS* inp = getInputObj();

  //set 2- and 3-body cutoff
  paramsSetRCut(2,rc_sio/BOHR);
  paramsSetRCut(3,r0_sio/BOHR);

  pot->tableNBin = inp->potentialTableNBin;
  int nBin = pot->tableNBin;

  int idSi = typeFromSymbol("SI");
  int idO = typeFromSymbol("O");


  paramsGetMass(atomMass);

  double deltaT = paramsGetDeltaT();
  if (paramsGetNType() != NC)
	terminateSingle("Specified atom type(s) are not not matched with potential",1);  

  reportPrint("Allocating temporary variables...");
  for (int i = 0;i < NC;i++)
	for (int j = 0;j < NC;j++){
		vtab0[i][j] = (double*)scMalloc(sizeof(double)*(pot->tableNBin+1));
		vtab1[i][j] = (double*)scMalloc(sizeof(double)*(pot->tableNBin+1));
		vtab2[i][j] = (double*)scMalloc(sizeof(double)*(pot->tableNBin+1));
	}

// Half timstep 
//  DeltaTH = 0.5 * DeltaT;

//	rcsize3b = 1;



// # of cells for linked cell lists 
/*
  for (a = 0; a < 3; a++) {

	lc[a] = la[a] / (RCUT / rcsize);
	rc[a] = la[a] / lc[a];
	rci[a] = 1.0 / rc[a];

  }

// # of cell for linked cell lists of 3body
for (a = 0; a < 3; a++) {

	lc3b[a] = (la[a]+RCUT) / RCUT3B;
	rc3b[a] = (la[a]+RCUT) / lc3b[a];
	rc3bi[a] = 1.0 / rc3b[a];

  }
*/

// Atomic masses AM
/*
  am[0] = AM_Ga * AMU;
  am[1] = AM_As * AMU;
  am[2] = AM_In * AMU;
  am[3] = AM_A2 * AMU;
  am[4] = AM_Si * AMU;
  am[5] = AM_O * AMU;
*/

  am[idSi] = atomMass[idSi];
  am[idO] = atomMass[idO];

// Inverse atomic masses AMI
  for (int a = 0; a < NC; a++)
	ami[a] = 1.0 / am[a];

  for (int i = 0; i < NC; i++)
	acon[i] = 0.5*deltaT*deltaT/am[i];
/*
  acon[0] = 0.0;
  acon[1] = 0.0;
  acon[2] = 0.0;
  acon[3] = 0.0;
*/

// Potential cut-off length

  rc1 = RCANG / BOHR;
  rc2 = rc1 * rc1;
  rp1 = RPANG / BOHR;
  rp2 = rp1 * rp1;
/*
  rc1 = (RCANG+deltaRCUT2B)/BOHR;
  rc2 = rc1*rc1;
  rp1 = (RPANG+deltaRCUT3B)/BOHR;
  rp2 = rp1*rp1;
*/


// Partial cut-off lengths RCIJ; note RC = Max{RCIJ(i,j)}


/*
  rcij[0][0] = rc_gaas / BOHR;
  rcij[0][1] = rc_gaas / BOHR;
  rcij[1][1] = rc_gaas / BOHR;
  rcij[2][2] = rc_ina2 / BOHR;
  rcij[2][3] = rc_ina2 / BOHR;
  rcij[3][3] = rc_ina2 / BOHR;
  rcij[4][4] = rc_sio / BOHR;
  rcij[4][5] = rc_sio / BOHR;
  rcij[5][5] = rc_sio / BOHR;
*/
  rcij[idSi][idSi] = rc_sio / BOHR;
  rcij[idSi][idO] = rc_sio / BOHR;
  rcij[idO][idSi] = rc_sio / BOHR;
  rcij[idO][idO] = rc_sio / BOHR;

/*
  for (int ia = 1; ia < NC; ia++)
	for (int ib = 0; ib < ia; ib++)
	  rcij[ia][ib] = rcij[ib][ia];
*/

  for (int ia = 0; ia < NC; ia++)
	for (int ib = 0; ib < NC; ib++)
	  rcij2[ia][ib] = rcij[ia][ib] * rcij[ia][ib];

// for Verlet-list cutoff ---------------------------------------------------
/*
  rcij_vl[0][0] = (rc_gaas + deltaRCUT2B) / BOHR;
  rcij_vl[0][1] = (rc_gaas + deltaRCUT2B) / BOHR;
  rcij_vl[1][1] = (rc_gaas + deltaRCUT2B) / BOHR;
  rcij_vl[2][2] = (rc_ina2 + deltaRCUT2B) / BOHR;
  rcij_vl[2][3] = (rc_ina2 + deltaRCUT2B) / BOHR;
  rcij_vl[3][3] = (rc_ina2 + deltaRCUT2B) / BOHR;
  rcij_vl[4][4] = (rc_sio + deltaRCUT2B) / BOHR;
  rcij_vl[4][5] = (rc_sio + deltaRCUT2B) / BOHR;
  rcij_vl[5][5] = (rc_sio + deltaRCUT2B) / BOHR;

  for (ia = 1; ia < NC; ia++)
	for (ib = 0; ib < ia; ib++)
	  rcij_vl[ia][ib] = rcij_vl[ib][ia];

  for (ia = 0; ia < NC; ia++)
	for (ib = 0; ib < NC; ib++)
	  rcij2_vl[ia][ib] = rcij_vl[ia][ib] * rcij_vl[ia][ib];
*/
//--------------------------------------------------------------------------------

/*
  rpij[0][0] = (r00_gaas + skinang) / BOHR;
  rpij[0][1] = rpij[0][0];
  rpij[1][1] = rpij[0][0];
  rpij[2][2] = (r00_ina2 + skinang) / BOHR;
  rpij[2][3] = rpij[2][2];
  rpij[3][3] = rpij[2][2];
  rpij[4][4] = (r00_sio + skinang) / BOHR;
  rpij[4][5] = rpij[4][4];
  rpij[5][5] = rpij[4][4];
*/

  rpij[idSi][idSi] = (r00_sio + skinang) / BOHR;
  rpij[idSi][idO ] = rpij[idSi][idSi];
  rpij[idO ][idSi] = rpij[idSi][idSi];
  rpij[idO ][idO ] = rpij[idSi][idSi];
  
/*
  for (ia = 1; ia < NC; ia++)
	for (ib = 0; ib < ia; ib++)
	  rpij[ia][ib] = rpij[ib][ia];
*/

  for (int ia = 0; ia < NC; ia++)
	for (int ib = 0; ib < NC; ib++)
	  rpij2[ia][ib] = rpij[ia][ib] * rpij[ia][ib];
/*
// for Verlet-list cutoff ---------------------------------------------------
  rpij_vl[0][0] = (r00_gaas + skinang + deltaRCUT3B) / BOHR;
  rpij_vl[0][1] = rpij_vl[0][0];
  rpij_vl[1][1] = rpij_vl[0][0];
  rpij_vl[2][2] = (r00_ina2 + skinang + deltaRCUT3B) / BOHR;
  rpij_vl[2][3] = rpij_vl[2][2];
  rpij_vl[3][3] = rpij_vl[2][2];
  rpij_vl[4][4] = (r00_sio + skinang + deltaRCUT3B) / BOHR;
  rpij_vl[4][5] = rpij_vl[4][4];
  rpij_vl[5][5] = rpij_vl[4][4];


  for (ia = 1; ia < NC; ia++)
	for (ib = 0; ib < ia; ib++)
	  rpij_vl[ia][ib] = rpij_vl[ib][ia];

  for (ia = 0; ia < NC; ia++)
	for (ib = 0; ib < NC; ib++)
	  rpij2_vl[ia][ib] = rpij_vl[ia][ib] * rpij_vl[ia][ib];
//------------------------------------------------------------------------------
*/

// Set 2-body parameters-----------------------------------------------
  for (int ia = 0; ia < NC; ia++)
	for (int ib = 0; ib < NC; ib++)
	  aa[ia][ib] = 0.0;

// Steric repulsion AA
/*
  aa[0][0] = aa_gaga / ENGAU;
  aa[0][1] = aa_gaas / ENGAU;
  aa[1][1] = aa_asas / ENGAU;
  aa[2][2] = aa_inin / ENGAU;
  aa[2][3] = aa_ina2 / ENGAU;
  aa[3][3] = aa_a2a2 / ENGAU;
  aa[4][4] = aa_sisi / ENGAU;
  aa[4][5] = aa_sio / ENGAU;
  aa[5][5] = aa_oo / ENGAU;
*/
  aa[idSi][idSi] = aa_sisi /ENGAU; 
  aa[idSi][idO] = aa_sio /ENGAU; 
  aa[idO][idSi] = aa_sio /ENGAU; 
  aa[idO][idO] = aa_oo /ENGAU; 

/*
  for (ia = 1; ia < NC; ia++)
	for (ib = 0; ib < ia; ib++)
	  aa[ia][ib] = aa[ib][ia];
*/

// Ionic radii SG
/*
  sg[0] = sg_ga / BOHR;
  sg[1] = sg_as / BOHR;
  sg[2] = sg_in / BOHR;
  sg[3] = sg_a2 / BOHR;
  sg[4] = sg_si / BOHR;
  sg[5] = sg_o / BOHR;
*/
  sg[idSi] = sg_si / BOHR;
  sg[idO ] = sg_o  / BOHR;

// Repulsive exponents ET

/*
  et[0][0] = et_gaga;
  et[0][1] = et_gaas;
  et[1][1] = et_asas;
  et[2][2] = et_inin;
  et[2][3] = et_ina2;
  et[3][3] = et_a2a2;
  et[4][4] = et_sisi;
  et[4][5] = et_sio;
  et[5][5] = et_oo;
*/

  et[idSi][idSi] = et_sisi;
  et[idSi][idO] = et_sio; 
  et[idO][idSi] = et_sio; 
  et[idO][idO] = et_oo;  

/*
  for (ia = 1; ia < NC; ia++)
	for (ib = 0; ib < ia; ib++)
	  et[ia][ib] = et[ib][ia];
*/

// Steric prefactors HH
	for (int ia = 0; ia < NC; ia++)
  	  for (int ib = 0; ib < NC; ib++)
	    hh[ia][ib] = aa[ia][ib] * pow((sg[ia] + sg[ib]), et[ia][ib]);

// Ionic charges ZZ
/*
  zz[0] = z_ga;
  zz[1] = z_as;
  zz[2] = z_in;
  zz[3] = z_a2;
  zz[4] = z_si;
  zz[5] = z_o;
*/

  zz[idSi] = z_si;
  zz[idO ] = z_o;

// Polarizabilities AL
/*
  bohr3 = BOHR * BOHR * BOHR;
  al[0] = al_ga / bohr3;
  al[1] = al_as / bohr3;
  al[2] = al_in / bohr3;
  al[3] = al_a2 / bohr3;
  al[4] = al_si / bohr3;
  al[5] = al_o / bohr3;
*/
  bohr3 = BOHR * BOHR * BOHR;
  al[idSi] = al_si / bohr3;
  al[idO ] = al_o / bohr3;

// Van der Waals coefficients WW
  bohr6 = bohr3 * bohr3;
  for (int ia = 0; ia < NC; ia++)
	for (int ib = 0; ib < NC; ib++)
	  ww[ia][ib] = 0.0;
/*
  ww[0][0] = ww_gaga / ENGAU / bohr6;
  ww[0][1] = ww_gaas / ENGAU / bohr6;
  ww[1][1] = ww_asas / ENGAU / bohr6;
  ww[2][2] = ww_inin / ENGAU / bohr6;
  ww[2][3] = ww_ina2 / ENGAU / bohr6;
  ww[3][3] = ww_a2a2 / ENGAU / bohr6;
  ww[4][4] = ww_sisi / ENGAU / bohr6;
  ww[4][5] = ww_sio / ENGAU / bohr6;
  ww[5][5] = ww_oo / ENGAU / bohr6;
  for (ia = 1; ia < NC; ia++)
	for (ib = 0; ib < ia; ib++)
	  ww[ia][ib] = ww[ib][ia];
*/

  ww[idSi][idSi] = ww_sisi/ENGAU/bohr6;
  ww[idSi][idO] = ww_sio/ENGAU/bohr6; 
  ww[idO][idSi] = ww_sio/ENGAU/bohr6; 
  ww[idO][idO] = ww_oo/ENGAU/bohr6;  

// Inverse Coulombic screening lengths R1SI
/*
  r1si[0][0] = BOHR / r1s_gaas;
  r1si[0][1] = BOHR / r1s_gaas;
  r1si[1][1] = BOHR / r1s_gaas;
  r1si[2][2] = BOHR / r1s_ina2;
  r1si[2][3] = BOHR / r1s_ina2;
  r1si[3][3] = BOHR / r1s_ina2;
  r1si[4][4] = BOHR / r1s_sio;
  r1si[4][5] = BOHR / r1s_sio;
  r1si[5][5] = BOHR / r1s_sio;
  for (ia = 1; ia < NC; ia++)
	for (ib = 0; ib < ia; ib++)
	  r1si[ia][ib] = r1si[ib][ia];
*/
  r1si[idSi][idSi] = BOHR / r1s_sio;
  r1si[idSi][idO ] = BOHR / r1s_sio;
  r1si[idO ][idSi] = BOHR / r1s_sio;
  r1si[idO ][idO ] = BOHR / r1s_sio;

// Inverse charge-dipole truncation lengths R4SI
/*
  r4si[0][0] = BOHR / r4s_gaas;
  r4si[0][1] = BOHR / r4s_gaas;
  r4si[1][1] = BOHR / r4s_gaas;
  r4si[2][2] = BOHR / r4s_ina2;
  r4si[2][3] = BOHR / r4s_ina2;
  r4si[3][3] = BOHR / r4s_ina2;
  r4si[4][4] = BOHR / r4s_sio;
  r4si[4][5] = BOHR / r4s_sio;
  r4si[5][5] = BOHR / r4s_sio;
  for (ia = 1; ia < NC; ia++)
	for (ib = 0; ib < ia; ib++)
	  r4si[ia][ib] = r4si[ib][ia];
*/


  r4si[idSi][idSi] = BOHR / r4s_sio;
  r4si[idSi][idO ] = BOHR / r4s_sio;
  r4si[idO ][idSi] = BOHR / r4s_sio;
  r4si[idO ][idO ] = BOHR / r4s_sio;

// Set 3-body parameters-----------------------------------------------

// 3-body strengths BB for Si3N4/SiC/SiO2 (remove cross terms)
  for (int ia = 0; ia < NC; ia++)
	for (int ib = 0; ib < NC; ib++)
	  for (int ic = 0; ic < NC; ic++) {
		bb[ia][ib][ic] = 0.0;
		cosb[ia][ib][ic] = 0.0;
		c3b[ia][ib][ic] = 0.0;
	  }
/*
  bb[1][0][1] = bb_ga / ENGAU;
  bb[0][1][0] = bb_as / ENGAU;
  bb[3][2][3] = bb_in / ENGAU;
  bb[2][3][2] = bb_a2 / ENGAU;
  bb[5][4][5] = bb_si / ENGAU;
  bb[4][5][4] = bb_o / ENGAU;
*/

  bb[idO ][idSi][idO ] = bb_si/ENGAU;
  bb[idSi][idO ][idSi] = bb_o/ENGAU;

// Characteristic cosines COSB
/*
  angfac = TWOPI / 360.0;
  cosb[1][0][1] = cos (angfac * cosb_ga);
  cosb[0][1][0] = cos (angfac * cosb_as);
  cosb[3][2][3] = cos (angfac * cosb_in);
  cosb[2][3][2] = cos (angfac * cosb_a2);
  cosb[5][4][5] = cos (angfac * cosb_si);
  cosb[4][5][4] = cos (angfac * cosb_o);
*/

  angfac = TWOPI / 360.0;
  cosb[idO ][idSi][idO ] = cos(angfac*cosb_si); 
  cosb[idSi][idO ][idSi] = cos(angfac*cosb_o);

// Fractional coefficients C3B
/*
  c3b[1][0][1] = c3b_ga;
  c3b[0][1][0] = c3b_as;
  c3b[3][2][3] = c3b_in;
  c3b[2][3][2] = c3b_a2;
  c3b[5][4][5] = c3b_si;
  c3b[4][5][4] = c3b_o;
*/

  c3b[idO ][idSi][idO ] = c3b_si;
  c3b[idSi][idO ][idSi] = c3b_o;


// Characteristic lengths DL
/*
  dl[0] = dl_gaas / BOHR;
  dl[1] = dl_gaas / BOHR;
  dl[2] = dl_ina2 / BOHR;
  dl[3] = dl_ina2 / BOHR;
  dl[4] = dl_sio / BOHR;
  dl[5] = dl_sio / BOHR;
*/
  dl[idSi] = dl_sio/BOHR;
  dl[idO ] = dl_sio/BOHR;


// Cut-off lengths R0
/*
  r0[0] = r0_gaas / BOHR;
  r0[1] = r0_gaas / BOHR;
  r0[2] = r0_ina2 / BOHR;
  r0[3] = r0_ina2 / BOHR;
  r0[4] = r0_sio / BOHR;
  r0[5] = r0_sio / BOHR;
  for (ic = 0; ic < NC; ic++)
	r02[ic] = r0[ic] * r0[ic];
*/

  r0[idSi] = r0_sio/BOHR;
  r0[idO ] = r0_sio/BOHR;

  for (int ic = 0; ic < NC; ic++)
	r02[ic] = r0[ic] * r0[ic];

// Evaluate the potential & derivative at the truncation length RCIJ---
  for (int ic = 0; ic < NC; ic++) {
	for (int jc = 0; jc < NC; jc++) {
	  rr = rcij[ic][jc];
	  ri = 1.0 / rr;
	  det = et[ic][jc];
	  dhh = hh[ic][jc];
	  facc = zz[ic] * zz[jc];
	  facp = 0.5 * (al[ic] * pow (zz[jc], 2.0) + al[jc] * pow (zz[ic], 2.0));
	  facw = ww[ic][jc];
	  r1siv = r1si[ic][jc];
	  r4siv = r4si[ic][jc];
	  rep = dhh * pow (ri, det);
	  col = facc * ri * exp (-rr * r1siv);
	  pol = -facp * pow (ri, 4.0) * exp (-rr * r4siv);
	  vdw = -facw * pow (ri, 6.0);
	  vrc[ic][jc][0] = rep + col + pol + vdw;
	  vrc[ic][jc][1] = -rep * det * ri - col * (ri + r1siv) - pol * (4.0 * ri + r4siv) - vdw * 6.0 * ri;
	}
  }

// 2-body potential & derivative table V-------------------------------

  dr2 = rc2 / nBin;
  //double dr2i = 1.0 / dr2;
  //printf("dr2i = %10.5f\n",dr2i);
  double rij2max = rc2 - dr2;			// To avoid segmentation fault of potential arrays

  for (int k = 1; k <= nBin; k++) {
	rr = sqrt (k * dr2);
	ri = 1.0 / rr;
	ri2 = ri * ri;
	ri3 = ri2 * ri;
	ri4 = ri3 * ri;
	ri5 = ri4 * ri;
	ri6 = ri5 * ri;
	ri7 = ri6 * ri;
	ri8 = ri7 * ri;
	ri9 = ri8 * ri;
	ri10 = ri9 * ri;

	for (int ic = 0; ic < NC; ic++) {
	  for (int jc = 0; jc < NC; jc++) {
		rcv = rcij[ic][jc];
		r1siv = r1si[ic][jc];
		r4siv = r4si[ic][jc];
		r1si2 = r1siv * r1siv;
		r4si2 = r4siv * r4siv;
		if (rr > rcv) {
		  //for(ia=0;ia<3;ia++) v[k][ic][jc][ia] = 0.0;
		  vtab0[ic][jc][k] = 0.0;
		  vtab1[ic][jc][k] = 0.0;
		  vtab2[ic][jc][k] = 0.0;
		}
		else {
		  det = et[ic][jc];
		  dhh = hh[ic][jc];
		  facc = zz[ic] * zz[jc];
		  facp = 0.5 * (al[ic] * zz[jc] * zz[jc] + al[jc] * zz[ic] * zz[ic]);
		  facw = ww[ic][jc];
		  exbd = exp (-rr * r1siv);
		  ex4s = exp (-rr * r4siv);
		  rhc = dhh * pow (ri, det);
		  //v[k][ic][jc][0] = rhc+facc*ri*exbd-facp*ri4*ex4s-facw*ri6;
		  vtab0[ic][jc][k] = rhc + facc * ri * exbd - facp * ri4 * ex4s - facw * ri6;
		  // Truncate the potential at RCIJ[IC][JC];
		  //v[k][ic][jc][0] = v[k][ic][jc][0]-vrc[ic][jc][0]-(rr-rcv)*vrc[ic][jc][1];
		  vtab0[ic][jc][k] = vtab0[ic][jc][k] - vrc[ic][jc][0] - (rr - rcv) * vrc[ic][jc][1];
		  rhc = rhc * det * ri2;
		  //v[k][ic][jc][1] = rhc+facc*(ri+r1siv)*ri2*exbd 
		  //               -facp*(4.0*ri+r4siv)*ri5*ex4s-6.0*facw*ri8;
		  vtab1[ic][jc][k] = rhc + facc * (ri + r1siv) * ri2 * exbd - facp * (4.0 * ri + r4siv) * ri5 * ex4s - 6.0 * facw * ri8;
		  // Truncate the 1st derivative at RCIJ[IC][JC]
		  //v[k][ic][jc][1] = v[k][ic][jc][1]+vrc[ic][jc][1]*ri;
		  vtab1[ic][jc][k] = vtab1[ic][jc][k] + vrc[ic][jc][1] * ri;
		  rhc = -rhc * (det + 2.0) * ri2;
		  //v[k][ic][jc][2] = rhc 
		  //               -facc*(3.0*ri2+3.0*ri*r1siv+r1si2)*ri3*exbd 
		  //               +facp*(24.0*ri2+9.0*ri*r4siv+r4si2)*ri6*ex4s 
		  //               +48.0*facw*ri10;
		  vtab2[ic][jc][k] = rhc - facc * (3.0 * ri2 + 3.0 * ri * r1siv + r1si2) * ri3 * exbd + facp * (24.0 * ri2 + 9.0 * ri * r4siv + r4si2) * ri6 * ex4s + 48.0 * facw * ri10;
		  // Truncate 3nd derivative at RCIJ[IC][JC]
		  //v[k][ic][jc][2] = v[k][ic][jc][2]-vrc[ic][jc][1]*pow(ri,3.0);
		  vtab2[ic][jc][k] = vtab2[ic][jc][k] - vrc[ic][jc][1] * pow (ri, 3.0);

		}

	  }
	}							// species (IC,JC)

  }								// potential grids R(K)


  // setup cache radius : max(RCUT2body,2*RCUT3body)
  //RCACHE = RCUT;
  /*
  for (int ic = 0; ic < NC - 1; ic++)
	for (int jc = ic + 1; jc < NC; jc++) {	//only upper triangle element is needed since rcij is symmetry
	  rcv = rpij[ic][jc];
	  if (RCACHE < rcv * 2.0)
		RCACHE = rcv * 2.0;
	}
  }
  */


	//initializing potential table
	reportPrint("Allocating 2-body potential/force table...");
 	pot->forceTable     = (double***)(scMalloc(sizeof(double**)*NC));
	pot->potentialTable = (double***)(scMalloc(sizeof(double**)*NC));
 	for(int ia = 0;ia < NC;ia++) {
		pot->forceTable[ia]     = (double**)(scMalloc(sizeof(double*)*NC));
		pot->potentialTable[ia] = (double**)(scMalloc(sizeof(double*)*NC));
  		for(int ib = 0;ib < NC;ib++){
			pot->forceTable[ia][ib]     = (double*)(scMalloc(sizeof(double)*(nBin+1+EXPANDED_BUFFER)));
			pot->potentialTable[ia][ib] = (double*)(scMalloc(sizeof(double)*(nBin+1+EXPANDED_BUFFER)));
		}
	}

	//reset all value
	for (int ia = 0;ia < NC;ia++){
		for (int ib = 0;ib < NC;ib++){
			double *ftab = pot->forceTable[ia][ib];
			double *ptab = pot->potentialTable[ia][ib];
			for (int k = 0;k < nBin+1+EXPANDED_BUFFER;k++){
				ptab[k] = 0.0;
				ftab[k] = 0.0;
			}
		}
	}
		
	// copy potential table data
	reportPrint("Saving potential/force table...");
	for (int ia = 0;ia < NC;ia++){
		for (int ib = 0;ib < NC;ib++){
			double *ftab = pot->forceTable[ia][ib];
			double *ptab = pot->potentialTable[ia][ib];
			for (int k = 1;k <= nBin;k++){
				ptab[k] = vtab0[ia][ib][k];
				ftab[k] = vtab1[ia][ib][k];
			}
		}
	}
	
/*	
	for (int i = 0;i < pot->tableNBin+1;i++)
		printf("%6d  %12.7lf: %12.7lf %12.7lf %12.7lf %12.7lf %12.7lf %12.7lf\n",i,rc1/nBin*i*BOHR,pot->forceTable[idSi][idSi][i],pot->forceTable[idSi][idO][i],pot->forceTable[idO][idO][i],pot->potentialTable[idSi][idSi][i],pot->potentialTable[idSi][idO][i],pot->potentialTable[idO][idO][i]);
	exit(1);
*/
	
	/*
	for (int i = 0;i < pot->tableNBin+1;i++)
		printf("%6d  %12.7lf: %12.7lf %12.7lf %12.7lf %12.7lf %12.7lf %12.7lf\n",i,rc1/nBin*i,vtab1[idSi][idSi][i],vtab1[idSi][idO][i],vtab1[idO][idO][i],vtab0[idSi][idSi][i],vtab0[idSi][idO][i],vtab0[idO][idO][i]);
	exit(1);
	*/

	// setup some potential parameters
	pot->dh2 = rc2/nBin;
	pot->rij2max = rij2max;
	// initializing 3-body - parameters
	reportPrint("Allocate 3-body parameters...");
	POTENTIAL_SILICA_EXTENSION * pot_sio2_ext;
	pot_sio2_ext = (POTENTIAL_SILICA_EXTENSION*)scMalloc(sizeof(POTENTIAL_SILICA_EXTENSION));

	pot_sio2_ext->r0   = (double*)scMalloc(sizeof(double)*NC);
	pot_sio2_ext->r02  = (double*)scMalloc(sizeof(double)*NC);
	pot_sio2_ext->dl   = (double*)scMalloc(sizeof(double)*NC);

	pot_sio2_ext->bb   = (double***)scMalloc(sizeof(double**)*NC);
	pot_sio2_ext->cosb = (double***)scMalloc(sizeof(double**)*NC);
	pot_sio2_ext->c3b  = (double***)scMalloc(sizeof(double**)*NC);
	for (int ia = 0;ia < NC;ia++){
		pot_sio2_ext->bb[ia]   = (double**)scMalloc(sizeof(double*)*NC);
		pot_sio2_ext->cosb[ia] = (double**)scMalloc(sizeof(double*)*NC);
		pot_sio2_ext->c3b[ia]  = (double**)scMalloc(sizeof(double*)*NC);
		for (int ib = 0;ib < NC;ib++){
			pot_sio2_ext->bb[ia][ib]   = (double*)scMalloc(sizeof(double)*NC);
			pot_sio2_ext->cosb[ia][ib] = (double*)scMalloc(sizeof(double)*NC);
			pot_sio2_ext->c3b[ia][ib]  = (double*)scMalloc(sizeof(double)*NC);
		}
	}

	
	//these parameters is not exactly for 3 body. But due to it specificity, we'll leave it here.
	pot_sio2_ext->rcut2_2b = (double**)scMalloc(sizeof(double*)*NC);
	for (int ia = 0; ia < NC; ia++)
		pot_sio2_ext->rcut2_2b[ia] = (double*)scMalloc(sizeof(double)*NC);

	for (int ia = 0; ia < NC; ia++)
		for (int ib = 0; ib < NC; ib++)
			pot_sio2_ext->rcut2_2b[ia][ib] = rcij2[ia][ib];

	#if (VERBOSE > 0)
	reportVerboseFixedDouble("2-body cutoff for SI-SI [Ang]:",sqrt(pot_sio2_ext->rcut2_2b[idSi][idSi]*BOHR*BOHR));
	reportVerboseFixedDouble("2-body cutoff for  SI-O [Ang]:",sqrt(pot_sio2_ext->rcut2_2b[idSi][idO]*BOHR*BOHR));
	reportVerboseFixedDouble("2-body cutoff for  O-SI [Ang]:",sqrt(pot_sio2_ext->rcut2_2b[idO][idSi]*BOHR*BOHR));
	reportVerboseFixedDouble("2-body cutoff for   O-O [Ang]:",sqrt(pot_sio2_ext->rcut2_2b[idO][idO]*BOHR*BOHR));
	#endif

	// copy potential parameter for 3-body potential
	reportPrint("Saving 3-body parameters...");
	for (int ia = 0;ia < NC;ia++){
		for (int ib = 0;ib < NC;ib++){
			for (int ic = 0;ic < NC;ic++){
				pot_sio2_ext->bb[ia][ib][ic] = bb[ia][ib][ic];
				pot_sio2_ext->cosb[ia][ib][ic] = cosb[ia][ib][ic];
				pot_sio2_ext->c3b[ia][ib][ic]  = c3b[ia][ib][ic];
			}
		}
	}

	for (int ia = 0; ia < NC; ia++){
		pot_sio2_ext->r0[ia] = r0[ia];
		pot_sio2_ext->r02[ia] = r02[ia];
		pot_sio2_ext->dl[ia] = dl[ia];
		
	}

	pot_sio2_ext->nType = NC;

	pot->extension = (void*) pot_sio2_ext;

	//determine an import radius from MAX(rcut2body,max(rcut3b)) 
	double rCache = pot->rcut[INDEX_2B];

	for (int ic = 0; ic < NC; ic++){
          	if (rCache < (r0[ic]+r0[ic]))
			rCache = r0[ic]+r0[ic];
	}
	paramsSetCacheRadius(rCache);
	//paramsSetCacheRadius(2*rCache);


	reportPrintInt("Atom type ID for Si:",idSi);
	reportPrintInt("Atom type ID for O :",idO);
	reportPrintFixedDouble("2-Body cutoff radius [Ang]:", paramsGetRCut(2)*BOHR);  	
	reportPrintFixedDouble("3-Body cutoff radius for Si center [Ang]:", r0[idSi]*BOHR);  	
	reportPrintFixedDouble("3-Body cutoff radius for O  center [Ang]:", r0[idO]*BOHR);  	
	reportPrintInt("Size of potential/force table:", nBin);  	
	reportPrintFixedDouble("Import radius [Ang]  :",rCache*BOHR);

	
  //free up temp vtab
	reportPrint("Free up temporary variables...");
  for (int i = 0;i < NC;i++)
	for (int j = 0;j < NC;j++){
		scFree(vtab0[i][j]);
		scFree(vtab1[i][j]);
		scFree(vtab2[i][j]);
	}

}
