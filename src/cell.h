#ifndef __CELL_LIST_H__
#define __CELL_LIST_H__

#include "state.h"
#include "params.h"

typedef struct compute_pattern_cell_st {
	//CELL_INFO*		cellInfo;
	//pattern = [pattern_i][0|1|2][cX|cY|cZ]
	int			**cellPatternOffset;
	int			nPattern;
	int			nBody;
} COMPUTE_PATTERN_CELL;

//CELL_INFO is a system-wide parameter exists for each nBody. Value will be initialized after related parameters is known.
//These variables will be copied to cell list when makeCellList is called.
typedef struct cell_info_st {
	//number of cells in x,y,z direction
	int		vCell[3];
	
	int		nCell;

	//size of cell in a.u. in x,y,z direction.
	double 		cellDimension[3];

	//How many cells are cached cell. i.e. cell that contain only atom imported from neighbor nodes.
	int		nCellCache[3];

	//compute pattern in terms of cell index.
	COMPUTE_PATTERN_CELL computePatternCell;

	int		nBody;
} CELL_INFO;


typedef struct cell_list_st {
	//number of cells in x,y,z direction
	int		vCell[3];

	int		nCell;

	//size of cell in a.u. in x,y,z direction.
	double 		cellDimension[3];

	//How many cells are cached cell. i.e. cell that contain only atom imported from neighbor nodes.
	int		nCellCache[3];

	//store cell list data (which atom in which cell)
	int**		cellAtomData;

	//Number of atoms in each cell
	int*		nAtomCell;
	
	//Upperbound of number of atoms in each cell
	int*	 	nAtomCellMax;

	//Cell's SC cache mask (for neighbor list operations)
	unsigned int*	cellSCCacheMask;
} CELL_LIST;



void initCellInfoSet(CELL_INFO *cellInfoSet);
CELL_LIST* makeCellList(CELL_LIST *cl,CELL_INFO *cellInfo, STATE* s,double* boxSize,double rcut, double rCache, int nlayer);
CELL_LIST* freeCellListData(CELL_LIST *cl);


#endif
