#include <stdio.h>
#include "simulate.h"
#include "state.h"
#include "input.h"
#include "mpi.h"
#include "params.h"
#include "utils.h"
#include "atomType.h"
#include "fileUtils.h"
#include "timing.h"
#include "memory.h"
#include <math.h>

#define ATOMIC_FORCE_HEADER_COUNT 1
#define ATOMIC_FORCE_DOUBLE_PER_ATOM 4
#define MPI_TAG_AGGREGATE_WRITE_ATOMIC_FORCE 1300

static void packAtomWriteAtomicForceBuffer(double* atomDataBuffer,int nAtomOwn,PARAMS *p,STATE *s);
static void writeAtomWriteAtomicForceBuffer(double* atomDataBuffer,int dataBufferPerNode, int nNode, char* fullFilePath, PARAMS* p,unsigned long nAtomGlobal);


void writeOutput_atomicForce(STATE* s, PARAMS* p, WRITE_MODES mode)
{
	char dirName[1000];
	char fileName[100];
	char fullFilePath[1100];

	createOutputDir(simulateGetCurrentStep(),dirName);
	sprintf(fileName,"%07d.frc",getMyId());
	sprintf(fullFilePath, "%s/%s",dirName,fileName);
	INPUT_PARAMS* inp = getInputObj();

	reportPrintInt("Writing output atomic force: Aggregate write for a group of MPI tasks = ",inp->writeAggregateOutputAtomicForce);

	FILE *fp;
	

	if (inp->writeAggregateOutputAtomicForce == 1) {

		fp = fopen(fullFilePath, "w");
		if (getMyId() == 0){
			fprintf(fp, "%ld\n",s->nAtomGlobal);
			fprintf(fp, "SCMD Atomic Force File (Global format) [a.u.]: Step %ld\n",simulateGetCurrentStep());
		}
		int nAtomOwn = s->nAtomOwn;
		double globCoord[3];
		double (*r)[3]     = s->r;
		double (*ra)[3]     = s->ra;
		int     *rType     = s->rType;
		unsigned long *rgid= s->rgid;
		double corner[3];
		char  atomTypeName[ATOM_TYPE_MAX][ATOM_TYPE_SYMBOL_MAX];
		//store atomType
		for (int i = 0;i < p->nAtomType;i++)
			symbolFromType(i,atomTypeName[i]);

		//calculating corner coordinate
		for (int a = 0;a < 3;a++)
			corner[a] = s->cornerCoord[a]*p->hMatrix[a][a];

		//write atom data
		for (int i = 0;i < nAtomOwn;i++) {
			//convert coordinate to global
			//for (int a = 0;a < 3;a++)
			//	globCoord[a] = (r[i][a] + corner[a]) * BOHR; 
			//fprintf(fp, "%3s %8.4f %8.4f %8.4f %ld\n", atomTypeName[rType[i]], globCoord[0],globCoord[1],globCoord[2],rgid[i]);
			
			double frc_sum2 = 0;
			for (int a = 0;a < 3;a++)
				frc_sum2 += ra[i][a]*ra[i][a];
			
			double frc = sqrt(frc_sum2);

			fprintf(fp, "%15.7le %15.7le %15.7le %15.7le %ld\n", 
					ra[i][0],ra[i][1],ra[i][2],frc,
					rgid[i]);

		}
		fclose(fp);
	} //end writeAggregateOutputAtomicForce == 1
	else if (inp->writeAggregateOutputAtomicForce > 1) { // for writeAggregateOutputAtomicForce > 1
		int nAtomOwn = s->nAtomOwn;
		int maxNatom = nAtomOwn; 
		MPI_Allreduce(&nAtomOwn,&maxNatom,1,MPI_INT,MPI_MAX,MPI_COMM_WORLD);

		int aggregateId = getMyId() % inp->writeAggregateOutputAtomicForce;
		int dataBufferPerNode = maxNatom*(ATOMIC_FORCE_DOUBLE_PER_ATOM)+ATOMIC_FORCE_HEADER_COUNT; //[nAtom]+nAtom*[atomdata,r*3]
		int masterId = getMyId()-(aggregateId);
		//printf("Atomic Force: MPI_task = %5d, aggregateId = %5d, masterId = %5d\n",getMyId(),aggregateId,masterId);
		//for master aggregate IO node
		if (aggregateId == 0) { 
			double* atomDataBuffer = (double*)scMalloc(sizeof(double)*inp->writeAggregateOutputAtomicForce*dataBufferPerNode);
			MPI_Request* req = (MPI_Request*)scMalloc(sizeof(MPI_Request)*inp->writeAggregateOutputAtomicForce);
			for (int i = 1;i < inp->writeAggregateOutputAtomicForce;i++) {
				MPI_Irecv(&atomDataBuffer[dataBufferPerNode*i], 
					   dataBufferPerNode, MPI_DOUBLE, getMyId()+i,MPI_TAG_AGGREGATE_WRITE_ATOMIC_FORCE, MPI_COMM_WORLD, &req[i]);
			}
			
			packAtomWriteAtomicForceBuffer(atomDataBuffer,nAtomOwn,p,s);
			MPI_Waitall(inp->writeAggregateOutputAtomicForce-1, &req[1], MPI_STATUSES_IGNORE); //Wait for req[1] to inp->writeAggregateOutputAtomicForce

			writeAtomWriteAtomicForceBuffer(atomDataBuffer,dataBufferPerNode, inp->writeAggregateOutputAtomicForce, fullFilePath,p,s->nAtomGlobal);

			scFree(atomDataBuffer);
			scFree(req);

		} else { //for slave aggregate IO node 
			double* atomDataBuffer = (double*)scMalloc(sizeof(double)*dataBufferPerNode);
			packAtomWriteAtomicForceBuffer(atomDataBuffer,nAtomOwn,p,s);
			MPI_Send(atomDataBuffer, nAtomOwn*(ATOMIC_FORCE_DOUBLE_PER_ATOM)+ATOMIC_FORCE_HEADER_COUNT, 
					MPI_DOUBLE, masterId, MPI_TAG_AGGREGATE_WRITE_ATOMIC_FORCE,MPI_COMM_WORLD);

			scFree(atomDataBuffer);
		}

	} //end writeAggregateOutputAtomicForce > 1
		
}

void packAtomWriteAtomicForceBuffer(double* atomDataBuffer,int nAtomOwn,PARAMS *p,STATE *s) {
	double corner[3];
	double (*r)[3] = s->r;
	double (*rv)[3] = s->rv;
	double (*ra)[3] = s->ra;
	int *rType = s->rType;
	unsigned long *rgid = s->rgid;

	int count = 0;

	//pack node header
	atomDataBuffer[count++] = nAtomOwn;
	//pack atom data
	for (int i = 0;i < nAtomOwn;i++) {
		atomDataBuffer[count++] = dataFromId(rType[i],rgid[i]); 

		//convert coordinate to global
		for (int a = 0;a < 3;a++) 
			atomDataBuffer[count  +a] = ra[i][a];

		count+=3;
	}
}


void writeAtomWriteAtomicForceBuffer(double* atomDataBuffer,int dataBufferPerNode, int nNode, char* fullFilePath, PARAMS* p,unsigned long nAtomGlobal) {

	FILE *fp;
	fp = fopen(fullFilePath, "w");

	if (getMyId() == 0) {
		fprintf(fp, "%ld\n",nAtomGlobal);
		fprintf(fp, "SCMD Atomic Force File (Global format) [a.u.]: Step %ld\n",simulateGetCurrentStep());
	}

	for (int iNode = 0;iNode < nNode;iNode++) {
		int offset = iNode*dataBufferPerNode;
		int nAtom = atomDataBuffer[offset];

		offset++;
		//write atom data
		for (int i = 0;i < nAtom;i++) {
			int rType = typeFromData(atomDataBuffer[offset]);
			unsigned long  rgid = idFromData(atomDataBuffer[offset]);
		
			double frc_sum2 = 0;
			for (int a = 0;a < 3;a++)
				frc_sum2 += atomDataBuffer[offset+1+a]*atomDataBuffer[offset+1+a];
			
			double frc = sqrt(frc_sum2);

			fprintf(fp, "%15.7le %15.7le %15.7le %15.7le %ld\n", 
					atomDataBuffer[offset+1],
					atomDataBuffer[offset+2],
					atomDataBuffer[offset+3],
					frc,
					rgid);
			/*
			fprintf(fp, "%8.4f %8.4f %8.4f %ld\n", 
					atomDataBuffer[offset+1],
					atomDataBuffer[offset+2],
					atomDataBuffer[offset+3],
					rgid);
			*/

			offset+=4;
		} //end write atom record
	} //end iNode
	fclose(fp);
}




