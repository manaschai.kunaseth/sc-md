#ifndef MEMORY_H
#define MEMORY_H

#include <stdlib.h>
#include <stdio.h>

void scMemInit(void);
void scMemReport(FILE*);
void writeMemUsage();

#ifdef FAST_MALLOC

#define scMalloc(size) malloc(size)
#define scFree(ptr) free(ptr)

#else

#define scMalloc(size) _scMalloc(size, _scLine(__FILE__, __LINE__))
#define scFree(ptr) _scFree(ptr, _scLine(__FILE__, __LINE__))

#endif
void* _scMalloc(size_t size, char* location);
void  _scFree(void* ptr, const char* location);

char* _scLine(const char* file, int lineNum);
#endif
