#include "adt.h"
#include "memory.h"
#include <stdlib.h>

//Binary tree code was adapted from http://www.cprogramming.com/tutorial/c/lesson18.html

STACK_INT* stackIntInit(STACK_INT* st,int size)
{
	st->stack = (int*)scMalloc(sizeof(int)*size);
	st->head = -1;
	st->capacity = size;
	st->isInit = 1;	
	return st;
}


STACK_INT* stackIntDestroy(STACK_INT* st)
{
	scFree(st->stack);
	return st;
}


void stackIntPush(STACK_INT* st,int data)
{
	st->head++;
	st->stack[st->head] = data;
}

int stackIntPop(STACK_INT* st)
{
	int data = st->stack[st->head];
	st->head--;
	return data;
}

int stackIntPeek(STACK_INT* st)
{
	return st->stack[st->head];
}

int stackIntSize(STACK_INT* st)
{
	return st->head+1;
}

int stackIntCapacity(STACK_INT* st)
{
	return st->capacity;
}

void btree_destroy_tree(B_NODE *leaf)
{
  if( leaf != 0 )
  {
      btree_destroy_tree(leaf->left);
      btree_destroy_tree(leaf->right);
      scFree( leaf );
  }
}

void btree_insert(int key, B_NODE **leaf)
{
    if( *leaf == 0 )
    {
        *leaf = (B_NODE*) scMalloc( sizeof(B_NODE) );
        (*leaf)->key_value = key;
        /* initialize the children to null */
        (*leaf)->left = 0;    
        (*leaf)->right = 0;  
    }
    else if(key < (*leaf)->key_value)
    {
        btree_insert( key, &(*leaf)->left );
    }
    else if(key > (*leaf)->key_value)
    {
        btree_insert( key, &(*leaf)->right );
    }
}


B_NODE *btree_search(int key, B_NODE *leaf)
{
  if( leaf != 0 )
  {
      if(key==leaf->key_value)
      {
          return leaf;
      }
      else if(key<leaf->key_value)
      {
          return btree_search(key, leaf->left);
      }
      else
      {
          return btree_search(key, leaf->right);
      }
  }
  else return 0;
}
