#include "state.h"
#include "params.h"
#include "utils.h"
#include "mpi.h"
#include <math.h>

/*--------------------------------------------------------------------*/
void analysis_velocityScaling(STATE* s, double reqTemp) {
/*----------------------------------------------------------------------*/
  //reqTemp in A.U.

  double e[3], vmSum[4], gvmSum[4];
  double seed;
  double atomMass[ATOM_TYPE_MAX];
  double ami[3];
  int ic;


  double (*rv)[3] = s->rv;
  int* rType = s->rType;
  int  nAtomOwn = s->nAtomOwn;

  /* Generate random velocities */
  seed = paramsGetSeed();
  //seed = 13597.0;
  paramsGetMass(atomMass);
  for (int a = 0; a < 3; a++)
	ami[a] = 1.0/atomMass[a];	
  for (int a = 0; a < 4; a++)
        vmSum[a] = 0.0;

  for (int i = 0; i < nAtomOwn; i++) {
        ic = rType[i];
        for (int a = 0; a < 3; a++) {
          e[0] = randomDouble(&seed);
          e[1] = randomDouble(&seed);
          rv[i][a] = sqrt(2.0 * reqTemp * ami[ic]) * sqrt(-log(e[0])) * cos(TWOPI * e[1]);
        }
        for (int a = 0; a < 3; a++)
          vmSum[a] += atomMass[ic] * rv[i][a];
        vmSum[3] += atomMass[ic];                     // 4th elem carries the total system mass
  }

  MPI_Allreduce (vmSum, gvmSum, 4, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

  /* Make the total momentum zero */
  for (int a = 0; a < 3; a++)
        gvmSum[a] /= gvmSum[3];
  for (int i = 0; i < nAtomOwn; i++)
        for (int a = 0; a < 3; a++)
          rv[i][a] -= gvmSum[a];
  paramsSetSeed(seed);
  reportVerboseFixedDouble("Velocity scaling completed. System temperature set to (K) = ",reqTemp * TEMPAU);
}

