#include "utils.h"
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "mpi.h"

#define D2P31M 2147483647.0
#define DMUL 16807.0

#if (GIT > 0)
extern char *gitversion;
extern char *gitCommitDate;
#else
char gitversion[100];
#endif

#ifndef VERBOSE
#define VERBOSE 0
#endif

#ifndef DEBUG_LEVEL 
#define DEBUG_LEVEL 0
#endif

static int myId;
static char* timeStamp();
static PARAMS *s_params;

#define TIME_STAMP_SIZE 21

static char timeStampStr[TIME_STAMP_SIZE];

void initUtils(PARAMS* p)
{
	MPI_Comm_rank(MPI_COMM_WORLD, &myId);
	s_params = p;
}

int getMyId()
{
	return myId;
}

char* timeStamp()
{

	//char *timestamp = (char *)malloc(sizeof(char) * 21);
	char *timestamp = timeStampStr;
	time_t ltime;
	ltime=time(NULL);
	struct tm *tm;
	tm=localtime(&ltime);

	sprintf(timestamp,"%04d/%02d/%02d %02d:%02d:%02d", tm->tm_year+1900, tm->tm_mon+1, 
	    tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);
	return timestamp;
}

void printBuildInfo()
{
	char dateTimeBuilt[1000];
	char gitCommitInfo[1000];
	char compileFlag[1000] = "";
	#ifdef NDEBUG
	sprintf(compileFlag,"%s ","NDEBUG");
	#endif
	sprintf(compileFlag,"%sVERBOSE=%d DEBUG_LEVEL=%d",compileFlag,VERBOSE,DEBUG_LEVEL);
	#if (GIT == 0)
	sprintf(gitversion,"%s", "Not on git repository.");
	sprintf(dateTimeCommit,"N/A");
	#else
	sprintf(gitCommitInfo,"%s %s",gitversion,gitCommitDate);
	#endif
	sprintf(dateTimeBuilt,"%s %s",__DATE__,__TIME__);
	reportVLine(" Shift/Collapse Molecular Dynamics Code (SC-MD)"); 
	reportPrintStr("Built             : ", dateTimeBuilt);
	reportPrintStr("GIT version & date: ", gitCommitInfo);
	reportPrintStr("Compile flag(s)   : ", compileFlag);
}

void reportVLine(const char *s)
{
	if (myId == 0){
		printf("=============================================================================\n");
		reportPrint(s);
		printf("=============================================================================\n");
	}
}

void reportPrint(const char *s)
{
	if (myId == 0)
		printf("[%21s]: %s\n",timeStamp(),s);
}

void reportPrintStr(const char *s,const char* val)
{
	if (myId == 0)
		printf("[%21s]: %s%s\n",timeStamp(),s,val);
}

void reportPrintInt(const char *s,int val)
{
	if (myId == 0)
		printf("[%21s]: %s%d\n",timeStamp(),s,val);
}

void reportPrintInt3(const char *s,int val1, int val2, int val3)
{
	if (myId == 0)
		printf("[%21s]: %s %8d %8d %8d\n",timeStamp(),s,val1,val2,val3);
}

void reportPrintIntArr3(const char *s,int* ar)
{
	if (myId == 0)
		printf("[%21s]: %s %8d %8d %8d\n",timeStamp(),s,ar[0],ar[1],ar[2]);
}

void reportPrintDouble(const char *s,double val)
{
	if (myId == 0)
		printf("[%21s]: %s%12.5le\n",timeStamp(),s,val);
}

void reportPrintDouble3(const char *s,double val1, double val2, double val3)
{
	if (myId == 0)
		printf("[%21s]: %s %12.5le %12.5le %12.5le\n",timeStamp(),s,val1,val2,val3);
}

void reportPrintDoubleArr3(const char *s,double* ar)
{
	if (myId == 0)
		printf("[%21s]: %s %12.5le %12.5le %12.5le\n",timeStamp(),s,ar[0],ar[1],ar[2]);
}

void reportPrintFixedDouble(const char *s,double val)
{
	if (myId == 0)
		printf("[%21s]: %s%12.5lf\n",timeStamp(),s,val);
}

void reportPrintFixedDouble3(const char *s,double val1, double val2, double val3)
{
	if (myId == 0)
		printf("[%21s]: %s %12.5lf %12.5lf %12.5lf\n",timeStamp(),s,val1,val2,val3);
}
void reportPrintFixedDoubleArr3(const char *s,double* ar)
{
	if (myId == 0)
		printf("[%21s]: %s %12.5lf %12.5lf %12.5lf\n",timeStamp(),s,ar[0],ar[1],ar[2]);
}

void reportVerbose(const char *s)
{
#if (VERBOSE > 0)
	if (myId == 0)
		printf("[%21s]: (V)%s\n",timeStamp(),s);
#endif
}

void reportVerboseStr(const char *s, const char *val)
{
#if (VERBOSE > 0)
	if (myId == 0)
		printf("[%21s]: (V)%s%s\n",timeStamp(),s,val);
#endif
}

void reportVerboseInt(const char *s,int val)
{
#if (VERBOSE > 0)
	if (myId == 0)
		printf("[%21s]: (V)%s%d\n",timeStamp(),s,val);
#endif
}

void reportVerboseInt3(const char *s,int val1, int val2, int val3)
{
#if (VERBOSE > 0)
	if (myId == 0)
		printf("[%21s]: (V)%s %8d %8d %8d\n",timeStamp(),s,val1,val2,val3);
#endif
}

void reportVerboseIntArr3(const char *s,int* ar)
{
#if (VERBOSE > 0)
	if (myId == 0)
		printf("[%21s]: (V)%s %8d %8d %8d\n",timeStamp(),s,ar[0],ar[1],ar[2]);
#endif
}

void reportVerboseDouble(const char *s,double val)
{
#if (VERBOSE > 0)
	if (myId == 0)
		printf("[%21s]: (V)%s%12.5le\n",timeStamp(),s,val);
#endif
}

void reportVerboseFixedDouble(const char *s,double val)
{
#if (VERBOSE > 0)
	if (myId == 0)
		printf("[%21s]: (V)%s%12.5lf\n",timeStamp(),s,val);
#endif
}

void reportVerboseDouble3(const char *s,double val1, double val2, double val3)
{
#if (VERBOSE > 0)
	if (myId == 0)
		printf("[%21s]: (V)%s %12.5le %12.5le %12.5le\n",timeStamp(),s,val1,val2,val3);
#endif
}

void reportVerboseDoubleArr3(const char *s,double* ar)
{
#if (VERBOSE > 0)
	if (myId == 0)
		printf("[%21s]: (V)%s %12.5le %12.5le %12.5le\n",timeStamp(),s,ar[0],ar[1],ar[2]);
#endif
}


void terminate(const char *s)
{
	printf("[%21s]: ERROR: %s\n",timeStamp(),s);
	exit(1);
}

void terminateSingle(const char *s,int errorCode)
{
	if (myId == 0)
		printf("[%21s]: ERROR: %s\n",timeStamp(),s);
	MPI_Abort(MPI_COMM_WORLD,errorCode);
}

void terminateNode(const char *s,int errorCode)
{
	printf("[%21s]: ERROR (MPI_ID=%d):%s\n",timeStamp(),myId,s);
	MPI_Abort(MPI_COMM_WORLD,errorCode);
}


/*--------------------------------------------------------------------*/
double dataFromId(int isi, unsigned long idi) {
/*--------------------------------------------------------------------*/
  return isi + idi * 1e-11 + 1e-15;
}

/*--------------------------------------------------------------------*/
unsigned long idFromData(double adata) {
/*--------------------------------------------------------------------*/
  long ic;
  ic = (unsigned long) adata;
  return (unsigned long) ((adata - ic) * 1e11);
}

/*--------------------------------------------------------------------*/
int typeFromData(double adata) {
/*--------------------------------------------------------------------*/
  return (int) adata;
}

void strToUpper(char* s)
{
	int len = strlen(s);
	for (int i = 0;i < len;i++)
		s[i] = toupper(s[i]);

}
void strToLower(char* s)
{
	int len = strlen(s);
	for (int i = 0;i < len;i++)
		s[i] = tolower(s[i]);

}


void inPlaceTrim(char * s) {
        int l = strlen(s);

        while(isspace(s[l - 1]))
        {
                s[l-1] = '\0';
                --l;
        }

        int spaceLeft = 0;
        while(isspace(s[spaceLeft])) {
                spaceLeft++;
                //--l;
        }

        if (spaceLeft > 0)
		for (int i = 0;i < l; i++) {
			s[i] = s[i+spaceLeft];
			s[i+spaceLeft] = '\0';
		}

}

/*--------------------------------------------------------------------*/
double doubleMod (double a, double b) {
/*--------------------------------------------------------------------*/
  int n;
  n = (int) (a / b);
  return (a - b * n);
}

/*--------------------------------------------------------------------*/
double randomDouble(double *seed) {
/*--------------------------------------------------------------------*/
  *seed = doubleMod(*seed * DMUL, D2P31M);
  return (*seed / D2P31M);
}


/*--------------------------------------------------------------------*/
void randomVec3 (double *p, double *seed) {
/*--------------------------------------------------------------------*/
  double x, y, s = 2.0;
  while (s > 1.0) {
        x = 2.0 * randomDouble(seed) - 1.0;
        y = 2.0 * randomDouble(seed) - 1.0;
        s = x * x + y * y;
  }
  p[2] = 1.0 - 2.0 * s;
  s = 2.0 * sqrt(1.0 - s);
  p[0] = s * x;
  p[1] = s * y;
}

