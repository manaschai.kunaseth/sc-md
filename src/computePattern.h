#ifndef __COMPUTE_PATTERN_H__
#define __COMPUTE_PATTERN_H__

#include "cell.h"

typedef enum {
	COMPUTE_PATTERN_FLAG_2B = 2,
	COMPUTE_PATTERN_FLAG_3B = 4,
	COMPUTE_PATTERN_FLAG_4B = 8,
	COMPUTE_PATTERN_FLAG_ANTI2B = 8192+2,
	COMPUTE_PATTERN_FLAG_2B_2LAYER = 16384+2,
	COMPUTE_PATTERN_FLAG_UNDEFINED = 0

} COMPUTE_PATTERN_FLAGS;

#if 0
typedef struct compute_pattern_cell_pair_st {
	//CELL_INFO*		cellInfo;
	int			(*pattern)[2];
	int			nPattern;
	int			nPatternMax;
} COMPUTE_PATTERN_CELL_2B;

typedef struct compute_pattern_cell_triplet_st {
	//CELL_INFO*		cellInfo;
	int			(*pattern)[3];
	int			nPattern;
	int			nPatternMax;
} COMPUTE_PATTERN_CELL_3B;

//NOTE: Cell pattern is an offset from the center cell.

typedef struct compute_pattern_st {
	COMPUTE_PATTERN_FLAGS		computePatternType;
	void*				computePattern;
} COMPUTE_PATTERN;
#endif


	
typedef struct compute_pattern_st {
	//CELL_INFO*		cellInfo;
	//pattern = [pattern_i][0|1|2][cX|cY|cZ]
	int			(**pattern)[3];
	//int***			pattern;
	int			nPattern;
	int			nPatternMax;
	int			nBody;
} COMPUTE_PATTERN;


/*
typedef struct compute_pattern_set_st {
	COMPUTE_PATTERN pattern2B;
	COMPUTE_PATTERN pattern3B;
} COMPUTE_PATTERN_SET;
*/


COMPUTE_PATTERN* initPattern(COMPUTE_PATTERN* pat, COMPUTE_PATTERN_FLAGS cpType);
COMPUTE_PATTERN* freePattern(COMPUTE_PATTERN* pat);




#endif
