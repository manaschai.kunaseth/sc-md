#Simulation input parameter for cacsMD code 

#============================================
# SIMULATION PARAMETER
#============================================
# MD_MODE = [mode(I)] 
# What kind of atom configuration to use.
# 	mode=0: create new atom configuration 
# 	mode=1: continue from previous configuration

MD_MODE = 0


# STEP_LIMIT = [N(I)]
# Perform MD simulation for N steps. 

STEP_LIMIT = 0

# TIME_STEP = [dt(D)]
# Time discretization dt in the unit of 10e-15 second.

DELTA_T = 1.0 


# POTENTIAL = [potentialName(S)]
# Potential function use in MD simulation.

POTENTIAL = SIO2 

# ATOM_TYPE = [ELEMENT_NAME(S) MASS(D)]
# ELEMENT_NAME: Type of atoms that used in a simulation.
# ATOMIC_MASS : Atomic mass of "ELEMENT_NAME" in unified atomic mass unit.  

ATOM_TYPE = SI 28.09
ATOM_TYPE = O  16.00

# POTENTIAL_TABLE_SIZE = [NBIN(I)]
# Size of potential/force lookup tables.
# Larger number has more accuracy during lookup interpolation but may degrade performance.
# Smaller number has less accuracy but may improve performance via better data locality.
# RECOMMENDED VALUE: 8192

POTENTIAL_TABLE_SIZE = 8192 


# MTS_DIR = [PATH(S)]
# Path to atom configuration files (.mts).

MTS_DIR = .


# RANDOM_SEED = [BASE_SEED(I)]
# Base value of seed for random number generator.
# Actual seed is different for each node, which is calculated from (BASE_SEED + MPI_ID)

RANDOM_SEED = 13597


# EVAL_PROP_FREQ = [FREQ(I)]
# Frequency that critical properties of system (e.g. kinetic/potential energy) will be printout on screen.
# Every FREQ MD step, properties will be printout on screen.

EVAL_PROP_FREQ = 100



#============================================
# PARALLELIZATION PARAMETERS
#============================================
# N_NODES = [NX(I) NY(I) NZ(I)]
# Number of MPI tasks uses in simulation.
# The number of the MPI tasks requested must be the same with NX*NY*NZ.
#	NX=[INT]: Number of MPI tasks in X direction
#	NY=[INT]: Number of MPI tasks in Y direction
#	NZ=[INT]: Number of MPI tasks in Z direction

N_NODES = 1 1 1


#============================================
# OUTPUT CONFIGURATION PARAMETERS
#============================================
# WRITE_XYZ = [FLAGS(I:0,1) FREQ(I) GMODE(I:0,1)]
# Parameter indicating whether XYZ output file(s) should be created or not.
# 	FLAG=0: No XYZ file(s) created. [default] 
# 	FLAG=1: XYZ file(s) are created at every FREQ steps. 
#	FREQ  : Frequency that XYZ file(s) are created. (Default=0)
#		If FREQ <= 0, XYZ are created at the end of the simulation only.
# GMODE indicates mode of XYZ output file: GLOBAL=0 and LOCAL=1
# 	GMODE=0: Local mode. Local XYZ coordinate will be used on each node.  
# 	GMODE=1: Global mode. Global XYZ coordinate across the simulation system will be used. [default] 

#WRITE_XYZ = 1 0 1  (default)
WRITE_XYZ = 1 1000 1

#============================================
# ATOM CONFIGURATION PARAMETERS
#============================================

#INCLUDE = atomConfig.in


#============================================
# ATOM CONFIGURATION PARAMETERS
#============================================
# INIT_UNIT_CELL = [UCELL_X(I) UCELL_Y(I) UCELL_Z(I)]
# Number of initial configuration unit cells in case of MD_MODE = 0
# This parameter has no effect for MD_MODE = 1

INIT_UNIT_CELL = 8 8 8

# LATTICE_CONST = [LATT_CONST_X(D) LATT_CONST_Y(D) LATT_CONST_Z(D)]
# Lattice constant in X, Y, Z directions of init unit cells in angstorm for MD_MODE = 0
# This parameter has no effect for MD_MODE = 1

#  Data for real beta-crystobalite SiO2
#    NPU=24, NUA=8, and NUB=16
#    Cubic unit cell A0 = B0 = C0 = 7.16 A = 13.5304 a.u.
#    (R.W.G. Wyckoff, Crystal Structures, Vol. 1, P.318)

LATTICE_CONST = 7.15997985 7.15997985 7.15997985

# ATOM_IN_UNIT_CELL = ATOM_IN_UNIT_CELL(I) 
# Number of atom in unit cell
# This parameter has no effect for MD_MODE = 1

#Current unused
#ATOM_IN_UNIT_CELL = 24


# UNIT_CELL_ATOM = ATOM_TYPE(S) COORD_X(D) COORD_Y(D) COORD(Z)
# Type and coordinate of atom in unit cell. Coordinate is in scaled unit.
# This parameter has no effect for MD_MODE = 1

UNIT_CELL_ATOM = SI 0.255 0.255 0.255
UNIT_CELL_ATOM = SI 0.755 0.245 0.745
UNIT_CELL_ATOM = SI 0.245 0.745 0.755
UNIT_CELL_ATOM = SI 0.745 0.755 0.245

UNIT_CELL_ATOM = SI 0.992 0.992 0.992
UNIT_CELL_ATOM = SI 0.492 0.508 0.008
UNIT_CELL_ATOM = SI 0.508 0.008 0.492
UNIT_CELL_ATOM = SI 0.008 0.492 0.508

UNIT_CELL_ATOM =  O 0.125 0.125 0.125
UNIT_CELL_ATOM =  O 0.625 0.375 0.875
UNIT_CELL_ATOM =  O 0.375 0.875 0.625
UNIT_CELL_ATOM =  O 0.875 0.625 0.375

UNIT_CELL_ATOM =  O 0.660 0.660 0.060
UNIT_CELL_ATOM =  O 0.160 0.840 0.940
UNIT_CELL_ATOM =  O 0.160 0.440 0.340
UNIT_CELL_ATOM =  O 0.560 0.840 0.340

UNIT_CELL_ATOM =  O 0.660 0.060 0.660
UNIT_CELL_ATOM =  O 0.340 0.160 0.440
UNIT_CELL_ATOM =  O 0.340 0.560 0.840
UNIT_CELL_ATOM =  O 0.940 0.160 0.860

UNIT_CELL_ATOM =  O 0.060 0.660 0.660
UNIT_CELL_ATOM =  O 0.840 0.340 0.560
UNIT_CELL_ATOM =  O 0.840 0.940 0.160
UNIT_CELL_ATOM =  O 0.440 0.340 0.160


# INIT_TEMP = [TEMP(D)] 
# Set temperature of the initialized atoms to TEMP Kelvin by velocity scaling.
# This parameter has no effect for MD_MODE = 1

INIT_TEMP = 300


